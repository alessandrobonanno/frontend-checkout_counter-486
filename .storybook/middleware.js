const proxy = require('http-proxy-middleware');

const brandOrigins = {
  ED: 'https://edreams.com',
  OP: 'https://opodo.com',
  GO: 'https://govoyages.com',
  GV: 'https://govoyages.com',
  get default() {
    return this.ED;
  },
};

function parseCookies(cookies = '') {
  return cookies.split(';').reduce(function (acc, cookie) {
    const parts = cookie.split('=');

    acc[parts.shift().trim()] = decodeURI(parts.join('='));

    return acc;
  }, {});
}

module.exports = function expressMiddleware(router) {
  router.use('/images', (req, res, next) => {
    const { brand } = parseCookies(req.headers.cookie);

    proxy({
      target: brandOrigins[brand] ? brandOrigins[brand] : brandOrigins.default,

      changeOrigin: true,
    })(req, res, next);
  });
};
