import {
  BaggageCondition,
  CabinClass,
  FieldObject,
  FieldTypeValue,
  FieldValidatorType,
  GetUserQuery,
  Itinerary,
  LocationType,
  Maybe,
  RequiredFieldType,
  ResidentGroup,
  ResidentGroupContainer,
  Section,
  ShoppingCart,
  TransportType,
  TravellerAgeType,
  TravellerGender,
  TravellerInformationDescription,
  TravellerTitleType,
  TripType,
  User,
  UserTraveller,
} from '../../src/generated/graphql';

const countryList = [
  {
    option: 'United States',
    value: 'US',
  },
  {
    option: 'Afghanistan',
    value: 'AF',
  },
  {
    option: 'Albania',
    value: 'AL',
  },
  {
    option: 'Algeria',
    value: 'DZ',
  },
  {
    option: 'American Samoa',
    value: 'AS',
  },
  {
    option: 'Andorra',
    value: 'AD',
  },
  {
    option: 'Angola',
    value: 'AO',
  },
  {
    option: 'Anguilla',
    value: 'AI',
  },
  {
    option: 'Antarctica',
    value: 'AQ',
  },
  {
    option: 'Antigua and Barbuda',
    value: 'AG',
  },
  {
    option: 'Argentina',
    value: 'AR',
  },
  {
    option: 'Armenia',
    value: 'AM',
  },
  {
    option: 'Aruba',
    value: 'AW',
  },
  {
    option: 'Australia',
    value: 'AU',
  },
  {
    option: 'Austria',
    value: 'AT',
  },
  {
    option: 'Azerbaijan',
    value: 'AZ',
  },
  {
    option: 'Bahamas',
    value: 'BS',
  },
  {
    option: 'Bahrain',
    value: 'BH',
  },
  {
    option: 'Bangladesh',
    value: 'BD',
  },
  {
    option: 'Barbados',
    value: 'BB',
  },
  {
    option: 'Belarus',
    value: 'BY',
  },
  {
    option: 'Belgium',
    value: 'BE',
  },
  {
    option: 'Belize',
    value: 'BZ',
  },
  {
    option: 'Benin',
    value: 'BJ',
  },
  {
    option: 'Bermuda',
    value: 'BM',
  },
  {
    option: 'Bhutan',
    value: 'BT',
  },
  {
    option: 'Bolivia',
    value: 'BO',
  },
  {
    option: 'Bosnia and Herzegovina',
    value: 'BA',
  },
  {
    option: 'Botswana',
    value: 'BW',
  },
  {
    option: 'Bouvet Island',
    value: 'BV',
  },
  {
    option: 'Brazil',
    value: 'BR',
  },
  {
    option: 'British Indian Ocean Territory',
    value: 'IO',
  },
  {
    option: 'Brunei',
    value: 'BN',
  },
  {
    option: 'Bulgaria',
    value: 'BG',
  },
  {
    option: 'Burkina Faso',
    value: 'BF',
  },
  {
    option: 'Burundi',
    value: 'BI',
  },
  {
    option: 'Cambodia',
    value: 'KH',
  },
  {
    option: 'Cameroon',
    value: 'CM',
  },
  {
    option: 'Canada',
    value: 'CA',
  },
  {
    option: 'Cape Verde',
    value: 'CV',
  },
  {
    option: 'Caribbean Netherlands',
    value: 'BQ',
  },
  {
    option: 'Cayman Islands',
    value: 'KY',
  },
  {
    option: 'Central African Republic',
    value: 'CF',
  },
  {
    option: 'Chad',
    value: 'TD',
  },
  {
    option: 'Chile',
    value: 'CL',
  },
  {
    option: 'China',
    value: 'CN',
  },
  {
    option: 'Christmas Island',
    value: 'CX',
  },
  {
    option: 'Cocos Islands',
    value: 'CC',
  },
  {
    option: 'Colombia',
    value: 'CO',
  },
  {
    option: 'Comoros',
    value: 'KM',
  },
  {
    option: 'Congo',
    value: 'CG',
  },
  {
    option: 'Cook Islands',
    value: 'CK',
  },
  {
    option: 'Costa Rica',
    value: 'CR',
  },
  {
    option: 'Croatia',
    value: 'HR',
  },
  {
    option: 'Cuba',
    value: 'CU',
  },
  {
    option: 'Curaçao',
    value: 'CW',
  },
  {
    option: 'Cyprus',
    value: 'CY',
  },
  {
    option: 'Czech Republic',
    value: 'CZ',
  },
  {
    option: 'Côte d’Ivoire',
    value: 'CI',
  },
  {
    option: 'Denmark',
    value: 'DK',
  },
  {
    option: 'Djibouti',
    value: 'DJ',
  },
  {
    option: 'Dominica',
    value: 'DM',
  },
  {
    option: 'Dominican Republic',
    value: 'DO',
  },
  {
    option: 'DR Congo',
    value: 'CD',
  },
  {
    option: 'Ecuador',
    value: 'EC',
  },
  {
    option: 'Egypt',
    value: 'EG',
  },
  {
    option: 'El Salvador',
    value: 'SV',
  },
  {
    option: 'Equatorial Guinea',
    value: 'GQ',
  },
  {
    option: 'Eritrea',
    value: 'ER',
  },
  {
    option: 'Estonia',
    value: 'EE',
  },
  {
    option: 'Ethiopia',
    value: 'ET',
  },
  {
    option: 'Falkland Islands',
    value: 'FK',
  },
  {
    option: 'Faroe Islands',
    value: 'FO',
  },
  {
    option: 'Fiji',
    value: 'FJ',
  },
  {
    option: 'Finland',
    value: 'FI',
  },
  {
    option: 'France',
    value: 'FR',
  },
  {
    option: 'French Guiana',
    value: 'GF',
  },
  {
    option: 'French Polynesia',
    value: 'PF',
  },
  {
    option: 'French Southern Territories',
    value: 'TF',
  },
  {
    option: 'Gabon',
    value: 'GA',
  },
  {
    option: 'Gambia',
    value: 'GM',
  },
  {
    option: 'Georgia',
    value: 'GE',
  },
  {
    option: 'Germany',
    value: 'DE',
  },
  {
    option: 'Ghana',
    value: 'GH',
  },
  {
    option: 'Gibraltar',
    value: 'GI',
  },
  {
    option: 'Greece',
    value: 'GR',
  },
  {
    option: 'Greenland',
    value: 'GL',
  },
  {
    option: 'Grenada',
    value: 'GD',
  },
  {
    option: 'Guadeloupe',
    value: 'GP',
  },
  {
    option: 'Guam',
    value: 'GU',
  },
  {
    option: 'Guatemala',
    value: 'GT',
  },
  {
    option: 'Guinea',
    value: 'GN',
  },
  {
    option: 'Guinea-Bissau',
    value: 'GW',
  },
  {
    option: 'Guyana',
    value: 'GY',
  },
  {
    option: 'Haiti',
    value: 'HT',
  },
  {
    option: 'Heard Island and McDonald Islands',
    value: 'HM',
  },
  {
    option: 'Honduras',
    value: 'HN',
  },
  {
    option: 'Hong Kong',
    value: 'HK',
  },
  {
    option: 'Hungary',
    value: 'HU',
  },
  {
    option: 'Iceland',
    value: 'IS',
  },
  {
    option: 'India',
    value: 'IN',
  },
  {
    option: 'Indonesia',
    value: 'ID',
  },
  {
    option: 'Iran',
    value: 'IR',
  },
  {
    option: 'Iraq',
    value: 'IQ',
  },
  {
    option: 'Ireland',
    value: 'IE',
  },
  {
    option: 'Israel',
    value: 'IL',
  },
  {
    option: 'Italy',
    value: 'IT',
  },
  {
    option: 'Jamaica',
    value: 'JM',
  },
  {
    option: 'Japan',
    value: 'JP',
  },
  {
    option: 'Jersey',
    value: 'JE',
  },
  {
    option: 'Jordan',
    value: 'JO',
  },
  {
    option: 'Kazakhstan',
    value: 'KZ',
  },
  {
    option: 'Kenya',
    value: 'KE',
  },
  {
    option: 'Kiribati',
    value: 'KI',
  },
  {
    option: 'Kosovo',
    value: 'XK',
  },
  {
    option: 'Kuwait',
    value: 'KW',
  },
  {
    option: 'Kyrgyzstan',
    value: 'KG',
  },
  {
    option: 'Laos',
    value: 'LA',
  },
  {
    option: 'Latvia',
    value: 'LV',
  },
  {
    option: 'Lebanon',
    value: 'LB',
  },
  {
    option: 'Lesotho',
    value: 'LS',
  },
  {
    option: 'Liberia',
    value: 'LR',
  },
  {
    option: 'Libya',
    value: 'LY',
  },
  {
    option: 'Liechtenstein',
    value: 'LI',
  },
  {
    option: 'Lithuania',
    value: 'LT',
  },
  {
    option: 'Luxembourg',
    value: 'LU',
  },
  {
    option: 'Macao',
    value: 'MO',
  },
  {
    option: 'Macedonia',
    value: 'MK',
  },
  {
    option: 'Madagascar',
    value: 'MG',
  },
  {
    option: 'Malawi',
    value: 'MW',
  },
  {
    option: 'Malaysia',
    value: 'MY',
  },
  {
    option: 'Maldives',
    value: 'MV',
  },
  {
    option: 'Mali',
    value: 'ML',
  },
  {
    option: 'Malta',
    value: 'MT',
  },
  {
    option: 'Marshall Islands',
    value: 'MH',
  },
  {
    option: 'Martinique',
    value: 'MQ',
  },
  {
    option: 'Mauritania',
    value: 'MR',
  },
  {
    option: 'Mauritius',
    value: 'MU',
  },
  {
    option: 'Mayotte',
    value: 'YT',
  },
  {
    option: 'Mexico',
    value: 'MX',
  },
  {
    option: 'Micronesia',
    value: 'FM',
  },
  {
    option: 'Moldova',
    value: 'MD',
  },
  {
    option: 'Monaco',
    value: 'MC',
  },
  {
    option: 'Mongolia',
    value: 'MN',
  },
  {
    option: 'Montenegro',
    value: 'ME',
  },
  {
    option: 'Montserrat',
    value: 'MS',
  },
  {
    option: 'Morocco',
    value: 'MA',
  },
  {
    option: 'Mozambique',
    value: 'MZ',
  },
  {
    option: 'Myanmar',
    value: 'MM',
  },
  {
    option: 'Namibia',
    value: 'NA',
  },
  {
    option: 'Nauru',
    value: 'NR',
  },
  {
    option: 'Nepal',
    value: 'NP',
  },
  {
    option: 'Netherlands',
    value: 'NL',
  },
  {
    option: 'Netherlands Antilles',
    value: 'AN',
  },
  {
    option: 'New Caledonia',
    value: 'NC',
  },
  {
    option: 'New Zealand',
    value: 'NZ',
  },
  {
    option: 'Nicaragua',
    value: 'NI',
  },
  {
    option: 'Niger',
    value: 'NE',
  },
  {
    option: 'Nigeria',
    value: 'NG',
  },
  {
    option: 'Niue',
    value: 'NU',
  },
  {
    option: 'Norfolk Island',
    value: 'NF',
  },
  {
    option: 'North Korea',
    value: 'KP',
  },
  {
    option: 'Northern Mariana Islands',
    value: 'MP',
  },
  {
    option: 'Norway',
    value: 'NO',
  },
  {
    option: 'Oman',
    value: 'OM',
  },
  {
    option: 'Pakistan',
    value: 'PK',
  },
  {
    option: 'Palau',
    value: 'PW',
  },
  {
    option: 'Palestinian Territories',
    value: 'PS',
  },
  {
    option: 'Panama',
    value: 'PA',
  },
  {
    option: 'Papua New Guinea',
    value: 'PG',
  },
  {
    option: 'Paraguay',
    value: 'PY',
  },
  {
    option: 'Peru',
    value: 'PE',
  },
  {
    option: 'Philippines',
    value: 'PH',
  },
  {
    option: 'Pitcairn',
    value: 'PN',
  },
  {
    option: 'Poland',
    value: 'PL',
  },
  {
    option: 'Portugal',
    value: 'PT',
  },
  {
    option: 'Puerto Rico',
    value: 'PR',
  },
  {
    option: 'Qatar',
    value: 'QA',
  },
  {
    option: 'Reunion',
    value: 'RE',
  },
  {
    option: 'Romania',
    value: 'RO',
  },
  {
    option: 'Russia',
    value: 'RU',
  },
  {
    option: 'Rwanda',
    value: 'RW',
  },
  {
    option: 'Saint Helena',
    value: 'SH',
  },
  {
    option: 'Saint Kitts and Nevis',
    value: 'KN',
  },
  {
    option: 'Saint Lucia',
    value: 'LC',
  },
  {
    option: 'Saint Pierre and Miquelon',
    value: 'PM',
  },
  {
    option: 'Saint Vincent and the Grenadines',
    value: 'VC',
  },
  {
    option: 'Samoa',
    value: 'WS',
  },
  {
    option: 'San Marino',
    value: 'SM',
  },
  {
    option: 'Saudi Arabia',
    value: 'SA',
  },
  {
    option: 'Senegal',
    value: 'SN',
  },
  {
    option: 'Serbia',
    value: 'RS',
  },
  {
    option: 'Seychelles',
    value: 'SC',
  },
  {
    option: 'Sierra Leone',
    value: 'SL',
  },
  {
    option: 'Singapore',
    value: 'SG',
  },
  {
    option: 'Sint Maarten',
    value: 'SX',
  },
  {
    option: 'Slovakia',
    value: 'SK',
  },
  {
    option: 'Slovenia',
    value: 'SI',
  },
  {
    option: 'Solomon Islands',
    value: 'SB',
  },
  {
    option: 'Somalia',
    value: 'SO',
  },
  {
    option: 'South Africa',
    value: 'ZA',
  },
  {
    option: 'South Georgia and the South Sandwich Islands',
    value: 'GS',
  },
  {
    option: 'South Korea',
    value: 'KR',
  },
  {
    option: 'South Sudan',
    value: 'SS',
  },
  {
    option: 'Spain',
    value: 'ES',
  },
  {
    option: 'Sri Lanka',
    value: 'LK',
  },
  {
    option: 'Sudan',
    value: 'SD',
  },
  {
    option: 'Suriname',
    value: 'SR',
  },
  {
    option: 'Svalbard and Jan Mayen',
    value: 'SJ',
  },
  {
    option: 'Swaziland',
    value: 'SZ',
  },
  {
    option: 'Sweden',
    value: 'SE',
  },
  {
    option: 'Switzerland',
    value: 'CH',
  },
  {
    option: 'Syria',
    value: 'SY',
  },
  {
    option: 'São Tomé and Príncipe',
    value: 'ST',
  },
  {
    option: 'Taiwan',
    value: 'TW',
  },
  {
    option: 'Tajikistan',
    value: 'TJ',
  },
  {
    option: 'Tanzania',
    value: 'TZ',
  },
  {
    option: 'Thailand',
    value: 'TH',
  },
  {
    option: 'Timor-Leste',
    value: 'TL',
  },
  {
    option: 'Togo',
    value: 'TG',
  },
  {
    option: 'Tokelau',
    value: 'TK',
  },
  {
    option: 'Tonga',
    value: 'TO',
  },
  {
    option: 'Trinidad and Tobago',
    value: 'TT',
  },
  {
    option: 'Tunisia',
    value: 'TN',
  },
  {
    option: 'Turkey',
    value: 'TR',
  },
  {
    option: 'Turkmenistan',
    value: 'TM',
  },
  {
    option: 'Turks and Caicos Islands',
    value: 'TC',
  },
  {
    option: 'Tuvalu',
    value: 'TV',
  },
  {
    option: 'Uganda',
    value: 'UG',
  },
  {
    option: 'Ukraine',
    value: 'UA',
  },
  {
    option: 'United Arab Emirates',
    value: 'AE',
  },
  {
    option: 'United Kingdom',
    value: 'GB',
  },
  {
    option: 'United States Minor Outlying Islands',
    value: 'UM',
  },
  {
    option: 'Uruguay',
    value: 'UY',
  },
  {
    option: 'Uzbekistan',
    value: 'UZ',
  },
  {
    option: 'Vanuatu',
    value: 'VU',
  },
  {
    option: 'Vatican City',
    value: 'VA',
  },
  {
    option: 'Venezuela',
    value: 'VE',
  },
  {
    option: 'Vietnam',
    value: 'VN',
  },
  {
    option: 'Virgin Islands, British',
    value: 'VG',
  },
  {
    option: 'Virgin Islands, U.S.',
    value: 'VI',
  },
  {
    option: 'Wallis and Futuna',
    value: 'WF',
  },
  {
    option: 'Western Sahara',
    value: 'EH',
  },
  {
    option: 'Yemen',
    value: 'YE',
  },
  {
    option: 'Zambia',
    value: 'ZM',
  },
  {
    option: 'Zimbabwe',
    value: 'ZW',
  },
  {
    option: 'Åland Islands',
    value: 'AX',
  },
];

const localityList = [
  {
    option: 'Alaior',
    value: '070027',
  },
  {
    option: 'Alaro',
    value: '070012',
  },
  {
    option: 'Alcudia',
    value: '070033',
  },
  {
    option: 'Algaida',
    value: '070048',
  },
  {
    option: 'Andratx',
    value: '070051',
  },
  {
    option: 'Ariany',
    value: '079013',
  },
  {
    option: 'Arta',
    value: '070064',
  },
  {
    option: 'Banyalbufar',
    value: '070070',
  },
  {
    option: 'Binisalem',
    value: '070086',
  },
  {
    option: 'Buger',
    value: '070099',
  },
  {
    option: 'Bunyola',
    value: '070103',
  },
  {
    option: 'CalviÃ ',
    value: '070110',
  },
  {
    option: 'Campanet',
    value: '070125',
  },
  {
    option: 'Campos',
    value: '070131',
  },
  {
    option: 'Capdepera',
    value: '070146',
  },
  {
    option: 'Castell -Es-',
    value: '070645',
  },
  {
    option: 'Ciutadella de Menorca',
    value: '070159',
  },
  {
    option: 'Consell',
    value: '070162',
  },
  {
    option: 'Costitx',
    value: '070178',
  },
  {
    option: 'DeiÃ ',
    value: '070184',
  },
  {
    option: 'Eivissa',
    value: '070260',
  },
  {
    option: 'Escorca',
    value: '070197',
  },
  {
    option: 'Esporles',
    value: '070201',
  },
  {
    option: 'Estellenchs',
    value: '070218',
  },
  {
    option: 'Felanitx',
    value: '070223',
  },
  {
    option: 'Ferreries',
    value: '070239',
  },
  {
    option: 'Formentera',
    value: '070244',
  },
  {
    option: 'Fornalutx',
    value: '070257',
  },
  {
    option: 'Inca',
    value: '070276',
  },
  {
    option: 'Lloret de Vista Alegre',
    value: '070282',
  },
  {
    option: 'Lloseta',
    value: '070295',
  },
  {
    option: 'Llubi',
    value: '070309',
  },
  {
    option: 'Llucmajor',
    value: '070316',
  },
  {
    option: 'Mahon',
    value: '070321',
  },
  {
    option: 'Manacor',
    value: '070337',
  },
  {
    option: 'Mancor de la Vall',
    value: '070342',
  },
  {
    option: 'Maria de La Salud',
    value: '070355',
  },
  {
    option: 'Marratxi',
    value: '070368',
  },
  {
    option: 'Mercadal -Es-',
    value: '070374',
  },
  {
    option: 'Migjom Gran -Es-',
    value: '079028',
  },
  {
    option: 'Montuiri',
    value: '070380',
  },
  {
    option: 'Muro',
    value: '070393',
  },
  {
    option: 'Palma de Mallorca',
    value: '070407',
  },
  {
    option: 'Petra',
    value: '070414',
  },
  {
    option: 'Pobla -Sa-',
    value: '070440',
  },
  {
    option: 'Pollenca',
    value: '070429',
  },
  {
    option: 'Porreres',
    value: '070435',
  },
  {
    option: 'Puigpunyent',
    value: '070453',
  },
  {
    option: 'Salines -Ses-',
    value: '070598',
  },
  {
    option: 'San Jose',
    value: '070488',
  },
  {
    option: 'Sant Antoni de Portmany',
    value: '070466',
  },
  {
    option: 'Sant Joan de Labritja',
    value: '070504',
  },
  {
    option: 'Sant Llorenc des Cardassar',
    value: '070511',
  },
  {
    option: 'Sant Lluis',
    value: '070526',
  },
  {
    option: 'Santa Eugenia',
    value: '070532',
  },
  {
    option: 'Santa Eulalia del Rio',
    value: '070547',
  },
  {
    option: 'Santa Margalida',
    value: '070550',
  },
  {
    option: 'Santa Maria del Cami',
    value: '070563',
  },
  {
    option: 'Santanyi',
    value: '070579',
  },
  {
    option: 'Santjoan',
    value: '070491',
  },
  {
    option: 'Selva',
    value: '070585',
  },
  {
    option: 'Sencel Les',
    value: '070472',
  },
  {
    option: 'Sineu',
    value: '070602',
  },
  {
    option: 'Soller',
    value: '070619',
  },
  {
    option: 'Son Servera',
    value: '070624',
  },
  {
    option: 'Valldemosa',
    value: '070630',
  },
  {
    option: 'Vilafranca de Bonany',
    value: '070658',
  },
];

export const VALIDATORS_MAP: Record<TravellerAgeType, FieldValidatorType> = {
  [TravellerAgeType.Adult]: FieldValidatorType.AdultDateValidator,
  [TravellerAgeType.Child]: FieldValidatorType.ChildDateValidator,
  [TravellerAgeType.Infant]: FieldValidatorType.InfantDateValidator,
};
const passengerFields: (type: TravellerAgeType) => Array<Array<FieldObject>> = (
  type
) => [
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'traveller-gender',
      dataName: 'travellerGender',
      fieldType: FieldTypeValue.RadioButton,
      mandatory: RequiredFieldType.Mandatory,
      options: [
        {
          option: 'booking.shopping.cart.MALE',
          value: 'MALE',
        },
        {
          option: 'booking.shopping.cart.FEMALE',
          value: 'FEMALE',
        },
      ],
      style: null,
      position: {
        row: 1,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.PaxNameRegex,
        },
        autoComplete: false,
        autoCapitalize: true,
        autoCorrect: false,
      },
      dataId: 'name',
      dataName: 'name',
      fieldType: FieldTypeValue.TextField,
      mandatory: RequiredFieldType.Mandatory,
      options: null,
      style: null,
      position: {
        row: 2,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.PaxNameRegex,
        },
        autoComplete: false,
        autoCapitalize: true,
        autoCorrect: false,
      },
      dataId: 'first-last-name',
      dataName: 'firstLastName',
      fieldType: FieldTypeValue.TextField,
      mandatory: RequiredFieldType.Mandatory,
      options: null,
      style: null,
      position: {
        row: 3,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: VALIDATORS_MAP[type],
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'date-of-birth-adult',
      dataName: 'dateOfBirth',
      fieldType: FieldTypeValue.DateField,
      mandatory: RequiredFieldType.Mandatory,
      options: null,
      style: null,
      position: {
        row: 4,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: null,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'identification-type',
      dataName: 'identificationType',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: [
        {
          option: 'booking.shopping.cart.NATIONAL_ID_CARD',
          value: 'NATIONAL_ID_CARD',
        },
        {
          option: 'booking.shopping.cart.PASSPORT',
          value: 'PASSPORT',
        },
      ],
      style: null,
      position: {
        row: 5,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.IdentificationValidator,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: false,
      },
      dataId: 'identification',
      dataName: 'identification',
      fieldType: FieldTypeValue.TextField,
      mandatory: RequiredFieldType.Optional,
      options: null,
      style: null,
      position: {
        row: 6,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.ExpirationDateValidator,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'identification-expiration-date',
      dataName: 'identificationExpirationDate',
      fieldType: FieldTypeValue.DateField,
      mandatory: RequiredFieldType.Optional,
      options: null,
      style: null,
      position: {
        row: 7,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'nationality-country-code',
      dataName: 'nationalityCountryCode',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: countryList,
      style: null,
      position: {
        row: 8,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'country-code-of-residence',
      dataName: 'countryCodeOfResidence',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: countryList,
      style: null,
      position: {
        row: 9,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'identification-issue-country-code',
      dataName: 'identificationIssueCountryCode',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: countryList,
      style: null,
      position: {
        row: 10,
        column: 1,
      },
    },
  ],
];
const noOptionalRequestFiedls = [];
const optionalRequestField: Array<Array<FieldObject>> = [
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.FfcCodeRegex,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: false,
      },
      dataId: 'passenger-card-number',
      dataName: 'passengerCardNumber',
      fieldType: FieldTypeValue.TextField,
      mandatory: RequiredFieldType.Optional,
      options: [
        {
          option: 'TK',
          value: 'Turkish Airlines',
        },
      ],
      style: null,
      position: {
        row: 11,
        column: 1,
      },
    },
  ],
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'meal',
      dataName: 'meal',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: [
        {
          option: 'booking.shopping.cart.meal.DIABETIC',
          value: 'DIABETIC',
        },
        {
          option: 'booking.shopping.cart.meal.HALAL',
          value: 'HALAL',
        },
        {
          option: 'booking.shopping.cart.meal.HINDU',
          value: 'HINDU',
        },
        {
          option: 'booking.shopping.cart.meal.STANDARD',
          value: 'STANDARD',
        },
        {
          option: 'booking.shopping.cart.meal.LOW_SODIUM',
          value: 'LOW_SODIUM',
        },
        {
          option: 'booking.shopping.cart.meal.GLUTEN_FREE',
          value: 'GLUTEN_FREE',
        },
        {
          option: 'booking.shopping.cart.meal.VEGETARIAN',
          value: 'VEGETARIAN',
        },
        {
          option: 'booking.shopping.cart.meal.KOSHER',
          value: 'KOSHER',
        },
        {
          option: 'booking.shopping.cart.meal.LOW_CHOLESTEROL',
          value: 'LOW_CHOLESTEROL',
        },
        {
          option: 'booking.shopping.cart.meal.CHILD',
          value: 'CHILD',
        },
        {
          option: 'booking.shopping.cart.meal.BABY',
          value: 'BABY',
        },
        {
          option: 'booking.shopping.cart.meal.VEGAN',
          value: 'VEGAN',
        },
      ],
      style: null,
      position: {
        row: 12,
        column: 1,
      },
    },
  ],
];
const optionalRequestFieldsWithoutFrequentFlyer = [
  [
    {
      attributes: {
        validator: {
          type: FieldValidatorType.NotEmpty,
          regex: null,
        },
        autoComplete: false,
        autoCapitalize: null,
        autoCorrect: null,
      },
      dataId: 'meal',
      dataName: 'meal',
      fieldType: FieldTypeValue.ComboBox,
      mandatory: RequiredFieldType.Optional,
      options: [
        {
          option: 'booking.shopping.cart.meal.DIABETIC',
          value: 'DIABETIC',
        },
        {
          option: 'booking.shopping.cart.meal.HALAL',
          value: 'HALAL',
        },
        {
          option: 'booking.shopping.cart.meal.HINDU',
          value: 'HINDU',
        },
        {
          option: 'booking.shopping.cart.meal.STANDARD',
          value: 'STANDARD',
        },
        {
          option: 'booking.shopping.cart.meal.LOW_SODIUM',
          value: 'LOW_SODIUM',
        },
        {
          option: 'booking.shopping.cart.meal.GLUTEN_FREE',
          value: 'GLUTEN_FREE',
        },
        {
          option: 'booking.shopping.cart.meal.VEGETARIAN',
          value: 'VEGETARIAN',
        },
        {
          option: 'booking.shopping.cart.meal.KOSHER',
          value: 'KOSHER',
        },
        {
          option: 'booking.shopping.cart.meal.LOW_CHOLESTEROL',
          value: 'LOW_CHOLESTEROL',
        },
        {
          option: 'booking.shopping.cart.meal.CHILD',
          value: 'CHILD',
        },
        {
          option: 'booking.shopping.cart.meal.BABY',
          value: 'BABY',
        },
        {
          option: 'booking.shopping.cart.meal.VEGAN',
          value: 'VEGAN',
        },
      ],
      style: null,
      position: {
        row: 1,
        column: 1,
      },
    },
  ],
];

const residentGroups: Array<ResidentGroupContainer> = [
  {
    residentLocalities: [
      {
        code: '070027',
        name: 'Alaior',
      },
      {
        code: '070012',
        name: 'Alaro',
      },
      {
        code: '070033',
        name: 'Alcudia',
      },
      {
        code: '070048',
        name: 'Algaida',
      },
      {
        code: '070051',
        name: 'Andratx',
      },
      {
        code: '079013',
        name: 'Ariany',
      },
      {
        code: '070064',
        name: 'Arta',
      },
      {
        code: '070070',
        name: 'Banyalbufar',
      },
      {
        code: '070086',
        name: 'Binisalem',
      },
      {
        code: '070099',
        name: 'Buger',
      },
      {
        code: '070103',
        name: 'Bunyola',
      },
      {
        code: '070110',
        name: 'Calvià',
      },
      {
        code: '070125',
        name: 'Campanet',
      },
      {
        code: '070131',
        name: 'Campos',
      },
      {
        code: '070146',
        name: 'Capdepera',
      },
      {
        code: '070645',
        name: 'Castell -Es-',
      },
      {
        code: '070159',
        name: 'Ciutadella de Menorca',
      },
      {
        code: '070162',
        name: 'Consell',
      },
      {
        code: '070178',
        name: 'Costitx',
      },
      {
        code: '070184',
        name: 'Deià',
      },
      {
        code: '070260',
        name: 'Eivissa',
      },
      {
        code: '070197',
        name: 'Escorca',
      },
      {
        code: '070201',
        name: 'Esporles',
      },
      {
        code: '070218',
        name: 'Estellenchs',
      },
      {
        code: '070223',
        name: 'Felanitx',
      },
      {
        code: '070239',
        name: 'Ferreries',
      },
      {
        code: '070244',
        name: 'Formentera',
      },
      {
        code: '070257',
        name: 'Fornalutx',
      },
      {
        code: '070276',
        name: 'Inca',
      },
      {
        code: '070282',
        name: 'Lloret de Vista Alegre',
      },
      {
        code: '070295',
        name: 'Lloseta',
      },
      {
        code: '070309',
        name: 'Llubi',
      },
      {
        code: '070316',
        name: 'Llucmajor',
      },
      {
        code: '070321',
        name: 'Mahon',
      },
      {
        code: '070337',
        name: 'Manacor',
      },
      {
        code: '070342',
        name: 'Mancor de la Vall',
      },
      {
        code: '070355',
        name: 'Maria de La Salud',
      },
      {
        code: '070368',
        name: 'Marratxi',
      },
      {
        code: '070374',
        name: 'Mercadal -Es-',
      },
      {
        code: '079028',
        name: 'Migjom Gran -Es-',
      },
      {
        code: '070380',
        name: 'Montuiri',
      },
      {
        code: '070393',
        name: 'Muro',
      },
      {
        code: '070407',
        name: 'Palma de Mallorca',
      },
      {
        code: '070414',
        name: 'Petra',
      },
      {
        code: '070440',
        name: 'Pobla -Sa-',
      },
      {
        code: '070429',
        name: 'Pollenca',
      },
      {
        code: '070435',
        name: 'Porreres',
      },
      {
        code: '070453',
        name: 'Puigpunyent',
      },
      {
        code: '070598',
        name: 'Salines -Ses-',
      },
      {
        code: '070488',
        name: 'San Jose',
      },
      {
        code: '070466',
        name: 'Sant Antoni de Portmany',
      },
      {
        code: '070504',
        name: 'Sant Joan de Labritja',
      },
      {
        code: '070511',
        name: 'Sant Llorenc des Cardassar',
      },
      {
        code: '070526',
        name: 'Sant Lluis',
      },
      {
        code: '070532',
        name: 'Santa Eugenia',
      },
      {
        code: '070547',
        name: 'Santa Eulalia del Rio',
      },
      {
        code: '070550',
        name: 'Santa Margalida',
      },
      {
        code: '070563',
        name: 'Santa Maria del Cami',
      },
      {
        code: '070579',
        name: 'Santanyi',
      },
      {
        code: '070491',
        name: 'Santjoan',
      },
      {
        code: '070585',
        name: 'Selva',
      },
      {
        code: '070472',
        name: 'Sencel Les',
      },
      {
        code: '070602',
        name: 'Sineu',
      },
      {
        code: '070619',
        name: 'Soller',
      },
      {
        code: '070624',
        name: 'Son Servera',
      },
      {
        code: '070630',
        name: 'Valldemosa',
      },
      {
        code: '070658',
        name: 'Vilafranca de Bonany',
      },
    ],
    name: ResidentGroup.Baleares,
  },
];

export const itineraryMock: Itinerary = {
  id: '',
  tripType: TripType.RoundTrip,
  key: '',
  meRating: 0,
  hotelXSellingEnabled: false,
  hasFreeRebooking: false,
  hasRefundCarriers: false,
  hasReliableCarriers: false,
  isFareUpgradeAvailable: false,
  fees: [],
  freeCancellationLimit: {
    hoursApart: 5,
  },
  ticketsLeft: 2,
  freeCancellation: null,
  transportTypes: [TransportType.Plane],
  legs: [
    {
      segmentKeys: [],
      segments: [
        {
          id: '0',
          seats: null,
          duration: 120,
          transportTypes: [TransportType.Plane],
          carrier: {
            id: 'AF',
            name: 'Air France',
          },
          baggageCondition: BaggageCondition.BasedOnAirline,
          sections: [
            {
              isHub: false,
              id: '0',
              flightCode: 'AF1449',
              departureDate: '2021-06-29T06:15:00+02:00',
              departureTerminal: null,
              departure: {
                id: '157',
                cityName: 'Barcelone',
                name: 'El Prat',
                cityIata: 'BCN',
                iata: 'BCN',
                locationType: LocationType.Airport,
                countryName: 'Espagne',
              },
              arrivalDate: '2021-06-29T08:15:00+02:00',
              destination: {
                id: '330',
                cityName: 'Paris',
                name: 'Charles De Gaulle',
                cityIata: 'PAR',
                iata: 'CDG',
                locationType: LocationType.Airport,
                countryName: 'France',
              },
              arrivalTerminal: null,
              transportType: TransportType.Plane,
              carrier: {
                id: 'AF',
                name: 'Air France',
              },
              operatingCarrier: null,
              cabinClass: CabinClass.Tourist,
              baggageAllowance: null,
              technicalStops: [],
            },
          ],
        },
      ],
    },
    {
      segmentKeys: [],
      segments: [
        {
          id: '1',
          seats: null,
          duration: 110,
          transportTypes: [TransportType.Plane],
          carrier: {
            id: 'AF',
            name: 'Air France',
          },
          baggageCondition: BaggageCondition.BasedOnAirline,
          sections: [
            {
              isHub: false,
              id: '1',
              flightCode: 'AF1448',
              departureDate: '2026-07-03T21:05:00+02:00',
              departureTerminal: null,
              departure: {
                id: '330',
                cityName: 'Paris',
                name: 'Charles De Gaulle',
                cityIata: 'PAR',
                iata: 'CDG',
                locationType: LocationType.Airport,
                countryName: 'France',
              },
              arrivalDate: '2026-07-03T22:55:00+02:00',
              destination: {
                id: '157',
                cityName: 'Barcelone',
                name: 'El Prat',
                cityIata: 'BCN',
                iata: 'BCN',
                locationType: LocationType.Airport,
                countryName: 'Espagne',
              },
              arrivalTerminal: null,
              transportType: TransportType.Plane,
              carrier: {
                id: 'AF',
                name: 'Air France',
              },
              operatingCarrier: null,
              cabinClass: CabinClass.Tourist,
              baggageAllowance: null,
              technicalStops: [],
            },
          ],
        },
      ],
    },
  ],
};

const userTravellerMock: UserTraveller = {
  id: 123,
  buyer: false,
  importedFromLocal: false,
  email: 'bbb@bb.es',
  middleName: 'Uuuu',
  personalInfo: {
    name: 'Juanita',
    firstLastName: 'Blabla',
    secondLastName: 'Bleble',
    ageType: TravellerAgeType.Adult,
    birthDate: '1989-12-25',
    title: TravellerTitleType.Mrs,
    travellerGender: TravellerGender.Female,
    nationalityCountryCode: 'es',
  },
};

const userMock: GetUserQuery['getUser'] = {
  id: 0,
  email: 'aaa@aa.es',
  membership: null,
  travellers: [userTravellerMock],
};

const userPrimeMock: GetUserQuery['getUser'] = {
  id: 3012,
  email: 'primeuser@gmail.com',
  membership: {
    id: 345,
  },
  travellers: [
    userTravellerMock,
    {
      id: 0,
      buyer: true,
      importedFromLocal: false,
      email: 'primeuser@gmail.com',
      middleName: 'Uuuu',
      personalInfo: {
        name: 'Martina',
        firstLastName: 'Gluu',
        secondLastName: 'Brave',
        ageType: TravellerAgeType.Adult,
        birthDate: '1970-12-12',
        title: TravellerTitleType.Mrs,
        travellerGender: TravellerGender.Female,
        nationalityCountryCode: 'es',
      },
    },
  ],
};

interface SegmentsMock {
  sections?: Array<Partial<Section>>;
}
interface LegMock {
  segments?: Array<SegmentsMock>;
}
interface ItineraryMock {
  legs?: Maybe<Array<LegMock>>;
}

export interface ShoppingCartMockResponse {
  requiredTravellerInformation: Maybe<
    Array<Partial<TravellerInformationDescription>>
  >;
  fieldGroups: ShoppingCart['fieldGroups'];
  itinerary: Partial<ItineraryMock>;
}

export const singleAdult = () => ({
  mocks: {
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        { travellerType: TravellerAgeType.Adult, residentGroups },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: [
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.NotEmpty,
                    },
                    autoComplete: false,
                    autoCapitalize: null,
                    autoCorrect: null,
                  },
                  dataId: 'traveller-gender',
                  dataName: 'travellerGender',
                  fieldType: FieldTypeValue.RadioButton,
                  mandatory: RequiredFieldType.Mandatory,
                  options: [
                    {
                      option: 'booking.shopping.cart.MALE',
                      value: 'MALE',
                    },
                    {
                      option: 'booking.shopping.cart.FEMALE',
                      value: 'FEMALE',
                    },
                  ],
                  style: null,
                  position: {
                    row: 1,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.PaxNameRegex,
                    },
                    autoComplete: false,
                    autoCapitalize: true,
                    autoCorrect: false,
                  },
                  dataId: 'name',
                  dataName: 'name',
                  fieldType: FieldTypeValue.TextField,
                  mandatory: RequiredFieldType.Mandatory,
                  options: null,
                  style: null,
                  position: {
                    row: 2,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.PaxNameRegex,
                    },
                    autoComplete: false,
                    autoCapitalize: true,
                    autoCorrect: false,
                  },
                  dataId: 'first-last-name',
                  dataName: 'firstLastName',
                  fieldType: FieldTypeValue.TextField,
                  mandatory: RequiredFieldType.Mandatory,
                  options: null,
                  style: null,
                  position: {
                    row: 3,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.PaxNameRegex,
                      regex: null,
                    },
                    autoComplete: false,
                    autoCapitalize: true,
                    autoCorrect: false,
                  },
                  dataId: 'second-last-name',
                  dataName: 'secondLastName',
                  fieldType: FieldTypeValue.TextField,
                  mandatory: RequiredFieldType.Mandatory,
                  options: null,
                  style: null,
                  position: {
                    row: 3,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: VALIDATORS_MAP[TravellerAgeType.Adult],
                    },
                    autoComplete: false,
                    autoCapitalize: null,
                    autoCorrect: null,
                  },
                  dataId: 'date-of-birth-adult',
                  dataName: 'dateOfBirth',
                  fieldType: FieldTypeValue.DateField,
                  mandatory: RequiredFieldType.Mandatory,
                  options: null,
                  style: null,
                  position: {
                    row: 4,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.NotEmpty,
                    },
                    autoComplete: null,
                    autoCapitalize: null,
                    autoCorrect: null,
                  },
                  dataId: 'identification-type',
                  dataName: 'identificationType',
                  fieldType: FieldTypeValue.ComboBox,
                  mandatory: RequiredFieldType.Mandatory,
                  options: [
                    {
                      option: 'booking.shopping.cart.NATIONAL_ID_CARD',
                      value: 'NATIONAL_ID_CARD',
                    },
                    {
                      option: 'booking.shopping.cart.PASSPORT',
                      value: 'PASSPORT',
                    },
                  ],
                  style: null,
                  position: {
                    row: 5,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.IdentificationValidator,
                    },
                    autoComplete: false,
                    autoCapitalize: null,
                    autoCorrect: false,
                  },
                  dataId: 'identification',
                  dataName: 'identification',
                  fieldType: FieldTypeValue.TextField,
                  mandatory: RequiredFieldType.Mandatory,
                  options: null,
                  style: null,
                  position: {
                    row: 6,
                    column: 1,
                  },
                },
              ],
              [
                {
                  attributes: {
                    validator: {
                      type: FieldValidatorType.NotEmpty,
                    },
                    autoComplete: null,
                    autoCapitalize: null,
                    autoCorrect: null,
                  },
                  dataId: 'locality-code-of-residence',
                  dataName: 'localityCodeOfResidence',
                  fieldType: FieldTypeValue.ComboBox,
                  mandatory: RequiredFieldType.Mandatory,
                  options: localityList,
                  style: null,
                  position: {
                    row: 5,
                    column: 1,
                  },
                },
              ],
            ],
            optionalRequestFields: null,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => null,
  },
});

const multipleAdults = () => ({
  mocks: {
    Long: () => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => null,
  },
});

export const multipleAdultsLogged = () => ({
  mocks: {
    Long: () => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => userMock,
  },
});

export const multipleAdultsPrimeLogged = () => ({
  mocks: {
    Long: () => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: [],
        },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => userPrimeMock,
  },
});

export const adultWithChild = () => ({
  mocks: {
    Long: () => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        { travellerType: TravellerAgeType.Adult, residentGroups: [] },
        { travellerType: TravellerAgeType.Child, residentGroups: [] },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
          {
            passengersFields: passengerFields(TravellerAgeType.Child),
            optionalRequestFields: optionalRequestFieldsWithoutFrequentFlyer,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => null,
  },
});

export const adultWithInfant = () => ({
  mocks: {
    Long: () => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        { travellerType: TravellerAgeType.Adult, residentGroups: [] },
        { travellerType: TravellerAgeType.Infant, residentGroups: [] },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(TravellerAgeType.Adult),
            optionalRequestFields: optionalRequestField,
          },
          {
            passengersFields: passengerFields(TravellerAgeType.Infant),
            optionalRequestFields: optionalRequestFieldsWithoutFrequentFlyer,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => null,
  },
});

export default multipleAdults;
