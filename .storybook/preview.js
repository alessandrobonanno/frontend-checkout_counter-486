import { initializeRTL } from 'storybook-addon-rtl';
import { TranslationsDecorator } from 'frontend-commons-storybook-cms';
import { initialize as initializeTrackingAddon } from 'frontend-commons-storybook-tracking';
import withProvider from './decorators/withProvider';
import { theme, site } from './utils/globals';
import { sortStories } from './utils/helpers';
import { components } from './utils/theme';
import { TestingProviders } from '../src/testUtils';

export const globalTypes = {
  theme,
};

const SORT_ORDER = {
  Components: ['FlightSummary', 'Persuasive'],
};

export const parameters = {
  controls: {
    hideNoControlsWarning: true,
  },
  options: {
    storySort: sortStories(SORT_ORDER),
    selectedPanel: 'addon-controls',
  },
  mockedProps: () => ({}),
  TestingProvider: TestingProviders,
  docs: { components },
};

export const decorators = [withProvider, TranslationsDecorator];

initializeRTL();
initializeTrackingAddon();
