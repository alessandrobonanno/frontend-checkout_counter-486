import { create } from '@storybook/theming/create';
import { Text, Link, PrismaProvider } from 'prisma-design-system';
import Checkout from '../public/images/Checkout.svg';

export default create({
  base: 'light',
  brandTitle: 'Frontend Checkout Storybook',
  brandImage: Checkout,
});

const withPrismaProvider =
  (Component, baseProps = {}) =>
  (props = {}) =>
    (
      <PrismaProvider brand="ED">
        <Component {...baseProps} {...props} />
      </PrismaProvider>
    );

export const components = {
  h1: withPrismaProvider(Text, {
    as: 'h1',
    fontSize: 'heading.2',
    fontWeight: 'bold',
  }),
  h2: withPrismaProvider(Text, {
    as: 'h2',
    fontSize: 'heading.1',
    fontWeight: 'bold',
  }),
  h3: withPrismaProvider(Text, {
    as: 'h3',
    fontSize: 'heading.1',
    fontWeight: 'medium',
  }),
  h4: withPrismaProvider(Text, { as: 'h4', fontSize: 'heading.1' }),
  h5: withPrismaProvider(Text, {
    as: 'h5',
    fontSize: 'heading.0',
    fontWeight: 'bold',
  }),
  h6: withPrismaProvider(Text, {
    as: 'h6',
    fontSize: 'heading.0',
    fontWeight: 'medium',
  }),
  p: withPrismaProvider(Text, { as: 'p' }),
  a: withPrismaProvider(Link),
};
