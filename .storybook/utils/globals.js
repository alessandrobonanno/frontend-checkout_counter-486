export const site = {
  name: 'Site',
  description: 'Brand and locale per site',
  defaultValue: 'ED_en',
  toolbar: {
    icon: 'globe',
    items: [
      { value: 'ED_en', right: '🌐', title: 'eDreams.com' },
      { value: 'ED_es-ES', right: '🇪🇸', title: 'eDreams ES' },
      { value: 'ED_en-GB', right: '🇬🇧', title: 'eDreams UK' },
      { value: 'ED_it-IT', right: '🇮🇹', title: 'eDreams IT' },
      { value: 'GO_fr-FR', right: '🇫🇷', title: 'GoVoyages FR' },
      { value: 'OP_de-DE', right: '🇩🇪', title: 'Opodo DE' },
      { value: 'TR_no-NO', right: '🇳🇴', title: 'Travellink NO' },
      { value: 'ED_en-US', right: '🇺🇸', title: 'eDreams US' },
      { value: 'ED_ja-JP', right: '🇯🇵', title: 'eDreams JP' },
      { value: 'ED_ar-QA', right: '🇶🇦', title: 'eDreams QA' },
      { value: 'OP_ar-AE', right: '🇦🇪', title: 'Opodo AE' },
    ],
  },
};

export const theme = {
  name: 'Theme',
  description: 'Theme version',
  defaultValue: '',
  toolbar: {
    icon: 'paintbrush',
    items: [
      { value: 'blueStone', title: 'Bluestone' },
      { value: 'cobalt', title: 'Cobalt' },
    ],
  },
};
