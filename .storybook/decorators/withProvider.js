import React, { useEffect } from 'react';
import * as dateFnsLocales from 'date-fns/locale';
import { withTranslations } from 'frontend-commons-storybook-cms';
import { parseThemeVersionToABs, sites } from '@frontend-shell/react-hooks';

const withProvider = (story, { globals, parameters }) => {
  let [brand, locale] = globals.site.split('_');
  const site = sites.getSite(brand, locale);
  const siteLocale = site.locale;
  const theme = globals.theme;
  const StorybookProvider = withTranslations(parameters.TestingProvider);
  const { mocks, abs = [] } = parameters.mockedProps();
  const cobaltABs = parseThemeVersionToABs(theme);
  const content = story();
  useEffect(() => {
    document.cookie = `brand=${brand}`;
  }, [brand]);
  return (
    <StorybookProvider
      site={site.websiteCode}
      brand={site.brandCode}
      dateFnsLocale={dateFnsLocales[siteLocale.dates]}
      currencyLocale={siteLocale.currency}
      continueWithPassengersPageValidations={(travellers) =>
        console.log('Action Called for OF1 with data: ', travellers)
      }
      locale={siteLocale}
      theme={theme}
      mocks={mocks}
      window={window}
      abs={[...cobaltABs, ...abs]}
      locale={locale}
    >
      {content}
    </StorybookProvider>
  );
};

export default withProvider;
