import { select } from '@storybook/addon-knobs';

export const carriers = {
  Vueling: {
    id: 'VY',
    name: 'Vueling',
  },
  Ryanair: {
    id: 'FR',
    name: 'Ryanair',
  },
  'Air Europa': {
    id: 'UX',
    name: 'Air Europa',
  },
  Renfe: {
    id: '4R',
    name: 'Renfe',
  },
};

export const carrierOptions = carriers;

const selectCarrier = (label = 'Carrier', defaultValue = 'Air Europa') =>
  select(label, carriers, carrierOptions[defaultValue]);

export default selectCarrier;
