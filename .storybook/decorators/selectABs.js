import { number } from '@storybook/addon-knobs/';

const selectABs = (aliases, max = 4) => {
  return aliases.map((alias) => ({
    alias: alias.name,
    partition: number(`${alias.name}`, alias.defaultPartition || 2, {
      min: 1,
      max,
    }),
  }));
};

export default selectABs;
