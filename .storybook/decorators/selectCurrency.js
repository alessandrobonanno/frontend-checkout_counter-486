import { select } from '@storybook/addon-knobs';

const selectCurrency = () =>
  select('Currency', ['EUR', 'USD', 'SAR', 'QAR', 'AED', 'IDR'], 'EUR');

export default selectCurrency;
