import addons from '@storybook/addons';

import theme from './utils/theme';

addons.setConfig({
  theme,
});
