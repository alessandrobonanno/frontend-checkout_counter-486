const path = require('path');

module.exports = ({ config }) => {
  return Object.assign(config, {
    devtool: 'source-map',
    resolve: {
      ...config.resolve,
      alias: {
        '@': path.resolve(__dirname, '../'),
        decorators: path.resolve(__dirname, './decorators'),
        public: path.resolve(__dirname, 'public'),
        translations: path.resolve(__dirname, '../translations'),
        utils: path.resolve(__dirname, '../utils/'),
      },
    },
  });
};
