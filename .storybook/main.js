module.exports = {
  stories: [
    '../docs/**/*.stories.@(js|mdx|tsx)',
    '../src/**/*.stories.@(js|mdx|tsx)',
  ],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    '@storybook/addon-actions',
    '@storybook/addon-viewport',
    'storybook-addon-rtl',
    'frontend-commons-storybook-cms',
    'frontend-commons-storybook-tracking',
    'storybook-addon-designs',
  ],
  features: {
    postcss: false,
  },
  reactOptions: {
    strictMode: true,
  },
};
