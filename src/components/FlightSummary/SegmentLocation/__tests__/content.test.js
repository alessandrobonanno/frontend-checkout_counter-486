import React from 'react';
import { screen, render } from '@testing-library/react';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';
import SegmentLocation from '..';

const station = {
  name: 'Athocha',
  cityName: 'Madrid',
  iata: 'MAD',
  locationType: 'TRAIN_STATION',
  countryName: 'Spain',
};

const airport = {
  name: 'Barajas',
  cityName: 'Madrid',
  iata: 'MAD',
  locationType: 'AIRPORT',
  countryName: 'Spain',
  terminal: '1',
};

describe('SegmentLocation', () => {
  test('Airport content', async () => {
    render(
      <TestingProviders>
        <SegmentLocation {...airport} />
      </TestingProviders>
    );

    expect(await screen.findByText('MAD')).toBeVisible();
  });
  test('Train station content', async () => {
    render(
      <TestingProviders>
        <SegmentLocation {...station} />
      </TestingProviders>
    );

    expect(await screen.findByText('Athocha (Madrid)')).toBeVisible();
  });
  test('Desktop content', async () => {
    setViewport({ width: 1200 });
    render(
      <TestingProviders>
        <SegmentLocation {...airport} />
      </TestingProviders>
    );

    expect(
      await screen.findByText('Barajas, T1, Madrid (Spain)')
    ).toBeVisible();
  });
});
