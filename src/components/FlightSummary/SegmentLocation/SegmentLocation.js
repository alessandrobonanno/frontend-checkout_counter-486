import React from 'react';
import PropTypes from 'prop-types';
import { Text, Body, useMediaQueries, Flex } from 'prisma-design-system';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentLocationVersioned = ({
  locationType,
  iata,
  name,
  cityName,
  countryName,
  terminal,
  versionConfigs,
  type,
}) => {
  const { md } = useMediaQueries();
  const isTrainStation = locationType === 'TRAIN_STATION';
  const isDestination = type === 'destination';
  return (
    <>
      {md && (
        <Body color={'neutrals.3'} type={versionConfigs.bodyType}>
          {name}, {terminal && 'T' + terminal + ','} {cityName} ({countryName})
        </Body>
      )}

      {!md && (
        <Flex
          width={50}
          justifyContent={isDestination ? 'flex-end' : 'flex-start'}
        >
          <Text
            color={'neutrals.3'}
            fontSize={isTrainStation ? 'body.1' : 'body.2'}
            ellipsis={isTrainStation}
          >
            {isTrainStation ? `${name} (${cityName})` : iata}
          </Text>
        </Flex>
      )}
    </>
  );
};

const SegmentLocation = cobaltDesktopThemedComponentVersion(
  SegmentLocationVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { bodyType: 'base' },
    C: { bodyType: 'small' },
  }
);

SegmentLocation.propTypes = {
  locationType: PropTypes.string.isRequired,
  iata: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  cityName: PropTypes.string.isRequired,
  countryName: PropTypes.string,
  terminal: PropTypes.string,
  type: PropTypes.oneOf(['departure', 'destination']),
};

export default SegmentLocation;
