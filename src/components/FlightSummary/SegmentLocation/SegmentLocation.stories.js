import React from 'react';
import SegmentLocation from './index';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});
const story = {
  title: 'Components/FlightSummary/SegmentLocation',
  component: SegmentLocation,
  parameters: {
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  return <SegmentLocation {...args} />;
};
Default.decorators = [withKnobs];
Default.args = {
  name: 'Athocha',
  cityName: 'Madrid',
  countryName: 'Spain',
  iata: 'MAD',
  locationType: 'TRAIN_STATION',
  terminal: '1',
};

export default story;
