import React from 'react';
import { render, screen } from '@testing-library/react';
import FlightSummaryHeader from '../';
import { TestingProviders } from '../../../../testUtils';

const REFUND_LABEL = 'refund label';
const RELIABLE_LABEL = 'reliable label';

const translations = {
  flightSummary: {
    recommended: {
      itinerary: {
        refund: {
          label: REFUND_LABEL,
        },
        reliable: {
          label: RELIABLE_LABEL,
        },
      },
    },
  },
};

describe('FlightSummaryHeader', () => {
  test('Quick refund message is shown', async () => {
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryHeader
          hasReliableCarriers={false}
          hasRefundCarriers={true}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(REFUND_LABEL)).toBeVisible();
  });

  test('Reliable message is shown', async () => {
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryHeader
          hasReliableCarriers={true}
          hasRefundCarriers={false}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(RELIABLE_LABEL)).toBeVisible();
  });
});
