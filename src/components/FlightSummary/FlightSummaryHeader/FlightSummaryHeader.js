import React from 'react';
import { useTranslation } from '@frontend-shell/react-hooks';
import { Flex, HighlightedHeader } from 'prisma-design-system';

const FlightSummaryHeader = ({ hasReliableCarriers, hasRefundCarriers }) => {
  const { t } = useTranslation();
  let textLabel = '';

  if (hasRefundCarriers) {
    textLabel = t('flightSummary.recommended.itinerary.refund.label');
  } else if (hasReliableCarriers) {
    textLabel = t('flightSummary.recommended.itinerary.reliable.label');
  }

  return textLabel ? (
    <HighlightedHeader type="informative" size="small">
      <Flex alignItems="center" fontWeight="medium">
        {textLabel}
      </Flex>
    </HighlightedHeader>
  ) : null;
};

export default FlightSummaryHeader;
