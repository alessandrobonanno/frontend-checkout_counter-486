const translations = [
  {
    inputPath: ['results', 'recommended.itinerary.refund.label'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'refund',
      'label',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
  {
    inputPath: ['results', 'recommended.itinerary.reliable.label'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'reliable',
      'label',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
];

module.exports = translations;
