import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import FlightSummaryHeader from './FlightSummaryHeader';
import configKeys from './config/keys';

const story = {
  title: 'Components/FlightSummary/FlightSummaryHeader',
  component: FlightSummaryHeader,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

const Template = (args) => {
  return <FlightSummaryHeader {...args} />;
};

export const FlightSummaryHeaderAirlineSteeringQuickRefund = Template.bind({});
FlightSummaryHeaderAirlineSteeringQuickRefund.args = {
  hasRefundCarriers: true,
  hasReliableCarriers: false,
};
FlightSummaryHeaderAirlineSteeringQuickRefund.parameters = {
  locale: true,
};

export const FlightSummaryHeaderAirlineSteeringReliable = Template.bind({});
FlightSummaryHeaderAirlineSteeringReliable.args = {
  hasRefundCarriers: false,
  hasReliableCarriers: true,
};
FlightSummaryHeaderAirlineSteeringReliable.parameters = {
  locale: true,
};

export default story;
