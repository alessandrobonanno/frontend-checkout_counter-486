import React from 'react';
import DepartureTime from './index';

const story = {
  title: 'Components/FlightSummary/DepartureTime',
  component: DepartureTime,
};

export const Default = (args) => {
  return <DepartureTime {...args} />;
};

Default.args = {
  departureDate: '2021-07-22T23:30:00+02:00',
};

export default story;
