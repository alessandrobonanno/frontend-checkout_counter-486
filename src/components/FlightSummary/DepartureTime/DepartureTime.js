import React from 'react';
import PropTypes from 'prop-types';
import SegmentTime from '../SegmentTime';

const DepartureTime = ({ departureDate }) => (
  <SegmentTime date={departureDate} />
);

DepartureTime.propTypes = {
  departureDate: PropTypes.string.isRequired,
};

export default DepartureTime;
