const translations = [
  {
    inputPath: ['home', 'durationShort'],
    outputPath: ['flightSummary', 'segment', 'duration'],
    args: ['hours', 'minutes'],
  },
];

module.exports = translations;
