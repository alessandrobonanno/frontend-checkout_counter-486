import React from 'react';
import PropTypes from 'prop-types';
import { Text, FlexRow, Col, TimeIcon } from 'prisma-design-system';
import Duration from './Duration';

const DurationWithIcon = ({ duration }) => {
  return (
    <FlexRow flexDirection="row" alignItems="center" gap={1}>
      <Col>
        <TimeIcon size="small" />
      </Col>
      <Col>
        <Text
          fontSize="body.1"
          color="neutrals.1"
          fontWeight="medium"
          preserveWhitespace={true}
        >
          <Duration durationInMinutes={duration} />
        </Text>
      </Col>
    </FlexRow>
  );
};

DurationWithIcon.propTypes = {
  duration: PropTypes.number,
};

export default DurationWithIcon;
