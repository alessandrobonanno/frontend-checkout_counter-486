import PropTypes from 'prop-types';
import { useTranslation } from '@frontend-shell/react-hooks';

const MINUTES_IN_A_HOUR = 60;

function Duration({ durationInMinutes }) {
  const { t } = useTranslation();

  if (!durationInMinutes && durationInMinutes !== 0) return false;

  const hours = Math.floor(durationInMinutes / MINUTES_IN_A_HOUR);
  const minutes = String(durationInMinutes % MINUTES_IN_A_HOUR).padStart(
    2,
    '0'
  );

  return t('flightSummary.segment.duration', { hours, minutes });
}

Duration.propTypes = {
  durationInMinutes: PropTypes.number,
};

export default Duration;
