const translations = [
  {
    inputPath: ['results', 'segmentname1'],
    outputPath: ['summary', 'segment', 'departure'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'segmentname2'],
    outputPath: ['summary', 'segment', 'return'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N'],
    outputPath: ['summary', 'segment', 'name', 'multiple'],
    args: ['multipleText'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb1'],
    outputPath: ['summary', 'segment', 'name', 'multiples0'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb2'],
    outputPath: ['summary', 'segment', 'name', 'multiples1'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb3'],
    outputPath: ['summary', 'segment', 'name', 'multiples2'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb4'],
    outputPath: ['summary', 'segment', 'name', 'multiples3'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb5'],
    outputPath: ['summary', 'segment', 'name', 'multiples4'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['results', 'flight_N.numb6'],
    outputPath: ['summary', 'segment', 'name', 'multiples5'],
    scope: ['?ARABIC_SITES'],
  },
];

module.exports = translations;
