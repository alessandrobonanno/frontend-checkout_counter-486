import React from 'react';
import { Flex, Box, Text, Heading } from 'prisma-design-system';
import SegmentDate from '../SegmentDate';
import DepartureTime from '../DepartureTime';
import DestinationTime from '../DestinationTime';
import SegmentLocation from '../SegmentLocation';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentTimeIata = ({ iata }) => {
  const TextVersioned = cobaltDesktopFlightDetailsComponent([
    ({ children, ...props }) => (
      <Text fontSize="heading.4" {...props}>
        {children}
      </Text>
    ),
    null,
    ({ children, ...props }) => (
      <Heading type="2" {...props}>
        {children}
      </Heading>
    ),
  ]);

  return (
    <TextVersioned fontWeight="normal" color="neutrals.1">
      {iata}
    </TextVersioned>
  );
};

const DestinationArrivalComponentHeaderVersioned = ({
  departureDate,
  arrivalDate,
  deparArriv,
  versionConfigs,
}) => {
  const { iataMarginLeft } = versionConfigs;
  const TimeAtLocale = arrivalDate ? DestinationTime : DepartureTime;

  return (
    <Flex justifyContent={'space-between'} alignItems={'center'}>
      <TimeAtLocale departureDate={departureDate} arrivalDate={arrivalDate} />

      <Box ml={iataMarginLeft}>
        <SegmentTimeIata iata={deparArriv.iata} />
      </Box>
    </Flex>
  );
};

const DestinationArrivalComponentHeader = cobaltDesktopThemedComponentVersion(
  DestinationArrivalComponentHeaderVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { iataMarginLeft: 0 },
    C: { iataMarginLeft: 2 },
  }
);

const DestinationArrivalComponentContent = ({
  departureDate,
  arrivalDate,
  deparArriv,
  terminal,
}) => {
  return (
    <>
      <SegmentDate date={arrivalDate || departureDate} />

      <SegmentLocation
        name={deparArriv.name}
        iata={deparArriv.iata}
        locationType={deparArriv.locationType}
        cityName={deparArriv.cityName}
        countryName={deparArriv.countryName}
        terminal={terminal}
      />
    </>
  );
};

const DestinationArrivalComponent = ({
  departureDate,
  arrivalDate,
  deparArriv,
  terminal,
}) => {
  return (
    <>
      <DestinationArrivalComponentHeader
        departureDate={departureDate}
        arrivalDate={arrivalDate}
        deparArriv={deparArriv}
      />
      <DestinationArrivalComponentContent
        departureDate={departureDate}
        arrivalDate={arrivalDate}
        deparArriv={deparArriv}
        terminal={terminal}
      />
    </>
  );
};

export default DestinationArrivalComponent;
export {
  DestinationArrivalComponentContent,
  DestinationArrivalComponentHeader,
};
