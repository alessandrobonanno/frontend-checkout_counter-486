import PropTypes from 'prop-types';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import SegmentDesktopHeaderCobalt from './SegmentDesktopHeaderCobalt';
import SegmentDesktopHeaderBluestone from './SegmentDesktopHeaderBluestone';

const SegmentDesktopHeader = cobaltDesktopFlightDetailsComponent([
  SegmentDesktopHeaderBluestone,
  null,
  SegmentDesktopHeaderCobalt,
]);

SegmentDesktopHeader.propTypes = {
  segment: PropTypes.object.isRequired,
};

export default SegmentDesktopHeader;
