import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import Segment from './';
import generateSegments from '../../../serverMocks/Itinerary';
import segmentResolver from './resolver';
import carrierKeys from '../Carrier/config/keys';
import durationKeys from '../Duration/config/keys';
import itineraryDetailLinkKeys from '../ItineraryDetailLink/config/keys';
import itineraryTitleKeys from '../ItineraryTitle/config/keys';
import stopoverQuantityKeys from '../StopoverQuantity/config/keys';
import summaryTitleKeys from '../SummaryTitle/config/keys';

const configKeys = [
  ...carrierKeys,
  ...durationKeys,
  ...itineraryDetailLinkKeys,
  ...itineraryTitleKeys,
  ...stopoverQuantityKeys,
  ...summaryTitleKeys,
];

const story = {
  title: 'Components/FlightSummary/Segment',
  component: Segment,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  const segments = generateSegments(
    1,
    args.withStopovers,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const segment = segmentResolver(segments[0]);
  return (
    <Segment
      segment={segment}
      index={args.index}
      isMultiTrip={args.isMultiTrip}
      detailsOpen={args.detailsOpen}
    />
  );
};

Default.args = {
  withStopovers: false,
  withTechnicalStopovers: false,
  withOperatingCarrier: true,
  index: 0,
  isMultiTrip: false,
  detailsOpen: false,
};
Default.argTypes = {
  baggageCondition: {
    control: {
      type: 'select',
      options: ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLINE'],
    },
    defaultValue: 'CHECKIN_INCLUDED',
  },
};

export default story;
