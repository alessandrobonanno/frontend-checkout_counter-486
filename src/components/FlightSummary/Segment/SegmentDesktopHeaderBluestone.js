import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'prisma-design-system';
import SegmentTrip from '../SegmentTrip';
import SegmentDuration from '../SegmentDuration';
import BaggageCondition from '../BaggageCondition';
import DestinationArrivalComponent from './DestinationArrivalComponent';

const SegmentDesktopHeaderBluestone = ({ segment }) => {
  const {
    transportTypes,
    departureDate,
    arrivalDate,
    departureTerminal,
    arrivalTerminal,
    carriers,
    duration,
    stops,
    departure,
    destination,
    baggageCondition,
  } = segment;

  return (
    <Box px={0}>
      <Flex
        justifyContent={'space-between'}
        alignItems={'flex-start'}
        mb={5}
        mt={0}
      >
        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          flexGrow={1}
          width={'25%'}
        >
          <DestinationArrivalComponent
            departureDate={departureDate}
            deparArriv={departure}
            terminal={departureTerminal}
          />
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'stretch'}
          flexGrow={1}
          width={'25%'}
          ml={5}
          mr={5}
          mt={5}
        >
          <SegmentTrip
            transportTypes={transportTypes}
            duration={duration}
            numberOfStops={stops.length}
          />
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          flexGrow={1}
          width={'25%'}
        >
          <DestinationArrivalComponent
            departureDate={departureDate}
            arrivalDate={arrivalDate}
            deparArriv={destination}
            terminal={arrivalTerminal}
          />
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          flexGrow={1}
          width={'25%'}
        >
          <SegmentDuration duration={duration} />
          <BaggageCondition
            baggageCondition={baggageCondition}
            carriers={carriers}
          />
        </Flex>
      </Flex>
    </Box>
  );
};

SegmentDesktopHeaderBluestone.propTypes = {
  segment: PropTypes.object.isRequired,
};

export default SegmentDesktopHeaderBluestone;
