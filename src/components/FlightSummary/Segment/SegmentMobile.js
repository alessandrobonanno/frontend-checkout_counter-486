import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'prisma-design-system';
import Carrier from '../Carrier';
import SegmentDate from '../SegmentDate';
import DepartureTime from '../DepartureTime';
import DestinationTime from '../DestinationTime';
import SegmentTrip from '../SegmentTrip';
import SegmentLocation from '../SegmentLocation';

const SegmentMobile = ({ segment }) => {
  const {
    transportTypes,
    departureDate,
    arrivalDate,
    carriers,
    duration,
    stops,
    departure,
    destination,
  } = segment;
  return (
    <>
      <Flex justifyContent={'space-between'} alignItems={'center'} mb={2}>
        <Carrier carriers={carriers} />
        <SegmentDate date={departureDate} />
      </Flex>

      <Flex justifyContent={'space-between'} alignItems={'flex-end'} mb={5}>
        <Flex flexDirection={'column'} alignItems={'flex-start'} flexGrow={1}>
          <DepartureTime departureDate={departureDate} />
          <SegmentLocation
            name={departure.name}
            iata={departure.iata}
            locationType={departure.locationType}
            cityName={departure.cityName}
            type="departure"
          />
        </Flex>

        <SegmentTrip
          transportTypes={transportTypes}
          duration={duration}
          numberOfStops={stops.length}
        />

        <Flex flexDirection={'column'} alignItems={'flex-end'} flexGrow={1}>
          <DestinationTime
            departureDate={departureDate}
            arrivalDate={arrivalDate}
          />
          <SegmentLocation
            name={destination.name}
            iata={destination.iata}
            locationType={destination.locationType}
            cityName={destination.cityName}
            type="destination"
          />
        </Flex>
      </Flex>
    </>
  );
};

SegmentMobile.propTypes = {
  segment: PropTypes.object.isRequired,
};

export default SegmentMobile;
