import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, AbsoluteCentered } from 'prisma-design-system';
import SegmentTrip from '../SegmentTrip';
import BaggageCondition from '../BaggageCondition';

import {
  DestinationArrivalComponentHeader,
  DestinationArrivalComponentContent,
} from './DestinationArrivalComponent';

const SegmentDesktopHeaderCobalt = ({ segment }) => {
  const {
    transportTypes,
    departureDate,
    arrivalDate,
    departureTerminal,
    arrivalTerminal,
    carriers,
    duration,
    stops,
    departure,
    destination,
    baggageCondition,
  } = segment;

  return (
    <Box px={5}>
      <Flex justifyContent={'space-between'} alignItems={'flex-start'} mt={5}>
        <Flex flexDirection={'column'} alignItems={'flex-start'}>
          <DestinationArrivalComponentHeader
            departureDate={departureDate}
            arrivalDate={undefined}
            deparArriv={departure}
          />
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'stretch'}
          flexGrow={1}
          width={'25%'}
          ml={8}
          mr={8}
          mt={4}
        >
          <SegmentTrip
            transportTypes={transportTypes}
            duration={duration}
            numberOfStops={stops.length}
          />

          <Flex justifyContent="center">
            <Box position="relative">
              <AbsoluteCentered>
                <BaggageCondition
                  baggageCondition={baggageCondition}
                  carriers={carriers}
                />
              </AbsoluteCentered>
            </Box>
          </Flex>
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          minWidth={5}
          maxWidth={5}
        >
          <DestinationArrivalComponentHeader
            departureDate={departureDate}
            arrivalDate={arrivalDate}
            deparArriv={destination}
          />
        </Flex>
      </Flex>

      {/* content */}
      <Flex justifyContent={'space-between'} alignItems={'flex-start'} mb={5}>
        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          flexGrow={1}
          maxWidth={'170px'}
        >
          <DestinationArrivalComponentContent
            departureDate={departureDate}
            arrivalDate={undefined}
            deparArriv={departure}
            terminal={departureTerminal}
          />
        </Flex>

        <Flex
          flexDirection={'column'}
          alignItems={'flex-start'}
          minWidth={5}
          maxWidth={5}
        >
          <DestinationArrivalComponentContent
            departureDate={departureDate}
            arrivalDate={arrivalDate}
            deparArriv={destination}
            terminal={arrivalTerminal}
          />
        </Flex>
      </Flex>
    </Box>
  );
};

SegmentDesktopHeaderCobalt.propTypes = {
  segment: PropTypes.object.isRequired,
};

export default SegmentDesktopHeaderCobalt;
