import calculateMinutesDuration from '../../../clientState/calculateMinutesDuration';
import sectionResolver from '../Section/resolver';

const nodeTypes = {
  PLANE: 'airport',
  TRAIN: 'station',
};

const calculateLocationChangeType = (
  destination,
  departure,
  arrivalTerminal,
  departureTerminal,
  transportType
) => {
  if (destination.id !== departure.id) {
    return nodeTypes[transportType];
  } else if (arrivalTerminal !== departureTerminal) {
    return 'terminal';
  }
  return 'none';
};

const hasChangeOfCountry = (previousSection, nextSection) => {
  return (
    previousSection.departure.countryName !== nextSection.departure.countryName
  );
};

const calculateTransportTypeChange = (
  previousTransportType,
  nextTransportType
) => previousTransportType !== nextTransportType;

const calculateSelfTransfer = (transportTypeChange, locationChangeType) =>
  transportTypeChange ||
  locationChangeType === 'airport' ||
  locationChangeType === 'station';

const createStopover = (previousSection, nextSection) => {
  const {
    destination,
    arrivalDate,
    arrivalTerminal,
    carrier,
    insuranceOffer,
    transportType: previousTransportType,
    isHub: previousIsHub,
  } = previousSection;
  const {
    departure,
    departureTerminal,
    departureDate,
    transportType,
    isHub: nextIsHub,
  } = nextSection;
  const transportTypeChange = calculateTransportTypeChange(
    previousTransportType,
    transportType
  );
  const locationChangeType = calculateLocationChangeType(
    destination,
    departure,
    arrivalTerminal,
    departureTerminal,
    transportType
  );
  const isVin = nextIsHub && previousIsHub;
  return {
    __typename: 'Stopover',
    duration: calculateMinutesDuration(departureDate, arrivalDate),
    transportType,
    transportTypeChange,
    locationChangeType,
    carrierChange: carrier.id !== nextSection.carrier.id,
    insuranceOffer: insuranceOffer || {
      policy: isVin ? 'ODIGEO_GUARANTEE' : 'CARRIER_GUARANTEE',
      carrier,
    },
    vinRestriction: {
      enabled: !!(hasChangeOfCountry(previousSection, nextSection) && isVin),
      isVin,
      country: departure.countryName,
    },
    selfTransfer: calculateSelfTransfer(
      transportTypeChange,
      locationChangeType
    ),
    changeCity: destination.cityName,
  };
};

const MAX_DISTINCT_CARRIERS = 3;

export default function segmentResolver(segment) {
  const { sections } = segment;
  const stops = sections.slice(0, -1).map((previousSection, index) => {
    const nextSection = sections[index + 1];
    return createStopover(previousSection, nextSection);
  });
  const carriers = Array.from(
    sections
      .reduce((carriers, { carrier }) => {
        return carriers.set(carrier.id, carrier);
      }, new Map())
      .values()
  ).slice(0, MAX_DISTINCT_CARRIERS);
  const firstSection = sections[0];
  const lastSection = sections[sections.length - 1];
  return {
    ...segment,
    carriers,
    departureDate: firstSection.departureDate,
    arrivalDate: lastSection.arrivalDate,
    departure: firstSection.departure,
    destination: lastSection.destination,
    duration: segment.duration,
    sections: sections.map((section, idx) =>
      sectionResolver(section, idx === sections.length - 1, idx === 0)
    ),
    stops,
    flightCode: firstSection.flightCode,
    departureTerminal: firstSection.departureTerminal,
    arrivalTerminal: lastSection.arrivalTerminal,
  };
}
