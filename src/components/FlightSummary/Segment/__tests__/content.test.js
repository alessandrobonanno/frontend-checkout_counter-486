import React from 'react';
import { screen, render } from '@testing-library/react';
import Segment from '../';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';
import generateSegments from '../../../../serverMocks/Itinerary';
import segmentResolver from '../../Segment/resolver';

const TOURIST_LABEL = 'Economy label';
const PREMIUM_ECONOMY_LABEL = 'Premium economy label';
const FIRST_LABEL = 'First label';
const ECONOMIC_DISCOUNTED_LABEL = 'Economy';
const BUSINESS_LABEL = 'Business';
const STOP_DURATION_LABEL = 'Stop duration label';
const DEPARTURE = 'Departure';
const RETURN = 'Return';
const SMALL_BAG = '1 small bag';
const HAND_LUGGAGE = 'Hand luggage';
const CHECK_IN_BAGGAGE = 'Check-in baggage';

const translations = {
  flightSummary: {
    section: {
      operatingBy: 'Operated by',
    },
    segment: {
      duration: "{{hours}}h {{minutes}}'",
      stops_none: 'direct',
      stops_some: '1 stop',
      stops_some_plural: '{{count}} stops',
      baggage: {
        smallBaggage: SMALL_BAG,
        checkInBaggage: CHECK_IN_BAGGAGE,
        handBaggage: HAND_LUGGAGE,
      },
    },
    cabinClass: {
      TOURIST: TOURIST_LABEL,
      PREMIUM_ECONOMY: PREMIUM_ECONOMY_LABEL,
      FIRST: FIRST_LABEL,
      ECONOMIC_DISCOUNTED: ECONOMIC_DISCOUNTED_LABEL,
      BUSINESS: BUSINESS_LABEL,
    },
    stop: {
      duration: STOP_DURATION_LABEL,
    },
  },
  summary: {
    segment: {
      departure: DEPARTURE,
      return: RETURN,
      name: {
        multiple: '{{multipleText}} flight',
        multiples0: '1st',
      },
    },
  },
};

describe('Segment', () => {
  test('Segment desktop content', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      false,
      'CHECKIN_INCLUDED',
      false,
      false
    );
    const segment = segmentResolver(segments[0]);
    render(
      <TestingProviders translations={translations}>
        <Segment segment={segment} index={0} />
      </TestingProviders>
    );

    expect(await screen.findByText(new RegExp(DEPARTURE, 'i'))).toBeVisible();
    expect(await screen.findAllByText('Ryanair AD2654')).toHaveLength(2);
    expect(await screen.findAllByText('06:30')).toHaveLength(2);
    expect(await screen.findAllByText('MAD')).toHaveLength(2);
    expect(await screen.findAllByText('09:45')).toHaveLength(2);
    expect(await screen.findAllByText('BCN')).toHaveLength(2);
    expect(await screen.findAllByText("4h 10'")).toHaveLength(3);
    expect(await screen.findAllByText('29 Apr')).toHaveLength(2);
    expect(
      await screen.findAllByText(
        'Adolfo Suárez Madrid - Barajas, T1, Madrid (Spain)'
      )
    ).toHaveLength(2);
    expect(
      await screen.findAllByText('El Prat, T3, Barcelona (Spain)')
    ).toHaveLength(2);
    expect(await screen.findByText('direct')).toBeVisible();
    expect(await screen.findByText(CHECK_IN_BAGGAGE)).toBeVisible();
  });
  test('Segment multitrip desktop content', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      false,
      'CHECKIN_INCLUDED',
      false,
      false
    );
    const segment = segmentResolver(segments[0]);
    render(
      <TestingProviders translations={translations}>
        <Segment segment={segment} index={0} isMultiTrip={true} />
      </TestingProviders>
    );

    expect(await screen.findByText(/1st flight/i)).toBeVisible();
  });
});
