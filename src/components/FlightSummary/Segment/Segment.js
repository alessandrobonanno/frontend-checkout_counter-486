import React from 'react';
import PropTypes from 'prop-types';
import { useMediaQueries } from 'prisma-design-system';
import SegmentDesktop from './SegmentDesktop';
import SegmentMobile from './SegmentMobile';

const Segment = (props) => {
  const { md } = useMediaQueries();
  const Component = md ? SegmentDesktop : SegmentMobile;
  return <Component {...props} />;
};

Segment.propTypes = {
  segment: PropTypes.object.isRequired,
  index: PropTypes.number,
  hasFlightSummaryHeader: PropTypes.bool,
};

export default Segment;
