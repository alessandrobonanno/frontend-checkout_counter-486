import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Hr, Box, Collapsible } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';
import Carrier from '../Carrier';
import SegmentDate from '../SegmentDate';
import SectionList from '../SectionList';
import SegmentDesktopHeader from './SegmentDesktopHeader';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentDesktopVersioned = ({
  index,
  segment,
  detailsOpen,
  isMultiTrip,
  placement,
  hasFlightSummaryHeader,
  versionConfigs,
}) => {
  const { t } = useTranslation();
  const {
    outerBorderTopRadius,
    outerPadding,
    bgColor,
    firstLevelMarginBottoms,
    innerCollapsibleBoxVerticalPadding,
    innerCollapsibleBoxHorizontalPadding,
    innerCollapsibleBoxBottomMargin,
    innerCollapsibleBoxBgColor,
    segmentDatedesktopFontColor,
  } = versionConfigs;
  const { departureDate, flightCode, carriers, stops, sections } = segment;
  const isPayment = placement === 'payment';
  const directionText = isMultiTrip
    ? t('summary.segment.name.multiple', {
        multipleText: t(`summary.segment.name.multiples${index}`),
      })
    : index === 0
    ? t('summary.segment.departure')
    : t('summary.segment.return');

  const borderTopRadiusInCobalt =
    hasFlightSummaryHeader && index === 0 ? outerBorderTopRadius : undefined;

  const CobaltDesktopFlightdetailsHr = cobaltDesktopFlightDetailsComponent([
    null,
    null,
    () => (
      <Box px={5} mb={0}>
        <Hr type="dashed" />
      </Box>
    ),
  ]);

  return (
    <>
      <Box
        backgroundColor={bgColor}
        p={outerPadding}
        borderTopLeftRadius={borderTopRadiusInCobalt}
        borderTopRightRadius={borderTopRadiusInCobalt}
      >
        <Flex
          justifyContent={'space-between'}
          alignItems={'center'}
          mb={firstLevelMarginBottoms}
        >
          <Carrier
            carriers={carriers}
            directionText={directionText}
            flightCode={flightCode}
          />
          <SegmentDate
            date={departureDate}
            desktopFontColor={segmentDatedesktopFontColor}
            cobaltColor="neutrals.90"
            cobaltType="base"
          />
        </Flex>
      </Box>
      {isPayment && (
        <Box p={0} mb={firstLevelMarginBottoms}>
          <SectionList sections={sections} stops={stops} />
        </Box>
      )}
      {!isPayment && (
        <>
          <SegmentDesktopHeader segment={segment} />

          <Collapsible open={detailsOpen}>
            <Box
              py={innerCollapsibleBoxVerticalPadding}
              px={innerCollapsibleBoxHorizontalPadding}
              mb={innerCollapsibleBoxBottomMargin}
              backgroundColor={innerCollapsibleBoxBgColor}
            >
              <CobaltDesktopFlightdetailsHr />
              <SectionList sections={sections} stops={stops} />
            </Box>
          </Collapsible>
        </>
      )}
    </>
  );
};

const SegmentDesktop = cobaltDesktopThemedComponentVersion(
  SegmentDesktopVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      outerBorderTopRadius: undefined,
      bgColor: '',
      outerPadding: 0,
      firstLevelMarginBottoms: 5,
      innerCollapsibleBoxVerticalPadding: 4,
      innerCollapsibleBoxHorizontalPadding: 2,
      innerCollapsibleBoxBottomMargin: 2,
      innerCollapsibleBoxBgColor: 'neutrals.7',
      segmentDatedesktopFontColor: 'neutrals.2',
    },
    C: {
      outerBorderTopRadius: '8px',
      bgColor: 'brandPrimary.10',
      outerPadding: 5,
      firstLevelMarginBottoms: 0,
      innerCollapsibleBoxVerticalPadding: 0,
      innerCollapsibleBoxHorizontalPadding: 0,
      innerCollapsibleBoxBottomMargin: 0,
      innerCollapsibleBoxBgColor: '',
      segmentDatedesktopFontColor: 'neutrals.1',
    },
  }
);

SegmentDesktop.propTypes = {
  segment: PropTypes.object.isRequired,
  index: PropTypes.number,
  detailsOpen: PropTypes.bool,
  isMultiTrip: PropTypes.bool,
  placement: PropTypes.string,
};

export default SegmentDesktop;
