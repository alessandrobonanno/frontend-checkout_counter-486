import React from 'react';
import { Box, Flex } from 'prisma-design-system';
import ItineraryTitle from '../ItineraryTitle';
import ItineraryDetailLink from '../ItineraryDetailLink';
import SummaryTitle from '../SummaryTitle';

const FlightSummaryTitleMobile = ({ legs, segments }) => {
  const getArrivalCityName = (legs) =>
    legs[0].segments[0].sections[legs[0].segments[0].sections.length - 1]
      .destination.cityName;
  return (
    <>
      <Box mb={5} mt={4}>
        <SummaryTitle />
      </Box>
      <Flex justifyContent={'space-between'} alignItems={'center'} mb={5}>
        <ItineraryTitle
          cityName={getArrivalCityName(legs)}
          segmentNumber={segments.length}
        />
        <ItineraryDetailLink />
      </Flex>
    </>
  );
};

export default FlightSummaryTitleMobile;
