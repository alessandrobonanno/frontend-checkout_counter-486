import React from 'react';
import { useMediaQueries } from 'prisma-design-system';
import PropTypes from 'prop-types';
import FlightSummaryTitleMobile from './FlightSummaryTitleMobile';
import FlightSummaryTitleDesktop from './FlightSummaryTitleDesktop';

const FlightSummaryTitle = ({ legs, segments }) => {
  const { md } = useMediaQueries();
  const FlightSummaryTitleComponent = md
    ? FlightSummaryTitleDesktop
    : FlightSummaryTitleMobile;
  return <FlightSummaryTitleComponent legs={legs} segments={segments} />;
};

FlightSummaryTitle.propTypes = {
  legs: PropTypes.array,
  segments: PropTypes.array,
};

export default FlightSummaryTitle;
