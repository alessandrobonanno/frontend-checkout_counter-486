import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import FlightSummaryTitle from './';
import selector from '../../../../.storybook/selectors/shoopingCartMockSelector';

const story = {
  title: 'Components/FlightSummary/FlightSummaryTitle',
  component: FlightSummaryTitle,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
  mockedProps: selector,
};

const shopingCart = selector().mocks.ShoppingCart();
const legs = shopingCart.itinerary.legs;
const segments = [legs[0].segments[0], legs[1].segments[0]];

export const Default = (args) => {
  return <FlightSummaryTitle {...args} />;
};

Default.args = {
  legs: legs,
  segments: segments,
};

export default story;
