import React from 'react';
import {
  FlightRightIcon,
  TrainFrontIcon,
  Flex,
  Text,
  Box,
} from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const icons = {
  PLANE: <FlightRightIcon key={1} color="flightProductColor" size="medium" />,
  TRAIN: <TrainFrontIcon key={0} color="trainProductColor" size="medium" />,
};

const keys = {
  PLANE: 'flightSummary.header.flight',
  TRAIN: 'flightSummary.header.train',
};

const getTransportKeys = (segments) =>
  Object.keys(keys).filter((key) =>
    segments.some(
      (segment) =>
        segment.transportTypes &&
        segment.transportTypes.length &&
        segment.transportTypes.indexOf(key) > -1
    )
  );

const FlightSummaryTitleDesktop = ({ segments }) => {
  const { t } = useTranslation();
  const transportKeys = getTransportKeys(segments);

  return (
    <Flex mb={4} justifyContent="space-between" alignItems="center">
      <Flex alignItems="center">
        <Text fontSize="heading.0" color="neutrals.0">
          {t('flightSummary.header.title')}
        </Text>
        <Box ml={1}>
          <Text capitalize mx={1} fontSize="heading.0" color="neutrals.2">
            {transportKeys.map((key) => t(keys[key])).join(' & ')}
          </Text>
        </Box>
      </Flex>
      <Flex>
        {transportKeys.map((key, index) => (
          <Flex
            key={index}
            data-testid={`${key.toLowerCase()}-icon`}
            px={2}
            py={2}
            borderRadius={5}
            bgColor={'bgSecondary'}
            ml={2}
          >
            {icons[key]}
          </Flex>
        ))}
      </Flex>
    </Flex>
  );
};

export default FlightSummaryTitleDesktop;
