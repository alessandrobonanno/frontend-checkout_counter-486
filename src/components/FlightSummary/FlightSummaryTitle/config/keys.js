const translations = [
  {
    inputPath: ['flightdetails', 'header.title'],
    outputPath: ['flightSummary', 'header', 'title'],
    scope: ['?ARABIC_SITES', '?ED_ar-QA'],
  },
  {
    inputPath: ['flightdetails', 'header.flight'],
    outputPath: ['flightSummary', 'header', 'flight'],
    scope: ['?ARABIC_SITES', '?ED_ar-QA'],
  },
  {
    inputPath: ['flightdetails', 'header.train'],
    outputPath: ['flightSummary', 'header', 'train'],
    scope: ['?ARABIC_SITES', '?ED_ar-QA'],
  },
];

module.exports = translations;
