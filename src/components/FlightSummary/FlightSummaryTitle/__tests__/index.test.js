import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../testUtils';
import { setViewport } from '../../../../testUtils/responsiveness';
import FlightSummaryTitle from '../FlightSummaryTitle';

const translations = {
  flightSummary: {
    header: {
      title: 'Itinerary',
      flight: 'Flight',
      train: 'Train',
    },
    summary: {
      title: 'Your trip summary',
    },
    itinerary: {
      toCity: '{{cityName}} itinerary',
    },
  },
};

const segments = [
  {
    id: '0',
    seats: null,
    duration: 120,
    transportTypes: ['PLANE'],
    carrier: {
      id: 'AF',
      name: 'Air France',
    },
    sections: [
      {
        id: '0',
        flightCode: 'AF1449',
        departureDate: '2021-06-29T06:15:00+02:00',
        departureTerminal: null,
        departure: {
          id: '157',
          cityName: 'Barcelone',
          name: 'El Prat',
          cityIata: 'BCN',
          iata: 'BCN',
          locationType: 'AIRPORT',
          countryName: 'Espagne',
        },
        arrivalDate: '2021-06-29T08:15:00+02:00',
        destination: {
          id: '330',
          cityName: 'Paris',
          name: 'Charles De Gaulle',
          cityIata: 'PAR',
          iata: 'CDG',
          locationType: 'AIRPORT',
          countryName: 'France',
        },
        arrivalTerminal: null,
        transportType: 'PLANE',
        carrier: {
          id: 'AF',
          name: 'Air France',
        },
        operatingCarrier: null,
        cabinClass: 'TOURIST',
        baggageAllowance: null,
        technicalStops: [],
      },
    ],
  },
  {
    id: '1',
    seats: null,
    duration: 110,
    transportTypes: ['PLANE'],
    carrier: {
      id: 'AF',
      name: 'Air France',
    },
    sections: [
      {
        id: 1,
        flightCode: 'AF1448',
        departureDate: '2021-07-03T21:05:00+02:00',
        departureTerminal: null,
        departure: {
          id: '330',
          cityName: 'Paris',
          name: 'Charles De Gaulle',
          cityIata: 'PAR',
          iata: 'CDG',
          locationType: 'AIRPORT',
          countryName: 'France',
        },
        arrivalDate: '2021-07-03T22:55:00+02:00',
        destination: {
          id: '157',
          cityName: 'Barcelone',
          name: 'El Prat',
          cityIata: 'BCN',
          iata: 'BCN',
          locationType: 'AIRPORT',
          countryName: 'Espagne',
        },
        arrivalTerminal: null,
        transportType: 'PLANE',
        carrier: {
          id: 'AF',
          name: 'Air France',
        },
        operatingCarrier: null,
        cabinClass: 'TOURIST',
        baggageAllowance: null,
        technicalStops: [],
      },
    ],
  },
];

const legs = [{ segments: [segments[0]] }, { segments: [segments[1]] }];

describe('FlightSummaryTitle', () => {
  test('Mobile content', async () => {
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryTitle legs={legs} segments={segments} />
      </TestingProviders>
    );

    expect(await screen.findByText('Your trip summary')).toBeVisible();
    expect(await screen.findByText('Paris itinerary')).toBeVisible();
  });
  test('Desktop content', async () => {
    setViewport({ width: 1200 });
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryTitle legs={legs} segments={segments} />
      </TestingProviders>
    );

    expect(await screen.findByText('Itinerary')).toBeVisible();
    expect(await screen.findByText('Flight')).toBeVisible();
  });
});
