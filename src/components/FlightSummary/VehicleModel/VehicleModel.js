import React from 'react';

import {
  FlightRightIcon,
  TrainFrontIcon,
  Text,
  Flex,
  Box,
  useDirection,
} from 'prisma-design-system';
import PropTypes from 'prop-types';

const isTrain = (transportType) => transportType === 'TRAIN';

const TransportTypeIcon = ({ transportType, color }) => {
  const Icon = isTrain(transportType) ? TrainFrontIcon : FlightRightIcon;
  return <Icon size="small" color={color} />;
};

const VehicleModel = ({ model, transportType }) => {
  const isRtl = useDirection();
  // TODO: can be description a hardcoded text 'TRAIN' regardless the language?
  const description = isTrain(transportType) ? 'TRAIN' : model;
  if (!description) return null;

  return (
    <Box pr={!isRtl ? 2 : 0} pl={isRtl ? 2 : 0}>
      <Flex alignItems="center">
        <Box mr={1}>
          <TransportTypeIcon transportType={transportType} color="neutrals.2" />
        </Box>
        <Text color="neutrals.2" fontSize="body.1">
          {description}
        </Text>
      </Flex>
    </Box>
  );
};

VehicleModel.propTypes = {
  transportType: PropTypes.string.isRequired,
  model: PropTypes.string,
};

export default VehicleModel;
