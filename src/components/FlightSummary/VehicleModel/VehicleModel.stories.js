import React from 'react';
import VehicleModel from './VehicleModel';

const story = {
  title: 'Components/FlightSummary/VehicleModel',
  component: VehicleModel,
};

export const Default = (args) => {
  return <VehicleModel {...args} />;
};
Default.args = {
  model: '320',
};
Default.argTypes = {
  transportType: {
    control: { type: 'select', options: ['PLANE', 'TRAIN'] },
    defaultValue: 'PLANE',
  },
};

export default story;
