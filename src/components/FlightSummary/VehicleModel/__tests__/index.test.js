import React from 'react';
import { screen, render } from '@testing-library/react';

import VehicleModel from '../VehicleModel';
import { TestingProviders } from '../../../../testUtils';

const PLANE = 'PLANE';
const TRAIN = 'TRAIN';

const SENTINEL = 'Sentiner_label';
const Sentinel = () => SENTINEL;

describe('VehicleModel', () => {
  test('PLANE - If model does not exist component is not shown.', async () => {
    render(
      <TestingProviders>
        <VehicleModel transportType={PLANE} />
        <Sentinel />
      </TestingProviders>
    );

    await screen.findByText(SENTINEL);
    expect(screen.queryByText('undefined', { exact: false })).toBeNull();
    expect(screen.queryByText('TRAIN', { exact: false })).toBeNull();
  });
  test('PLANE - If model exists, it shows the vehicle model.', async () => {
    const model = '320';
    render(<VehicleModel model={model} transportType={PLANE} />);

    expect(await screen.findByText(model)).toBeVisible();
  });
  test('TRAIN - it shows TRAIN.', async () => {
    render(
      <TestingProviders>
        <VehicleModel transportType={TRAIN} />
      </TestingProviders>
    );

    expect(await screen.findByText('TRAIN')).toBeVisible();
  });
  test('TRAIN - If model exists, it shows TRAIN.', async () => {
    const model = '320';
    render(<VehicleModel model={model} transportType={TRAIN} />);

    expect(await screen.findByText('TRAIN')).toBeVisible();
  });
});
