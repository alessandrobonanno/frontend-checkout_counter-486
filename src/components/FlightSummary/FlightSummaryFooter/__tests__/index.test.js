import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../testUtils';
import FlightSummaryFooter from '..';
import generateSegments from '../../../../serverMocks/Itinerary';
import segmentResolver from '../../Segment/resolver';

const SHOW_DETAILS = 'Show details';
const HIDE_DETAILS = 'Hide details';

const translations = {
  flightSummary: {
    footer: {
      showDetails: SHOW_DETAILS,
      hideDetails: HIDE_DETAILS,
    },
  },
};
const segments = generateSegments(
  1,
  true,
  false,
  'CHECKIN_INCLUDED',
  false,
  false
);
const segmentsParsed = segments.map(segmentResolver);

describe('FlightSummaryFooter', () => {
  test('Summary footer show', async () => {
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryFooter open={false} segments={segmentsParsed} />
      </TestingProviders>
    );

    expect(await screen.findByText(SHOW_DETAILS)).toBeVisible();
  });
  test('Summary footer hide', async () => {
    render(
      <TestingProviders translations={translations}>
        <FlightSummaryFooter open={true} segments={segmentsParsed} />
      </TestingProviders>
    );

    expect(await screen.findByText(HIDE_DETAILS)).toBeVisible();
  });
});
