import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import FlightSummaryFooter from './';
import generateSegments from '../../../serverMocks/Itinerary';
import segmentResolver from '../Segment/resolver';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/FlightSummaryFooter',
  component: FlightSummaryFooter,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

const segments = generateSegments(
  1,
  true,
  false,
  'CHECKIN_INCLUDED',
  false,
  false
);
const segmentsParsed = segments.map(segmentResolver);

export const Default = (args) => {
  return <FlightSummaryFooter {...args} segments={segmentsParsed} />;
};

Default.decorators = [withKnobs];
Default.args = {
  open: false,
  hasChangeOfCountry: false,
};

export default story;
