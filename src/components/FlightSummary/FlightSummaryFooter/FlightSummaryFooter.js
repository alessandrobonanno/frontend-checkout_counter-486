import React from 'react';
import PropTypes from 'prop-types';
import {
  Link,
  Body,
  Hr,
  Box,
  Flex,
  ArrowDownIcon,
  ArrowUpIcon,
  InformationIcon,
} from 'prisma-design-system';
import { useTranslation, useTracking } from '@frontend-shell/react-hooks';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const hasStops = (segments) => {
  return segments
    .map(({ sections }) => {
      const hasMultipleTrips = sections
        .map(({ trips }) => {
          return trips.length;
        })
        .some((stops) => stops > 1);
      return sections.length || hasMultipleTrips;
    })
    .some((stops) => stops > 1);
};

const FlightSummaryFooterVersioned = ({
  open,
  setOpen,
  segments,
  hasChangeOfCountry,
  versionConfigs,
}) => {
  const { t } = useTranslation();
  const { trackEvent } = useTracking();
  const {
    outerHorizontalPadding,
    hrType,
    contentFlexJustifyContent,
    isLinkUnderlined,
    linkHorizontalMargin,
    linkBodyType,
  } = versionConfigs;

  const handleClick = () => {
    trackEvent({
      action: 'summary_widget',
      label: open ? 'summary_details_close' : 'summary_details_open',
    });
    setOpen(!open);
  };

  const key = open
    ? 'flightSummary.footer.hideDetails'
    : 'flightSummary.footer.showDetails';
  const Icon = open ? ArrowUpIcon : ArrowDownIcon;
  const hasToShowFooter = hasStops(segments);
  if (!hasToShowFooter) return null;

  return (
    <Box px={outerHorizontalPadding} onClick={handleClick}>
      <Hr type={hrType} />
      <Flex
        my={4}
        justifyContent={
          (hasChangeOfCountry && 'space-between') || contentFlexJustifyContent
        }
        alignItems="center"
      >
        {hasChangeOfCountry && (
          <Flex alignItems="center" justifyContent="left">
            <Box mr={1}>
              <InformationIcon mr={1} color="informative.default" size="tiny" />
            </Box>
            <Body type="small" weight="normal" color={'informative.default'}>
              {t('flightSummary.stopInfo.vinmessage.title')}
            </Body>
          </Flex>
        )}
        <Flex mx={linkHorizontalMargin} alignItems="stretch">
          <Link isUnderlined={isLinkUnderlined}>
            <Body type={linkBodyType}>{t(key)}</Body>
          </Link>
          <Flex>
            <Box ml={1}>
              <Icon size="tiny" color="brandPrimary.1" />
            </Box>
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

const FlightSummaryFooter = cobaltDesktopThemedComponentVersion(
  FlightSummaryFooterVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      outerHorizontalPadding: 0,
      hrType: 'solid',
      contentFlexJustifyContent: 'space-between',
      isLinkUnderlined: true,
      linkHorizontalMargin: 0,
      linkBodyType: 'base',
    },
    C: {
      outerHorizontalPadding: 5,
      hrType: 'dashed',
      contentFlexJustifyContent: 'flex-end',
      isLinkUnderlined: false,
      linkHorizontalMargin: 1,
      linkBodyType: 'small',
    },
  }
);

FlightSummaryFooter.propTypes = {
  open: PropTypes.bool,
  hasChangeOfCountry: PropTypes.bool,
  setOpen: PropTypes.func,
  segments: PropTypes.array,
};

export default FlightSummaryFooter;
