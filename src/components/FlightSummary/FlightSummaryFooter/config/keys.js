const translations = [
  {
    inputPath: ['collapsibleFlightSummary', 'showDetails'],
    outputPath: ['flightSummary', 'footer', 'showDetails'],
  },
  {
    inputPath: ['collapsibleFlightSummary', 'hideDetails'],
    outputPath: ['flightSummary', 'footer', 'hideDetails'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.vinmessage.title'],
    outputPath: ['flightSummary', 'stopInfo', 'vinmessage', 'title'],
  },
];

module.exports = translations;
