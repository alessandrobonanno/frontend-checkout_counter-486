const translations = [
  {
    inputPath: ['itineraryDetails', 'stopInfo.stopduration'],
    outputPath: ['flightSummary', 'stop', 'duration'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['itineraryDetails', 'stopInfo.technical'],
    outputPath: ['flightSummary', 'stop', 'technical'],
    scope: ['?ARABIC_SITES'],
  },
];

module.exports = translations;
