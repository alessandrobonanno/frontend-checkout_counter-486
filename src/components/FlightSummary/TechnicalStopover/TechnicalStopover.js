import React from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Flex,
  Body,
  DecorationLine,
  TimeIcon,
  useDirection,
} from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

import Duration from '../Duration';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const TechnicalStopoverVersioned = ({ duration, versionConfigs }) => {
  const { t } = useTranslation();
  const isRtl = useDirection();

  const {
    decorationLine,
    flightDurationColor,
    descriptionTextColor,
    bodyType,
    bgColor,
    fontSize,
    decorationLineMargin,
    outerPaddingLeft,
    outerPaddingVertical,
    innerColumnPaddingTop,
    innerColumnPaddingBottom,
  } = versionConfigs;

  return (
    <Flex bgColor={bgColor} pl={outerPaddingLeft} py={outerPaddingVertical}>
      <DecorationLine
        decorationType={decorationLine.decorationType}
        lineType="dashed"
        theme={decorationLine.decorationLineTheme}
        lineBottom={decorationLine.lineBottom}
        width={decorationLine.width}
      />
      <Flex
        flexDirection="column"
        pl={!isRtl ? decorationLineMargin : 0}
        pr={isRtl ? decorationLineMargin : 0}
        pt={innerColumnPaddingTop}
        pb={innerColumnPaddingBottom}
      >
        <Box mb={1}>
          <Body
            fontWeight="medium"
            color={flightDurationColor}
            fontSize="body.1"
            type={bodyType}
          >
            {t('flightSummary.stop.duration')}:{' '}
            <Duration durationInMinutes={duration} />
          </Body>
        </Box>
        <Flex alignItems="baseline">
          <TimeIcon size="tiny" color={descriptionTextColor} />

          <Box mx={1}>
            <Body
              color={descriptionTextColor}
              fontSize={fontSize}
              type={bodyType}
            >
              {t('flightSummary.stop.technical')}
            </Body>
          </Box>
        </Flex>
      </Flex>
    </Flex>
  );
};

const TechnicalStopover = cobaltDesktopThemedComponentVersion(
  TechnicalStopoverVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      bodyType: 'base',
      descriptionTextColor: 'neutrals.3',
      flightDurationColor: 'neutrals.0',
      bgColor: undefined,
      fontSize: 'body.0',
      decorationLineMargin: 1,
      decorationLine: {
        decorationLineTheme: 'dark',
        decorationType: 'circleDot',
        lineBottom: undefined,
        width: undefined,
      },
      outerPaddingLeft: 0,
      outerPaddingVertical: 0,
      innerColumnPaddingTop: 2,
      innerColumnPaddingBottom: 4,
    },
    C: {
      bodyType: 'small',
      descriptionTextColor: 'neutrals.70',
      flightDurationColor: 'neutrals.90',
      bgColor: 'neutral10',
      fontSize: 'bodies.small',
      decorationLineMargin: 5,
      decorationLine: {
        decorationLineTheme: 'primary',
        decorationType: 'disk',
        lineBottom: '-52px',
        width: 2,
      },
      outerPaddingLeft: 5,
      outerPaddingVertical: 5,
      innerColumnPaddingTop: 0,
      innerColumnPaddingBottom: 0,
    },
  }
);

TechnicalStopover.propTypes = {
  duration: PropTypes.number.isRequired,
};

export default TechnicalStopover;
