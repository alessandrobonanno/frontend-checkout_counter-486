import technicalStopoverResolver from '../resolver';
import { Gatwick, PalmaDeMallorca } from '../../../../serverMocks/Location';

describe('technicalStopoverResolver', () => {
  test('add the stopover duration in minutes', () => {
    const arrivalDate = '2025-04-10T14:50:00-02:00';
    const departureDate = '2025-04-10T16:05:00-02:00';
    const technicalStopover = {
      arrivalDate,
      departureDate,
      location: Gatwick(),
    };
    const extendedTechnicalStopover =
      technicalStopoverResolver(technicalStopover);

    expect(extendedTechnicalStopover).toStrictEqual({
      duration: 75,
      arrivalDate,
      departureDate,
      location: Gatwick(),
    });
  });

  test('duration difference takes care of time zone', () => {
    const arrivalDate = '2025-04-10T18:05:00+0100';
    const departureDate = '2025-04-10T21:00:00-0500';
    const technicalStopover = {
      arrivalDate,
      departureDate,
      location: PalmaDeMallorca(),
    };
    const extendedTechnicalStopover =
      technicalStopoverResolver(technicalStopover);

    expect(extendedTechnicalStopover).toStrictEqual({
      duration: 535,
      arrivalDate,
      departureDate,
      location: PalmaDeMallorca(),
    });
  });
});
