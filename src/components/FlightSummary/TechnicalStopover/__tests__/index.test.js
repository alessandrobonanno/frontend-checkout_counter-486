import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../testUtils';

import TechnicalStopover from '../';

const STOP_DURATION_LABEL = 'Stop duration label';

const translations = {
  flightSummary: {
    stop: {
      duration: STOP_DURATION_LABEL,
    },
    segment: {
      duration: `{{hours}}h {{minutes}}'`,
    },
  },
};

describe('SectionTechnicalStop', () => {
  test('Technical stop duration is visible', async () => {
    const duration = 180;

    render(
      <TestingProviders translations={translations} lang="en-GB">
        <TechnicalStopover duration={duration} />
      </TestingProviders>
    );

    expect(
      await screen.findByText(`${STOP_DURATION_LABEL}: 3h 00'`)
    ).toBeVisible();
  });
});
