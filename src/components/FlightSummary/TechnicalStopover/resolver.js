import calculateMinutesDuration from '../../../clientState/calculateMinutesDuration';

export default function technicalStopoverResolver(technicalStop) {
  const duration = calculateMinutesDuration(
    technicalStop.departureDate,
    technicalStop.arrivalDate
  );
  return {
    ...technicalStop,
    duration,
  };
}
