import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const StopoverQuantityVersioned = ({ quantity, versionConfigs }) => {
  const isDirect = quantity === 0;
  const { t } = useTranslation();
  const { fontSize, color } = versionConfigs;

  return (
    <Text color={color} fontSize={fontSize} ellipsis={true}>
      {t('flightSummary.segment.stops', {
        count: quantity,
        context: isDirect ? 'none' : 'some',
      })}
    </Text>
  );
};

const StopoverQuantity = cobaltDesktopThemedComponentVersion(
  StopoverQuantityVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      fontSize: 'body.1',
      color: 'neutrals.3',
    },
    C: {
      fontSize: 'body.2',
      color: 'neutrals.1',
    },
  }
);

StopoverQuantity.propTypes = {
  quantity: PropTypes.number.isRequired,
};

export default StopoverQuantity;
