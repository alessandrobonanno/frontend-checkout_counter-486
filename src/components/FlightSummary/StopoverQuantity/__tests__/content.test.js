import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import StopoverQuantity from '..';

const translations = {
  flightSummary: {
    segment: {
      stops_none: 'direct',
      stops_some: '1 stop',
      stops_some_plural: '{{count}} stops',
    },
  },
};

describe('StopoverQuantity', () => {
  test('StopoverQuantity', async () => {
    render(
      <TestingProviders translations={translations}>
        <StopoverQuantity quantity={1} />
      </TestingProviders>
    );

    expect(await screen.findByText('1 stop')).toBeVisible();
  });
  test('StopoverQuantity', async () => {
    render(
      <TestingProviders translations={translations}>
        <StopoverQuantity quantity={3} />
      </TestingProviders>
    );

    expect(await screen.findByText('3 stops')).toBeVisible();
  });
  test('No stops', async () => {
    render(
      <TestingProviders translations={translations}>
        <StopoverQuantity quantity={0} />
      </TestingProviders>
    );

    expect(await screen.findByText('direct')).toBeVisible();
  });
});
