import React from 'react';
import { useTranslation } from '@frontend-shell/react-hooks';
import {
  DrawerContent,
  Drawer,
  DrawerHeader,
  Text,
} from 'prisma-design-system';
import ExternalInsuranceContent from './ExternalInsuranceContent';
import OdigeoInsuranceContent from './OdigeoInsuranceContent';

const ConnectionInsuranceModal = ({ open, policy, handleClose }) => {
  const { t } = useTranslation();
  const header = (
    <Text fontSize="body.3" fontWeight="medium">
      {t('flightInsurance.missed.connection.title')}
    </Text>
  );

  return (
    <Drawer
      open={open}
      size="large"
      onClose={() => handleClose(false)}
      header={
        <DrawerHeader onClose={() => handleClose(false)} title={header} />
      }
      fixed
    >
      <DrawerContent>
        {policy === 'ODIGEO_GUARANTEE' && <OdigeoInsuranceContent />}
        {policy === 'EXTERNAL_INSURANCE' && <ExternalInsuranceContent />}
      </DrawerContent>
    </Drawer>
  );
};

export default ConnectionInsuranceModal;
