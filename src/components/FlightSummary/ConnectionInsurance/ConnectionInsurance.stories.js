import React from 'react';
import { carrierOptions } from 'decorators/selectCarrier';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import ConnectionInsurance from './';
import configKeys from './config/keys';

const story = {
  title: 'Component/FlightSummary/ConnectionInsurance',
  component: ConnectionInsurance,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <ConnectionInsurance {...args} />;
};
Default.argTypes = {
  policy: {
    control: {
      type: 'select',
      options: ['CARRIER_GUARANTEE', 'ODIGEO_GUARANTEE', 'EXTERNAL_INSURANCE'],
    },
    defaultValue: 'CARRIER_GUARANTEE',
  },
  carrier: {
    control: { type: 'select', options: carrierOptions },
    defaultValue: { id: 'UX', name: 'Air Europa' },
  },
};

export default story;
