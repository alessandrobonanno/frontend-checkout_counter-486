import React from 'react';
import { render, screen } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import { Vueling } from '../../../../serverMocks/Carrier';
import ConnectionInsurance from '..';
import userEvent from '@testing-library/user-event';

const CARRIER_INSURANCE_TEXT = 'Connection covered by {{ carrierName }}';
const EXTERNAL_INSURANCE_TEXT =
  'Connection covered by your {{ insuranceName }}';
const EDREAMS_CONNECTION_TITLE = 'eDreams Connection Guarantee';

const translations = {
  flightSummary: {
    stop: {
      missedConnection: CARRIER_INSURANCE_TEXT,
    },
  },
  trip: {
    stop: {
      insuranceHub: EXTERNAL_INSURANCE_TEXT,
    },
  },
  flightInsurance: {
    missed: {
      connection: {
        title: EDREAMS_CONNECTION_TITLE,
      },
    },
  },
};

describe('ConnectionInsurance', () => {
  test('CARRIER_GUARANTEE policy', async () => {
    const carrier = Vueling();

    render(
      <TestingProviders translations={translations}>
        <ConnectionInsurance policy="CARRIER_GUARANTEE" carrier={carrier} />
      </TestingProviders>
    );
    expect(
      await screen.findByText('Connection covered by Vueling')
    ).toBeVisible();
  });

  test('other policies policy', async () => {
    const carrier = Vueling();

    render(
      <TestingProviders translations={translations}>
        <ConnectionInsurance policy="CARRIER_GUARANTEE" carrier={carrier} />
      </TestingProviders>
    );
    expect(
      await screen.findByText('Connection covered by Vueling')
    ).toBeVisible();
  });
});

describe('Connection Insurance details Modal', () => {
  test('External Insurance shows content', async () => {
    render(
      <TestingProviders translations={translations}>
        <ConnectionInsurance policy="ODIGEO_GUARANTEE" />
      </TestingProviders>
    );

    const connectionTitle = await screen.findByText(
      'eDreams Connection Guarantee'
    );
    expect(connectionTitle).toBeVisible();
    userEvent.click(connectionTitle);
    expect(screen.getAllByText(EDREAMS_CONNECTION_TITLE)[0]).toBeVisible();
  });
});
