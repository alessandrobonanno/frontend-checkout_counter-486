import React from 'react';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const ExternalInsuranceContent = () => {
  const { t } = useTranslation();
  return (
    <>
      <div>
        <Text color="neutrals.0" fontSize="body.1">
          {t('flightInsurance.missed.connection.description')}
        </Text>
      </div>
      <div>
        <Text color="neutrals.0" fontSize="body.1" fontWeight="bold">
          {t('flightInsurance.cover.provided.title')}
        </Text>
      </div>
      <div>
        <ul>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.provided.item1')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.provided.item2')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.provided.item3')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.provided.item4')}
            </Text>
          </li>
        </ul>
      </div>
      <div>
        <Text color="neutrals.0" fontSize="body.1">
          {t('flightInsurance.cover.provided.description')}
        </Text>
      </div>
      <div>
        <Text color="neutrals.0" fontSize="body.1" fontWeight="bold">
          {t('flightInsurance.cover.not.provided.title')}
        </Text>
      </div>
      <div>
        <ul>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.not.provided.item1')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.not.provided.item2')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.not.provided.item3')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.cover.not.provided.item4')}
            </Text>
          </li>
        </ul>
      </div>
      <div>
        <Text color="neutrals.0" fontSize="body.1" fontWeight="bold">
          {t('flightInsurance.exclusions.title')}
        </Text>
      </div>
      <div>
        <ul>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.exclusions.item1')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.exclusions.item2')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.exclusions.item3')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.exclusions.item4')}
            </Text>
          </li>
          <li>
            <Text color="neutrals.0" fontSize="body.1">
              {t('flightInsurance.exclusions.item5')}
            </Text>
          </li>
        </ul>
      </div>
      <div>
        <Text color="neutrals.0" fontSize="body.1">
          {t('flightInsurance.termsAndConditions')}
        </Text>
      </div>
    </>
  );
};

export default ExternalInsuranceContent;
