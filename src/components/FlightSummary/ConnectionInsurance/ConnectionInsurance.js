import React, { useState, Suspense } from 'react';
import { useTranslation, Trans } from '@frontend-shell/react-hooks';
import {
  Link,
  InsuranceBadgeIcon,
  Box,
  Body,
  Flex,
  useGetThemeVersion,
} from 'prisma-design-system';
import PropTypes from 'prop-types';

const ConnectionInsuranceModal = React.lazy(() =>
  import('./ConnectionInsuranceModal')
);

const TextByPolicy = ({ policy, handleOpen, carrier }) => {
  const { t } = useTranslation();
  if (policy !== 'CARRIER_GUARANTEE') {
    return (
      <>
        <Trans
          i18nKey={'trip.stop.insuranceHub'}
          values={{
            insuranceName: t('flightInsurance.missed.connection.title'),
          }}
          components={{ insuranceName: <Link onClick={handleOpen} /> }}
        />
      </>
    );
  }
  return t('flightSummary.stop.missedConnection', {
    carrierName: carrier.name,
  });
};

const ConnectionInsurance = ({ policy, carrier }) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(!open);
  const themeName = useGetThemeVersion();

  const themeConfiguration = {
    blueStone: {
      iconSize: 'tiny',
      fontSize: 'body.0',
      bodyType: 'base',
      colorText: 'neutrals.3',
    },
    cobalt: {
      iconSize: 'small',
      fontSize: 'body.1',
      bodyType: 'small',
      colorText: 'neutrals.80',
    },
  };

  const { iconSize, fontSize, bodyType, colorText } =
    themeConfiguration[themeName];

  return (
    <>
      <Box pb={1}>
        <Flex alignItems="center">
          <InsuranceBadgeIcon size={iconSize} color={colorText} />
          <Box mx={1}>
            <Body color={colorText} fontSize={fontSize} type={bodyType}>
              <TextByPolicy
                policy={policy}
                carrier={carrier}
                handleOpen={handleOpen}
              />
            </Body>
          </Box>
        </Flex>
      </Box>
      <Suspense fallback={null}>
        <ConnectionInsuranceModal
          open={open}
          handleClose={handleOpen}
          policy={policy}
        />
      </Suspense>
    </>
  );
};

ConnectionInsurance.propTypes = {
  policy: PropTypes.oneOf([
    'CARRIER_GUARANTEE',
    'ODIGEO_GUARANTEE',
    'EXTERNAL_INSURANCE',
  ]).isRequired,
};

export default ConnectionInsurance;
