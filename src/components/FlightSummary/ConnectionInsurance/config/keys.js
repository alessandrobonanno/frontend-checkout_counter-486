const translations = [
  {
    inputPath: ['variables', 'brand.name'],
    outputPath: ['variables', 'brand', 'name', 'default'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.insuranceHub'],
    outputPath: ['trip', 'stop', 'insuranceHub'],
    args: ['insuranceName'],
    TODO: 'fix_ED_zh-TW & ED_ko-KR locales',
    scope: ['?ARABIC_SITES'],
    silence: {
      noMatchArgs: ['ED_ko-KR', 'ED_zh-TW'],
    },
  },
  {
    inputPath: ['flightsummary', 'stopInfo.missedConnection'],
    outputPath: ['flightSummary', 'stop', 'missedConnection'],
    args: ['carrierName'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['flightInsurance', 'missed.connection.title'],
    outputPath: ['flightInsurance', 'missed', 'connection', 'title'],
    scope: ['VIN_INSURANCE'],
  },
  {
    inputPath: ['flightInsurance', 'missed.connection.description'],
    outputPath: ['flightInsurance', 'missed', 'connection', 'description'],
    scope: ['VIN_INSURANCE'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.title'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'title'],
    scope: ['VIN_INSURANCE'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.item1'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'item1'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.item2'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'item2'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.item3'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'item3'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.item4'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'item4'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.provided.description'],
    outputPath: ['flightInsurance', 'cover', 'provided', 'description'],
    scope: ['VIN_INSURANCE'],
  },
  {
    inputPath: ['flightInsurance', 'cover.not.provided.title'],
    outputPath: ['flightInsurance', 'cover', 'not', 'provided', 'title'],
    scope: ['VIN_INSURANCE'],
  },
  {
    inputPath: ['flightInsurance', 'cover.not.provided.item1'],
    outputPath: ['flightInsurance', 'cover', 'not', 'provided', 'item1'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.not.provided.item2'],
    outputPath: ['flightInsurance', 'cover', 'not', 'provided', 'item2'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.not.provided.item3'],
    outputPath: ['flightInsurance', 'cover', 'not', 'provided', 'item3'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'cover.not.provided.item4'],
    outputPath: ['flightInsurance', 'cover', 'not', 'provided', 'item4'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.title'],
    outputPath: ['flightInsurance', 'exclusions', 'title'],
    scope: ['VIN_INSURANCE', '?OP_de-AT', '?OP_nl-NL'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.item1'],
    outputPath: ['flightInsurance', 'exclusions', 'item1'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.item2'],
    outputPath: ['flightInsurance', 'exclusions', 'item2'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.item3'],
    outputPath: ['flightInsurance', 'exclusions', 'item3'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.item4'],
    outputPath: ['flightInsurance', 'exclusions', 'item4'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
  {
    inputPath: ['flightInsurance', 'exclusions.item5'],
    outputPath: ['flightInsurance', 'exclusions', 'item5'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
  {
    inputPath: ['flightInsurance', 'terms.and.conditions'],
    outputPath: ['flightInsurance', 'termsAndConditions'],
    scope: ['VIN_INSURANCE', 'NOT_NEEDED_ODIGEO_GUARANTEE_KEYS'],
  },
];

module.exports = translations;
