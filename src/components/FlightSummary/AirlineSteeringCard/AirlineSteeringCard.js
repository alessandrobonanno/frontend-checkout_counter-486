import React from 'react';
import PropTypes from 'prop-types';
import { Box, Hr } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';
import segmentResolver from '../Segment/resolver';

import ListItemCard from './ListItemCard';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

import CarrierList from './CarrierList';

const cobaltDesktopFlightDetailsVersionConfigs = {
  A: {
    horizontalPaddingOutside: 0,
    hrLineType: 'solid',
    verticalPaddingInside: 3,
  },
  C: {
    horizontalPaddingOutside: 5,
    hrLineType: 'dashed',
    verticalPaddingInside: 5,
  },
};

const AirlineSteeringCardVersioned = ({
  hasReliableCarriers,
  hasRefundCarriers,
  selectedSegments,
  versionConfigs,
}) => {
  const { t } = useTranslation();
  const { horizontalPaddingOutside, hrLineType, verticalPaddingInside } =
    versionConfigs;

  const carriers = [
    ...new Map(
      selectedSegments.map((segment) => {
        const segmentParsed = segmentResolver(segment);
        const { carriers } = segmentParsed;
        return [carriers[0].id, carriers[0]];
      })
    ).values(),
  ];
  const showCard = hasReliableCarriers || hasRefundCarriers;

  return showCard ? (
    <Box px={horizontalPaddingOutside}>
      <Hr type={hrLineType} />
      <Box py={verticalPaddingInside}>
        <CarrierList carriers={carriers} />

        <Box mx={0}>
          {hasRefundCarriers && (
            <ListItemCard
              title={t('flightSummary.recommended.itinerary.refund.title')}
              description={t(
                'flightSummary.recommended.itinerary.refund.description'
              )}
            />
          )}
          {hasReliableCarriers && (
            <ListItemCard
              title={t('flightSummary.recommended.itinerary.reliable.title')}
              description={t(
                'flightSummary.recommended.itinerary.reliable.description'
              )}
            />
          )}
        </Box>
      </Box>
    </Box>
  ) : null;
};

const AirlineSteeringCard = cobaltDesktopThemedComponentVersion(
  AirlineSteeringCardVersioned,
  cobaltDesktopFlightDetailsComponent,
  cobaltDesktopFlightDetailsVersionConfigs
);

AirlineSteeringCard.propTypes = {
  hasRefundCarriers: PropTypes.bool,
  hasReliableCarriers: PropTypes.bool,
  selectedSegments: PropTypes.array,
};

export default AirlineSteeringCard;
