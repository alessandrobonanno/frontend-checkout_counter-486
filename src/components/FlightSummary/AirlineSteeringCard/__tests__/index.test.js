import React from 'react';
import { cleanup, screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../testUtils';
import { setViewport } from '../../../../testUtils/responsiveness';
import generateSegments from '../../../../serverMocks/Itinerary';
import AirlineSteeringCard from '../AirlineSteeringCard';

const REFUND_TITLE = 'refund title';
const REFUND_DESCRIPTION = 'refund description';

const RELIABLE_TITLE = 'reliable title';
const RELIABLE_DESCRIPTION = 'reliable description';

const translations = {
  flightSummary: {
    recommended: {
      itinerary: {
        refund: {
          title: REFUND_TITLE,
          description: REFUND_DESCRIPTION,
        },
        reliable: {
          title: RELIABLE_TITLE,
          description: RELIABLE_DESCRIPTION,
        },
      },
    },
  },
};

afterEach(() => {
  cleanup();
});

describe('AirlineSteeringCard', () => {
  describe('Airline desktop content', () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      false,
      'CHECKIN_INCLUDED',
      false,
      false
    );

    const renderTestWithAb = (abs) => {
      render(
        <TestingProviders abs={abs} translations={translations}>
          <AirlineSteeringCard
            hasReliableCarriers={true}
            hasRefundCarriers={true}
            selectedSegments={segments}
          />
        </TestingProviders>
      );
    };

    test('with FBO_BOOK3467 not Active', async () => {
      const abs = [{ alias: 'FBO_BOOK3467', partition: 1 }];

      renderTestWithAb(abs);

      expect(await screen.findByText(/Ryanair/i)).toBeVisible();
      expect(await screen.findByText(RELIABLE_TITLE)).toBeVisible();
      expect(await screen.findByText(RELIABLE_DESCRIPTION)).toBeVisible();
      expect(await screen.findByText(REFUND_TITLE)).toBeVisible();
      expect(await screen.findByText(REFUND_DESCRIPTION)).toBeVisible();
    });

    test('with FBO_BOOK3467 Active', async () => {
      const abs = [{ alias: 'FBO_BOOK3467', partition: 3 }];

      renderTestWithAb(abs);

      expect(await screen.findByText(/Ryanair/i)).toBeVisible();
      expect(await screen.findByText(RELIABLE_TITLE)).toBeVisible();
      expect(await screen.findByText(RELIABLE_DESCRIPTION)).toBeVisible();
      expect(await screen.findByText(REFUND_TITLE)).toBeVisible();
      expect(await screen.findByText(REFUND_DESCRIPTION)).toBeVisible();
    });
  });
});
