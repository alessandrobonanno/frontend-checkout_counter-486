import React from 'react';
import PropTypes from 'prop-types';

import {
  Flex,
  FlexRow,
  Box,
  Text,
  useDirection,
  Col,
} from 'prisma-design-system';
import Carrier from '../Carrier';
import CarrierLogo from '../Carrier/CarrierLogo';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

const CobaltCarriers = ({ carriers }) => {
  const isRtl = useDirection();

  let carrierNames = carriers.map((carrier) => carrier.name);
  if (isRtl) {
    carrierNames.reverse();
  }
  const carrierNamesText = carrierNames.join(', ');

  const marginRightFirstAndRestDifferenceByRtl = {
    isRtl: {
      first: 2,
      others: 2,
    },
    isLtr: {
      first: 0,
      others: 2,
    },
  };
  let carrierIconMarginRightConfig =
    marginRightFirstAndRestDifferenceByRtl[isRtl ? 'isRtl' : 'isLtr'];

  return (
    <FlexRow
      mb={2}
      gap={2}
      flexDirection="row"
      alignItems="center"
      justifyContent="start"
    >
      <Col>
        <Flex
          flexDirection="row-reverse"
          alignItems="center"
          justifyContent="flex-end"
        >
          {carriers.reverse().map((carrier, index) => {
            const isFirst = 0 === index;

            return (
              <Box
                key={carrier.id}
                width={isFirst ? 2 : 0}
                marginRight={
                  isFirst
                    ? carrierIconMarginRightConfig.first
                    : carrierIconMarginRightConfig.others
                }
              >
                <CarrierLogo
                  key={carrier.id}
                  name={carrier.id}
                  id={carrier.id}
                  imageSize={'large'}
                />
              </Box>
            );
          })}
        </Flex>
      </Col>

      <Col>
        <Text color="neutrals.1" fontSize={'body.1'} fontWeight="medium">
          {carrierNamesText}
        </Text>
      </Col>
    </FlexRow>
  );
};

const BlueStoneCarriers = ({ carriers }) => (
  <Flex flexDirection="row" justifyContent="space-between" flexWrap="wrap">
    <Box mb={2}>
      <Flex as="span" alignItems="center">
        {carriers.map((carrier) => (
          <Carrier key={carrier.id} carriers={[carrier]} placement="section" />
        ))}
      </Flex>
    </Box>
  </Flex>
);

const CarrierList = cobaltDesktopFlightDetailsComponent([
  BlueStoneCarriers,
  null,
  CobaltCarriers,
]);

CarrierList.defaultProps = {
  carrier: [],
};
CarrierList.propTypes = {
  carriers: PropTypes.arrayOf(PropTypes.object),
};

export default CarrierList;
