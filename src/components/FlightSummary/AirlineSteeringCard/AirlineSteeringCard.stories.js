import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import AirlineSteeringCard from './';
import generateSegments from '../../../serverMocks/Itinerary';
import configKeys from './config/keys';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/AirlineSteeringCard',
  component: AirlineSteeringCard,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  const segments = generateSegments(
    2,
    true,
    false,
    'CHECKIN_INCLUDED',
    true,
    true
  );
  return <AirlineSteeringCard selectedSegments={segments} {...args} />;
};

Default.decorators = [withKnobs];
Default.args = {
  hasReliableCarriers: true,
  hasRefundCarriers: true,
  hasFreeRebooking: true,
};

export const MultipleAirlines = (args) => {
  const segments = generateSegments(
    1,
    true,
    false,
    'CHECKIN_INCLUDED',
    true,
    true
  );
  return <AirlineSteeringCard selectedSegments={segments} {...args} />;
};

MultipleAirlines.decorators = [withKnobs];
MultipleAirlines.args = {
  hasReliableCarriers: true,
  hasRefundCarriers: true,
  hasFreeRebooking: true,
};

export default story;
