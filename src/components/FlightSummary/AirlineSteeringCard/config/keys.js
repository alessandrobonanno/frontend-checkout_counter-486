const translations = [
  {
    inputPath: ['results', 'recommended.itinerary.refund.title'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'refund',
      'title',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
  {
    inputPath: ['results', 'recommended.itinerary.refund.description'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'refund',
      'description',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
  {
    inputPath: ['results', 'recommended.itinerary.reliable.title'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'reliable',
      'title',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
  {
    inputPath: ['results', 'recommended.itinerary.reliable.description'],
    outputPath: [
      'flightSummary',
      'recommended',
      'itinerary',
      'reliable',
      'description',
    ],
    scope: ['PRIME', '?OP_pt-PT', '?ED_pt-PT', '?TR_de-DE', '?ED_es-US'],
  },
];

module.exports = translations;
