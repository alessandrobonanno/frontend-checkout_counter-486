import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Body, CheckIcon } from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const cobaltDesktopFlightDetailsVersionConfigs = {
  A: {
    iconColor: 'neutrals.0',
    imageBgColor: undefined,
    bodyType: 'base',
    descriptionColor: 'neutrals.2',
    outsideMarginBottom: 1,
    iconRightMargin: 0,
    iconRightPadding: 5,
    iconAlignItems: undefined,
    iconFlexDirection: undefined,
    isWithCircleBackground: false,
  },
  C: {
    iconColor: 'positive.default',
    imageBgColor: 'positive.lightest',
    bodyType: 'small',
    descriptionColor: 'neutrals.3',
    outsideMarginBottom: 4,
    iconRightMargin: 2,
    iconRightPadding: 0,
    iconAlignItems: 'center',
    iconFlexDirection: 'column',
    isWithCircleBackground: true,
  },
};

const ListItemCardVersioned = ({
  title,
  description,
  color = 'neutrals.1',
  iconColor,
  Icon,
  versionConfigs,
}) => {
  const {
    bodyType,
    descriptionColor,
    imageBgColor,
    outsideMarginBottom,
    iconRightMargin,
    iconRightPadding,
    iconAlignItems,
    iconFlexDirection,
    isWithCircleBackground,
  } = versionConfigs;

  if (!iconColor) {
    iconColor = versionConfigs.iconColor;
  }

  let hasCircleBackground = false;

  const withCircleBackground =
    (WrappedComponent, imageBgColor) =>
    ({ ...props }) => {
      return (
        <Box backgroundColor={imageBgColor} borderRadius="50%" height={0}>
          <WrappedComponent {...props} />
        </Box>
      );
    };

  Icon =
    Icon ||
    (isWithCircleBackground
      ? withCircleBackground(CheckIcon, imageBgColor)
      : CheckIcon);

  return (
    <Box mb={outsideMarginBottom}>
      <Flex>
        <Flex
          mr={iconRightMargin}
          pr={iconRightPadding}
          pt={1}
          height={2}
          width={2}
          alignItems={iconAlignItems}
          flexDirection={iconFlexDirection}
        >
          <Icon
            bgColor={imageBgColor}
            hasCircleBackground={hasCircleBackground}
            color={iconColor}
            size={'small'}
          />
        </Flex>
        <Box>
          <Box>
            <Body
              m={0}
              color={color}
              fontWeight="medium"
              fontSize={'body.1'}
              type={bodyType}
              as={'div'}
            >
              {title}
            </Body>
          </Box>
          <Body
            m={0}
            color={descriptionColor}
            fontSize="body.1"
            type={bodyType}
            as="div"
          >
            {description}
          </Body>
        </Box>
      </Flex>
    </Box>
  );
};

const ListItemCard = cobaltDesktopThemedComponentVersion(
  ListItemCardVersioned,
  cobaltDesktopFlightDetailsComponent,
  cobaltDesktopFlightDetailsVersionConfigs
);

ListItemCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  color: PropTypes.string,
  iconColor: PropTypes.string,
};

export default ListItemCard;
