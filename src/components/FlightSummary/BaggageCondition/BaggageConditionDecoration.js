import React, { useRef, useState, useEffect } from 'react';
import { useTranslation } from '@frontend-shell/react-hooks';
import {
  Flex,
  Text,
  Box,
  PopoverContainer,
  Popover,
  WarningIcon,
  InformationIcon,
  CrossIcon,
  useGetThemeVersion,
} from 'prisma-design-system';
import BaggageConditionCopy from './BaggageConditionCopy';

const carriersWithHint = ['fr', 'oe', 'w6'];

const BaggageConditionDecoration = ({
  baggageCondition,
  carriers,
  children,
}) => {
  const ref = useRef();
  const { t } = useTranslation();
  const themeName = useGetThemeVersion();
  const themeConfigurations = {
    blueStone: {
      justifyContent: 'space-between',
      textColor: 'neutrals.2',
      fontWeight: undefined,
      baggageTopPadding: 0,
    },
    cobalt: {
      justifyContent: 'center',
      textColor: 'warning.default',
      fontWeight: 'medium',
      baggageTopPadding: 1,
    },
  };
  const [open, setOpen] = useState(false);
  const isBasedOnAirline =
    baggageCondition === 'BASED_ON_AIRLINE' ||
    baggageCondition === 'CABIN_INCLUDED';
  const carrierId = carriers ? carriers[0].id.toLowerCase() : '';
  const title = t(`flightSummary.segment.baggage.title.${carrierId}`);
  const description = t(
    `flightSummary.segment.baggage.description.${carrierId}`
  );
  const descriptions = description.split('</br>');
  const hasDecorator = isBasedOnAirline && carriersWithHint.includes(carrierId);

  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      setOpen(false);
    };
    document.addEventListener('mousedown', listener);
    return () => {
      document.removeEventListener('mousedown', listener);
    };
  }, [ref]);

  const { justifyContent, textColor, fontWeight, baggageTopPadding } =
    themeConfigurations[themeName];

  return (
    <>
      {hasDecorator && (
        <Flex
          justifyContent={justifyContent}
          alignItems="center"
          pt={baggageTopPadding}
        >
          <WarningIcon size="small" color="warning.default" />
          <Box mx={1} width="max-content">
            <Text fontSize="body.1" color={textColor} fontWeight={fontWeight}>
              <BaggageConditionCopy baggageCondition={'BASED_ON_AIRLINE'} />
            </Text>
          </Box>
          <Box pt={1} ml={1}>
            <PopoverContainer>
              <InformationIcon
                size="small"
                color={textColor}
                onClick={() => {
                  setOpen(true);
                }}
              />
              <Popover
                open={open}
                size="large"
                placement="bottom"
                align="middle"
              >
                <Flex
                  mb={2}
                  justifyContent={'space-between'}
                  alignItems={'center'}
                  ref={ref}
                >
                  <Text fontSize="body.1" fontWeight="medium">
                    {title}
                  </Text>
                  <CrossIcon
                    size="small"
                    color="neutrals.2"
                    onClick={() => {
                      setOpen(false);
                    }}
                  />
                </Flex>
                <Box>
                  {descriptions.map((desc, idx) => (
                    <Box key={`desc${idx}`} mb={2}>
                      <Text fontSize="body.0">{desc}</Text>
                    </Box>
                  ))}
                </Box>
              </Popover>
            </PopoverContainer>
          </Box>
        </Flex>
      )}
      {!hasDecorator && children}
    </>
  );
};

export default BaggageConditionDecoration;
