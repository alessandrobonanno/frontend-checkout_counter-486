import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import BaggageCondition from './';
import configKeys from './config/keys';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/BaggageCondition',
  component: BaggageCondition,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  return <BaggageCondition {...args} />;
};
Default.decorators = [withKnobs];
Default.argTypes = {
  baggageCondition: {
    control: {
      type: 'select',
      options: ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLINE'],
    },
    defaultValue: 'CHECKIN_INCLUDED',
  },
};

export default story;
