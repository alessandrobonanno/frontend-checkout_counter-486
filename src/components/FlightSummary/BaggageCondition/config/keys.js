const translations = [
  {
    inputPath: ['flightsummary', 'baggage.not.included.message'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'handBaggage'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['baggagesmanager', 'title.included'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'checkInBaggage'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'fr.option.bo_sb.title'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'smallBaggage'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'fr.hint.title'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'title', 'fr'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'fr.hint.description'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'description', 'fr'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'oe.hint.title'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'title', 'oe'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'oe.hint.description'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'description', 'oe'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['boardingoptions', 'w6.hint.title'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'title', 'w6'],
    scope: ['?ARABIC_SITES', '!ED_zh-TW', '!ED_ko-KR'],
  },
  {
    inputPath: ['boardingoptions', 'w6.hint.description'],
    outputPath: ['flightSummary', 'segment', 'baggage', 'description', 'w6'],
    scope: ['?ARABIC_SITES', '!ED_zh-TW', '!ED_ko-KR'],
  },
];

module.exports = translations;
