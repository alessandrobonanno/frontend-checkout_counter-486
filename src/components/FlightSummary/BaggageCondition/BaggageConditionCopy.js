import { useTranslation } from '@frontend-shell/react-hooks';

const baggageConditionToKey = {
  CHECKIN_INCLUDED: 'flightSummary.segment.baggage.checkInBaggage',
  CABIN_INCLUDED: 'flightSummary.segment.baggage.handBaggage',
  BASED_ON_AIRLINE: 'flightSummary.segment.baggage.smallBaggage',
};

const BaggageConditionCopy = ({ baggageCondition }) => {
  const { t } = useTranslation();
  return t(baggageConditionToKey[baggageCondition]);
};

export default BaggageConditionCopy;
