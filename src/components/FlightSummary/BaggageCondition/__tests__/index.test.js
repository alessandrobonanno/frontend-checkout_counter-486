import React from 'react';
import { screen, render } from '@testing-library/react';

import BaggageCondition from '../';

import { TestingProviders } from '../../../../testUtils';

const SMALL_BAG = '1 small bag';
const HAND_LUGGAGE = 'Hand luggage';
const CHECK_IN_BAGGAGE = 'Check-in baggage';

const translations = {
  flightSummary: {
    segment: {
      baggage: {
        smallBaggage: SMALL_BAG,
        checkInBaggage: CHECK_IN_BAGGAGE,
        handBaggage: HAND_LUGGAGE,
      },
    },
  },
};

const carriers = [
  {
    id: 'VY',
    name: 'Vueling',
  },
];

describe('BaggageCondition', () => {
  test('CHECKIN_INCLUDED should be visible', async () => {
    render(
      <TestingProviders translations={translations}>
        <BaggageCondition
          baggageCondition="CHECKIN_INCLUDED"
          carriers={carriers}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(CHECK_IN_BAGGAGE)).toBeVisible();
  });
  test('CABIN_INCLUDED should be visible', async () => {
    render(
      <TestingProviders translations={translations}>
        <BaggageCondition
          baggageCondition="CABIN_INCLUDED"
          carriers={carriers}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(HAND_LUGGAGE)).toBeVisible();
  });
  test('BASED_ON_AIRLANE should be visible', async () => {
    render(
      <TestingProviders translations={translations}>
        <BaggageCondition
          baggageCondition="BASED_ON_AIRLINE"
          carriers={carriers}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(SMALL_BAG)).toBeVisible();
  });
});
