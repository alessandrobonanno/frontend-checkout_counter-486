import React from 'react';
import { Text, Box } from 'prisma-design-system';
import BaggageConditionCopy from './BaggageConditionCopy';
import BaggageConditionDecoration from './BaggageConditionDecoration';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const TextTypeVersioned = ({
  baggageCondition,
  fontSize = 'bodies.small',
  children,
  versionConfigs,
}) => {
  const color =
    baggageCondition === 'CHECKIN_INCLUDED' ? 'positive.default' : 'neutrals.2';

  const { fontWeight } = versionConfigs;
  return (
    <Text fontWeight={fontWeight} fontSize={fontSize} color={color}>
      {children}
    </Text>
  );
};

const TextType = cobaltDesktopThemedComponentVersion(
  TextTypeVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { fontWeight: 'normal' },
    C: { fontWeight: 'medium' },
  }
);

const BaggageConditionVersioned = ({
  baggageCondition,
  carriers,
  fontSize,
  versionConfigs,
}) => {
  const { textPaddingTop } = versionConfigs;
  return (
    <BaggageConditionDecoration
      baggageCondition={baggageCondition}
      carriers={carriers}
    >
      <Box pt={textPaddingTop} width="max-content">
        <TextType baggageCondition={baggageCondition} fontSize={fontSize}>
          <BaggageConditionCopy baggageCondition={baggageCondition} />
        </TextType>
      </Box>
    </BaggageConditionDecoration>
  );
};

const BaggageCondition = cobaltDesktopThemedComponentVersion(
  BaggageConditionVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { textPaddingTop: 0 },
    C: { textPaddingTop: 1 },
  }
);

export default BaggageCondition;
