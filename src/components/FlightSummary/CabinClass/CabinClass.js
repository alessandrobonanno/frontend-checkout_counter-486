import React from 'react';

import { Text, Flex } from 'prisma-design-system';

import PropTypes from 'prop-types';

import { useTranslation } from '@frontend-shell/react-hooks';

function CabinClass({ type }) {
  const { t } = useTranslation();
  return (
    <Flex alignItems="center">
      <Text color="neutrals.3" fontSize="body.1">
        {t(`flightSummary.cabinClass.${type}`)}
      </Text>
    </Flex>
  );
}

CabinClass.propTypes = {
  type: PropTypes.oneOf([
    'BUSINESS',
    'ECONOMIC_DISCOUNTED',
    'FIRST',
    'PREMIUM_ECONOMY',
    'TOURIST',
  ]),
};
CabinClass.defaultProps = {
  type: 'BUSINESS',
};

export default CabinClass;
