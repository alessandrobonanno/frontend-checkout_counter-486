const translations = [
  {
    inputPath: ['itineraryDetails', 'flightRow.BUSINESS'],
    outputPath: ['flightSummary', 'cabinClass', 'BUSINESS'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['itineraryDetails', 'flightRow.ECONOMIC_DISCOUNTED'],
    outputPath: ['flightSummary', 'cabinClass', 'ECONOMIC_DISCOUNTED'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['itineraryDetails', 'flightRow.FIRST'],
    outputPath: ['flightSummary', 'cabinClass', 'FIRST'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['itineraryDetails', 'flightRow.PREMIUM_ECONOMY'],
    outputPath: ['flightSummary', 'cabinClass', 'PREMIUM_ECONOMY'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['itineraryDetails', 'flightRow.TOURIST'],
    outputPath: ['flightSummary', 'cabinClass', 'TOURIST'],
    scope: ['?ARABIC_SITES'],
  },
];

module.exports = translations;
