import React from 'react';
import { screen, render } from '@testing-library/react';
import CabinClass from '../CabinClass';

import { TestingProviders } from '../../../../testUtils';

const TOURIST_LABEL = 'Economy label';
const PREMIUM_ECONOMY_LABEL = 'Premium economy label';
const FIRST_LABEL = 'First label';
const ECONOMIC_DISCOUNTED_LABEL = 'Economy';
const BUSINESS_LABEL = 'Business';

const translations = {
  flightSummary: {
    cabinClass: {
      TOURIST: TOURIST_LABEL,
      PREMIUM_ECONOMY: PREMIUM_ECONOMY_LABEL,
      FIRST: FIRST_LABEL,
      ECONOMIC_DISCOUNTED: ECONOMIC_DISCOUNTED_LABEL,
      BUSINESS: BUSINESS_LABEL,
    },
  },
};

describe('CabinClass', () => {
  test('Check type BUSINESS', async () => {
    render(
      <TestingProviders translations={translations}>
        <CabinClass type="BUSINESS" />
      </TestingProviders>
    );
    expect(await screen.findByText(BUSINESS_LABEL)).toBeVisible();
  });
  test('Check type FIRST', async () => {
    render(
      <TestingProviders translations={translations}>
        <CabinClass type="FIRST" />
      </TestingProviders>
    );
    expect(await screen.findByText(FIRST_LABEL)).toBeVisible();
  });
  test('Check type TOURIST', async () => {
    render(
      <TestingProviders translations={translations}>
        <CabinClass type="TOURIST" />
      </TestingProviders>
    );
    expect(await screen.findByText(TOURIST_LABEL)).toBeVisible();
  });
  test('Check type PREMIUM_ECONOMY', async () => {
    render(
      <TestingProviders translations={translations}>
        <CabinClass type="PREMIUM_ECONOMY" />
      </TestingProviders>
    );
    expect(await screen.findByText(PREMIUM_ECONOMY_LABEL)).toBeVisible();
  });
  test('Check type ECONOMIC_DISCOUNTED', async () => {
    render(
      <TestingProviders translations={translations}>
        <CabinClass type="ECONOMIC_DISCOUNTED" />
      </TestingProviders>
    );
    expect(await screen.findByText(ECONOMIC_DISCOUNTED_LABEL)).toBeVisible();
  });
});
