import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import CabinClass from './CabinClass';
import configKeys from './config/keys';

const story = {
  title: 'Components/FlightSummary/CabinClass',
  component: CabinClass,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <CabinClass {...args} />;
};
Default.argTypes = {
  type: {
    control: {
      type: 'select',
      options: [
        'BUSINESS',
        'ECONOMIC_DISCOUNTED',
        'FIRST',
        'PREMIUM_ECONOMY',
        'TOURIST',
      ],
    },
    defaultValue: 'ECONOMIC_DISCOUNTED',
  },
};

export default story;
