import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import ItineraryTitle from './index';

const story = {
  title: 'Components/FlightSummary/ItineraryTitle',
  component: ItineraryTitle,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <ItineraryTitle {...args} />;
};

Default.args = {
  cityName: 'Madrid',
  segmentNumber: 0,
};

export default story;
