import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const ItineraryTitle = ({ cityName, segmentNumber }) => {
  const { t } = useTranslation();
  return (
    <Text color={'neutrals.1'} fontSize={'body.2'} fontWeight={'medium'}>
      {segmentNumber > 2
        ? t('flightSummary.itinerary.toMultiDestination')
        : t('flightSummary.itinerary.toCity', { cityName })}
    </Text>
  );
};

ItineraryTitle.propTypes = {
  cityName: PropTypes.string.isRequired,
  segmentNumber: PropTypes.number.isRequired,
};

export default ItineraryTitle;
