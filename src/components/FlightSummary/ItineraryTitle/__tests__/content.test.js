import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import ItineraryTitle from '..';

const translations = {
  flightSummary: {
    itinerary: {
      toMultiDestination: 'Your itinerary',
      toCity: 'Madrid itinerary',
    },
  },
};

describe('ItineraryTitle', () => {
  test('City name is correctly displayed', async () => {
    render(
      <TestingProviders translations={translations}>
        <ItineraryTitle segmentNumber={1} cityName={'Madrid'} />
      </TestingProviders>
    );

    expect(await screen.findByText('Madrid itinerary')).toBeVisible();
  });
  test('Multiple destination content', async () => {
    render(
      <TestingProviders translations={translations}>
        <ItineraryTitle segmentNumber={3} cityName={'Madrid'} />
      </TestingProviders>
    );

    expect(await screen.findByText('Your itinerary')).toBeVisible();
  });
});
