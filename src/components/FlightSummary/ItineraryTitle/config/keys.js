const translations = [
  {
    inputPath: ['collapsibleFlightSummary', 'tripsummary.toCity'],
    outputPath: ['flightSummary', 'itinerary', 'toCity'],
    args: ['cityName'],
  },
  {
    inputPath: ['collapsibleFlightSummary', 'tripsummary.toMultidestination'],
    outputPath: ['flightSummary', 'itinerary', 'toMultiDestination'],
  },
];

module.exports = translations;
