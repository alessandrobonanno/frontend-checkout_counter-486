import React from 'react';
import TransportIcon from './index';

const story = {
  title: 'Components/FlightSummary/TransportIcon',
  component: TransportIcon,
};

export const Default = (args) => {
  return <TransportIcon {...args} />;
};

export default story;
