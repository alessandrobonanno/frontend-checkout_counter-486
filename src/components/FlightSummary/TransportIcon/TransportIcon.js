import React from 'react';
import PropTypes from 'prop-types';
import {
  Flex,
  FlightRightIcon,
  FlightLeftIcon,
  TrainFrontIcon,
  useDirection,
  useGetThemeVersion,
} from 'prisma-design-system';

const TransportIcon = ({ type }) => {
  const isRtl = useDirection();

  const flightColor =
    useGetThemeVersion() === 'cobalt'
      ? 'brandPrimary.30'
      : 'flightProductColor';
  const FlightIcon = isRtl ? FlightLeftIcon : FlightRightIcon;
  return (
    <Flex alignItems="center">
      {type === 'PLANE' ? (
        <FlightIcon color={flightColor} size="small" />
      ) : (
        <TrainFrontIcon color="trainProductColor" size="small" />
      )}
    </Flex>
  );
};

TransportIcon.propTypes = {
  type: PropTypes.oneOf(['TRAIN', 'PLANE']),
};

export default TransportIcon;
