const translations = [
  {
    inputPath: ['collapsibleFlightSummary', 'tripsummary.link'],
    outputPath: ['flightSummary', 'link'],
  },
];

module.exports = translations;
