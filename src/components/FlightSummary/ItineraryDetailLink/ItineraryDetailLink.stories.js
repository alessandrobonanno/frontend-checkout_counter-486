import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import ItineraryDetailLink from './index';

const story = {
  title: 'Components/FlightSummary/ItineraryDetailLink',
  component: ItineraryDetailLink,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <ItineraryDetailLink {...args} />;
};

export default story;
