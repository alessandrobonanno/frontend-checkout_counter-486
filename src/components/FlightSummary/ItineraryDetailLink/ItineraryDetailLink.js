import React from 'react';
import {
  Link,
  Text,
  Flex,
  ArrowRightIcon,
  ArrowLeftIcon,
  useDirection,
  useGetThemeVersion,
} from 'prisma-design-system';
import {
  useTranslation,
  useFlowAction,
  useTracking,
} from '@frontend-shell/react-hooks';

const ItineraryDetailLink = () => {
  const { t } = useTranslation();
  const { openFlightSummary } = useFlowAction();
  const { trackEvent } = useTracking();
  const isRtl = useDirection();
  const theme = useGetThemeVersion();
  const handleClick = () => {
    openFlightSummary();
    trackEvent({
      page: '/flights/flightinfo/',
      action: 'trip_information',
      label: 'open_trip_details',
      category: 'flights_info',
    });
  };
  return (
    <Link onClick={handleClick}>
      <Text fontSize={'body.2'}>
        <Flex alignItems={'center'}>
          {t('flightSummary.link')}
          {theme === 'cobalt' ? null : isRtl ? (
            <ArrowLeftIcon size={'tiny'} />
          ) : (
            <ArrowRightIcon size={'tiny'} />
          )}
        </Flex>
      </Text>
    </Link>
  );
};

export default ItineraryDetailLink;
