import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import CarrierConnectionChange from '..';
import { act } from 'react-dom/test-utils';

const translations = {
  flightsummary: {
    connectionCarrierChange: {
      plural: 'Change of {{0}} and {{1}}',
      singular: 'Change of {{0}}',
    },
    connection: {
      terminal: 'terminal',
      station: 'station',
      airport: 'airport',
    },
    carrier: {
      train: 'train',
      plane: 'plane',
    },
  },
};

describe('CarrierConnectionChange', () => {
  test('default - no options change', async () => {
    const { container } = render(
      <TestingProviders translations={translations}>
        <CarrierConnectionChange connectionChange="none" />
      </TestingProviders>
    );

    await act(async () => {
      expect(container.innerHTML).toHaveLength(0);
    });
  });
  test('with connection change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierConnectionChange connectionChange="terminal" />
      </TestingProviders>
    );
    expect(await screen.findByText('Change of terminal')).toBeVisible();
  });
  test('with carrier change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierConnectionChange carrierChange={true} transportType="TRAIN" />
      </TestingProviders>
    );
    expect(await screen.findByText('Change of train')).toBeVisible();
  });
  test('with carrier and connection change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierConnectionChange
          carrierChange={true}
          transportType="TRAIN"
          connectionChange="terminal"
        />
      </TestingProviders>
    );

    expect(
      await screen.findByText('Change of train and terminal')
    ).toBeVisible();
  });
});
