const translations = [
  {
    inputPath: ['flightsummary', 'connectionCarrierChange.singular'],
    outputPath: ['flightsummary', 'connectionCarrierChange', 'singular'],
    args: ['0'],
  },
  {
    inputPath: ['flightsummary', 'connectionCarrierChange.plural'],
    outputPath: ['flightsummary', 'connectionCarrierChange', 'plural'],
    args: ['0', '1'],
  },
  {
    inputPath: ['flightsummary', 'connection.airport'],
    outputPath: ['flightsummary', 'connection', 'airport'],
  },
  {
    inputPath: ['flightsummary', 'connection.station'],
    outputPath: ['flightsummary', 'connection', 'station'],
  },
  {
    inputPath: ['flightsummary', 'connection.terminal'],
    outputPath: ['flightsummary', 'connection', 'terminal'],
  },
  {
    inputPath: ['flightsummary', 'carrier.plane'],
    outputPath: ['flightsummary', 'carrier', 'plane'],
  },
  {
    inputPath: ['flightsummary', 'carrier.train'],
    outputPath: ['flightsummary', 'carrier', 'train'],
  },
];

module.exports = translations;
