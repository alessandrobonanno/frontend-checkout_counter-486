import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import CarrierConnectionChange from './';
import configKeys from './config/keys';

const story = {
  title: 'Components/FlightSummary/CarrierConnectionChange',
  component: CarrierConnectionChange,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <CarrierConnectionChange {...args} />;
};
export default story;
