import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from '@frontend-shell/react-hooks';
import { Heading, Box } from 'prisma-design-system';

const CarrierConnectionChange = ({
  carrierChange,
  transportType = '',
  connectionChange,
}) => {
  const { t } = useTranslation();
  const kindsOfChanges = [];

  if (carrierChange && transportType) {
    const transportTypeLowerCase = transportType.toLowerCase();
    kindsOfChanges.push(t(`flightsummary.carrier.${transportTypeLowerCase}`));
  }

  if (connectionChange !== 'none') {
    kindsOfChanges.push(t(`flightsummary.connection.${connectionChange}`));
  }

  const textContent = t(
    `flightsummary.connectionCarrierChange.${
      kindsOfChanges.length > 1 ? 'plural' : 'singular'
    }`,
    { ...kindsOfChanges }
  );

  return kindsOfChanges.length ? (
    <Box mb={1}>
      <Heading color="informative.default" weight="medium" type="6">
        {textContent}
      </Heading>
    </Box>
  ) : (
    <></>
  );
};

CarrierConnectionChange.propTypes = {
  carrierChange: PropTypes.bool,
  transportType: PropTypes.oneOf(['PLANE', 'TRAIN']),
  connectionChange: PropTypes.oneOf(['airport', 'station', 'terminal', 'none'])
    .isRequired,
};

CarrierConnectionChange.defaultProps = {
  carrierChange: undefined,
  connectionChange: 'none',
};

export default CarrierConnectionChange;
