import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Box, Hr, useMediaQueries } from 'prisma-design-system';
import Segment from '../Segment';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentsListVersioned = ({
  segments,
  isMultiTrip,
  detailsOpen,
  placement,
  hasFlightSummaryHeader,
  versionConfigs,
}) => {
  const { md } = useMediaQueries();

  return (
    <>
      {segments.map((segment, index) => {
        const showHr =
          versionConfigs.hasHr && md && index < segments.length - 1;

        return (
          <Fragment key={index}>
            <Segment
              index={index}
              segment={segment}
              detailsOpen={detailsOpen}
              isMultiTrip={isMultiTrip}
              placement={placement}
              hasFlightSummaryHeader={hasFlightSummaryHeader}
            />
            {showHr && (
              <Box mb={3}>
                <Hr type="dashed" />
              </Box>
            )}
          </Fragment>
        );
      })}
    </>
  );
};

const SegmentsList = cobaltDesktopThemedComponentVersion(
  SegmentsListVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { hasHr: true },
    C: { hasHr: false },
  }
);

SegmentsList.propTypes = {
  segments: PropTypes.array.isRequired,
  isMultiTrip: PropTypes.bool,
  detailsOpen: PropTypes.bool,
  placement: PropTypes.string,
  hasFlightSummaryHeader: PropTypes.bool,
};

export default SegmentsList;
