import React from 'react';
import SegmentList from './';
import generateSegments from '../../../serverMocks/Itinerary';
import segmentResolver from '../Segment/resolver';

const story = {
  title: 'Components/FlightSummary/SegmentList',
  component: SegmentList,
};

export const OneWay = (args) => {
  const segments = generateSegments(
    1,
    args.withStopovers,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const segmentsParsed = segments.map(segmentResolver);
  return (
    <SegmentList
      segments={segmentsParsed}
      isMultiTrip={false}
      detailsOpen={args.detailsOpen}
      placement={args.placement}
    />
  );
};

OneWay.args = {
  withStopovers: false,
  withTechnicalStopovers: false,
  withOperatingCarrier: true,
  detailsOpen: false,
};
OneWay.argTypes = {
  baggageCondition: {
    control: {
      type: 'select',
      options: ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLINE'],
    },
    defaultValue: 'CHECKIN_INCLUDED',
  },
  placement: {
    control: {
      type: 'select',
      options: ['details', 'payment'],
    },
    defaultValue: 'details',
  },
};

export const RoundTrip = (args) => {
  const segments = generateSegments(
    2,
    args.withStopovers,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const segmentsParsed = segments.map(segmentResolver);
  return (
    <SegmentList
      segments={segmentsParsed}
      isMultiTrip={false}
      detailsOpen={args.detailsOpen}
      placement={args.placement}
    />
  );
};

RoundTrip.args = { ...OneWay.args };
RoundTrip.argTypes = { ...OneWay.argsTypes };

export const MultiTrip = (args) => {
  const segments = generateSegments(
    3,
    args.withStopovers,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const segmentsParsed = segments.map(segmentResolver);
  return (
    <SegmentList
      segments={segmentsParsed}
      isMultiTrip={true}
      detailsOpen={args.detailsOpen}
      placement={args.placement}
    />
  );
};

MultiTrip.args = { ...OneWay.args };
MultiTrip.argTypes = { ...OneWay.argsTypes };

export default story;
