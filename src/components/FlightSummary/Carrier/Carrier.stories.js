import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import Carrier from '.';
import { carrierOptions } from 'decorators/selectCarrier';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/Carrier',
  component: Carrier,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const SingleCarrier = ({ carrier }, args) => {
  return <Carrier carriers={[carrier]} {...args} />;
};

SingleCarrier.decorators = [withKnobs];
SingleCarrier.argTypes = {
  carrier: {
    control: { type: 'select', options: carrierOptions },
    defaultValue: { id: 'UX', name: 'Air Europa' },
  },
};

export const MultipleCarriers = ({ carrier, carrier2 }, args) => {
  return <Carrier carriers={[carrier, carrier2]} {...args} />;
};

MultipleCarriers.decorators = [withKnobs];
MultipleCarriers.argTypes = {
  carrier: {
    control: { type: 'select', options: carrierOptions },
    defaultValue: { id: 'UX', name: 'Air Europa' },
  },
  carrier2: {
    control: { type: 'select', options: carrierOptions },
    defaultValue: { id: 'FR', name: 'Vueling' },
  },
};

MultipleCarriers.decorators = [withKnobs];
MultipleCarriers.args = {
  carrier: { id: 'UX', name: 'Air Europa' },
  carrier2: { id: 'FR', name: 'Vueling' },
};

export default story;
