import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../testUtils';
import { setViewport } from '../../../../testUtils/responsiveness';
import Carrier from '..';

const MULTIPLE_AIRLINES = 'Multiple airlines label';
const DIRECTION_TEXT = 'Departure';
const FLIGHT_CODE = 'AB1234';

const translations = {
  flightSummary: {
    multiple: {
      airlines: MULTIPLE_AIRLINES,
    },
  },
};

const carriers = [
  {
    id: 'VY',
    name: 'Vueling',
  },
  {
    id: 'IB',
    name: 'Iberia',
  },
];

describe('Carrier', () => {
  test('Single carrier content', async () => {
    render(
      <TestingProviders translations={translations}>
        <Carrier carriers={[carriers[0]]} />
      </TestingProviders>
    );

    expect(await screen.findByText(carriers[0].name)).toBeVisible();
  });
  test('Multiple carriers content', async () => {
    render(
      <TestingProviders translations={translations}>
        <Carrier carriers={carriers} />
      </TestingProviders>
    );

    expect(await screen.findByText(MULTIPLE_AIRLINES)).toBeVisible();
  });
  test('Direction Text content', async () => {
    setViewport({ width: 1200 });
    render(
      <TestingProviders translations={translations}>
        <Carrier carriers={[carriers[0]]} directionText={DIRECTION_TEXT} />
      </TestingProviders>
    );

    expect(
      await screen.findByText(new RegExp(DIRECTION_TEXT, 'i'))
    ).toBeVisible();
  });
  test('Flight Code content', async () => {
    setViewport({ width: 1200 });
    render(
      <TestingProviders translations={translations}>
        <Carrier carriers={[carriers[0]]} flightCode={FLIGHT_CODE} />
      </TestingProviders>
    );

    expect(
      await screen.findByText(`${carriers[0].name} ${FLIGHT_CODE}`)
    ).toBeVisible();
  });
});
