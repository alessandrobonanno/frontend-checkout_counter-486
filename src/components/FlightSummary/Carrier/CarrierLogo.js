import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

const images = {
  MA: '/images/mobile/airline_logos/MA.png',
  MIX: '/images/onefront/airlines/smVIN2.gif',
};

const CarrierLogoDefault = ({ id, name, imageSize, ...props }) => {
  const src = images[id] || `/images/onefront/airlines/sm${id}.gif`;

  return <Image src={src} alt={name} size={imageSize} rounded {...props} />;
};

const CarrierLogoCobalt = ({ ...props }) => (
  <CarrierLogoDefault borderColor={'neutrals.40'} bordered={true} {...props} />
);

const CarrierLogo = cobaltDesktopFlightDetailsComponent([
  CarrierLogoDefault,
  CarrierLogoDefault,
  CarrierLogoCobalt,
]);

CarrierLogo.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  imageSize: PropTypes.string.isRequired,
};

export default CarrierLogo;
