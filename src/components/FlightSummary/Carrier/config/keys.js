const translations = [
  {
    inputPath: ['results', 'multiple.airlines'],
    outputPath: ['flightSummary', 'multiple', 'airlines'],
  },
];

module.exports = translations;
