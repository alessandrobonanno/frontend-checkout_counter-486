import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from '@frontend-shell/react-hooks';
import {
  Flex,
  Text,
  Box,
  useMediaQueries,
  useDirection,
} from 'prisma-design-system';
import CarrierLogo from './CarrierLogo';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const cobaltDesktopFlightDetailsVersionConfigs = {
  A: {
    displayTextColor: 'neutrals.1',
    notSectionFontSize: 'body.2',
    logoMargin: 1,
    carrierLogoSize: 'medium',
  },
  C: {
    displayTextColor: 'neutrals.3',
    notSectionFontSize: 'headings.5',
    logoMargin: 3,
    carrierLogoSize: 'large',
  },
};

const CarrierVersioned = ({
  carriers,
  directionText,
  flightCode,
  placement,
  versionConfigs,
}) => {
  const { t } = useTranslation();
  const { md } = useMediaQueries();
  const isRtl = useDirection();

  const displayText =
    carriers.length > 1
      ? t('flightSummary.multiple.airlines')
      : carriers[0].name;

  const directionTextColor = md
    ? placement !== 'section'
      ? 'neutrals.1'
      : 'neutrals.3'
    : 'neutrals.2';
  const textColor = md
    ? placement !== 'section'
      ? versionConfigs.displayTextColor
      : 'neutrals.3'
    : 'neutrals.2';

  const fontSize =
    md && placement !== 'section'
      ? versionConfigs.notSectionFontSize
      : 'body.1';
  const logoMargin = md ? versionConfigs.logoMargin : 1;
  const logoSize = md ? versionConfigs.carrierLogoSize : 'medium';

  return (
    <Flex alignItems="center">
      <Flex alignItems="center">
        {carriers.map(({ id, name }) => (
          <Flex
            key={id}
            pr={!isRtl ? logoMargin : 0}
            pl={isRtl ? logoMargin : 0}
          >
            <CarrierLogo id={id} name={name} imageSize={logoSize} />
          </Flex>
        ))}
      </Flex>
      {directionText && (
        <Box mx={1}>
          <Text
            color={directionTextColor}
            fontSize={fontSize}
            fontWeight="medium"
          >
            {directionText.toUpperCase()}{' '}
          </Text>
        </Box>
      )}
      <Box mx={1}>
        <Text color={textColor} fontSize={fontSize}>
          {displayText}
          {flightCode && carriers.length === 1 && ` ${flightCode}`}
        </Text>
      </Box>
    </Flex>
  );
};

const Carrier = cobaltDesktopThemedComponentVersion(
  CarrierVersioned,
  cobaltDesktopFlightDetailsComponent,
  cobaltDesktopFlightDetailsVersionConfigs
);

Carrier.propTypes = {
  carriers: PropTypes.array.isRequired,
  directionText: PropTypes.string,
};

export default Carrier;
