import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import { carrierOptions } from 'decorators/selectCarrier';
import Stopover from './Stopover';
import configKeys from './config/keys';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/Stopover',
  component: Stopover,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  const { carrier, policy } = args;
  return (
    <Stopover
      duration={args.duration}
      locationChangeType={args.locationChangeType}
      carrierChange={args.carrierChange}
      insuranceOffer={{
        policy,
        carrier,
      }}
      vinRestriction={args.vinRestriction}
    />
  );
};
Default.decorators = [withKnobs];
Default.args = {
  carrierChange: false,
  duration: 999,
  vinRestriction: { enabled: true, country: 'Spain' },
  policy: 'CARRIER_GUARANTEE',
};
Default.argTypes = {
  locationChangeType: {
    control: {
      type: 'select',
      options: ['terminal', 'airport', 'station', 'none'],
    },
    defaultValue: 'none',
  },
  carrier: {
    control: { type: 'select', options: carrierOptions },
    defaultValue: { id: 'UX', name: 'Air Europa' },
  },
};

export default story;
