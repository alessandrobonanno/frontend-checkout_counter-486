import React from 'react';
import { DecorationLine, Text, Box, Flex } from 'prisma-design-system';
import PropTypes from 'prop-types';

import { useTranslation } from '@frontend-shell/react-hooks';

import CovidRestriction from '../CovidRestriction';
import Duration from '../Duration';

import CarrierChange from '../CarrierChange';
import ConnectionChangeType from '../ConnectionChangeType';
import ConnectionInsurance from '../ConnectionInsurance';
import CarrierConnectionChange from '../CarrierConnectionChange';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const StopoverVersioned = ({
  vinRestriction,
  duration,
  locationChangeType,
  transportType,
  carrierChange,
  insuranceOffer,
  versionConfigs,
}) => {
  const { t } = useTranslation();

  const {
    decorationLineMargin,
    bgColor,
    outerPaddingLeft,
    outerPaddingVertical,
    decorationLineType,
    decorationLineTheme,
    decorationLineLineBottom,
    decorationLineWidth,
    innerColumnPaddingTop,
    innerColumnPaddingBottom,
  } = versionConfigs;

  const ConnectionChangeVersioned = cobaltDesktopFlightDetailsComponent([
    ({ ...props }) => (
      <>
        {props.carrierChange && <CarrierChange />}
        <ConnectionChangeType type={props.locationChangeType || 'none'} />
      </>
    ),
    null,
    ({ ...props }) => (
      <>
        <CarrierConnectionChange
          connectionChange={props.locationChangeType}
          carrierChange={props.carrierChange}
          transportType={props.transportType}
        />
      </>
    ),
  ]);

  return (
    <Flex
      backgroundColor={bgColor}
      pl={outerPaddingLeft}
      py={outerPaddingVertical}
    >
      <DecorationLine
        decorationType={decorationLineType}
        lineType="dashed"
        theme={decorationLineTheme}
        lineBottom={decorationLineLineBottom}
        width={decorationLineWidth}
      />
      <Flex
        flexDirection="column"
        px={decorationLineMargin}
        pt={innerColumnPaddingTop}
        pb={innerColumnPaddingBottom}
      >
        <Box pb={2}>
          <Text fontWeight="medium" color="neutrals.1" fontSize="body.1">
            {t('flightSummary.stop.duration')}:{' '}
          </Text>
          <Text fontWeight="normal" color={'neutrals.1'} fontSize="body.1">
            <Duration durationInMinutes={duration} />
          </Text>
        </Box>
        <Box>
          <ConnectionChangeVersioned
            carrierChange={carrierChange}
            transportType={transportType}
            locationChangeType={locationChangeType}
          />

          <ConnectionInsurance {...insuranceOffer} />
          {vinRestriction.enabled && (
            <CovidRestriction country={vinRestriction.country} />
          )}
        </Box>
      </Flex>
    </Flex>
  );
};

const Stopover = cobaltDesktopThemedComponentVersion(
  StopoverVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      decorationLineMargin: 1,
      bgColor: undefined,
      outerPaddingLeft: 0,
      outerPaddingVertical: 0,
      innerColumnPaddingTop: 2,
      innerColumnPaddingBottom: 4,
      decorationLineType: 'circleDot',
      decorationLineTheme: 'default',
      decorationLineLineBottom: undefined,
      decorationLineWidth: undefined,
    },
    C: {
      decorationLineMargin: 5,
      bgColor: 'neutrals.10',
      outerPaddingLeft: 5,
      outerPaddingVertical: 5,
      innerColumnPaddingTop: 0,
      innerColumnPaddingBottom: 0,
      decorationLineType: 'disk',
      decorationLineTheme: 'primary',
      decorationLineLineBottom: '-48px',
      decorationLineWidth: 2,
    },
  }
);

Stopover.defaultProps = {
  locationChangeType: 'none',
  carrierChange: false,
  insuranceOffer: {},
  vinRestriction: {},
};

Stopover.propTypes = {
  locationChangeType: PropTypes.oneOf([
    'terminal',
    'airport',
    'station',
    'none',
  ]),
  carrierChange: PropTypes.bool,
  insuranceOffer: PropTypes.object,
  duration: PropTypes.number,
  vinRestriction: PropTypes.object,
};

export default Stopover;
