import React from 'react';
import { screen, render } from '@testing-library/react';
import Stopover from '../';

import { TestingProviders } from '../../../../testUtils';
import { Vueling } from '../../../../serverMocks/Carrier';

const DURATION = '{{ hours }} h {{ minutes }} min';
const TERMINAL_CHANGE_TEXT = 'terminal change';
const CARRIER_CHANGE_TEXT = 'carrier change';
const STOP_DURATION_TEXT = 'Stop duration label';
const VINMESSAGE_TITLE = 'Title vinmessage';
const VINMESSAGE_BODY = 'Body vinmessage {{country}}';
const COUNTRY = 'Spain';
const connectionCoveredBy = (carrierName) =>
  `Connection covered by carrier: ${carrierName}`;

const translations = {
  flightSummary: {
    segment: {
      duration: DURATION,
    },
    stop: {
      terminalChange: TERMINAL_CHANGE_TEXT,
      airlineChange: CARRIER_CHANGE_TEXT,
      duration: STOP_DURATION_TEXT,
      missedConnection: connectionCoveredBy('{{carrierName}}'),
    },
    stopInfo: {
      vinmessage: {
        title: VINMESSAGE_TITLE,
        body: VINMESSAGE_BODY,
      },
    },
  },
};

describe('Stopover', () => {
  test('content passed by prop should be shown', async () => {
    const abs = [{ alias: 'FBO_BOOK3467', partition: 1 }];

    const carrier = Vueling();
    render(
      <TestingProviders abs={abs} translations={translations}>
        <Stopover
          insuranceOffer={{
            policy: 'CARRIER_GUARANTEE',
            carrier,
          }}
          duration={110}
          locationChangeType={'terminal'}
          carrierChange={true}
          vinRestriction={{
            enabled: true,
            country: COUNTRY,
          }}
        />
      </TestingProviders>
    );
    expect(
      await screen.findByText(connectionCoveredBy(Vueling().name))
    ).toBeVisible();
    expect(screen.getByText(`${STOP_DURATION_TEXT}:`)).toBeVisible();
    expect(screen.getByText(`1 h 50 min`)).toBeVisible();
    expect(screen.getByText(TERMINAL_CHANGE_TEXT)).toBeVisible();
    expect(screen.getByText(CARRIER_CHANGE_TEXT)).toBeVisible();
    expect(screen.getByText(CARRIER_CHANGE_TEXT)).toBeVisible();
    expect(screen.getByText(VINMESSAGE_TITLE)).toBeVisible();
    expect(screen.getByText(`Body vinmessage Spain`)).toBeVisible();
  });
});
