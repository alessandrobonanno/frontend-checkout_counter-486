const translations = [
  {
    inputPath: ['itineraryDetails', 'stopInfo.stopduration'],
    outputPath: ['flightSummary', 'stop', 'duration'],
    scope: ['?ARABIC_SITES'],
  },
];

module.exports = translations;
