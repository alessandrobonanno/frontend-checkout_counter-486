import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import ConnectionChangeType from '..';

const SELF_TRANSFER_TRAIN_STATION_TEXT = 'self transfer train station';
const AIRPORT_CHANGE_TEXT = 'airport change';
const TERMINAL_CHANGE_TEXT = 'terminal change';

const translations = {
  flightSummary: {
    stop: {
      airportChange: AIRPORT_CHANGE_TEXT,
      terminalChange: TERMINAL_CHANGE_TEXT,
      selfTransfer: {
        trainStation: SELF_TRANSFER_TRAIN_STATION_TEXT,
      },
    },
  },
};

const SENTINEL = 'Sentinel_label';
const Sentinel = () => SENTINEL;

describe('ConnectionChangeType', () => {
  test('Change of airport', async () => {
    render(
      <TestingProviders translations={translations}>
        <ConnectionChangeType type="airport" />
      </TestingProviders>
    );
    expect(await screen.findByText(AIRPORT_CHANGE_TEXT)).toBeVisible();
  });

  test('Change of station', async () => {
    render(
      <TestingProviders translations={translations}>
        <ConnectionChangeType type="station" />
      </TestingProviders>
    );
    expect(
      await screen.findByText(SELF_TRANSFER_TRAIN_STATION_TEXT)
    ).toBeVisible();
  });

  test('Change of terminal', async () => {
    render(
      <TestingProviders translations={translations}>
        <ConnectionChangeType type="terminal" />
      </TestingProviders>
    );
    expect(await screen.findByText(TERMINAL_CHANGE_TEXT)).toBeVisible();
  });

  test('No change', async () => {
    render(
      <TestingProviders translations={translations}>
        <ConnectionChangeType type="none" />
        <Sentinel />
      </TestingProviders>
    );
    await screen.findByText(SENTINEL);
    expect(
      screen.queryByText(AIRPORT_CHANGE_TEXT, { exact: false })
    ).toBeNull();
    expect(
      screen.queryByText(TERMINAL_CHANGE_TEXT, { exact: false })
    ).toBeNull();
  });
});
