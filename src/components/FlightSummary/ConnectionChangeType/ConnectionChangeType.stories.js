import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import ConnectionChangeType from './';
import configKeys from './config/keys';

const story = {
  title: 'Component/FlightSummary/ConnectionChangeType',
  component: ConnectionChangeType,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <ConnectionChangeType {...args} />;
};
Default.argTypes = {
  type: {
    control: {
      type: 'select',
      options: ['airport', 'terminal', 'station', 'none'],
    },
    defaultValue: 'airport',
  },
};

export default story;
