const translations = [
  {
    inputPath: ['flightsummary', 'stopInfo.terminalChange'],
    outputPath: ['flightSummary', 'stop', 'terminalChange'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.airportChange'],
    outputPath: ['flightSummary', 'stop', 'airportChange'],
    scope: ['?ARABIC_SITES'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.selfTransfer.airport'],
    outputPath: ['flightSummary', 'stop', 'selfTransfer', 'airport'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.selfTransfer.trainStation'],
    outputPath: ['flightSummary', 'stop', 'selfTransfer', 'trainStation'],
  },
];

module.exports = translations;
