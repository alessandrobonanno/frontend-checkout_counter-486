import React from 'react';

import { useTranslation } from '@frontend-shell/react-hooks';
import {
  Box,
  ManBaggageWalkingIcon,
  Text,
  Flex,
  useGetThemeVersion,
} from 'prisma-design-system';
import PropTypes from 'prop-types';

const keys = {
  station: 'flightSummary.stop.selfTransfer.trainStation',
  airport: 'flightSummary.stop.airportChange',
  terminal: 'flightSummary.stop.terminalChange',
};

const theme = {
  blueStone: {
    fontSize: 'body.0',
    iconSize: 'tiny',
  },
  cobalt: {
    fontSize: 'body.1',
    iconSize: 'small',
  },
};

const ConnectionChangeType = ({ type }) => {
  const { t } = useTranslation();

  const { fontSize, iconSize } = theme[useGetThemeVersion()];

  return (
    type !== 'none' && (
      <Box pb={1}>
        <Flex alignItems="center">
          <Box mr={1}>
            <ManBaggageWalkingIcon size={iconSize} color="neutrals.3" />
          </Box>
          <Text color="neutrals.3" fontSize={fontSize}>
            {t(keys[type])}
          </Text>
        </Flex>
      </Box>
    )
  );
};

ConnectionChangeType.propTypes = {
  type: PropTypes.oneOf(['airport', 'station', 'terminal', 'none']).isRequired,
};

export default ConnectionChangeType;
