import React, { FC } from 'react';
import useShoppingCartItineraryQuery from '../../../clientState/hooks/useShoppingCartItineraryQuery';
import { FlightSummaryPageTrackingProvider } from '../../../clientState/providers/TrackingProviders';
import SegmentsList from '../SegmentsList';
import FlightSummaryTitle from '../FlightSummaryTitle';
import summaryResolver from './resolver';

const FlightSummaryMobile: FC = () => {
  const { data, loading, error } = useShoppingCartItineraryQuery();
  if (loading || error) {
    return null;
  }

  const {
    itinerary: { legs },
  } = data?.getShoppingCart || { itinerary: {} };
  const { segments } = summaryResolver(legs);

  return (
    <FlightSummaryPageTrackingProvider>
      <FlightSummaryTitle legs={legs} segments={segments} />
      <SegmentsList segments={segments} />
    </FlightSummaryPageTrackingProvider>
  );
};

export default FlightSummaryMobile;
