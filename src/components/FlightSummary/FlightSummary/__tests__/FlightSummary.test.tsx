import React from 'react';
import { screen, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import FlightSummary from '../';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';
import { oneWay } from '../../../../serverMocks/ShoppingCart';

const SHOW_DETAILS = 'Show details';
const HIDE_DETAILS = 'Hide details';
const translations = {
  flightSummary: {
    header: {
      title: 'Itinerary:',
      flight: 'Flight',
      train: 'Train',
    },
    footer: {
      showDetails: SHOW_DETAILS,
      hideDetails: HIDE_DETAILS,
    },
  },
};

const mocks = oneWay().mocks;

describe('FlightSummary', () => {
  test('FlightSummary desktop content', async () => {
    setViewport({ width: 1200 });
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={mocks}
        translations={translations}
        window={{ ga }}
      >
        <FlightSummary />
      </TestingProviders>
    );
    await waitFor(() => {
      expect(screen.getByText('Itinerary:')).toBeVisible();
    });
    expect(await screen.findByText('Flight')).toBeVisible();
    expect(await screen.findByText(SHOW_DETAILS)).toBeVisible();
  });
  test('FlightSummary desktop click show/hide details', async () => {
    setViewport({ width: 1200 });
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={mocks}
        translations={translations}
        window={{ ga }}
      >
        <FlightSummary />
      </TestingProviders>
    );

    await waitFor(() => {
      expect(screen.getByText('Itinerary:')).toBeVisible();
    });
    const showDetails = await screen.findByText(SHOW_DETAILS);
    userEvent.click(showDetails);
    expect(ga).toTrackFlightSummaryOnceWith({
      action: 'summary_widget',
      label: 'summary_details_open',
    });
    expect(await screen.findByText(HIDE_DETAILS)).toBeVisible();
    const hideDetails = await screen.findByText(HIDE_DETAILS);
    userEvent.click(hideDetails);
    expect(ga).toTrackFlightSummaryOnceWith({
      action: 'summary_widget',
      label: 'summary_details_close',
    });
    expect(await screen.findByText(SHOW_DETAILS)).toBeVisible();
  });
});
