import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import FlightSummary from './index';
import FlightSummaryPayment from './FlightSummaryPayment';
import selector from '../../../../.storybook/selectors/shoopingCartMockSelector';
import {
  oneWay,
  roundTrip,
  multiTrip,
} from '../../../serverMocks/ShoppingCart';
import carrierKeys from '../Carrier/config/keys';
import durationKeys from '../Duration/config/keys';
import itineraryDetailLinkKeys from '../ItineraryDetailLink/config/keys';
import itineraryTitleKeys from '../ItineraryTitle/config/keys';
import stopoverQuantityKeys from '../StopoverQuantity/config/keys';
import summaryTitleKeys from '../SummaryTitle/config/keys';
import selectABs from 'decorators/selectABs';
import { withKnobs, boolean } from '@storybook/addon-knobs';

const withSelectAbProps = (mocks) => () => {
  const isVin = boolean('isVin', false);
  const isTrain = boolean('isTrain', false);
  return {
    abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
    ...mocks(isVin, isTrain),
  };
};

const configKeys = [
  ...carrierKeys,
  ...durationKeys,
  ...itineraryDetailLinkKeys,
  ...itineraryTitleKeys,
  ...stopoverQuantityKeys,
  ...summaryTitleKeys,
];
const story = {
  title: 'Components/FlightSummary/FlightSummary',
  component: FlightSummary,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: withSelectAbProps(selector),
  },
};

export const OneWay = (args) => {
  return <FlightSummary {...args} />;
};
OneWay.parameters = [withKnobs];
OneWay.parameters = {
  mockedProps: withSelectAbProps(oneWay),
};

export const RoundTrip = (args) => {
  return <FlightSummary {...args} />;
};
RoundTrip.decorators = [withKnobs];
RoundTrip.parameters = {
  mockedProps: withSelectAbProps(roundTrip),
};

export const MultiTrip = (args) => {
  return <FlightSummary {...args} />;
};
MultiTrip.decorators = [withKnobs];
MultiTrip.parameters = {
  mockedProps: withSelectAbProps(multiTrip),
};

export const OneWayPaymentPage = (args) => {
  return <FlightSummaryPayment {...args} />;
};
OneWayPaymentPage.decorators = [withKnobs];
OneWayPaymentPage.parameters = {
  mockedProps: withSelectAbProps(oneWay),
};

export const RoundTripPaymentPage = (args) => {
  return <FlightSummaryPayment {...args} />;
};
RoundTripPaymentPage.decorators = [withKnobs];
RoundTripPaymentPage.parameters = {
  mockedProps: withSelectAbProps(roundTrip),
};

export const MultiTripPaymentPage = (args) => {
  return <FlightSummaryPayment {...args} />;
};
MultiTripPaymentPage.decorators = [withKnobs];
MultiTripPaymentPage.parameters = {
  mockedProps: withSelectAbProps(multiTrip),
};

export default story;
