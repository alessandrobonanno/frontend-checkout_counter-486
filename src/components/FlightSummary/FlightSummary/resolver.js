import segmentResolver from '../Segment/resolver';

const getSegmentsList = (legs) => legs.map(({ segments }) => segments[0]);

export default function summaryResolver(legs) {
  const segments = getSegmentsList(legs);
  const segmentParsed = segments.map(segmentResolver);
  const isRoundTrip =
    segmentParsed.length === 2 &&
    JSON.stringify(segmentParsed[0].departure) ===
      JSON.stringify(segmentParsed[1].destination) &&
    JSON.stringify(segmentParsed[1].departure) ===
      JSON.stringify(segmentParsed[0].destination);
  const isMultiTrip = segmentParsed.length > 1 && !isRoundTrip;
  return {
    isRoundTrip: isRoundTrip,
    isMultiTrip: isMultiTrip,
    segments: segmentParsed,
  };
}
