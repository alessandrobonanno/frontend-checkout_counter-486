import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Card } from 'prisma-design-system';
import useShoppingCartItineraryQuery from '../../../clientState/hooks/useShoppingCartItineraryQuery';
import { FlightSummaryPageTrackingProvider } from '../../../clientState/providers/TrackingProviders';
import SegmentsList from '../SegmentsList';
import FlightSummaryTitle from '../FlightSummaryTitle';
import FlightSummaryFooter from '../FlightSummaryFooter';
import AirlineSteeringCard from '../AirlineSteeringCard';
import FlightSummaryHeader from '../FlightSummaryHeader';
import summaryResolver from './resolver';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const FlightSummaryDesktopVersioned = ({ placement, versionConfigs }) => {
  const { data, loading, error } = useShoppingCartItineraryQuery();
  const { cardPadding } = versionConfigs;
  const [open, setOpen] = useState(false);
  const [lastState, setLast] = useState(false);

  const myRef = React.createRef();

  useEffect(
    function scrollTopWhenCloseDetails() {
      if (lastState && !open) {
        window.scrollTo(0, {
          top: myRef.current.offsetTop,
          behavior: 'smooth',
        });
      }
      setLast(open);
    },
    [open, lastState, setLast, myRef]
  );

  if (loading || error) {
    return null;
  }

  const {
    itinerary: { legs, hasReliableCarriers, hasRefundCarriers },
  } = data?.getShoppingCart || { itinerary: {} };
  const { isMultiTrip, segments } = summaryResolver(legs);
  const hasChangeOfCountry = segments.some((segment) =>
    segment?.stops?.some((stop) => stop?.vinRestriction.enabled)
  );
  const isPayment = placement === 'payment';

  const FlightSummaryTitleAux = cobaltDesktopFlightDetailsComponent([
    FlightSummaryTitle,
    null,
    null,
  ]);

  return (
    <FlightSummaryPageTrackingProvider>
      <div ref={myRef} />
      <FlightSummaryHeader
        hasReliableCarriers={hasReliableCarriers}
        hasRefundCarriers={hasRefundCarriers}
      />
      <Card p={cardPadding} hideOverflow={true}>
        <FlightSummaryTitleAux legs={legs} segments={segments} />

        <SegmentsList
          hasFlightSummaryHeader={hasReliableCarriers || hasRefundCarriers}
          segments={segments}
          isMultiTrip={isMultiTrip}
          detailsOpen={open}
          placement={placement}
        />
        {!isPayment && (
          <FlightSummaryFooter
            open={open}
            setOpen={setOpen}
            segments={segments}
            hasChangeOfCountry={hasChangeOfCountry}
          />
        )}
        <AirlineSteeringCard
          hasReliableCarriers={hasReliableCarriers}
          hasRefundCarriers={hasRefundCarriers}
          selectedSegments={segments}
        />
      </Card>
    </FlightSummaryPageTrackingProvider>
  );
};

const FlightSummaryDesktop = cobaltDesktopThemedComponentVersion(
  FlightSummaryDesktopVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { cardPadding: 4 },
    C: { cardPadding: 0 },
  }
);

FlightSummaryDesktop.propTypes = {
  placement: PropTypes.string,
};

export default FlightSummaryDesktop;
