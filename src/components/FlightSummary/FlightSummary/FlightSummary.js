import React from 'react';

import PropTypes from 'prop-types';
import { useMediaQueries } from 'prisma-design-system';
import FlightSummaryDesktop from './FlightSummaryDesktop';
import FlightSummaryMobile from './FlightSummaryMobile';

const FlightSummary = ({ placement }) => {
  const { md } = useMediaQueries();
  const FlightSummaryComponent = md
    ? FlightSummaryDesktop
    : FlightSummaryMobile;

  return <FlightSummaryComponent placement={placement} />;
};

FlightSummary.propTypes = {
  placement: PropTypes.string,
};

export default FlightSummary;
