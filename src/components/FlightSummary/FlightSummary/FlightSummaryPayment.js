import React from 'react';
import FlightSummary from './FlightSummary';

const FlightSummaryPayment = () => {
  return <FlightSummary placement="payment" />;
};

export default FlightSummaryPayment;
