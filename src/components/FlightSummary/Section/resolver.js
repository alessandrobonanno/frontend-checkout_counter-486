import technicalStopoverResolver from '../TechnicalStopover/resolver';

const extendLocation = (location, terminal) => ({
  ...location,
  terminal,
  id: location.id + (terminal ? '_' + terminal : ''),
});

const addLocationType = (location, type) => ({
  ...location,
  type,
});

const createTrip = (
  section,
  { departure, destination, departureDate, arrivalDate, id, duration }
) => ({
  ...section,
  __typename: 'Trip',
  id,
  departure,
  destination,
  departureDate,
  arrivalDate,
  duration,
});

export default function sectionResolver(
  section,
  isLastInSegment,
  isFirstSegment
) {
  const technicalStops = [];
  const trips = [];
  let departureLocation = extendLocation(
    addLocationType(
      section.departure,
      isFirstSegment ? 'absolute' : 'connection'
    ),
    section.departureTerminal
  );
  let tripDepartureDate = section.departureDate;
  section.technicalStops.forEach((technicalStop, index) => {
    trips.push(
      createTrip(section, {
        id: `${section.id}_${index}`,
        departure: departureLocation,
        departureDate: tripDepartureDate,
        destination: addLocationType(technicalStop.location, 'connection'),
        arrivalDate: technicalStop.arrivalDate,
        duration: section.duration,
      })
    );
    technicalStops.push(technicalStopoverResolver(technicalStop));
    departureLocation = addLocationType(technicalStop.location, 'connection');
    tripDepartureDate = technicalStop.departureDate;
  });

  trips.push(
    createTrip(section, {
      id: `${section.id}_${technicalStops.length}`,
      departure: departureLocation,
      destination: extendLocation(
        addLocationType(
          section.destination,
          isLastInSegment ? 'absolute' : 'connection'
        ),
        section.arrivalTerminal
      ),
      departureDate: tripDepartureDate,
      arrivalDate: section.arrivalDate,
      duration: section.duration,
    })
  );

  return {
    ...section,
    trips,
    technicalStops,
  };
}
