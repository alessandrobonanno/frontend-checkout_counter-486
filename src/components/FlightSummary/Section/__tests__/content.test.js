import React from 'react';
import { screen, render } from '@testing-library/react';
import Section from '../';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';
import generateSegments from '../../../../serverMocks/Itinerary';
import sectionResolver from '../resolver';

const TOURIST_LABEL = 'Economy label';
const PREMIUM_ECONOMY_LABEL = 'Premium economy label';
const FIRST_LABEL = 'First label';
const ECONOMIC_DISCOUNTED_LABEL = 'Economy';
const BUSINESS_LABEL = 'Business';
const STOP_DURATION_LABEL = 'Stop duration label';

const translations = {
  flightSummary: {
    section: {
      operatingBy: 'Operated by',
    },
    segment: {
      duration: "{{hours}}h {{minutes}}'",
    },
    cabinClass: {
      TOURIST: TOURIST_LABEL,
      PREMIUM_ECONOMY: PREMIUM_ECONOMY_LABEL,
      FIRST: FIRST_LABEL,
      ECONOMIC_DISCOUNTED: ECONOMIC_DISCOUNTED_LABEL,
      BUSINESS: BUSINESS_LABEL,
    },
    stop: {
      duration: STOP_DURATION_LABEL,
    },
  },
};

describe('Section', () => {
  test('Section desktop content', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      false,
      'CHECKIN_INCLUDED',
      false,
      false
    );
    const section = sectionResolver(segments[0].sections[0]);
    render(
      <TestingProviders translations={translations}>
        <Section section={section} index={0} numberOfSections={1} />
      </TestingProviders>
    );

    expect(await screen.findByText('06:30')).toBeVisible();
    expect(await screen.findByText('MAD')).toBeVisible();
    expect(await screen.findByText('09:45')).toBeVisible();
    expect(await screen.findByText('BCN')).toBeVisible();
    expect(await screen.findAllByText("4h 10'")).toHaveLength(1);
    expect(await screen.findAllByText('29 Apr')).toHaveLength(2);
    expect(await screen.findByText('Ryanair AD2654')).toBeVisible();
    expect(
      await screen.findByText(
        'Adolfo Suárez Madrid - Barajas, T1, Madrid (Spain)'
      )
    ).toBeVisible();
    expect(
      await screen.findByText('El Prat, T3, Barcelona (Spain)')
    ).toBeVisible();
    expect(await screen.findByText('320')).toBeVisible();
    expect(await screen.findByText(ECONOMIC_DISCOUNTED_LABEL)).toBeVisible();
  });

  test('Section desktop content with operating carrier', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      false,
      'CHECKIN_INCLUDED',
      true,
      false
    );
    const section = sectionResolver(segments[0].sections[0]);
    render(
      <TestingProviders translations={translations}>
        <Section section={section} index={0} numberOfSections={1} />
      </TestingProviders>
    );

    expect(await screen.findByText('Operated by: Ryanair')).toBeVisible();
  });

  test('Section desktop content with technical stopover', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      false,
      true,
      'CHECKIN_INCLUDED',
      false,
      false
    );
    const section = sectionResolver(segments[0].sections[0]);
    render(
      <TestingProviders translations={translations}>
        <Section section={section} index={0} numberOfSections={1} />
      </TestingProviders>
    );

    expect(await screen.findByText('06:30')).toBeVisible();
    expect(await screen.findByText('MAD')).toBeVisible();
    expect(await screen.findByText('07:15')).toBeVisible();
    expect(await screen.findAllByText('ZRH')).toHaveLength(2);
    expect(await screen.findByText('07:40')).toBeVisible();
    expect(await screen.findByText('09:45')).toBeVisible();
    expect(await screen.findByText('BCN')).toBeVisible();
    expect(await screen.findAllByText("4h 10'")).toHaveLength(2);
    expect(await screen.findAllByText('29 Apr')).toHaveLength(4);
    expect(
      await screen.findByText(
        'Adolfo Suárez Madrid - Barajas, T1, Madrid (Spain)'
      )
    ).toBeVisible();
    expect(
      await screen.findByText('Zurich, T3, Zurich (Switzerland)')
    ).toBeVisible();
    expect(
      await screen.findByText('Zurich, T1, Zurich (Switzerland)')
    ).toBeVisible();
    expect(
      await screen.findByText('El Prat, T3, Barcelona (Spain)')
    ).toBeVisible();
  });
});
