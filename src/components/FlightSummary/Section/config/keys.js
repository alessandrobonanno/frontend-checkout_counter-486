const translations = [
  {
    inputPath: ['itineraryDetails', 'flightRow.operatingAirline'],
    outputPath: ['flightSummary', 'section', 'operatingBy'],
    scope: ['?ARABIC_SITES'],
  },
];

module.exports = translations;
