import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Text, useDirection } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';
import DurationWithIcon from '../Duration/DurationWithIcon';
import Carrier from '../Carrier';
import VehicleModel from '../VehicleModel';
import CabinClass from '../CabinClass';
import SectionLocationInfo from './SectionLocationInfo';
import DecorationLineComponent from './DecorationLineComponent';

const SectionTripBluestone = ({
  trip,
  index,
  numberOfSections,
  numberOfTrips,
  isLastSection,
}) => {
  const {
    departure,
    departureDate,
    departureTerminal,
    destination,
    arrivalDate,
    arrivalTerminal,
    duration,
    carrier,
    flightCode,
    transportType,
    vehicleModel,
    cabinClass,
    operatingCarrier,
  } = trip;
  const { t } = useTranslation();
  const isRtl = useDirection();
  const isLastTrip = index === numberOfTrips - 1;
  const hasMultipleTrips = numberOfTrips > 1 || numberOfSections > 1;
  const lineType = isLastSection && isLastTrip ? 'none' : 'solid';

  return (
    <Flex px={0} py={0}>
      {hasMultipleTrips && <DecorationLineComponent lineType={lineType} />}
      <Flex
        flexDirection="column"
        pl={!isRtl ? 1 : 0}
        pr={isRtl ? 1 : 0}
        pb={2}
        flexGrow={1}
      >
        <Flex justifyContent={'space-between'} alignItems={'flex-start'}>
          <Flex alignItems={'stretch'} flexGrow={1} width={'37%'}>
            <Box pl={0} pr={0}>
              <SectionLocationInfo
                location={departure}
                date={departureDate}
                terminal={departureTerminal}
              />
            </Box>
          </Flex>

          <Flex
            flexDirection={'column'}
            alignItems={'flex-start'}
            flexGrow={1}
            width={'37%'}
          >
            <SectionLocationInfo
              location={destination}
              date={arrivalDate}
              terminal={arrivalTerminal}
            />
          </Flex>

          <Flex
            flexDirection={'column'}
            alignItems={'flex-start'}
            flexGrow={1}
            width={'26%'}
          >
            <Box mb={1}>
              <DurationWithIcon duration={duration} />
            </Box>
            <Box mb={1}>
              <Carrier
                carriers={[carrier]}
                flightCode={flightCode}
                placement="section"
              />
            </Box>
            {operatingCarrier && (
              <Flex mb={1} flexDirection="row">
                <Text color="neutrals.3" fontSize="body.1">
                  {t('flightSummary.section.operatingBy')}:{' '}
                  {operatingCarrier.name}
                </Text>
              </Flex>
            )}
            <Flex flexDirection="row" alignItems="center">
              {vehicleModel && (
                <Box mr={5}>
                  <VehicleModel
                    model={vehicleModel}
                    transportType={transportType}
                  />
                </Box>
              )}
              {cabinClass && (
                <Box>
                  <CabinClass type={cabinClass} />
                </Box>
              )}
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

SectionTripBluestone.propTypes = {
  trip: PropTypes.object.isRequired,
  index: PropTypes.number,
  numberOfSections: PropTypes.number,
  numberOfTrips: PropTypes.number,
  isLastSection: PropTypes.bool,
};

export default SectionTripBluestone;
