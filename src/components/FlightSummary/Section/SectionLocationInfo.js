import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Body, Text, DateTime } from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SectionDateLocationVersioned = ({ location, date, versionConfigs }) => {
  const { secondFontWeight, iataMarginLeft } = versionConfigs;

  return (
    <Flex justifyContent={'space-between'} alignItems={'center'}>
      <Box mb={1}>
        <Text color="neutrals.1" fontSize="body.2" fontWeight="medium">
          <DateTime value={date} type="time" pattern="short" />
        </Text>
      </Box>
      <Box ml={iataMarginLeft} mb={1}>
        <Text
          color="neutrals.1"
          fontSize="body.2"
          fontWeight={secondFontWeight}
        >
          {location.iata}
        </Text>
      </Box>
    </Flex>
  );
};

const SectionDateLocation = cobaltDesktopThemedComponentVersion(
  SectionDateLocationVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { iataMarginLeft: 1, secondFontWeight: 'medium' },
    C: { iataMarginLeft: 2, secondFontWeight: 'normal' },
  }
);

SectionDateLocation.propTypes = {
  location: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
  terminal: PropTypes.string,
};
export { SectionDateLocation };

const SectionDateLocationExtraVersioned = ({
  location,
  date,
  terminal,
  versionConfigs,
}) => {
  const { secondFontWeight, bodyType } = versionConfigs;

  return (
    <>
      <Box>
        <Body
          color="neutrals.3"
          fontSize="body.1"
          fontWeight={secondFontWeight}
          type={bodyType}
        >
          <DateTime value={date} format={'dd LLL'} />
        </Body>
      </Box>
      <Box>
        <Body color="neutrals.3" fontSize="body.1" type={bodyType}>
          {location.name}, {terminal && 'T' + terminal + ','}{' '}
          {location.cityName} ({location.countryName})
        </Body>
      </Box>
    </>
  );
};

const SectionDateLocationExtra = cobaltDesktopThemedComponentVersion(
  SectionDateLocationExtraVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { bodyType: 'base', secondFontWeight: 'medium' },
    C: { bodyType: 'small', secondFontWeight: 'normal' },
  }
);

SectionDateLocationExtra.propTypes = {
  location: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
  terminal: PropTypes.string,
};
export { SectionDateLocationExtra };

const SectionLocationInfo = ({ ...props }) => {
  return (
    <Flex flexDirection={'column'} alignItems={'flex-start'}>
      <SectionDateLocation {...props} />
      <SectionDateLocationExtra {...props} />
    </Flex>
  );
};

SectionLocationInfo.propTypes = {
  location: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
  terminal: PropTypes.string,
};

export default SectionLocationInfo;
