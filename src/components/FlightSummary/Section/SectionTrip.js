import React from 'react';
import PropTypes from 'prop-types';

import SectionTripBluestone from './SectionTripBluestone';
import SectionTripCobalt from './SectionTripCobalt';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

const SectionTrip = ({
  trip,
  index,
  numberOfSections,
  numberOfTrips,
  isLastSection,
}) => {
  const ThemedSectionTripComponent = cobaltDesktopFlightDetailsComponent([
    SectionTripBluestone,
    null,
    SectionTripCobalt,
  ]);

  return (
    <ThemedSectionTripComponent
      trip={trip}
      index={index}
      numberOfSections={numberOfSections}
      numberOfTrips={numberOfTrips}
      isLastSection={isLastSection}
    />
  );
};

SectionTrip.propTypes = {
  trip: PropTypes.object.isRequired,
  index: PropTypes.number,
  numberOfSections: PropTypes.number,
  numberOfTrips: PropTypes.number,
  isLastSection: PropTypes.bool,
};

export default SectionTrip;
