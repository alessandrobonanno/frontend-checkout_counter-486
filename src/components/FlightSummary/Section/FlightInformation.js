import React from 'react';
import PropTypes from 'prop-types';
import { FlexRow, Box, Text, Col } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

import CarrierLogo from '../Carrier/CarrierLogo';

const FlightInformation = ({
  carrier,
  vehicleModel,
  transportType,
  cabinClass,
  flightCode,
  operatingCarrier,
}) => {
  const { t } = useTranslation();

  const fontSize = 'bodies.xSmall';
  const textColor = 'neutrals.3';

  const PrefixBallotWrapper = ({ children }) => (
    <>
      <Text color={textColor} fontSize={fontSize}>
        {'\u2022'}
      </Text>
      {children}
    </>
  );

  return (
    <FlexRow flexDirection="row" alignItems="center" mt={3} gap={2}>
      <Col>
        <Box pt={1}>
          <CarrierLogo id={carrier.id} name={carrier.name} imageSize="medium" />
        </Box>
      </Col>

      <Col>
        <Text color={textColor} fontSize={fontSize} ml={1}>
          {carrier.name}
          {flightCode && ` ${flightCode}`}
        </Text>
      </Col>

      {operatingCarrier && (
        <PrefixBallotWrapper>
          <Col>
            <Text color={textColor} fontSize={fontSize} ml={1}>
              {t('flightSummary.section.operatingBy')} {operatingCarrier.name}
            </Text>
          </Col>
        </PrefixBallotWrapper>
      )}

      {vehicleModel && (
        <PrefixBallotWrapper>
          <Col>
            <Text color={textColor} fontSize={fontSize} ml={1}>
              {transportType === 'TRAIN' ? 'TRAIN' : vehicleModel}
            </Text>
          </Col>
        </PrefixBallotWrapper>
      )}

      {cabinClass && (
        <PrefixBallotWrapper>
          <Col>
            <Text color={textColor} fontSize={fontSize} ml={1}>
              {t(`flightSummary.cabinClass.${cabinClass}`)}
            </Text>
          </Col>
        </PrefixBallotWrapper>
      )}
    </FlexRow>
  );
};

FlightInformation.propTypes = {
  carrier: PropTypes.object.isRequired,
  vehicleModel: PropTypes.string,
  transportType: PropTypes.string,
  cabinClass: PropTypes.string,
  flightCode: PropTypes.string,
  operatingCarrier: PropTypes.object,
};

export default FlightInformation;
