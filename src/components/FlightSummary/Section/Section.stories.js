import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import Section from './';
import generateSegments from '../../../serverMocks/Itinerary';
import sectionResolver from './resolver';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/Section',
  component: Section,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  const segments = generateSegments(
    1,
    false,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const section = sectionResolver(segments[0].sections[0]);
  return (
    <Section
      section={section}
      index={args.index}
      numberOfSections={args.numberOfSections}
    />
  );
};
Default.decorators = [withKnobs];
Default.args = {
  withTechnicalStopovers: false,
  withOperatingCarrier: true,
  index: 0,
  numberOfSections: 1,
};
Default.argTypes = {
  baggageCondition: {
    control: {
      type: 'select',
      options: ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLINE'],
    },
    defaultValue: 'CHECKIN_INCLUDED',
  },
};

export default story;
