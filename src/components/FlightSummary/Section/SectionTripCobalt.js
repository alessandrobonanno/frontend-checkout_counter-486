import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, useDirection } from 'prisma-design-system';
import {
  SectionDateLocation,
  SectionDateLocationExtra,
} from './SectionLocationInfo';
import SegmentTrip from '../SegmentTrip';
import FlightInformation from './FlightInformation';
import DecorationLineComponent from './DecorationLineComponent';

const SectionTripCobalt = ({
  trip,
  index,
  numberOfSections,
  numberOfTrips,
  isLastSection,
}) => {
  const {
    departure,
    departureDate,
    departureTerminal,
    destination,
    arrivalDate,
    arrivalTerminal,
    duration,
    carrier,
    flightCode,
    transportType,
    vehicleModel,
    cabinClass,
    operatingCarrier,
  } = trip;
  const isRtl = useDirection();
  const isLastTrip = index === numberOfTrips - 1;
  const hasMultipleTrips = numberOfTrips > 1 || numberOfSections > 1;
  const lineType = isLastSection && isLastTrip ? 'none' : 'solid';

  return (
    <Flex px={5} py={5}>
      <Flex flexDirection="column" pl={0} pr={0} flexGrow={1}>
        <Flex justifyContent={'space-between'} alignItems={'flex-start'}>
          <Flex alignItems={'stretch'}>
            {hasMultipleTrips && (
              <DecorationLineComponent lineType={lineType} />
            )}
            <Box pl={!isRtl ? 5 : 0} pr={isRtl ? 5 : 0}>
              <SectionDateLocation
                location={departure}
                date={departureDate}
                terminal={departureTerminal}
              />
            </Box>
          </Flex>

          <Flex
            flexDirection={'column'}
            alignItems={'center'}
            flexGrow={1}
            width={'26%'}
            mx={8}
            mt="10px"
          >
            <Flex
              flexDirection={'column'}
              alignItems={'stretch'}
              flexGrow={1}
              width={'100%'}
            >
              <SegmentTrip
                transportTypes={[transportType]}
                duration={duration}
                invertOrder={true}
              />
            </Flex>
          </Flex>

          <Flex
            flexDirection={'column'}
            alignItems={'flex-start'}
            minWidth={5}
            maxWidth={5}
          >
            <SectionDateLocation
              location={destination}
              date={arrivalDate}
              terminal={arrivalTerminal}
            />
          </Flex>
        </Flex>

        <Flex justifyContent={'space-between'} alignItems={'flex-start'}>
          <Flex alignItems={'stretch'} flexGrow={1} width={'25%'}>
            {hasMultipleTrips && (
              <DecorationLineComponent
                lineType={lineType}
                decorationType="none"
              />
            )}
            <Box ml={!isRtl ? 5 : 0} mr={isRtl ? 5 : 0} maxWidth={'170px'}>
              <SectionDateLocationExtra
                location={departure}
                date={departureDate}
                terminal={departureTerminal}
              />
            </Box>
          </Flex>

          <Flex
            flexDirection={'column'}
            alignItems={'flex-start'}
            minWidth={5}
            maxWidth={5}
          >
            <SectionDateLocationExtra
              location={destination}
              date={arrivalDate}
              terminal={arrivalTerminal}
            />
          </Flex>
        </Flex>

        <>
          <Flex>
            {hasMultipleTrips && (
              <DecorationLineComponent
                lineType={lineType}
                decorationType="none"
              />
            )}
            <Box pl={!isRtl ? 5 : 0} pr={isRtl ? 5 : 0}>
              <FlightInformation
                carrier={carrier}
                vehicleModel={vehicleModel}
                transportType={transportType}
                cabinClass={cabinClass}
                flightCode={flightCode}
                operatingCarrier={operatingCarrier}
              />
            </Box>
          </Flex>
        </>
      </Flex>
    </Flex>
  );
};

SectionTripCobalt.propTypes = {
  trip: PropTypes.object.isRequired,
  index: PropTypes.number,
  numberOfSections: PropTypes.number,
  numberOfTrips: PropTypes.number,
  isLastSection: PropTypes.bool,
};

export default SectionTripCobalt;
