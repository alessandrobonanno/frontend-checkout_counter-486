import React from 'react';
import { DecorationLine } from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const DecorationLineComponentVersioned = ({
  lineType = 'solid',
  decorationType = 'circle',
  versionConfigs,
}) => {
  const { decorationLineTheme, decorationLineLineBottom, decorationLineWidth } =
    versionConfigs;

  return (
    <DecorationLine
      decorationType={decorationType}
      theme={decorationLineTheme}
      lineType={lineType}
      lineGrey={decorationLineTheme === 'grey'}
      lineBottom={decorationLineLineBottom}
      width={decorationLineWidth}
    />
  );
};

const DecorationLineComponent = cobaltDesktopThemedComponentVersion(
  DecorationLineComponentVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: {
      decorationLineTheme: 'grey',
      decorationLineLineBottom: undefined,
      decorationLineWidth: undefined,
    },
    C: {
      decorationLineTheme: 'primary',
      decorationLineLineBottom: '-48px',
      decorationLineWidth: 2,
    },
  }
);

export default DecorationLineComponent;
