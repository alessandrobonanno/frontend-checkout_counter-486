import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import SectionTrip from './SectionTrip';
import TechnicalStopover from '../TechnicalStopover';

const Section = ({ section, index, numberOfSections }) => {
  const numberOfTrips = section.trips.length;
  const isLastSection = index === numberOfSections - 1;
  return (
    <>
      {section.trips.map((trip, idx) => (
        <Fragment key={trip.id}>
          <SectionTrip
            trip={trip}
            index={idx}
            numberOfSections={numberOfSections}
            numberOfTrips={numberOfTrips}
            isLastSection={isLastSection}
          />
          {idx + 1 !== section.trips.length && section.technicalStops[idx] && (
            <TechnicalStopover
              duration={section.technicalStops[idx].duration}
            />
          )}
        </Fragment>
      ))}
    </>
  );
};

Section.propTypes = {
  section: PropTypes.object.isRequired,
  index: PropTypes.number,
  numberOfSections: PropTypes.number,
};

export default Section;
