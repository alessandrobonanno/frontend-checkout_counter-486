import React from 'react';
import { screen, render } from '@testing-library/react';
import SegmentDuration from '../';
import { TestingProviders } from '../../../../testUtils';

const translations = {
  flightSummary: {
    segment: {
      duration: "{{hours}}h {{minutes}}'",
    },
  },
};

describe('SegmentDuration', () => {
  test('Segment duration is displayed correctly', async () => {
    render(
      <TestingProviders translations={translations}>
        <SegmentDuration duration={105} />
      </TestingProviders>
    );

    expect(await screen.findByText("1h 45'")).toBeVisible();
  });
});
