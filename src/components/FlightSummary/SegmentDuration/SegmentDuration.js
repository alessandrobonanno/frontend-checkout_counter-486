import React from 'react';
import PropTypes from 'prop-types';
import { Text, useMediaQueries } from 'prisma-design-system';
import Duration from '../Duration';

const SegmentDuration = ({ duration }) => {
  const { md } = useMediaQueries();
  const fontSize = md ? 'body.2' : 'body.1';
  const color = md ? 'neutrals.1' : 'neutrals.3';

  return (
    <Text fontSize={fontSize} color={color} preserveWhitespace={true}>
      <Duration durationInMinutes={duration} />
    </Text>
  );
};

SegmentDuration.propTypes = {
  duration: PropTypes.number.isRequired,
};

export default SegmentDuration;
