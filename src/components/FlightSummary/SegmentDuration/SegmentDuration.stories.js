import React from 'react';
import SegmentDuration from './index';

const story = {
  title: 'Components/FlightSummary/SegmentDuration',
  component: SegmentDuration,
};

export const Default = (args) => {
  return <SegmentDuration {...args} />;
};

Default.args = {
  duration: 60,
};

export default story;
