import React from 'react';
import DestinationTime from './index';

const story = {
  title: 'Components/FlightSummary/DestinationTime',
  component: DestinationTime,
};

export const Default = (args) => {
  return <DestinationTime {...args} />;
};

Default.args = {
  departureDate: '2021-07-22T23:30:00+02:00',
  arrivalDate: '2021-07-23T01:15:00+01:00',
};

export default story;
