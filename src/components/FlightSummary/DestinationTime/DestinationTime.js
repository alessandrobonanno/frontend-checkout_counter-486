import React from 'react';
import PropTypes from 'prop-types';
import { Box, Text, Flex, useMediaQueries } from 'prisma-design-system';
import { differenceInCalendarDays, parseISO } from 'date-fns';
import SegmentTime from '../SegmentTime';

const parseLocalDay = (ISODateString = '') => {
  const [localDay] = ISODateString.match(/^\d+-\d{2}-\d{2}/) || [];
  return parseISO(localDay);
};

const howManyTravelDays = (arrivalDate, departureDate) => {
  const localDepartureDay = parseLocalDay(departureDate);
  const localDestinationDay = parseLocalDay(arrivalDate);
  if (!localDepartureDay || !localDestinationDay) {
    return undefined;
  }
  return differenceInCalendarDays(localDestinationDay, localDepartureDay);
};

const OtherDay = ({ days }) => {
  const otherDay = days !== 0;
  const daysSign = Math.sign(days) < 0 ? '-' : '+';

  return (
    <Box mt={-1}>
      <Text color="neutrals.0" fontSize="body.0">
        {otherDay && `${daysSign}${Math.abs(days)}`}
      </Text>
    </Box>
  );
};

const DestinationTime = ({ arrivalDate, departureDate }) => {
  const { md } = useMediaQueries();

  const travelDays = howManyTravelDays(arrivalDate, departureDate);

  return (
    <Flex justifyContent={'flex-end'}>
      <SegmentTime date={arrivalDate} />
      {!md && travelDays ? <OtherDay days={travelDays} /> : null}
    </Flex>
  );
};

DestinationTime.propTypes = {
  arrivalDate: PropTypes.string.isRequired,
  departureDate: PropTypes.string.isRequired,
};

export default DestinationTime;
