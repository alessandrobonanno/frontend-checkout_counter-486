import React from 'react';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const SummaryTitle = () => {
  const { t } = useTranslation();
  return (
    <Text fontSize={'heading.1'} fontWeight={'bold'} color={'neutrals.1'}>
      {t('flightSummary.summary.title')}
    </Text>
  );
};

export default SummaryTitle;
