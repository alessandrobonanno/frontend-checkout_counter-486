import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import SummaryTitle from './index';

const story = {
  title: 'Components/FlightSummary/SummaryTitle',
  component: SummaryTitle,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <SummaryTitle {...args} />;
};

export default story;
