const translations = [
  {
    inputPath: ['collapsibleFlightSummary', 'tripsummary.title'],
    outputPath: ['flightSummary', 'summary', 'title'],
  },
];

module.exports = translations;
