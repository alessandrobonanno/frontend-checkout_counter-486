import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import SummaryTitle from '..';

const translations = {
  flightSummary: {
    summary: {
      title: 'Your trip summary',
    },
  },
};

describe('SummaryTitle', () => {
  test('Flight summary title content', async () => {
    render(
      <TestingProviders translations={translations}>
        <SummaryTitle />
      </TestingProviders>
    );

    expect(await screen.findByText('Your trip summary')).toBeVisible();
  });
});
