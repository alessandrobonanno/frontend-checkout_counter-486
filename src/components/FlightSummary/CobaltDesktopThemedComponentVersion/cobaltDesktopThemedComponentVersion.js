import React from 'react';
import PropTypes from 'prop-types';

export const cobaltDesktopThemedComponentVersion = (
  ThemedComponent,
  versionedComponentFunction,
  configs = {}
) => {
  const partitionArray = ['A', 'B', 'C'];

  configs.A = configs.A || {};

  return versionedComponentFunction(
    partitionArray.map((key) => {
      const configKeyToLoad = configs[key] ? key : partitionArray[0];

      return ({ ...props }) => (
        <ThemedComponent versionConfigs={configs[configKeyToLoad]} {...props} />
      );
    })
  );
};
cobaltDesktopThemedComponentVersion.propTypes = {
  ThemedComponent: PropTypes.any,
  versionedComponentFunction: PropTypes.any,
  configs: PropTypes.objectOf(PropTypes.object),
};
