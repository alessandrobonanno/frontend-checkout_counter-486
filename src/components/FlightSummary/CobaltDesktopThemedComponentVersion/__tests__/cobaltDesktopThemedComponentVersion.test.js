import React from 'react';
import { render, screen } from '@testing-library/react';
import { versionedComponent } from '@frontend-shell/react-hooks';
import { cobaltDesktopThemedComponentVersion } from '../cobaltDesktopThemedComponentVersion';
import TestingProviders from '../../../../testUtils/providers/TestingProviders';

const VersionableComponent = ({ ...props }) => (
  <span>
    {' '}
    VersionableConstantComponent:{'-' + props.versionConfigs.text + '-'}{' '}
  </span>
);

const versionConfigsAux = {
  A: { text: 'version A' },
  B: { text: 'version B' },
};

const SOME_ALIAS = 'SOME_ALIAS';

const testVersionedComponent = versionedComponent(SOME_ALIAS);

describe('VersionedComponents', () => {
  describe('When alias for B is active', () => {
    const abs = [{ alias: SOME_ALIAS, partition: 2 }];

    const VersionedComponent = cobaltDesktopThemedComponentVersion(
      VersionableComponent,
      testVersionedComponent,
      versionConfigsAux
    );

    test('should render VersionableComponent with configuration of B', async () => {
      render(
        <TestingProviders abs={abs}>
          <VersionedComponent />
        </TestingProviders>
      );

      expect(await screen.findByText(/version B/)).toBeVisible();
    });
    test('should not render configuration from A', async () => {
      const SENTINEL_TEXT = 'sentinel text';

      render(
        <TestingProviders abs={abs}>
          <VersionedComponent />
          <div>{SENTINEL_TEXT}</div>
        </TestingProviders>
      );

      expect(await screen.findByText(SENTINEL_TEXT)).toBeVisible();
      expect(screen.queryByText(/version A/)).toBeNull();
    });
  });

  describe('When alias is active for a partition that has no configuration', () => {
    const abs = [{ alias: SOME_ALIAS, partition: 3 }];
    const VersionedComponent = cobaltDesktopThemedComponentVersion(
      VersionableComponent,
      testVersionedComponent,
      versionConfigsAux
    );
    test('should render configuration from A', async () => {
      render(
        <TestingProviders abs={abs}>
          <VersionedComponent />
        </TestingProviders>
      );

      expect(await screen.findByText(/-version A-/)).toBeVisible();
    });
  });

  let sameTestDifferentPartition = (partition) => {
    const abs = [{ alias: 'FBO_BOOK3467', partition: 1 }];

    const VersionedComponent = cobaltDesktopThemedComponentVersion(
      VersionableComponent,
      testVersionedComponent,
      versionConfigsAux
    );
    render(
      <TestingProviders abs={abs}>
        <VersionedComponent />
      </TestingProviders>
    );
  };

  describe('Same component is rendered for every partition equally', () => {
    test('In partition A', async () => {
      sameTestDifferentPartition(1);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeVisible();
    });
    test('In partition B', async () => {
      sameTestDifferentPartition(2);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeVisible();
    });
    test('In partition C', async () => {
      sameTestDifferentPartition(3);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeVisible();
    });
  });

  sameTestDifferentPartition = (selectedPartition) => {
    const abs = [{ alias: 'FBO_BOOK3467', partition: selectedPartition }];

    const VersionedComponent = cobaltDesktopThemedComponentVersion(
      VersionableComponent,
      testVersionedComponent,
      versionConfigsAux
    );
    render(
      <TestingProviders abs={abs}>
        <VersionedComponent />
      </TestingProviders>
    );
  };

  describe('Same component is rendered for every partition equally', () => {
    test('In partition A', async () => {
      sameTestDifferentPartition(1);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeInTheDocument();
    });
    test('In partition B', async () => {
      sameTestDifferentPartition(2);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeInTheDocument();
    });
    test('In partition C', async () => {
      sameTestDifferentPartition(3);

      expect(
        await screen.findByText(/VersionableConstantComponent/)
      ).toBeInTheDocument();
    });
  });
});
