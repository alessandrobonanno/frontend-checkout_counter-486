import React from 'react';
import { screen, render } from '@testing-library/react';
import SectionList from '../';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';
import generateSegments from '../../../../serverMocks/Itinerary';
import segmentResolver from '../../Segment/resolver';

const TOURIST_LABEL = 'Economy label';
const PREMIUM_ECONOMY_LABEL = 'Premium economy label';
const FIRST_LABEL = 'First label';
const ECONOMIC_DISCOUNTED_LABEL = 'Economy';
const BUSINESS_LABEL = 'Business';
const STOP_DURATION_LABEL = 'Stop duration label';

const translations = {
  flightSummary: {
    section: {
      operatingBy: 'Operated by',
    },
    segment: {
      duration: "{{hours}}h {{minutes}}'",
    },
    cabinClass: {
      TOURIST: TOURIST_LABEL,
      PREMIUM_ECONOMY: PREMIUM_ECONOMY_LABEL,
      FIRST: FIRST_LABEL,
      ECONOMIC_DISCOUNTED: ECONOMIC_DISCOUNTED_LABEL,
      BUSINESS: BUSINESS_LABEL,
    },
    stop: {
      duration: STOP_DURATION_LABEL,
    },
  },
};

describe('SectionList', () => {
  test('SectionList desktop content with stopover', async () => {
    setViewport({ width: 1200 });
    const segments = generateSegments(
      1,
      true,
      false,
      'CHECKIN_INCLUDED',
      false,
      false
    );
    const segment = segmentResolver(segments[0]);
    render(
      <TestingProviders translations={translations}>
        <SectionList sections={segment.sections} stops={segment.stops} />
      </TestingProviders>
    );

    expect(await screen.findByText('06:30')).toBeVisible();
    expect(await screen.findByText('MAD')).toBeVisible();
    expect(await screen.findByText('09:45')).toBeVisible();
    expect(await screen.findAllByText('BCN')).toHaveLength(2);
    expect(await screen.findByText('10:30')).toBeVisible();
    expect(await screen.findByText('11:45')).toBeVisible();
    expect(await screen.findByText('LGW')).toBeVisible();
    expect(await screen.findAllByText("4h 10'")).toHaveLength(2);
    expect(await screen.findAllByText('29 Apr')).toHaveLength(4);
    expect(
      await screen.findByText(
        'Adolfo Suárez Madrid - Barajas, T1, Madrid (Spain)'
      )
    ).toBeVisible();
    expect(
      await screen.findByText('Gatwick, T1, London (United Kingdom)')
    ).toBeVisible();
    expect(
      await screen.findAllByText('El Prat, T3, Barcelona (Spain)')
    ).toHaveLength(2);
  });
});
