import React from 'react';
import SectionList from '.';
import generateSegments from '../../../serverMocks/Itinerary';
import segmentResolver from '../Segment/resolver';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/SectionList',
  component: SectionList,
  parameters: {
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  const segments = generateSegments(
    1,
    args.withStopovers,
    args.withTechnicalStopovers,
    args.baggageCondition,
    args.withOperatingCarrier,
    false
  );
  const segment = segmentResolver(segments[0]);
  return <SectionList sections={segment.sections} stops={segment.stops} />;
};

Default.decorators = [withKnobs];
Default.args = {
  withStopovers: false,
  withTechnicalStopovers: false,
  withOperatingCarrier: true,
};
Default.argTypes = {
  baggageCondition: {
    control: {
      type: 'select',
      options: ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLINE'],
    },
    defaultValue: 'CHECKIN_INCLUDED',
  },
};

export default story;
