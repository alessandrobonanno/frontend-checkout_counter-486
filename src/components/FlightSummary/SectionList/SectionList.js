import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Section from '../Section';
import Stopover from '../Stopover';

const SectionList = ({ sections, stops }) => {
  const numberOfSections = sections.length;
  return (
    <>
      {sections.map((section, idx) => (
        <Fragment key={`${section.id}_${idx}`}>
          <Section
            section={section}
            index={idx}
            numberOfSections={numberOfSections}
          />
          {idx + 1 !== sections.length && stops[idx] && (
            <Stopover {...stops[idx]} />
          )}
        </Fragment>
      ))}
    </>
  );
};

SectionList.propTypes = {
  sections: PropTypes.array.isRequired,
};

export default SectionList;
