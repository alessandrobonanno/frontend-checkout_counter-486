const translations = [
  {
    inputPath: ['flightsummary', 'stopInfo.airlineChange'],
    outputPath: ['flightSummary', 'stop', 'airlineChange'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.airlineChange.TRAINS'],
    outputPath: ['flightSummary', 'stop', 'trainChange'],
  },
];

module.exports = translations;
