import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import CarrierChange from '..';

const AIRLINE_CHANGE_TEXT = 'airline change';
const TRAIN_CHANGE_TEXT = 'train change';

const translations = {
  flightSummary: {
    stop: {
      airlineChange: AIRLINE_CHANGE_TEXT,
      trainChange: TRAIN_CHANGE_TEXT,
    },
  },
};

describe('CarrierChange', () => {
  test('default - airline change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierChange />
      </TestingProviders>
    );
    expect(await screen.findByText(AIRLINE_CHANGE_TEXT)).toBeVisible();
  });

  test('flight - airline change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierChange transportType={'PLANE'} />
      </TestingProviders>
    );
    expect(await screen.findByText(AIRLINE_CHANGE_TEXT)).toBeVisible();
  });

  test('train - train change', async () => {
    render(
      <TestingProviders translations={translations}>
        <CarrierChange transportType={'TRAIN'} />
      </TestingProviders>
    );
    expect(await screen.findByText(TRAIN_CHANGE_TEXT)).toBeVisible();
  });
});
