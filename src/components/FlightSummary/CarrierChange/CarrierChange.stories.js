import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import CarrierChange from './';
import configKeys from './config/keys';

const story = {
  title: 'Components/FlightSummary/CarrierChange',
  component: CarrierChange,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <CarrierChange {...args} />;
};

export default story;
