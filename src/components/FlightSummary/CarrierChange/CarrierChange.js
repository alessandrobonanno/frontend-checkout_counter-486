import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from '@frontend-shell/react-hooks';
import {
  Box,
  TrainFrontIcon,
  FlightRightIcon,
  Text,
  Flex,
  useGetThemeVersion,
} from 'prisma-design-system';

const values = {
  TRAIN: {
    icon: TrainFrontIcon,
    key: 'trainChange',
  },
  PLANE: {
    icon: FlightRightIcon,
    key: 'airlineChange',
  },
};

const theme = {
  blueStone: {
    fontSize: 'body.0',
    iconSize: 'tiny',
  },
  cobalt: {
    fontSize: 'body.1',
    iconSize: 'small',
  },
};

const CarrierChange = ({ transportType }) => {
  const { t } = useTranslation();

  const { fontSize, iconSize } = theme[useGetThemeVersion()];

  const Icon = values[transportType].icon;

  return (
    <Box pb={1}>
      <Flex alignItems="center">
        <Box mr={1}>
          <Icon size={iconSize} color="neutrals.3" />
        </Box>
        <Text color="neutrals.3" fontSize={fontSize}>
          {t(`flightSummary.stop.${values[transportType].key}`)}
        </Text>
      </Flex>
    </Box>
  );
};

CarrierChange.propTypes = {
  transportType: PropTypes.oneOf(['PLANE', 'TRAIN']),
};

CarrierChange.defaultProps = {
  transportType: 'PLANE',
};

export default CarrierChange;
