import React from 'react';
import SegmentDate from './index';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/SegmentDate',
  component: SegmentDate,
  parameters: {
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  return <SegmentDate {...args} />;
};

Default.decorators = [withKnobs];
Default.args = {
  date: '2021-07-22T23:30:00+02:00',
};

export default story;
