import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import SegmentDate from '..';

describe('SegmentDate', () => {
  test('Date is displayed with the correct format', async () => {
    render(
      <TestingProviders>
        <SegmentDate date={'2021-07-22T23:30:00+02:00'} />
      </TestingProviders>
    );

    expect(await screen.findByText('Thu, 22 Jul')).toBeVisible();
  });
});
