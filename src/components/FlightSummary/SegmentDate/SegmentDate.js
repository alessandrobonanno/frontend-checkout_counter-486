import React from 'react';
import PropTypes from 'prop-types';
import { DateTime, Text, Body, useMediaQueries } from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentDateVersioned = ({
  date,
  desktopFontColor = 'neutrals.3',
  versionConfigs,
  ...props
}) => {
  const { md } = useMediaQueries();

  const fontSize = md ? versionConfigs.fontSize : 'body.1';
  const fontColor = md ? desktopFontColor : 'neutrals.2';

  const TextDefault = ({ children }) => (
    <Text color={fontColor} fontSize={fontSize}>
      {children}
    </Text>
  );

  const TextCobalt = ({
    children,
    cobaltColor = 'neutrals.3',
    cobaltType = 'small',
  }) => (
    <Body color={cobaltColor} type={cobaltType} weight="normal">
      {children}
    </Body>
  );

  const VersionedTextStyle = cobaltDesktopFlightDetailsComponent([
    TextDefault,
    TextDefault,
    TextCobalt,
  ]);

  return (
    <VersionedTextStyle {...props}>
      <DateTime value={date} format={'E, dd LLL'} />
    </VersionedTextStyle>
  );
};

const SegmentDate = cobaltDesktopThemedComponentVersion(
  SegmentDateVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { fontSize: 'body.2' },
    C: { fontSize: 'body.1' },
  }
);

SegmentDate.propTypes = {
  date: PropTypes.string.isRequired,
};

export default SegmentDate;
