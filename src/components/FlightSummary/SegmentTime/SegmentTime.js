import React from 'react';
import PropTypes from 'prop-types';
import { DateTime, Heading, useMediaQueries } from 'prisma-design-system';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

import cobaltDesktopThemedComponentVersion from '../CobaltDesktopThemedComponentVersion';

const SegmentTimeVersioned = ({ date, versionConfigs }) => {
  const { md } = useMediaQueries();

  const fontSize = md ? versionConfigs.fontSize : 'heading.0';

  return (
    <Heading
      color="neutrals.1"
      type={versionConfigs.headingType}
      fontSize={fontSize}
      weight={versionConfigs.fontWeight}
    >
      <DateTime value={date} type="time" pattern="short" />
    </Heading>
  );
};

const SegmentTime = cobaltDesktopThemedComponentVersion(
  SegmentTimeVersioned,
  cobaltDesktopFlightDetailsComponent,
  {
    A: { headingType: '3', fontWeight: 'normal' },
    C: { headingType: '2', fontWeight: 'medium' },
  }
);

SegmentTime.propTypes = {
  date: PropTypes.string.isRequired,
};

export default SegmentTime;
