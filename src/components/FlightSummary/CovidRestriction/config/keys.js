const translations = [
  {
    inputPath: ['flightsummary', 'stopInfo.vinmessage.title'],
    outputPath: ['flightSummary', 'stopInfo', 'vinmessage', 'title'],
  },
  {
    inputPath: ['flightsummary', 'stopInfo.vinmessage'],
    outputPath: ['flightSummary', 'stopInfo', 'vinmessage', 'body'],
    args: ['country'],
  },
];

module.exports = translations;
