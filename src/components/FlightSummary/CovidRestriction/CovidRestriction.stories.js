import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import CovidRestriction from './';
import configKeys from './config/keys';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});

const story = {
  title: 'Components/FlightSummary/CovidRestriction',
  component: CovidRestriction,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  return <CovidRestriction {...args} />;
};
Default.decorators = [withKnobs];
Default.args = {
  country: 'Spain',
};

export default story;
