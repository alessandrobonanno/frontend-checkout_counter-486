import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from '@frontend-shell/react-hooks';
import {
  Flex,
  FlexRow,
  Box,
  Body,
  Text,
  InformationIcon,
  Col,
  useMediaQueries,
} from 'prisma-design-system';

import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

const CobaltVersion = ({ country }) => {
  const { t } = useTranslation();
  const { md } = useMediaQueries();

  const { color, fontSize } = {
    color: 'informative.default',
    fontSize: 'body.1',
  };

  return (
    <Flex alignItems="start">
      <Box mr={md ? 3 : 4} pt={1}>
        <InformationIcon size="small" color={color} />
      </Box>

      <Flex flexDirection="column">
        <Body color={color} fontSize={fontSize} type="small">
          {t('flightSummary.stopInfo.vinmessage.title')}
        </Body>
        <Body color={color} fontSize={fontSize} type="small">
          {t('flightSummary.stopInfo.vinmessage.body', { country })}
        </Body>
      </Flex>
    </Flex>
  );
};

const BlueStoneVersion = ({ country }) => {
  const { t } = useTranslation();

  const { textColor, fontSize } = {
    fontSize: 'body.0',
    textColor: 'informative.default',
  };

  return (
    <Flex flexDirection="column">
      <FlexRow pb={1} alignItems="baseline" gap={2}>
        <Col display="flex">
          <InformationIcon color="informative.default" size="tiny" />
        </Col>
        <Col display="flex">
          <Text fontWeight="bold" fontSize={fontSize} color={textColor}>
            {t('flightSummary.stopInfo.vinmessage.title')}
          </Text>
        </Col>
      </FlexRow>
      <Flex pl={5}>
        <Text fontSize={fontSize} color={textColor}>
          {t('flightSummary.stopInfo.vinmessage.body', { country })}
        </Text>
      </Flex>
    </Flex>
  );
};

const CovidRestriction = cobaltDesktopFlightDetailsComponent([
  BlueStoneVersion,
  null,
  CobaltVersion,
]);

CovidRestriction.propTypes = {
  country: PropTypes.string.isRequired,
};

export default CovidRestriction;
