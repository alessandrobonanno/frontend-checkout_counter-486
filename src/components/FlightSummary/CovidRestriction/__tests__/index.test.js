import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import CovidRestriction from '..';

const VINMESSAGE_TITLE = 'Title vinmessage';
const VINMESSAGE_BODY = 'Body vinmessage {{country}}';
const COUNTRY = 'Spain';

const translations = {
  flightSummary: {
    stopInfo: {
      vinmessage: {
        title: VINMESSAGE_TITLE,
        body: VINMESSAGE_BODY,
      },
    },
  },
};

describe('ConnectionChangeType', () => {
  describe('Default ABs', () => {
    test('Change of airport', async () => {
      render(
        <TestingProviders translations={translations}>
          <CovidRestriction country={COUNTRY} />
        </TestingProviders>
      );
      expect(await screen.findByText(VINMESSAGE_TITLE)).toBeVisible();
      expect(await screen.findByText(`Body vinmessage Spain`)).toBeVisible();
    });
  });
  describe('FBO_BOOK3467 Active', () => {
    test('Change of airport', async () => {
      const abs = [{ alias: 'FBO_BOOK3467', partition: 3 }];

      render(
        <TestingProviders abs={abs} translations={translations}>
          <CovidRestriction country={COUNTRY} />
        </TestingProviders>
      );
      expect(await screen.findByText(VINMESSAGE_TITLE)).toBeVisible();
      expect(await screen.findByText(`Body vinmessage Spain`)).toBeVisible();
    });
  });
});
