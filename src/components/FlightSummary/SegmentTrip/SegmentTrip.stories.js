import React from 'react';
import SegmentTrip from './index';
import selectABs from 'decorators/selectABs';
import { withKnobs } from '@storybook/addon-knobs';

const selectAbProps = () => ({
  abs: selectABs([{ name: 'FBO_BOOK3467', defaultPartition: 3 }]),
});
const story = {
  title: 'Components/FlightSummary/SegmentTrip',
  component: SegmentTrip,
  parameters: {
    mockedProps: selectAbProps,
  },
};

export const Default = (args) => {
  return <SegmentTrip {...args} />;
};
Default.decorators = [withKnobs];
Default.args = {
  duration: 60,
  numberOfStops: 2,
  transportTypes: ['PLANE', 'TRAIN'],
};

export default story;
