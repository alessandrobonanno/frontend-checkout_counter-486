const translations = [
  {
    inputPath: ['sidebarSummary', 'flight.stop.pluralForms'],
    outputPath: ['flightSummary', 'segment', 'stops'],
  },
];

module.exports = translations;
