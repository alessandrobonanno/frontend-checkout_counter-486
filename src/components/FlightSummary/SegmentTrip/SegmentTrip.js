import React from 'react';
import PropTypes from 'prop-types';
import { useMediaQueries } from 'prisma-design-system';

import SegmentTripCobalt from './SegmentTripCobalt';
import SegmentTripBlueStone from './SegmentTripBlueStone';
import { cobaltDesktopFlightDetailsComponent } from '../../../versionedComponents';

const SegmentTrip = ({ transportTypes, duration, numberOfStops, ...props }) => {
  const { md } = useMediaQueries();

  const SegmentTripVersion = cobaltDesktopFlightDetailsComponent([
    SegmentTripCobalt,
    null,
    md ? SegmentTripCobalt : SegmentTripBlueStone,
  ]);

  return (
    <SegmentTripVersion
      transportTypes={transportTypes}
      duration={duration}
      numberOfStops={numberOfStops}
      {...props}
    />
  );
};

SegmentTrip.propTypes = {
  duration: PropTypes.number.isRequired,
  numberOfStops: PropTypes.number,
  transportTypes: PropTypes.array,
};

SegmentTrip.defaultProps = {
  transportTypes: [],
};

export default SegmentTrip;
