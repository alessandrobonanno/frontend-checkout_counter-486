import React from 'react';
import PropTypes from 'prop-types';
import { Box, Body, Flex, Hr, useMediaQueries } from 'prisma-design-system';
import TransportIcon from '../TransportIcon';
import Duration from '../Duration';

import { useTranslation } from '@frontend-shell/react-hooks';

const SegmentTripCobalt = ({
  transportTypes,
  duration,
  numberOfStops,
  invertOrder,
}) => {
  const { md } = useMediaQueries();
  const { t } = useTranslation();

  const TextStyle = ({ children }) => (
    <Body
      ellipsis={true}
      color={md ? 'neutrals.1' : 'neutrals.3'}
      type={md ? 'small' : 'small'}
    >
      {children}
    </Body>
  );

  const isDirect = numberOfStops === 0;
  const quantityText = t('flightSummary.segment.stops', {
    count: numberOfStops,
    context: isDirect ? 'none' : 'some',
  });

  const transportType = (
    <Flex
      px={2}
      flexDirection={invertOrder ? 'row-reverse' : ''}
      alignItems="flex-end"
    >
      {duration !== undefined && (
        <Box px={2}>
          <TextStyle>
            <Duration durationInMinutes={duration} />
          </TextStyle>
        </Box>
      )}
      <Flex pb="1">
        {transportTypes.map((transportType, index) => (
          <TransportIcon type={transportType} key={index} />
        ))}{' '}
      </Flex>
      {numberOfStops !== undefined && (
        <Box px={2}>
          <TextStyle>{quantityText}</TextStyle>
        </Box>
      )}
    </Flex>
  );
  return (
    <Flex flexGrow={3} alignItems="stretch" flexDirection="column">
      <Hr type="solid" decoration={transportType} color="neutrals.6" />
      <Flex alignItems="flex-start" justifyContent="center" pt={4}></Flex>
    </Flex>
  );
};

SegmentTripCobalt.propTypes = {
  duration: PropTypes.number.isRequired,
  numberOfStops: PropTypes.number,
  transportTypes: PropTypes.array,
  invertOrder: PropTypes.bool,
};

SegmentTripCobalt.defaultProps = {
  transportTypes: [],
  invertOrder: false,
};

export default SegmentTripCobalt;
