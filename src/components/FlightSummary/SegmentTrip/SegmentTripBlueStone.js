import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Hr, Text, useMediaQueries } from 'prisma-design-system';
import TransportIcon from '../TransportIcon';
import SegmentDuration from '../SegmentDuration';
import StopoverQuantity from '../StopoverQuantity';

const SegmentTripBlueStone = ({ transportTypes, duration, numberOfStops }) => {
  const { md } = useMediaQueries();
  const transportType = (
    <Flex px={transportTypes.length > 1 ? 1 : 0}>
      {transportTypes.map((transportType, index) => (
        <TransportIcon type={transportType} key={index} />
      ))}{' '}
    </Flex>
  );

  return (
    <Flex flexGrow={3} alignItems="stretch" flexDirection="column">
      <Hr type="dotted" decoration={transportType} />
      <Flex alignItems="flex-start" justifyContent="center" pt={4}>
        {numberOfStops && <StopoverQuantity quantity={numberOfStops} />}
        {!md && (
          <>
            <Text
              preserveWhitespace={true}
              color={'neutrals.3'}
              fontSize={'body.1'}
            >
              {' · '}
            </Text>
            <SegmentDuration duration={duration} />
          </>
        )}
      </Flex>
    </Flex>
  );
};

SegmentTripBlueStone.propTypes = {
  duration: PropTypes.number.isRequired,
  numberOfStops: PropTypes.number.isRequired,
  transportTypes: PropTypes.array,
};

SegmentTripBlueStone.defaultProps = {
  transportTypes: [],
};

export default SegmentTripBlueStone;
