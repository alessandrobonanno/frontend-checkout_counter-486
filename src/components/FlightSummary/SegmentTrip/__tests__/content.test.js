import React from 'react';
import { screen, render } from '@testing-library/react';
import SegmentTrip from '../';
import { setViewport } from '../../../../testUtils/responsiveness';
import { TestingProviders } from '../../../../testUtils';

const translations = {
  flightSummary: {
    segment: {
      duration: "{{hours}}h {{minutes}}'",
      stops_none: 'direct',
      stops_some: '1 stop',
      stops_some_plural: '{{count}} stops',
    },
  },
};

describe('SegmentTrip', () => {
  test('SegmentTrip mobile content', async () => {
    render(
      <TestingProviders translations={translations}>
        <SegmentTrip duration={105} numberOfStops={2} />
      </TestingProviders>
    );

    expect(await screen.findByText("1h 45'")).toBeVisible();
    expect(await screen.findByText('2 stops')).toBeVisible();
  });
  test('SegmentTrip desktop content', async () => {
    setViewport({ width: 1200 });
    render(
      <TestingProviders translations={translations}>
        <SegmentTrip duration={105} numberOfStops={2} />
      </TestingProviders>
    );

    expect(await screen.findByText('2 stops')).toBeVisible();
  });
});
