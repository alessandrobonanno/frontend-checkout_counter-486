import React from 'react';
import PropTypes from 'prop-types';
import { Message } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

import ThemedText from '../../Shared/Persuasive/ThemedText';
import ThemedMessage from '../../Shared/Persuasive/ThemedMessage';

const FreeCancellationText = ({ hours }) => {
  const { t } = useTranslation();

  return (
    <ThemedText>
      {t('priceSummary.freecancellation.msg.before', { count: hours })}
      <Message type="positive">
        <ThemedMessage>
          {t('priceSummary.freecancellation.msg.bold')}
        </ThemedMessage>
      </Message>
      {t('priceSummary.freecancellation.msg.after', { count: hours })}
    </ThemedText>
  );
};

FreeCancellationText.propTypes = {
  hours: PropTypes.number.isRequired,
};

export default FreeCancellationText;
