import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import FreeCancellationText from './';

const story = {
  title: 'Components/Persuasive/FreeCancellationText',
  component: FreeCancellationText,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <FreeCancellationText {...args} />;
};

Default.args = {
  hours: 1,
};
Default.parameters = {
  actions: { disabled: true },
  knobs: { disabled: true },
};

export default story;
