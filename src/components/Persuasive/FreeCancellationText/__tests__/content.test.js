import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import FreeCancellationText from '..';

describe('FreeCancellationText', () => {
  test('Multiple hours - before', async () => {
    const translations = {
      priceSummary: {
        title: {
          gooddeal: 'What a deal',
        },
        freecancellation: {
          msg: {
            before: '',
            bold: 'Free cancellation',
            after: 'within the next {{count}} hours',
          },
        },
      },
    };

    render(
      <TestingProviders translations={translations}>
        <FreeCancellationText hours={2} />
      </TestingProviders>
    );

    expect(await screen.findByText('Free cancellation')).toBeVisible();
    expect(screen.getByText('within the next 2 hours')).toBeVisible();
  });

  test('Multiple hours - after', async () => {
    const translations = {
      priceSummary: {
        freecancellation: {
          msg: {
            before: 'Within {{count}} hours',
            bold: 'free cancellation',
            after: '',
          },
        },
      },
    };

    render(
      <TestingProviders translations={translations}>
        <FreeCancellationText hours={3} />
      </TestingProviders>
    );

    expect(await screen.findByText('Within 3 hours')).toBeVisible();
    expect(screen.getByText('free cancellation')).toBeVisible();
  });
});
