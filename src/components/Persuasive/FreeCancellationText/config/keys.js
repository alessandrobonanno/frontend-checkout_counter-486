const translations = [
  {
    inputPath: ['priceSummary', 'freecancellation.msg.before'],
    outputPath: ['priceSummary', 'freecancellation', 'msg', 'before'],
  },
  {
    inputPath: ['priceSummary', 'freecancellation.msg.bold'],
    outputPath: ['priceSummary', 'freecancellation', 'msg', 'bold'],
  },
  {
    inputPath: ['priceSummary', 'freecancellation.msg.after'],
    outputPath: ['priceSummary', 'freecancellation', 'msg', 'after'],
  },
];

module.exports = translations;
