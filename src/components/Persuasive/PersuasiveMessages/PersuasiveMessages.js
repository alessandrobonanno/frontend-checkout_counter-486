import React from 'react';
import { Box, Table, TableBody } from 'prisma-design-system';

import useShoppingCartItineraryQuery from '../../../clientState/hooks/useShoppingCartItineraryQuery';
import ThemedContainer from '../../Shared/Persuasive/ThemedContainer';

import FreeCancellationText from '../FreeCancellationText';
import TicketsLeft from '../TicketsLeft';
import TaxesText from '../TaxesText';

import TitleText from './TitleText';
import PersuasiveRow from './PersuasiveRow';

const TICKETS_LIMIT = {
  MIN: 0,
  MAX: 10,
};

const PersuasiveMessages = () => {
  const { data, loading, error } = useShoppingCartItineraryQuery();

  if (loading || error) {
    return null;
  }

  const { itinerary } = data?.getShoppingCart || {};
  const { ticketsLeft } = itinerary || {};
  const hoursApart = itinerary?.freeCancellationLimit?.hoursApart;

  const validTicketsLeft =
    !!ticketsLeft &&
    ticketsLeft > TICKETS_LIMIT.MIN &&
    ticketsLeft < TICKETS_LIMIT.MAX;

  return (
    <ThemedContainer>
      <Box pb={4}>
        <TitleText goodDeal={!!hoursApart} />
      </Box>
      <Table>
        <TableBody>
          {!!hoursApart && (
            <PersuasiveRow hasBorderTop={false}>
              <FreeCancellationText hours={hoursApart} />
            </PersuasiveRow>
          )}
          {validTicketsLeft && (
            <PersuasiveRow hasBorderTop={!!hoursApart}>
              <TicketsLeft ticketsLeft={ticketsLeft} />
            </PersuasiveRow>
          )}
          <PersuasiveRow hasBorderTop={validTicketsLeft || !!hoursApart}>
            <TaxesText />
          </PersuasiveRow>
        </TableBody>
      </Table>
    </ThemedContainer>
  );
};

export default PersuasiveMessages;
