import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import PersuasiveMessages from '..';

const translations = {
  results: {
    ticketsLeft: {
      message: 'Ticket left {{count}}',
      message_plural: 'Tickets left {{count}}',
    },
  },
  priceSummary: {
    title: {
      gooddeal: 'What a deal',
    },
    tax: {
      msg: 'Taxes included',
    },
    freecancellation: {
      msg: {
        before: '',
        bold: 'Free cancellation',
        after: 'within the next {{count}} hours',
      },
    },
  },
  paymentManager: {
    securely: {
      paysecurely: 'Pay securely',
    },
  },
};

describe('PersuasiveMessages', () => {
  test('Free cancellation and Tickets left informed', async () => {
    const ga = jest.fn();
    const mocks = {
      ShoppingCart: () => ({
        itinerary: {
          freeCancellationLimit: {
            hoursApart: 10,
          },
          ticketsLeft: 2,
          freeCancellation: null,
          transportTypes: ['PLANE'],
          legs: [
            {
              segments: [
                {
                  id: '0',
                  seats: null,
                  duration: 120,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: '0',
                      flightCode: 'AF1449',
                      departureDate: '2021-06-29T06:15:00+02:00',
                      departureTerminal: '1',
                      departure: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalDate: '2021-06-29T08:15:00+02:00',
                      destination: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalTerminal: '3',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 100,
                      vehicleModel: '320',
                    },
                  ],
                },
              ],
            },
            {
              segments: [
                {
                  id: '1',
                  seats: null,
                  duration: 110,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: 1,
                      flightCode: 'AF1448',
                      departureDate: '2021-07-03T21:05:00+02:00',
                      departureTerminal: '3',
                      departure: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalDate: '2021-07-03T22:55:00+02:00',
                      destination: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalTerminal: '1',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 90,
                      vehicleModel: '310',
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    };

    render(
      <TestingProviders
        mocks={mocks}
        translations={translations}
        window={{ ga }}
      >
        <PersuasiveMessages />
      </TestingProviders>
    );

    expect(await screen.findByText('What a deal')).toBeVisible();
    expect(screen.getByText('Free cancellation')).toBeVisible();
    expect(screen.getByText('within the next 10 hours')).toBeVisible();
    expect(screen.getByText('Tickets left 2')).toBeVisible();
    expect(screen.getByText('Taxes included')).toBeVisible();

    expect(ga).toTrackOnceWith({
      action: 'seats_left',
      label: 'seats_left_2',
    });
  });

  test('Free cancellation', async () => {
    const mocks = {
      ShoppingCart: () => ({
        itinerary: {
          freeCancellationLimit: {
            hoursApart: 10,
          },
          ticketsLeft: 2,
          freeCancellation: null,
          transportTypes: ['PLANE'],
          legs: [
            {
              segments: [
                {
                  id: '0',
                  seats: null,
                  duration: 120,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: '0',
                      flightCode: 'AF1449',
                      departureDate: '2021-06-29T06:15:00+02:00',
                      departureTerminal: '1',
                      departure: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalDate: '2021-06-29T08:15:00+02:00',
                      destination: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalTerminal: '3',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 100,
                      vehicleModel: '320',
                    },
                  ],
                },
              ],
            },
            {
              segments: [
                {
                  id: '1',
                  seats: null,
                  duration: 110,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: 1,
                      flightCode: 'AF1448',
                      departureDate: '2021-07-03T21:05:00+02:00',
                      departureTerminal: '3',
                      departure: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalDate: '2021-07-03T22:55:00+02:00',
                      destination: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalTerminal: '1',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 90,
                      vehicleModel: '310',
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    };

    render(
      <TestingProviders mocks={mocks} translations={translations}>
        <PersuasiveMessages />
      </TestingProviders>
    );

    expect(await screen.findByText('What a deal')).toBeVisible();
    expect(screen.getByText('Free cancellation')).toBeVisible();
    expect(screen.getByText('within the next 10 hours')).toBeVisible();
    expect(screen.queryByText('Tickets left')).toBeNull();
    expect(screen.getByText('Taxes included')).toBeVisible();
  });

  test('Tickets left', async () => {
    const ga = jest.fn();
    const mocks = {
      ShoppingCart: () => ({
        itinerary: {
          freeCancellationLimit: null,
          ticketsLeft: 1,
          freeCancellation: null,
          transportTypes: ['PLANE'],
          legs: [
            {
              segments: [
                {
                  id: '0',
                  seats: null,
                  duration: 120,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: '0',
                      flightCode: 'AF1449',
                      departureDate: '2021-06-29T06:15:00+02:00',
                      departureTerminal: '1',
                      departure: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalDate: '2021-06-29T08:15:00+02:00',
                      destination: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalTerminal: '3',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 100,
                      vehicleModel: '320',
                    },
                  ],
                },
              ],
            },
            {
              segments: [
                {
                  id: '1',
                  seats: null,
                  duration: 110,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: 1,
                      flightCode: 'AF1448',
                      departureDate: '2021-07-03T21:05:00+02:00',
                      departureTerminal: '3',
                      departure: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalDate: '2021-07-03T22:55:00+02:00',
                      destination: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalTerminal: '1',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 90,
                      vehicleModel: '310',
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    };

    render(
      <TestingProviders
        mocks={mocks}
        translations={translations}
        window={{ ga }}
      >
        <PersuasiveMessages />
      </TestingProviders>
    );

    expect(await screen.findByText('Pay securely')).toBeVisible();
    expect(screen.queryByText('Free cancellation')).toBeNull();
    expect(screen.getByText('Ticket left 1')).toBeVisible();
    expect(screen.getByText('Taxes included')).toBeVisible();

    expect(ga).toTrackOnceWith({
      action: 'seats_left',
      label: 'seats_left_1',
    });
  });

  test('Only taxes included', async () => {
    const mocks = {
      ShoppingCart: () => ({
        itinerary: {
          freeCancellationLimit: null,
          ticketsLeft: 1,
          freeCancellation: null,
          transportTypes: ['PLANE'],
          legs: [
            {
              segments: [
                {
                  id: '0',
                  seats: null,
                  duration: 120,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: '0',
                      flightCode: 'AF1449',
                      departureDate: '2021-06-29T06:15:00+02:00',
                      departureTerminal: '1',
                      departure: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalDate: '2021-06-29T08:15:00+02:00',
                      destination: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalTerminal: '3',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 100,
                      vehicleModel: '320',
                    },
                  ],
                },
              ],
            },
            {
              segments: [
                {
                  id: '1',
                  seats: null,
                  duration: 110,
                  transportTypes: ['PLANE'],
                  carrier: {
                    id: 'AF',
                    name: 'Air France',
                  },
                  sections: [
                    {
                      id: 1,
                      flightCode: 'AF1448',
                      departureDate: '2021-07-03T21:05:00+02:00',
                      departureTerminal: '3',
                      departure: {
                        id: '330',
                        cityName: 'Paris',
                        name: 'Charles De Gaulle',
                        cityIata: 'PAR',
                        iata: 'CDG',
                        locationType: 'AIRPORT',
                        countryName: 'France',
                      },
                      arrivalDate: '2021-07-03T22:55:00+02:00',
                      destination: {
                        id: '157',
                        cityName: 'Barcelone',
                        name: 'El Prat',
                        cityIata: 'BCN',
                        iata: 'BCN',
                        locationType: 'AIRPORT',
                        countryName: 'Espagne',
                      },
                      arrivalTerminal: '1',
                      transportType: 'PLANE',
                      carrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      operatingCarrier: {
                        id: 'AF',
                        name: 'Air France',
                      },
                      cabinClass: 'TOURIST',
                      baggageAllowance: null,
                      technicalStops: [],
                      duration: 90,
                      vehicleModel: '310',
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    };

    render(
      <TestingProviders mocks={mocks} translations={translations}>
        <PersuasiveMessages />
      </TestingProviders>
    );
    expect(await screen.findByText('Pay securely')).toBeVisible();
    expect(screen.queryByText('Free cancellation')).toBeNull();
    expect(screen.queryByText('Tickets left')).toBeNull();
    expect(screen.getByText('Taxes included')).toBeVisible();
  });
});
