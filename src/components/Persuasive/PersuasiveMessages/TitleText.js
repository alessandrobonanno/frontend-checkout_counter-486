import React from 'react';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const TitleText = ({ goodDeal = true }) => {
  const { t } = useTranslation();
  const key = goodDeal
    ? 'priceSummary.title.gooddeal'
    : 'paymentManager.securely.paysecurely';

  return (
    <Text fontSize="heading.0" fontWeight="bold" color="neutrals.1">
      {t(key)}
    </Text>
  );
};

export default TitleText;
