import React from 'react';
import {
  Box,
  Flex,
  TableCell,
  TableRow,
  CheckIcon,
  useGetThemeVersion,
} from 'prisma-design-system';

const Icon = () => (
  <Flex py={2} minWidth={1} justifyContent="flex-end">
    <CheckIcon size="small" color="positive.default" />
  </Flex>
);

const PersuasiveRow = ({ hasBorderTop, children }) => {
  const themeVersion = useGetThemeVersion();

  return (
    <TableRow hasBorderTop={hasBorderTop}>
      <TableCell>
        <Box py={2}>{children}</Box>
      </TableCell>
      {themeVersion === 'blueStone' && (
        <TableCell>
          <Icon />
        </TableCell>
      )}
    </TableRow>
  );
};

export default PersuasiveRow;
