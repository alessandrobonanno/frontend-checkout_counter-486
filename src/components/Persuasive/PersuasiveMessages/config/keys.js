const translations = [
  {
    inputPath: ['priceSummary', 'title.gooddeal'],
    outputPath: ['priceSummary', 'title', 'gooddeal'],
  },
  {
    inputPath: ['paymentManager', 'securely.paysecurely'],
    outputPath: ['paymentManager', 'securely', 'paysecurely'],
  },
];

module.exports = translations;
