import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import { withKnobs } from '@storybook/addon-knobs';
import PersuasiveMessages from '.';
import selector from '../../../../.storybook/selectors/shoopingCartMockSelector';
import freeCancellationTextKeys from '../FreeCancellationText/config/keys';
import taxesTextKeys from '../TaxesText/config/keys';
import ticketsTextKeys from '../TicketsLeft/config/keys';

const keys = [
  ...configKeys,
  ...freeCancellationTextKeys,
  ...taxesTextKeys,
  ...ticketsTextKeys,
];

const story = {
  title: 'Components/Persuasive/PersuasiveMessages',
  component: PersuasiveMessages,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(keys),
    mockedProps: selector,
  },
};

export const Default = (args) => <PersuasiveMessages {...args} />;

Default.decorators = [withKnobs];
Default.parameters = {
  options: {
    selectedPanel: 'storybookjs/knobs/panel',
  },
  controls: { disabled: true },
  actions: { disabled: true },
};

export default story;
