import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import TicketsLeft from '..';

const translations = {
  results: {
    ticketsLeft: {
      message: 'Ticket left {{count}}',
      message_plural: 'Tickets left {{count}}',
    },
  },
};

describe('TicketsLeft', () => {
  test('Zero tickets left', async () => {
    render(
      <TestingProviders translations={translations}>
        <TicketsLeft ticketsLeft={0} />
      </TestingProviders>
    );

    expect(await screen.findByText('Tickets left 0')).toBeVisible();
  });

  test('Single ticket left', async () => {
    render(
      <TestingProviders translations={translations}>
        <TicketsLeft ticketsLeft={1} />
      </TestingProviders>
    );

    expect(await screen.findByText('Ticket left 1')).toBeVisible();
  });

  test('Multiple tickets left', async () => {
    render(
      <TestingProviders translations={translations}>
        <TicketsLeft ticketsLeft={5} />
      </TestingProviders>
    );

    expect(await screen.findByText('Tickets left 5')).toBeVisible();
  });
});
