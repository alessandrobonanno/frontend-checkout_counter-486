import React from 'react';
import PropTypes from 'prop-types';
import { Message } from 'prisma-design-system';
import { useTranslation, useTracking } from '@frontend-shell/react-hooks';

import ThemedMessage from '../../Shared/Persuasive/ThemedMessage';

const TicketsLeft = ({ ticketsLeft }) => {
  const { t } = useTranslation();
  const { trackEvent } = useTracking();

  trackEvent({
    action: 'seats_left',
    label: `seats_left_${ticketsLeft}`,
  });

  return (
    <Message type="persuasive">
      <ThemedMessage>
        {t('results.ticketsLeft.message', { count: ticketsLeft })}
      </ThemedMessage>
    </Message>
  );
};

TicketsLeft.propTypes = {
  ticketsLeft: PropTypes.number.isRequired,
};

export default TicketsLeft;
