const translations = [
  {
    inputPath: ['results', 'seats_left_variation.pluralForms.2'],
    outputPath: ['results', 'ticketsLeft', 'message'],
  },
];

module.exports = translations;
