import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import TicketsLeft from '.';

const story = {
  title: 'Components/Persuasive/TicketsLeft',
  component: TicketsLeft,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = (args) => {
  return <TicketsLeft {...args} />;
};

Default.args = {
  ticketsLeft: 3,
};
Default.parameters = {
  actions: { disabled: true },
  knobs: { disabled: true },
};

export default story;
