import React from 'react';
import { useTranslation } from '@frontend-shell/react-hooks';

import ThemedText from '../../Shared/Persuasive/ThemedText';

const TaxesText = () => {
  const { t } = useTranslation();

  return <ThemedText>{t('priceSummary.tax.msg')}</ThemedText>;
};

export default TaxesText;
