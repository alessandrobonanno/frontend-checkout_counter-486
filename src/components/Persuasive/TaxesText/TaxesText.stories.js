import React from 'react';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import TaxesText from '.';

const story = {
  title: 'Components/Persuasive/TaxesText',
  component: TaxesText,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = () => {
  return <TaxesText />;
};

Default.parameters = {
  options: {
    selectedPanel: 'storybook/rtl/rtl-panel',
  },
  controls: { disabled: true },
  actions: { disabled: true },
  knobs: { disabled: true },
};

export default story;
