const translations = [
  {
    inputPath: ['priceSummary', 'tax.msg'],
    outputPath: ['priceSummary', 'tax', 'msg'],
  },
];

module.exports = translations;
