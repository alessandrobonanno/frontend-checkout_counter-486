import React from 'react';
import { screen, render } from '@testing-library/react';

import { TestingProviders } from '../../../../testUtils';
import TaxesText from '..';

const translations = {
  priceSummary: {
    tax: {
      msg: 'Taxes included',
    },
  },
};

describe('TaxesText', () => {
  test('Displays text', async () => {
    render(
      <TestingProviders translations={translations}>
        <TaxesText />
      </TestingProviders>
    );

    expect(await screen.findByText('Taxes included')).toBeVisible();
  });
});
