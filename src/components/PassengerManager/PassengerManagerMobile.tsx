import React, { FC, Fragment } from 'react';
import { Box, Hr } from 'prisma-design-system';
import useTravellersFieldsQuery from '../../clientState/hooks/useTravellersFieldsQuery';
import TravellerForm from './TravellerForm';

const PassengerManagerMobile: FC = () => {
  const { data, loading, error } = useTravellersFieldsQuery();
  if (loading || error) {
    return null;
  }

  const { requiredTravellerInformation } = data?.getShoppingCart || {};
  return (
    <>
      {requiredTravellerInformation?.map((traveller, index) => {
        return (
          <Fragment key={index}>
            <TravellerForm
              indx={index}
              isResident={!!traveller?.residentGroups?.length}
              travellerType={traveller.travellerType}
              key={index}
            />
            {!(index + 1 === requiredTravellerInformation.length) && (
              <Box mt={6} mb={8}>
                <Hr type={'solid'} />
              </Box>
            )}
          </Fragment>
        );
      })}
    </>
  );
};

export default PassengerManagerMobile;
