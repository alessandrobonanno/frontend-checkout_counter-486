import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import multipleAdults, {
  singleAdult,
  adultWithInfant,
  adultWithChild,
  multipleAdultsLogged,
  multipleAdultsPrimeLogged,
} from '../../../.storybook/selectors/travellersFieldMockSelector';
import travellerFormKeys from './TravellerForm/config/keys';
import storedTravellerSelectorKeys from './StoredTravellerSelector/config/keys';
import headerKeys from './Header/config/keys';
import PassengerManagerMobile from '.';
import PaxPageContinueButton from '../PaxPageContinueButton';

const configKeys = [
  ...travellerFormKeys,
  ...storedTravellerSelectorKeys,
  ...headerKeys,
];

export default {
  title: 'Components/PassengerManager/PassengerManagerMobile',
  component: PassengerManagerMobile,
  parameters: {
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
} as Meta;

export const SinglePax: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

SinglePax.parameters = {
  mockedProps: singleAdult,
};

export const MultiplePax: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

MultiplePax.parameters = {
  mockedProps: multipleAdults,
};

export const MultiplePaxLogged: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

MultiplePaxLogged.parameters = {
  mockedProps: multipleAdultsLogged,
  translationsEndPaths: ['/default'],
  ...cmsAddonApi.createPanelParameters(configKeys),
};

export const MultiplePaxLoggedWithPrime: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

MultiplePaxLoggedWithPrime.parameters = {
  mockedProps: multipleAdultsPrimeLogged,
  translationsEndPaths: ['/default'],
  ...cmsAddonApi.createPanelParameters(configKeys),
};

export const AdultWithInfant: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

AdultWithInfant.parameters = {
  mockedProps: adultWithInfant,
};

export const AdultWithChild: Story = (args) => {
  return (
    <>
      <PassengerManagerMobile {...args} />
      <PaxPageContinueButton />
    </>
  );
};

AdultWithChild.parameters = {
  mockedProps: adultWithChild,
};
