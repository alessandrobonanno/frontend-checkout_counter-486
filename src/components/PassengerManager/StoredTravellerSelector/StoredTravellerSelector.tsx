import React, { FC, useState, useEffect, useMemo } from 'react';
import { Flex, Select } from 'prisma-design-system';
import { useTracking } from '@frontend-shell/react-hooks';
import { useTranslation } from '@frontend-shell/react-hooks';
import { useStoredTravellers } from '../../../clientState/providers/Travellers/StoredTravellersProvider';
import useUserQuery from '../../../clientState/hooks/useUserQuery';
import StoredTravellersModal from './StoredTravellersModal';

interface StoredTravellersSelectorProps {
  indx: number;
}
type SelectOptions = Array<{ label: string; value: number }>;

const StoredTravellersSelector: FC<StoredTravellersSelectorProps> = ({
  indx,
}) => {
  const [open, setOpen] = useState(false);
  const { storedTravellers, pickSavedTraveller } = useStoredTravellers();
  const { t } = useTranslation();
  const { trackEvent } = useTracking();
  const [aboutToChangeValue, setAboutToChangeValue] = useState();
  const [selectedStoredTraveller, setSelectedStoredTraveller] = useState(-1);
  const { data: userData, loading: userDataLoading } = useUserQuery();
  const storedTravellersOptions: SelectOptions = useMemo(() => {
    const storedTravellersOptions = [
      {
        label: t('passengersManager.storedTravellers.default'),
        value: -1,
      },
    ];
    storedTravellers.forEach(
      ({ travellerIndexPick, personalInfo, id, buyer }) => {
        if (travellerIndexPick === undefined || travellerIndexPick === indx) {
          const label = `${personalInfo.name} ${
            personalInfo.firstLastName || ''
          } ${personalInfo.secondLastName || ''}`;

          storedTravellersOptions.push({
            label:
              buyer && id === 0
                ? `${label} ${t('passengersManager.prime.primeOption.label')}`
                : label,
            value: id,
          });
        }
      }
    );
    return storedTravellersOptions;
  }, [storedTravellers, indx, t]);

  const onClickChangePax = (): void => {
    if (aboutToChangeValue) pickSavedTraveller(indx, aboutToChangeValue);
    setOpen(false);
  };

  const onChangeValue = ({ value }): void => {
    if (
      userData?.getUser?.membership &&
      selectedStoredTraveller === 0 &&
      value !== 0
    ) {
      setOpen(true);
      setAboutToChangeValue(value);
    } else {
      pickSavedTraveller(indx, value);
    }
  };

  useEffect(() => {
    const selectedTraveller = storedTravellers.find(
      ({ travellerIndexPick }) => travellerIndexPick === indx
    );
    const isSelectedTravellerPickedByIndex =
      selectedTraveller && selectedTraveller?.id !== selectedStoredTraveller;
    const haveNotSelectedTraveller =
      !selectedTraveller && selectedStoredTraveller !== -1;

    if (isSelectedTravellerPickedByIndex || haveNotSelectedTraveller) {
      if (isSelectedTravellerPickedByIndex) {
        trackEvent({
          action: 'pax_details',
          label: `select_stored_pax_${
            storedTravellers.findIndex(
              ({ id }) => id === selectedTraveller.id
            ) + 1
          }`,
        });
      }
      if (haveNotSelectedTraveller) {
        trackEvent({
          action: 'pax_details',
          label: 'select_new_pax',
        });
      }
      setSelectedStoredTraveller(
        selectedTraveller?.id === 0
          ? selectedTraveller?.id
          : selectedTraveller?.id || -1
      );
    }
  }, [storedTravellers, indx, trackEvent, selectedStoredTraveller]);

  useEffect(() => {
    if (!userData?.getUser && !userDataLoading) {
      trackEvent({
        action: 'sso_pax_page',
        label: 'prefilled_none_none',
      });
    }
  }, [userData, userDataLoading, trackEvent]);

  useEffect(() => {
    if (userData?.getUser && !userDataLoading) {
      trackEvent({
        action: 'sso_pax_page',
        label: 'prefilled_pax_none',
      });
    }
  }, [userData, trackEvent, userDataLoading]);

  if (!storedTravellers || !storedTravellers.length) {
    return null;
  }
  return (
    <Flex width="100%" mb={5}>
      <Select
        id={'storedTraveller'}
        options={storedTravellersOptions}
        onChange={onChangeValue}
        value={`${selectedStoredTraveller}`}
        label={
          selectedStoredTraveller === 0
            ? t('passengersManager.prime.primeOptionSelected.label')
            : t('passengersManager.storedTravellers.label')
        }
      />
      <StoredTravellersModal
        open={open}
        onClickChangePax={onClickChangePax}
        onClickDontChangePax={(): void => setOpen(false)}
      />
    </Flex>
  );
};

export default StoredTravellersSelector;
