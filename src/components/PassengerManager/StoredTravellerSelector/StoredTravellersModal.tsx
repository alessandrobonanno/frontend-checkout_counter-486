import React, { FC } from 'react';
import {
  Modal,
  ModalHeader,
  ModalContent,
  Box,
  Button,
  Body,
  Heading,
  Flex,
  BackgroundImage,
} from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

interface StoredTravellersModalProps {
  open: boolean;
  onClickChangePax: () => void;
  onClickDontChangePax: () => void;
}
const StoredTravellersModal: FC<StoredTravellersModalProps> = ({
  open,
  onClickChangePax,
  onClickDontChangePax,
}) => {
  const { t } = useTranslation();

  return (
    <Modal blocker size="full" p={3} open={open} onClose={onClickDontChangePax}>
      <ModalHeader onClose={onClickDontChangePax} />
      <ModalContent px={6} pb={6}>
        <Flex justifyContent="center" alignItems="center">
          <BackgroundImage
            width="50%"
            height={'66px'}
            backgroundRepeat="no-repeat"
            backgroundPosition="center"
            backgroundSize="auto 100%"
            backgroundImage={`url('/images/onefront/bluestone/ED/ic-change-pax-icon.png')`}
          ></BackgroundImage>
        </Flex>
        <Box pt={3}>
          <Heading type="3" lineHeight="heading" textAlign="center">
            {t('passengersManager.prime.changePaxModal.header')}
          </Heading>
        </Box>
        <Box pt={3}>
          <Body textAlign="center" type="small">
            {t('passengersManager.prime.changePaxModal.description')}
          </Body>
        </Box>
        <Box pt={3}>
          <Button fullWidth type="primary" onClick={onClickChangePax}>
            {t('passengersManager.prime.changePaxModal.changeLabel')}
          </Button>
        </Box>
        <Box pt={2}>
          <Button fullWidth type="secondary" onClick={onClickDontChangePax}>
            {t('passengersManager.prime.changePaxModal.dontChangeLabel')}
          </Button>
        </Box>
      </ModalContent>
    </Modal>
  );
};

export default StoredTravellersModal;
