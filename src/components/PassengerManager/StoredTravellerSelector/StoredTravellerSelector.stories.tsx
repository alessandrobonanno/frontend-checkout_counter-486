import { Story, Meta } from '@storybook/react/types-6-0';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';

import React from 'react';
import { multipleAdultsLogged } from '../../../../.storybook/selectors/travellersFieldMockSelector';
import StoredTravellersSelector from './StoredTravellerSelector';
import configKeys from './config/keys';

export default {
  title: 'Components/PassengerManager/StoredTravellersSelector',
  component: StoredTravellersSelector,
  parameters: {
    mockedProps: multipleAdultsLogged,
    translationsEndPaths: ['/default'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
} as Meta;

export const Default: Story = (args) => {
  return <StoredTravellersSelector indx={0} {...args} />;
};
