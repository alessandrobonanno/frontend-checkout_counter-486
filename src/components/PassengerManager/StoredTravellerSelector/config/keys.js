const translations = [
  {
    inputPath: ['passengersManager', 'addPassenger.label'],
    outputPath: ['passengersManager', 'storedTravellers', 'default'],
  },
  {
    inputPath: ['passengersManager', 'addPassenger.newPassenger'],
    outputPath: ['passengersManager', 'storedTravellers', 'label'],
  },
  {
    inputPath: ['prime', 'membership.lightbox.edit.header'],
    outputPath: ['passengersManager', 'prime', 'changePaxModal', 'header'],
    scope: ['PRIME'],
  },
  {
    inputPath: ['prime', 'membership.lightbox.edit.description'],
    outputPath: ['passengersManager', 'prime', 'changePaxModal', 'description'],
    scope: ['PRIME'],
  },
  {
    inputPath: ['prime', 'membership.lightbox.edit.primary'],
    outputPath: ['passengersManager', 'prime', 'changePaxModal', 'changeLabel'],
    scope: ['PRIME'],
  },
  {
    inputPath: ['prime', 'membership.lightbox.edit.secondary'],
    outputPath: [
      'passengersManager',
      'prime',
      'changePaxModal',
      'dontChangeLabel',
    ],
    scope: ['PRIME'],
  },
  {
    inputPath: ['variables', 'brand.name.prime'],
    outputPath: ['variables', 'brand', 'name', 'prime'],
    scope: ['PRIME'],
  },
  {
    inputPath: ['prime', 'membership.passenger.option.premium'],
    outputPath: ['passengersManager', 'prime', 'primeOption', 'label'],
    scope: ['PRIME'],
  },
  {
    inputPath: ['prime', 'membership.passenger.select.premium.member'],
    outputPath: ['passengersManager', 'prime', 'primeOptionSelected', 'label'],
    scope: ['PRIME'],
  },
];

module.exports = translations;
