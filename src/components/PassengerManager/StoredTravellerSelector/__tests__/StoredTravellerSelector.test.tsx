import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import StoredTravellerSelector from '../StoredTravellerSelector';
import TravellersFieldsDescriptorProvider from '../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import TravellersSelectionProvider from '../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { TestingProviders } from '../../../../testUtils';
import {
  userMocks,
  primeUserMocks,
  emptyUserMocks,
  getGenericTraveller,
  PRIME_TRAVELLER,
} from '../../../../clientState/providers/__tests__/userMocks';
import userEvent from '@testing-library/user-event';

const PRIME_LABEL = 'prime';
const PRIME_OPTION_LABEL = 'prime member';
const DEFAULT_LABEL = 'new pax';
const STORED_TRAVELLER_LABEL = 'stored';
const MODAL_HEADER_TEXT = 'header';
const MODAL_BODY_TEXT = 'body';
const CHANGE_TEXT = 'change';
const DONT_CHANGE_TEXT = 'dont change';

const translations = {
  passengersManager: {
    storedTravellers: {
      label: STORED_TRAVELLER_LABEL,
      default: DEFAULT_LABEL,
    },
    prime: {
      primeOption: {
        label: PRIME_OPTION_LABEL,
      },
      primeOptionSelected: {
        label: PRIME_LABEL,
      },
      changePaxModal: {
        header: MODAL_HEADER_TEXT,
        description: MODAL_BODY_TEXT,
        changeLabel: CHANGE_TEXT,
        dontChangeLabel: DONT_CHANGE_TEXT,
      },
    },
  },
};

describe('StoredTravellerSelector', () => {
  it('Displays the proper label and option for prime user', async () => {
    render(
      <TestingProviders
        mocks={primeUserMocks().mocks}
        translations={translations}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );
    const select = await screen.findByLabelText(PRIME_LABEL);
    const GENERIC_TRAVELLER = getGenericTraveller();
    expect(select).toBeVisible();

    userEvent.click(select);

    expect(screen.getByText(DEFAULT_LABEL)).toBeVisible();
    expect(
      screen.getByText(GENERIC_TRAVELLER.personalInfo.name, { exact: false })
    ).toBeVisible();
    if (GENERIC_TRAVELLER.personalInfo.firstLastName)
      expect(
        screen.getByText(GENERIC_TRAVELLER.personalInfo.firstLastName, {
          exact: false,
        })
      ).toBeVisible();
    if (GENERIC_TRAVELLER.personalInfo.secondLastName)
      expect(
        screen.getByText(GENERIC_TRAVELLER.personalInfo.secondLastName, {
          exact: false,
        })
      ).toBeVisible();

    expect(
      screen.getByText(PRIME_TRAVELLER.personalInfo.name, { exact: false })
    ).toBeVisible();
    expect(
      screen.getByText(PRIME_TRAVELLER.personalInfo.firstLastName, {
        exact: false,
      })
    ).toBeVisible();
    expect(
      screen.getByText(PRIME_TRAVELLER.personalInfo.secondLastName, {
        exact: false,
      })
    ).toBeVisible();
    expect(
      screen.getByText(PRIME_OPTION_LABEL, { exact: false })
    ).toBeVisible();
  });

  it('Displays the proper label and option for non prime user', async () => {
    render(
      <TestingProviders mocks={userMocks().mocks} translations={translations}>
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    const select = await screen.findByLabelText(STORED_TRAVELLER_LABEL);
    const GENERIC_TRAVELLER = getGenericTraveller();

    userEvent.click(select);
    expect(screen.getByText(DEFAULT_LABEL)).toBeVisible();
    expect(
      screen.getByText(GENERIC_TRAVELLER.personalInfo.name, { exact: false })
    ).toBeVisible();
    if (GENERIC_TRAVELLER.personalInfo.firstLastName)
      expect(
        screen.getByText(GENERIC_TRAVELLER.personalInfo.firstLastName, {
          exact: false,
        })
      ).toBeVisible();
    if (GENERIC_TRAVELLER.personalInfo.secondLastName)
      expect(
        screen.getByText(GENERIC_TRAVELLER.personalInfo.secondLastName, {
          exact: false,
        })
      ).toBeVisible();
  });

  it('Opens modal upon changing prime user', async () => {
    render(
      <TestingProviders
        mocks={primeUserMocks().mocks}
        translations={translations}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    const select = await screen.findByLabelText(PRIME_LABEL);
    const GENERIC_TRAVELLER = getGenericTraveller();

    userEvent.click(select);
    userEvent.selectOptions(
      select,
      screen.getByText(GENERIC_TRAVELLER.personalInfo.name, { exact: false })
    );

    await waitFor(() => {
      expect(screen.getByText(MODAL_BODY_TEXT)).toBeVisible();
    });
    expect(screen.getByText(CHANGE_TEXT)).toBeVisible();
    expect(screen.getByText(DONT_CHANGE_TEXT)).toBeVisible();
  });

  it('Sends tracking about prefilled status when user data is not available', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={emptyUserMocks().mocks}
        translations={translations}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        window={{ ga }}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    await waitFor(() => {
      expect(ga).toHaveBeenCalledTimes(1);
    });
    expect(ga).toHaveBeenCalledTimes(1);
    expect(ga).toTrackPaxPageNthWith(1, {
      action: 'sso_pax_page',
      label: 'prefilled_none_none',
    });
  });

  it('Sends tracking about prefilled status when user data is available', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={primeUserMocks().mocks}
        translations={translations}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        window={{ ga }}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    await waitFor(() => {
      expect(ga).toHaveBeenCalledTimes(2);
    });

    expect(ga).toTrackPaxPageNthWith(1, {
      action: 'sso_pax_page',
      label: 'prefilled_pax_none',
    });
    expect(ga).toTrackPaxPageNthWith(2, {
      action: 'pax_details',
      label: 'select_stored_pax_2',
    });
  });

  it('Sends tracking about stored passenger selection', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={primeUserMocks().mocks}
        translations={translations}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        window={{ ga }}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    const select = await screen.findByLabelText(PRIME_LABEL);

    userEvent.click(select);
    userEvent.selectOptions(
      select,
      screen.getByText(getGenericTraveller().personalInfo.name, {
        exact: false,
      })
    );

    const changeText = await screen.getByText(CHANGE_TEXT);
    await waitFor(() => {
      expect(changeText).toBeVisible();
    });
    userEvent.click(changeText);

    expect(ga).toHaveBeenCalledTimes(3);
    expect(ga).toTrackPaxPageNthWith(2, {
      action: 'pax_details',
      label: 'select_stored_pax_2',
    });
    expect(ga).toTrackPaxPageNthWith(3, {
      action: 'pax_details',
      label: 'select_stored_pax_1',
    });
  });

  it('Sends tracking about new passenger selection', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        mocks={primeUserMocks().mocks}
        translations={translations}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        window={{ ga }}
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <StoredTravellerSelector indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    const select = await screen.findByLabelText(PRIME_LABEL);

    userEvent.click(select);
    userEvent.selectOptions(select, screen.getByText(DEFAULT_LABEL));

    const changeText = await screen.getByText(CHANGE_TEXT);
    await waitFor(() => {
      expect(changeText).toBeVisible();
    });
    userEvent.click(changeText);

    expect(ga).toHaveBeenCalledTimes(3);
    expect(ga).toTrackPaxPageNthWith(3, {
      action: 'pax_details',
      label: 'select_new_pax',
    });
  });
});
