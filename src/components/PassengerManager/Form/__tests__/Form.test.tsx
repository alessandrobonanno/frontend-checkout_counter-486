import React from 'react';
import { screen, render } from '@testing-library/react';
import Form from '../Form';
import { TestingProviders } from '../../../../testUtils';
import {
  fieldsMock,
  FEMALE_TEXT,
  MALE_TEXT,
  NATIONAL_ID_CARD_TEXT,
  PASSPORT_TEXT,
} from '../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import TravellersSelectionProvider from '../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import TravellersFieldsDescriptorProvider from '../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';

const DATE_OF_BIRTH_LABEL = 'date of birth';
const OPTIONAL_FIELD_LABEL = 'optional fields';
const OPTIONAL_REQUEST_FIELD_LABEL = 'optional request fields';
const IDENTIFICATION_LABEL = 'identification';
const PAX_CARD_NUMBER_LABEL = 'card number';
const SECOND_EMAIL_LABEL = 'second email';
const EMAIL_LABEL = 'email';

const translations = {
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    form: {
      dateOfBirth: {
        ADULT: {
          label: DATE_OF_BIRTH_LABEL,
        },
      },
      optional: {
        check: OPTIONAL_REQUEST_FIELD_LABEL,
      },
      apis: {
        check: OPTIONAL_FIELD_LABEL,
      },
      identificationType: {
        label: IDENTIFICATION_LABEL,
      },
      passengerCardNumber: {
        label: PAX_CARD_NUMBER_LABEL,
      },
      secondEmail: {
        label: SECOND_EMAIL_LABEL,
      },
      email: {
        label: EMAIL_LABEL,
      },
    },
  },
};

describe('Form', () => {
  it('Displays the fields', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <Form indx={0} />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    expect(await screen.findByLabelText(MALE_TEXT)).toBeInTheDocument();
    expect(screen.getByLabelText(FEMALE_TEXT)).toBeInTheDocument();
    expect(screen.getByLabelText(DATE_OF_BIRTH_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(SECOND_EMAIL_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(EMAIL_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(OPTIONAL_FIELD_LABEL)).toBeInTheDocument();
    expect(
      screen.getByLabelText(OPTIONAL_REQUEST_FIELD_LABEL)
    ).toBeInTheDocument();
    expect(screen.getByLabelText(IDENTIFICATION_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(PAX_CARD_NUMBER_LABEL)).toBeInTheDocument();
  });
});
