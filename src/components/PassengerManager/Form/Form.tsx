import React, { FC, useEffect } from 'react';
import { Box } from 'prisma-design-system';
import { useTravellerDescriptor } from '../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import FieldGroupsProvider from '../../../clientState/providers/Travellers/FieldsGroupProvider';
import FieldGroup, { CollapsibleFieldGroup } from './FieldGroup';
import { FieldGroups } from '../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { useAllTravellersSelectionActions } from '../../../clientState/providers/Travellers/AllTravellersFieldsSelectionProvider';

const Form: FC<{ indx: number }> = ({ indx }) => {
  const { mandatoryFields, optionalFields, optionalRequestFields } =
    useTravellerDescriptor();
  const { setTravellerRef } = useAllTravellersSelectionActions();

  const travellerFormRef = React.useRef<HTMLDivElement>();

  useEffect(() => {
    if (travellerFormRef?.current)
      setTravellerRef({ indx, ref: travellerFormRef });
  }, [travellerFormRef, setTravellerRef, indx]);

  return (
    <Box ref={travellerFormRef}>
      <FieldGroupsProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
        <FieldGroup fieldsData={mandatoryFields} indx={indx} />
      </FieldGroupsProvider>

      {!!optionalFields?.length && (
        <FieldGroupsProvider fieldsGroup={FieldGroups.OPTIONAL_FIELDS}>
          <CollapsibleFieldGroup
            indx={indx}
            fieldsData={optionalFields}
            groupId={FieldGroups.OPTIONAL_FIELDS}
          />
        </FieldGroupsProvider>
      )}
      {!!optionalRequestFields?.length && (
        <FieldGroupsProvider fieldsGroup={FieldGroups.OPTIONAL_REQUEST_FIELDS}>
          <CollapsibleFieldGroup
            indx={indx}
            fieldsData={optionalRequestFields}
            groupId={FieldGroups.OPTIONAL_REQUEST_FIELDS}
          />
        </FieldGroupsProvider>
      )}
    </Box>
  );
};

export default Form;
