import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../../testUtils';
import FieldGroup from '../FieldGroup';
import {
  DATE_OF_BIRTH_TEXT,
  EMAIL_TEXT,
  FEMALE_TEXT,
  fieldsMock,
  MALE_TEXT,
  mandatoryDayOfBirthFieldParsed,
  mandatoryEmailFieldParsed,
  mandatoryFieldParsed,
  mandatorySecondEmailFieldParsed,
  NATIONAL_ID_CARD_TEXT,
  optionalFieldParsed,
  optionalRequestFieldParsed,
  PASSENGER_CARD_TEXT,
  PASSPORT_TEXT,
  SECOND_EMAIL_TEXT,
} from '../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';

const translations = {
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    form: {
      dateOfBirth: {
        ADULT: {
          label: DATE_OF_BIRTH_TEXT,
        },
      },
      email: {
        label: EMAIL_TEXT,
      },
      secondEmail: {
        label: SECOND_EMAIL_TEXT,
      },
      passengerCardNumber: {
        label: PASSENGER_CARD_TEXT,
      },
    },
  },
};

describe('Field Group', () => {
  it('Displays mandatory fields', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <FieldGroup
          indx={0}
          fieldsData={[
            [mandatoryFieldParsed],
            [mandatoryDayOfBirthFieldParsed],
            [mandatoryEmailFieldParsed],
            [mandatorySecondEmailFieldParsed],
          ]}
        />
      </TestingProviders>
    );
    expect(await screen.findByText(MALE_TEXT)).toBeInTheDocument();
    expect(screen.getByText(FEMALE_TEXT)).toBeInTheDocument();
    expect(screen.getByText(DATE_OF_BIRTH_TEXT)).toBeInTheDocument();
    expect(screen.getByText(EMAIL_TEXT)).toBeInTheDocument();
    expect(screen.getByText(SECOND_EMAIL_TEXT)).toBeInTheDocument();
  });

  it('Displays optional fields', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <FieldGroup indx={0} fieldsData={[[optionalFieldParsed]]} />
      </TestingProviders>
    );
    expect(await screen.findByText(PASSPORT_TEXT)).toBeInTheDocument();
  });

  it('Displays optional requests fields', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <FieldGroup indx={0} fieldsData={[[optionalRequestFieldParsed]]} />
      </TestingProviders>
    );
    expect(await screen.findByText(PASSENGER_CARD_TEXT)).toBeInTheDocument();
  });
});
