import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../../../testUtils';
import CollapsibleFieldGroup from '../CollapsibleFieldGroup';
import {
  fieldsMock,
  optionalFieldParsed,
  PASSPORT_TEXT,
} from '../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { FieldGroups } from '../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import userEvent from '@testing-library/user-event';

const OPTIONAL_FIELD_TEXT = 'Optional fields';
const OPTIONAL_REQUEST_FIELDS_TEXT = 'Optional Request fields';

const translations = {
  booking: {
    shopping: {
      cart: {
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    form: {
      apis: {
        check: OPTIONAL_FIELD_TEXT,
      },
      optional: {
        check: OPTIONAL_REQUEST_FIELDS_TEXT,
      },
    },
  },
};

describe('Collapsible Field Group', () => {
  it('Displays the field group', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <CollapsibleFieldGroup
          groupId={FieldGroups.OPTIONAL_FIELDS}
          indx={0}
          fieldsData={[[optionalFieldParsed]]}
        />
      </TestingProviders>
    );

    expect(
      await screen.findByLabelText(OPTIONAL_FIELD_TEXT)
    ).toBeInTheDocument();
    expect(screen.getByText(PASSPORT_TEXT)).toBeInTheDocument();
  });

  it('Open and closes the field group', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        window={{ ga }}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        mocks={fieldsMock().mocks}
        translations={translations}
      >
        <CollapsibleFieldGroup
          groupId={FieldGroups.OPTIONAL_FIELDS}
          indx={0}
          fieldsData={[[optionalFieldParsed]]}
        />
      </TestingProviders>
    );

    const label = await screen.findByLabelText(OPTIONAL_FIELD_TEXT);
    const collapsible = screen.getByLabelText(FieldGroups.OPTIONAL_FIELDS);

    userEvent.click(label);
    expect(collapsible).toHaveAttribute('aria-expanded', 'true');

    userEvent.click(label);
    expect(collapsible).toHaveAttribute('aria-expanded', 'false');
    expect(ga).not.toBeCalled();
  });

  it('Launch traking when optionalRequest group is open', async () => {
    const ga = jest.fn();
    render(
      <TestingProviders
        window={{ ga }}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        mocks={fieldsMock().mocks}
        translations={translations}
      >
        <CollapsibleFieldGroup
          groupId={FieldGroups.OPTIONAL_REQUEST_FIELDS}
          indx={0}
          fieldsData={[[optionalFieldParsed]]}
        />
      </TestingProviders>
    );

    const label = await screen.findByLabelText(OPTIONAL_REQUEST_FIELDS_TEXT);
    const collapsible = screen.getByLabelText(
      FieldGroups.OPTIONAL_REQUEST_FIELDS
    );

    userEvent.click(label);
    expect(collapsible).toHaveAttribute('aria-expanded', 'true');
    expect(ga).toTrackPaxPageNthWith(1, {
      action: 'frequent_flyer_widget',
      label: 'frequent_flyer_widget_click_open',
    });
  });
});
