import {
  TravellerField,
  FieldOptions,
} from '../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldsSelectionStateType,
  UpdateFieldDispatcher,
} from '../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';

export { default as Radio } from './Radio';
export { default as Text } from './Text';
export { default as TextWithImage } from './TextWithImage';
export { default as Date } from './Date';
export { default as Select } from './Select';

export enum AutoCorrectCompleteTypes {
  ON = 'on',
  OFF = 'off',
}

export interface FieldsProps {
  dataId: TravellerField['dataId'];
  dataName: TravellerField['dataName'];
  options: FieldOptions;
  initialized: boolean | undefined;
  invalid: boolean | undefined;
  value: string | undefined;
  autocapitalize: AutoCorrectCompleteTypes;
  autocorrect: AutoCorrectCompleteTypes;
  autocomplete: AutoCorrectCompleteTypes;
  onChange: UpdateFieldDispatcher;
  onBlur: (e: { target: { value: string } }) => void;
  onFocus: React.EventHandler<React.FocusEvent<HTMLFormElement>>;
  lastFlightDate?: FieldsSelectionStateType['lastFlightDate'];
  travellerType?: FieldsSelectionStateType['travellerType'];
  helperLabel: string;
  label: string;
}
