import React, { FC } from 'react';
import { Flex, TextInput } from 'prisma-design-system';
import { AutoCorrectCompleteTypes, FieldsProps } from '.';
import { useFieldsGroup } from '../../../../../../clientState/providers/Travellers/FieldsGroupProvider';

interface TextInputProps extends FieldsProps {
  type?: 'text' | 'date';
  min?: string;
  max?: string;
  onClick?: () => void;
}

const Text: FC<TextInputProps> = (props) => {
  const fieldGroup = useFieldsGroup();
  const { dataId, dataName, onChange, label, autocapitalize } = props;
  const onChangeValue = ({ value }): void => {
    const capitalizedValue =
      autocapitalize === AutoCorrectCompleteTypes.ON
        ? `${value.charAt(0).toUpperCase()}${value.slice(1)}`
        : value;
    onChange({
      name: dataName,
      value: capitalizedValue,
      fieldGroup,
    });
  };

  return (
    <Flex width="100%" mb={5}>
      <TextInput
        resetInputAriaLabel={'Reset Value'}
        invalidValueAriaLabel={'Invalid Value'}
        validValueAriaLabel={'Valid Value'}
        {...props}
        id={dataId}
        onChange={onChangeValue}
      >
        {label}
      </TextInput>
    </Flex>
  );
};

Text.defaultProps = {
  type: 'text',
  onChange: (): void => {
    /* noop */
  },
};

export default Text;
