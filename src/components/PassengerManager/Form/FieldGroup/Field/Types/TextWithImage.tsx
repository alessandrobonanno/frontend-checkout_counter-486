import React, { FC } from 'react';
import { Flex, TextInput } from 'prisma-design-system';
import { FieldsProps } from '.';
import { useFieldsGroup } from '../../../../../../clientState/providers/Travellers/FieldsGroupProvider';

const TextVariation: FC<FieldsProps> = (props) => {
  const fieldGroup = useFieldsGroup();
  const { dataId, dataName, onChange, label, options } = props;
  const onChangeValue = ({ value }): void => {
    onChange({ name: dataName, value, fieldGroup });
  };

  return (
    <Flex width="100%" mb={5} alignItems={'flex-end'}>
      <TextInput
        {...props}
        resetInputAriaLabel={'Reset Value'}
        invalidValueAriaLabel={'Invalid Value'}
        validValueAriaLabel={'Valid Value'}
        type="text"
        id={dataId}
        imgSrc={`/images/onefront/airlines/sm${options?.[0]?.label}.gif`}
        onChange={onChangeValue}
        imageAltText={options?.[0].label}
      >
        {label}
      </TextInput>
    </Flex>
  );
};

TextVariation.defaultProps = {
  onChange: (): void => {
    /* noop */
  },
};

export default TextVariation;
