import React, { FC, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, RadioButton, RadioButtonGroup } from 'prisma-design-system';
import { FieldsProps } from '.';
import { useFieldsGroup } from '../../../../../../clientState/providers/Travellers/FieldsGroupProvider';

const Radio: FC<FieldsProps> = ({
  dataName,
  options,
  onChange,
  value,
  invalid,
  ...rest
}) => {
  const [selected, setSelected] = useState(value);
  const fieldGroup = useFieldsGroup();
  const onChangeValue = (value): void => {
    setSelected(value);
    onChange({ name: dataName, value, fieldGroup });
  };

  useEffect(() => {
    setSelected(value);
  }, [value, setSelected]);

  return (
    <Flex width="100%" mb={1}>
      <RadioButtonGroup
        name={dataName}
        onChange={onChangeValue}
        direction="row"
        {...rest}
      >
        {options.map(({ label, value }) => (
          <Box key={value} mr={3}>
            <RadioButton
              value={value}
              checked={value === selected}
              hasError={invalid}
            >
              {label}
            </RadioButton>
          </Box>
        ))}
      </RadioButtonGroup>
    </Flex>
  );
};

Radio.propTypes = {
  options: PropTypes.array.isRequired,
};

Radio.defaultProps = {
  onChange: (): void => {
    /* noop */
  },
};

export default Radio;
