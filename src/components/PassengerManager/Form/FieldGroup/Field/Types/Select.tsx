import React, { useCallback, FC } from 'react';
import { Flex, Select as SelectInput } from 'prisma-design-system';
import { FieldsProps } from '.';

const Select: FC<FieldsProps> = (props) => {
  const { onBlur, options: fieldOptions, value, label, dataId } = props;
  const onChangeValue = useCallback(
    ({ value }) => {
      onBlur({ target: { value } });
    },
    [onBlur]
  );

  return (
    <Flex width="100%" mb={5}>
      <SelectInput
        {...props}
        id={dataId}
        options={fieldOptions}
        value={value}
        onChange={onChangeValue}
        label={label}
      />
    </Flex>
  );
};

Select.defaultProps = {
  onChange: (): void => {
    /* noop */
  },
};

export default Select;
