import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Select from '../Select';
import { FieldGroups } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import FieldsGroupProvider from '../../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { AutoCorrectCompleteTypes } from '..';
import { UpdateFieldAction } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import TestInputWrapper, { FORM_TITLE } from './TestInputWrapper';

const LABEL_TEXT = 'label text';
const HELPER_TEXT = 'helper text';
const LABEL_TEXT_OPTION1 = 'label option 1';
const VALUE_TEXT_OPTION1 = 'val1';
const LABEL_TEXT_OPTION2 = 'label option 2';
const VALUE_TEXT_OPTION2 = 'val2';

let onChange: (payload: UpdateFieldAction['payload']) => void;
let onBlur: () => void;
let onFocus: () => void;

beforeEach(() => {
  onChange = jest.fn();
  onBlur = jest.fn();
  onFocus = jest.fn();
});

describe('Input type Select', () => {
  test('Input shows label and events callbacks are called', async () => {
    render(
      <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
        <TestInputWrapper
          Component={Select}
          dataId={'123'}
          dataName="name"
          options={[
            {
              label: LABEL_TEXT_OPTION1,
              value: VALUE_TEXT_OPTION1,
            },
            {
              label: LABEL_TEXT_OPTION2,
              value: VALUE_TEXT_OPTION2,
            },
          ]}
          initialized={false}
          invalid={false}
          value={''}
          autocapitalize={AutoCorrectCompleteTypes.ON}
          autocorrect={AutoCorrectCompleteTypes.OFF}
          autocomplete={AutoCorrectCompleteTypes.OFF}
          onChange={onChange}
          onBlur={onBlur}
          onFocus={onFocus}
          helperLabel={HELPER_TEXT}
          label={LABEL_TEXT}
        />
      </FieldsGroupProvider>
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);

    userEvent.click(input);
    expect(await screen.findByText(LABEL_TEXT_OPTION1)).toBeVisible();
    expect(await screen.findByText(LABEL_TEXT_OPTION2)).toBeVisible();
    userEvent.selectOptions(input, await screen.findByText(LABEL_TEXT_OPTION1));
    userEvent.click(formTitle);

    expect(await screen.findByLabelText(LABEL_TEXT)).toBeVisible();
    expect(onFocus).toBeCalledTimes(1);
    expect(onBlur).toBeCalledTimes(2);
    expect(onBlur).toHaveBeenNthCalledWith(1, {
      target: { value: VALUE_TEXT_OPTION1 },
    });
    expect(onChange).not.toHaveBeenCalled();
  });
});
