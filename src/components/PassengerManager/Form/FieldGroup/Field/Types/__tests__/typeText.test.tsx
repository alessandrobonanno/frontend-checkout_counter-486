import React, { FC } from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TextInput from '../Text';
import { FieldGroups } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import FieldsGroupProvider from '../../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { AutoCorrectCompleteTypes } from '..';
import { UpdateFieldAction } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import TestInputWrapper, { FORM_TITLE } from './TestInputWrapper';

const LABEL_TEXT = 'label text';
const HELPER_TEXT = 'helper text';

let onChange: (payload: UpdateFieldAction['payload']) => void;
let onBlur: () => void;
let onFocus: () => void;

const TypeDateRenderComponent: FC<{
  dataName: string;
  invalid?: boolean;
  autocapitalize?: AutoCorrectCompleteTypes;
}> = ({ dataName, invalid, autocapitalize = AutoCorrectCompleteTypes.OFF }) => (
  <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
    <TestInputWrapper
      Component={TextInput}
      dataId={'123'}
      dataName={dataName}
      options={[]}
      initialized={false}
      invalid={!!invalid}
      value={''}
      autocapitalize={autocapitalize}
      autocorrect={AutoCorrectCompleteTypes.OFF}
      autocomplete={AutoCorrectCompleteTypes.OFF}
      onChange={onChange}
      onBlur={onBlur}
      onFocus={onFocus}
      helperLabel={HELPER_TEXT}
      label={LABEL_TEXT}
    />
  </FieldsGroupProvider>
);

beforeEach(() => {
  onChange = jest.fn();
  onBlur = jest.fn();
  onFocus = jest.fn();
});

describe('Input type Text', () => {
  test('Input shows label, helperText and events callbacks are called', async () => {
    render(<TypeDateRenderComponent dataName="name" />);
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);
    const typedValue = 'someText';

    userEvent.type(input, typedValue);
    expect(await screen.findByText(HELPER_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Reset Value')).toBeVisible();

    userEvent.click(formTitle);

    expect(await screen.findByLabelText(LABEL_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    expect(onFocus).toBeCalledTimes(1);
    expect(onBlur).toBeCalledTimes(1);
    expect(onChange).toHaveBeenNthCalledWith(typedValue.length, {
      name: 'name',
      value: typedValue,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });
  test('Input shall show invalid icon', async () => {
    render(
      <TypeDateRenderComponent
        dataName="name"
        invalid={true}
        autocapitalize={AutoCorrectCompleteTypes.ON}
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);

    userEvent.click(input);
    userEvent.click(formTitle);

    expect(await screen.getByLabelText('Invalid Value')).toBeVisible();
  });

  test('Input shall capitalize the value if autocapitalize is on', async () => {
    render(
      <TypeDateRenderComponent
        dataName="name"
        autocapitalize={AutoCorrectCompleteTypes.ON}
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    const typedValue = 'someText';

    userEvent.type(input, typedValue);

    expect(onChange).toHaveBeenNthCalledWith(typedValue.length, {
      name: 'name',
      value: `${typedValue.charAt(0).toUpperCase()}${typedValue.slice(1)}`,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });
});
