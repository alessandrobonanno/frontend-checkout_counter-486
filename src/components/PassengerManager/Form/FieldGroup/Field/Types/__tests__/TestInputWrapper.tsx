import React, { FC, useState } from 'react';
import { FieldsProps } from '..';

export const FORM_TITLE = 'Form title';
interface TestInputWrapperProps extends FieldsProps {
  Component: FC<FieldsProps>;
}
const TestInputWrapper: FC<TestInputWrapperProps> = ({
  onChange,
  value,
  Component,
  ...props
}) => {
  const [state, setState] = useState(value);
  const change = (fieldData): void => {
    setState(fieldData.value);
    onChange(fieldData);
  };

  return (
    <>
      <div>{FORM_TITLE}</div>
      <Component {...props} value={state} onChange={change} />
    </>
  );
};

export default TestInputWrapper;
