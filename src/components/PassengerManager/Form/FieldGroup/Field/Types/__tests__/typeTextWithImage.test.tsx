import React, { FC } from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TextWithImageInput from '../TextWithImage';
import { FieldGroups } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import FieldsGroupProvider from '../../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { AutoCorrectCompleteTypes } from '..';
import { UpdateFieldAction } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import TestInputWrapper, { FORM_TITLE } from './TestInputWrapper';

const LABEL_TEXT = 'label text';
const HELPER_TEXT = 'helper text';

let onChange: (payload: UpdateFieldAction['payload']) => void;
let onBlur: () => void;
let onFocus: () => void;

const TypeDateRenderComponent: FC<{
  dataName: string;
  invalid?: boolean;
}> = ({ dataName, invalid }) => (
  <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
    <TestInputWrapper
      Component={TextWithImageInput}
      dataId={'123'}
      dataName={dataName}
      options={[
        {
          label: 'IB',
          value: '',
        },
      ]}
      initialized={false}
      invalid={!!invalid}
      value={''}
      autocapitalize={AutoCorrectCompleteTypes.OFF}
      autocorrect={AutoCorrectCompleteTypes.OFF}
      autocomplete={AutoCorrectCompleteTypes.OFF}
      onChange={onChange}
      onBlur={onBlur}
      onFocus={onFocus}
      helperLabel={HELPER_TEXT}
      label={LABEL_TEXT}
    />
  </FieldsGroupProvider>
);

beforeEach(() => {
  onChange = jest.fn();
  onBlur = jest.fn();
  onFocus = jest.fn();
});

describe('Input type Text With Image', () => {
  test('Input shows label, helperText and events callbacks are called', async () => {
    render(<TypeDateRenderComponent dataName="name" />);
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);
    const typedValue = 'someText';

    userEvent.type(input, typedValue);
    expect(await screen.findByText(HELPER_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Reset Value')).toBeVisible();
    expect(await screen.getByAltText('IB')).toBeVisible();

    userEvent.click(formTitle);

    expect(await screen.findByLabelText(LABEL_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    expect(onFocus).toBeCalledTimes(1);
    expect(onBlur).toBeCalledTimes(1);
    expect(onChange).toHaveBeenNthCalledWith(typedValue.length, {
      name: 'name',
      value: typedValue,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });
  test('Input shall show invalid icon', async () => {
    render(<TypeDateRenderComponent dataName="name" invalid={true} />);
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);

    userEvent.click(input);
    userEvent.click(formTitle);

    expect(await screen.getByLabelText('Invalid Value')).toBeVisible();
  });
});
