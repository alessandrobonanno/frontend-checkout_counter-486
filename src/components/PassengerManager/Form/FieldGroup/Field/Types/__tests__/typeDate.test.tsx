import React, { FC, useState } from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TestingProviders } from '../../../../../../../testUtils';
import DateComponent, { AGE_AVERAGE } from '../Date';
import TravellersFieldsDescriptorProvider, {
  useTravellerDescriptor,
} from '../../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import TravellersFieldsSelectionProvider, {
  FieldGroups,
} from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import FieldsGroupProvider from '../../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { fieldsMock } from '../../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { TravellerAgeType } from '../../../../../../../generated/graphql';
import { AutoCorrectCompleteTypes, FieldsProps } from '..';
import { UpdateFieldAction } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';

const LABEL_TEXT = 'label text';
const HELPER_TEXT = 'helper text';
const FORM_TITLE = 'Form title';

let onChange: (payload: UpdateFieldAction['payload']) => void;
let onBlur: () => void;
let onFocus: () => void;

const InputWrapper: FC<FieldsProps> = ({ onChange, value, ...props }) => {
  const [state, setState] = useState(value);
  const { travellerType } = useTravellerDescriptor();
  const change = (fieldData): void => {
    setState(fieldData.value);
    onChange(fieldData);
  };

  return (
    <>
      <div>{FORM_TITLE}</div>
      <div>{travellerType}</div>
      <DateComponent {...props} value={state} onChange={change} />
    </>
  );
};

const TypeDateRenderComponent: FC<{
  travellerType: TravellerAgeType;
  dataName: string;
  invalid?: boolean;
  value?: string;
}> = ({ travellerType, dataName, invalid, value = '' }) => (
  <TestingProviders mocks={fieldsMock(travellerType).mocks}>
    <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
      <TravellersFieldsDescriptorProvider indx={0}>
        <TravellersFieldsSelectionProvider indx={0}>
          <InputWrapper
            dataId={'123'}
            dataName={dataName}
            options={[]}
            initialized={false}
            invalid={!!invalid}
            value={value}
            travellerType={travellerType}
            lastFlightDate="2026-07-03"
            autocapitalize={AutoCorrectCompleteTypes.ON}
            autocorrect={AutoCorrectCompleteTypes.OFF}
            autocomplete={AutoCorrectCompleteTypes.OFF}
            onChange={onChange}
            onBlur={onBlur}
            onFocus={onFocus}
            helperLabel={HELPER_TEXT}
            label={LABEL_TEXT}
          />
        </TravellersFieldsSelectionProvider>
      </TravellersFieldsDescriptorProvider>
    </FieldsGroupProvider>
  </TestingProviders>
);

beforeEach(() => {
  onChange = jest.fn();
  onBlur = jest.fn();
  onFocus = jest.fn();
});

describe('Input type Date', () => {
  test('Input shows label, helperText and events callbacks are called', async () => {
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Adult}
        dataName="name"
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    const formTitle = await screen.findByText(FORM_TITLE);
    const typedValue = '2020-12-20';

    userEvent.type(input, typedValue);
    expect(await screen.findByText(HELPER_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Reset Value')).toBeVisible();

    userEvent.click(formTitle);

    expect(await screen.findByLabelText(LABEL_TEXT)).toBeVisible();
    expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    expect(onFocus).toBeCalledTimes(1);
    expect(onBlur).toBeCalledTimes(1);
    expect(onChange).toHaveBeenNthCalledWith(1, {
      name: 'name',
      value: '2020-12-20',
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });

  test('Input shall not set a default value if data name is not dateOfBirth', async () => {
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Adult}
        dataName="name"
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    await screen.findByText(TravellerAgeType.Adult);

    userEvent.click(input);
    expect(onChange).toBeCalledTimes(0);
  });

  test('Input shall not set a default value if data name is dateOfBirth but traveller type is not Adult', async () => {
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Child}
        dataName="name"
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    await screen.findByText(TravellerAgeType.Child);

    userEvent.click(input);
    expect(onChange).toBeCalledTimes(0);
  });

  test('Input shall not set a default value if data name is dateOfBirth but has already a value', async () => {
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Child}
        dataName="name"
        value="1990-12-20"
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    await screen.findByText(TravellerAgeType.Child);

    userEvent.click(input);
    expect(onChange).toBeCalledTimes(0);
    expect(input).toHaveAttribute('value', '1990-12-20');
  });

  test('Input shall set a default value if data name is dateOfBirth and travellerType is Adult', async () => {
    const expectedDate = new Date();
    expectedDate.setFullYear(expectedDate.getFullYear() - AGE_AVERAGE);
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Adult}
        dataName="dateOfBirth"
      />
    );
    const input = await screen.findByLabelText(LABEL_TEXT);
    await screen.findByText(TravellerAgeType.Adult);

    userEvent.click(input);

    expect(onChange).toBeCalledWith({
      name: 'dateOfBirth',
      value: `${expectedDate.getFullYear()}-${(
        '0' +
        (expectedDate.getMonth() + 1)
      ).slice(-2)}-${('0' + expectedDate.getDate()).slice(-2)}`,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });

  test('Input shall have min and max attributes and invalid icon', async () => {
    render(
      <TypeDateRenderComponent
        travellerType={TravellerAgeType.Adult}
        dataName="dateOfBirth"
        invalid={true}
      />
    );
    const input = (await screen.findByLabelText(
      LABEL_TEXT
    )) as HTMLInputElement;
    const formTitle = await screen.findByText(FORM_TITLE);
    await screen.findByText(TravellerAgeType.Adult);
    userEvent.type(input, '2021-02-15');
    userEvent.click(formTitle);

    expect(await screen.getByLabelText('Invalid Value')).toBeVisible();
    expect(input.min).toEqual('1900-07-03');
    expect(input.max).toEqual('2014-07-03');
  });
});
