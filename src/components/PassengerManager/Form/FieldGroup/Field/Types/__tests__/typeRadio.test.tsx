import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TestingProviders } from '../../../../../../../testUtils';
import Radio from '../Radio';
import FieldsGroupProvider from '../../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { fieldsMock } from '../../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { TravellerAgeType } from '../../../../../../../generated/graphql';
import { AutoCorrectCompleteTypes } from '..';
import { UpdateFieldAction } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { FieldGroups } from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import TestInputWrapper, { FORM_TITLE } from './TestInputWrapper';

const LABEL_TEXT = 'label text';
const LABEL_TEXT_OPTION1 = 'label option 1';
const VALUE_TEXT_OPTION1 = 'val1';
const LABEL_TEXT_OPTION2 = 'label option 2';
const VALUE_TEXT_OPTION2 = 'val2';
const HELPER_TEXT = 'helper text';

let onChange: (payload: UpdateFieldAction['payload']) => void;
let onBlur: () => void;
let onFocus: () => void;

beforeEach(() => {
  onChange = jest.fn();
  onBlur = jest.fn();
  onFocus = jest.fn();
});

describe('Input type Radio', () => {
  test('Input dont show label, helperText and events callbacks are called', async () => {
    render(
      <TestingProviders mocks={fieldsMock(TravellerAgeType.Adult).mocks}>
        <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
          <TestInputWrapper
            Component={Radio}
            dataId={'123'}
            dataName={'name'}
            options={[
              {
                label: LABEL_TEXT_OPTION1,
                value: VALUE_TEXT_OPTION1,
              },
              {
                label: LABEL_TEXT_OPTION2,
                value: VALUE_TEXT_OPTION2,
              },
            ]}
            initialized={false}
            invalid={false}
            value={''}
            autocapitalize={AutoCorrectCompleteTypes.ON}
            autocorrect={AutoCorrectCompleteTypes.OFF}
            autocomplete={AutoCorrectCompleteTypes.OFF}
            onChange={onChange}
            onBlur={onBlur}
            onFocus={onFocus}
            helperLabel={HELPER_TEXT}
            label={LABEL_TEXT}
          />
        </FieldsGroupProvider>
      </TestingProviders>
    );
    const radio1 = (await screen.findByLabelText(
      LABEL_TEXT_OPTION1
    )) as HTMLInputElement;
    const radio2 = (await screen.findByLabelText(
      LABEL_TEXT_OPTION2
    )) as HTMLInputElement;
    const formTitle = await screen.findByText(FORM_TITLE);

    expect(radio1.checked).toEqual(false);
    userEvent.click(radio1);
    userEvent.click(formTitle);

    expect(radio1.checked).toEqual(true);
    expect(radio2.checked).toEqual(false);
    expect(await screen.findByText(LABEL_TEXT_OPTION1)).toBeVisible();
    expect(await screen.findByText(LABEL_TEXT_OPTION2)).toBeVisible();
    expect(await screen.queryByLabelText(LABEL_TEXT)).not.toBeInTheDocument();
    expect(await screen.queryByText(HELPER_TEXT)).not.toBeInTheDocument();
    expect(onFocus).toBeCalledTimes(1);
    expect(onBlur).toBeCalledTimes(1);
    expect(onChange).toHaveBeenNthCalledWith(1, {
      name: 'name',
      value: VALUE_TEXT_OPTION1,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });

    userEvent.click(radio2);
    expect(radio1.checked).toEqual(false);
    expect(radio2.checked).toEqual(true);
    expect(onChange).toHaveBeenNthCalledWith(2, {
      name: 'name',
      value: VALUE_TEXT_OPTION2,
      fieldGroup: FieldGroups.MANDATORY_FIELDS,
    });
  });
});
