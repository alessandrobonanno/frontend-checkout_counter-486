import React, { FC, useCallback } from 'react';
import { FieldsProps } from '.';
import { useFieldsGroup } from '../../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import { TRAVELLERS_TYPES_AGE_LIMITS } from '../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { TravellerAgeType } from '../../../../../../generated/graphql';
import Text from './Text';

export const AGE_AVERAGE = 44;

const buildDateFomat = (date: Date): string =>
  `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${(
    '0' + date.getDate()
  ).slice(-2)}`;

const buildLimitDates = (baseDate: Date, yearsLess: number): string => {
  baseDate.setFullYear(baseDate.getFullYear() - yearsLess);
  return buildDateFomat(baseDate);
};
type LimitDateBuilderType = (lastSectionDepartureDate: string) => string;

const LIMIT_DATES_BY_DATANAME: Record<
  string,
  Record<
    TravellerAgeType,
    { min: LimitDateBuilderType; max: LimitDateBuilderType }
  >
> = {
  dateOfBirth: {
    [TravellerAgeType.Adult]: {
      min: (lastFlightDate = Date()): string => {
        const earliestDateObj = new Date(lastFlightDate);
        buildLimitDates(new Date(lastFlightDate), 1900);
        earliestDateObj.setFullYear(1900);
        return buildDateFomat(earliestDateObj);
      },
      max: (lastFlightDate = Date()): string =>
        buildLimitDates(
          new Date(lastFlightDate),
          TRAVELLERS_TYPES_AGE_LIMITS[TravellerAgeType.Adult].min
        ),
    },
    [TravellerAgeType.Child]: {
      min: (lastFlightDate = Date()): string =>
        buildLimitDates(
          new Date(lastFlightDate),
          TRAVELLERS_TYPES_AGE_LIMITS[TravellerAgeType.Child].max
        ),
      max: (lastFlightDate = Date()): string =>
        buildLimitDates(
          new Date(lastFlightDate),
          TRAVELLERS_TYPES_AGE_LIMITS[TravellerAgeType.Child].min
        ),
    },
    [TravellerAgeType.Infant]: {
      min: (lastFlightDate = Date()): string =>
        buildLimitDates(
          new Date(lastFlightDate),
          TRAVELLERS_TYPES_AGE_LIMITS[TravellerAgeType.Infant].max
        ),
      max: (): string => {
        const lastestDateObj = new Date();
        lastestDateObj.setDate(lastestDateObj.getDate() - 1);
        return buildDateFomat(lastestDateObj);
      },
    },
  },
};
const getDefaultBithDateValue = (
  travellerType: TravellerAgeType | undefined,
  dataName: string
): string | undefined => {
  let defaultValue;
  if (travellerType === TravellerAgeType.Adult && dataName === 'dateOfBirth') {
    const averageBithDateForAdults = new Date();
    averageBithDateForAdults.setFullYear(
      averageBithDateForAdults.getFullYear() - AGE_AVERAGE
    );
    defaultValue = buildDateFomat(averageBithDateForAdults);
  }
  return defaultValue;
};

const DateComponent: FC<FieldsProps> = ({
  dataName,
  initialized,
  onChange,
  value,
  lastFlightDate,
  travellerType,
  ...props
}) => {
  const fieldGroup = useFieldsGroup();
  const minDate = travellerType
    ? LIMIT_DATES_BY_DATANAME[dataName]?.[travellerType].min(
        lastFlightDate || ''
      )
    : undefined;
  const maxDate = travellerType
    ? LIMIT_DATES_BY_DATANAME[dataName]?.[travellerType].max(
        lastFlightDate || ''
      )
    : undefined;
  const setDefaultvalue = useCallback((): void => {
    const defaultValue = getDefaultBithDateValue(travellerType, dataName);
    if (!value && defaultValue && !initialized) {
      onChange({ name: dataName, fieldGroup, value: defaultValue });
    }
  }, [value, initialized, onChange, dataName, fieldGroup, travellerType]);

  return (
    <Text
      type="date"
      {...props}
      value={value}
      min={minDate?.toString?.()}
      max={maxDate?.toString?.()}
      onClick={setDefaultvalue}
      onChange={onChange}
      dataName={dataName}
      initialized={initialized}
    />
  );
};

export default DateComponent;
