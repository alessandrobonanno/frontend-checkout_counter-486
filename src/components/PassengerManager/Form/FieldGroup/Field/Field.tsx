import React, { FC, useState, useEffect, useMemo, useCallback } from 'react';
import { useTranslation, useTracking } from '@frontend-shell/react-hooks';
import {
  Radio,
  Text,
  Date,
  Select,
  TextWithImage,
  FieldsProps as TypesFieldsProps,
  AutoCorrectCompleteTypes,
} from './Types';
import { TravellerField } from '../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import { FieldTypeValue } from '../../../../../generated/graphql';
import { useFieldsGroup } from '../../../../../clientState/providers/Travellers/FieldsGroupProvider';
import {
  useTravellersSelectionActions,
  useFieldSelection,
  useTravellerSelectionTravellerData,
} from '../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import type { UpdateFieldAction } from '../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { useStoredTravellers } from '../../../../../clientState/providers/Travellers/StoredTravellersProvider';
import { useTryingToContinueWithInvalidForm } from '../../../../../clientState/providers/Travellers/AllTravellersFieldsSelectionProvider';

const fieldTypes: Record<FieldTypeValue, React.ElementType> = {
  TEXT_FIELD: Text,
  RADIO_BUTTON: Radio,
  DATE_FIELD: Date,
  COMBO_BOX: Select,
  DATE_FIELD_3SELECTS: Date,
};

const fieldVariations = {
  passengerCardNumber: TextWithImage,
};

export interface FieldComponentProps {
  fieldData: Omit<TravellerField, 'position'>;
  indx: number;
}
export type TrackEventObject = {
  category?: string;
  action: string;
  label: string;
  page?: string;
};

const trackFieldEvents = (
  trackEvent: (config: TrackEventObject) => void,
  label
): void => {
  trackEvent({
    action: 'pax_details_error',
    label,
  });
};

const HIDDEN_FIELDS_WHEN_PRIME_TRAVELLER_SELECTED = ['name', 'firstLastName'];

const Field: FC<FieldComponentProps> = ({
  fieldData: {
    dataId,
    dataName,
    fieldType,
    options,
    labelId,
    helpers,
    attributes,
  },
  indx,
}) => {
  const { t } = useTranslation();
  const { trackEvent } = useTracking();
  const { updateTravellerField } = useTravellersSelectionActions();
  const { hasPickedAPrimeStoredTraveller } = useStoredTravellers();
  const fieldGroup = useFieldsGroup();
  const { value, valid, included } = useFieldSelection(dataName, fieldGroup);
  const { lastFlightDate, travellerType } =
    useTravellerSelectionTravellerData();
  const tryingToContinueWithInvalidForm = useTryingToContinueWithInvalidForm();
  const [localValue, setLocalValue] = useState(value);
  const [focusState, setFocusState] = useState(false);
  const [initialized, setInitialized] = useState(false);
  const emptyValue = !value;
  const isInvalid = !focusState && !valid && !emptyValue && initialized;
  const isEmpty = !focusState && !valid && emptyValue && initialized;

  useEffect(() => {
    setLocalValue(value);
  }, [value, setLocalValue]);

  useEffect(() => {
    if (isEmpty) {
      trackFieldEvents(
        trackEvent,
        `pax_${dataName.toLowerCase()}_${
          tryingToContinueWithInvalidForm ? 'submit' : 'inline'
        }_missing_error`
      );
    }
  }, [isEmpty, dataName, trackEvent, tryingToContinueWithInvalidForm]);

  useEffect(() => {
    if (isInvalid) {
      trackFieldEvents(
        trackEvent,
        `pax_${dataName.toLowerCase()}_${
          tryingToContinueWithInvalidForm ? 'submit' : 'inline'
        }_invalid_error`
      );
    }
  }, [isInvalid, dataName, trackEvent, tryingToContinueWithInvalidForm]);

  useEffect(() => {
    if (tryingToContinueWithInvalidForm && included) {
      setInitialized(true);
    }
  }, [tryingToContinueWithInvalidForm, setInitialized, included]);

  const onBlur: (e: { target: { value: string } }) => void = useCallback(
    ({ target: { value } }) => {
      setFocusState(false);
      if (!initialized) setInitialized(true);
      updateTravellerField({
        name: dataName,
        fieldGroup,
        value: value?.toString() || '',
      });
    },
    [dataName, initialized, setInitialized, updateTravellerField, fieldGroup]
  );

  const onChange: (fieldData: UpdateFieldAction['payload']) => void =
    useCallback(
      (fieldData) => {
        setLocalValue(fieldData.value);
      },
      [setLocalValue]
    );
  const onFocus = useCallback((): void => setFocusState(true), [setFocusState]);

  const helperLabel: string = useMemo(() => {
    const { help, error } = helpers;
    const baseKey = 'passengersManager.form';
    if (focusState && help) {
      return t(`${baseKey}.${labelId}.help`);
    } else if (!focusState && !valid && error) {
      return t(
        `${baseKey}.${
          !value ? `${dataName}.errorMandatory` : `${labelId}.error`
        }`
      );
    }
    return undefined;
  }, [dataName, value, t, focusState, helpers, labelId, valid]);

  const Input: FC<TypesFieldsProps> = useMemo(
    () => fieldVariations[dataName] || fieldTypes[fieldType],
    [dataName, fieldType]
  );

  if (
    hasPickedAPrimeStoredTraveller(indx) &&
    HIDDEN_FIELDS_WHEN_PRIME_TRAVELLER_SELECTED.includes(dataName) &&
    valid
  ) {
    return null;
  }

  return (
    <Input
      dataId={`${dataId}_${indx}`}
      dataName={dataName}
      options={options}
      autocapitalize={
        attributes?.autoCapitalize
          ? AutoCorrectCompleteTypes.ON
          : AutoCorrectCompleteTypes.OFF
      }
      autocorrect={
        attributes?.autoCorrect
          ? AutoCorrectCompleteTypes.ON
          : AutoCorrectCompleteTypes.OFF
      }
      autocomplete={
        attributes?.autoComplete
          ? AutoCorrectCompleteTypes.ON
          : AutoCorrectCompleteTypes.OFF
      }
      invalid={!valid && initialized}
      initialized={initialized}
      onChange={onChange}
      onBlur={onBlur}
      value={localValue}
      onFocus={onFocus}
      lastFlightDate={lastFlightDate}
      travellerType={travellerType}
      helperLabel={helperLabel}
      label={t(`passengersManager.form.${labelId}.label`)}
    />
  );
};

export default Field;
