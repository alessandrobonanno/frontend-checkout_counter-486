import React, { FC } from 'react';
import Field from '../../Field';
import { render, screen } from '@testing-library/react';
import { TestingProviders } from '../../../../../../../testUtils';
import TravellersFieldsDescriptorProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldTypeValue,
  FieldValidatorType,
} from '../../../../../../../generated/graphql';
import TravellersFieldsSelectionProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { passengerWithGenericMandatoryFieldsMock } from '../../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import userEvent from '@testing-library/user-event';

const translations = {
  passengersManager: {
    form: {
      expirationDate: {
        label: `expirationDateLabel`,
        help: `expirationDateHelp`,
        error: `expirationDateErrorValidation`,
        errorMandatory: `ErrorEmptyexpirationDate`,
      },
    },
  },
};

const fieldTranslations = translations.passengersManager.form.expirationDate;

const dataName = 'expirationDate';
const RenderComponent: FC = () => (
  <TestingProviders
    translations={translations}
    mocks={
      passengerWithGenericMandatoryFieldsMock(
        [dataName],
        [dataName],
        [FieldValidatorType.ExpirationDateValidator]
      ).mocks
    }
  >
    <TravellersFieldsDescriptorProvider indx={0}>
      <TravellersFieldsSelectionProvider indx={0}>
        <Field
          fieldData={{
            dataName: dataName,
            options: [],
            labelId: dataName,
            helpers: {
              help: true,
              error: true,
            },
            dataId: dataName,
            fieldType: FieldTypeValue.DateField,
          }}
          indx={1}
        />
      </TravellersFieldsSelectionProvider>
    </TravellersFieldsDescriptorProvider>
  </TestingProviders>
);
describe('ExpirationDate Validator', () => {
  it(`Should be invalid if it is empty`, async () => {
    render(<RenderComponent />);
    expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
    const input = await screen.getByLabelText(fieldTranslations.label);
    input.focus();
    expect(await screen.findByText(fieldTranslations.help)).toBeVisible();
    input.blur();
    expect(
      await screen.findByText(fieldTranslations.errorMandatory)
    ).toBeVisible();
  });
  it(`Should be invalid if it is expired at the lastFlight date`, async () => {
    render(<RenderComponent />);
    expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
    const input = await screen.getByLabelText(fieldTranslations.label);
    userEvent.type(input, '2020-04-01');
    input.blur();
    expect(await screen.findByText(fieldTranslations.error)).toBeVisible();
  });
  it(`Should be valid if it not expired at the lastFlight date`, async () => {
    render(<RenderComponent />);
    expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
    const input = await screen.getByLabelText(fieldTranslations.label);
    userEvent.type(input, '2027-10-16');
    input.blur();
    expect(
      await screen.queryByText(fieldTranslations.error)
    ).not.toBeInTheDocument();
    expect(await screen.getByLabelText('Valid Value')).toBeVisible();
  });
});
