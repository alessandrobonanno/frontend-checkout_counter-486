import Field from '../../Field';
import { render, screen } from '@testing-library/react';
import React, { FC } from 'react';

import { TestingProviders } from '../../../../../../../testUtils';
import TravellersFieldsDescriptorProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldTypeValue,
  TravellerAgeType,
} from '../../../../../../../generated/graphql';
import TravellersFieldsSelectionProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { passengerWithGenericMandatoryFieldsMock } from '../../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { VALIDATORS_MAP } from '../../../../../../../../.storybook/selectors/travellersFieldMockSelector';
import userEvent from '@testing-library/user-event';

const translations = {
  passengersManager: {
    form: {
      dateOfBirth: {
        label: `dateOfBirthLabel`,
        help: `dateOfBirthHelp`,
        error: `dateOfBirthErrorValidation`,
        errorMandatory: `ErrorEmptydateOfBirth`,
      },
    },
  },
};

const fieldTranslations = translations.passengersManager.form.dateOfBirth;

const dataName = 'dateOfBirth';
const RenderComponent: FC<{
  travellerType: TravellerAgeType;
}> = ({ travellerType }) => (
  <TestingProviders
    translations={translations}
    mocks={
      passengerWithGenericMandatoryFieldsMock(
        [dataName],
        [dataName],
        [VALIDATORS_MAP[travellerType]],
        travellerType
      ).mocks
    }
  >
    <TravellersFieldsDescriptorProvider indx={0}>
      <TravellersFieldsSelectionProvider indx={0}>
        <Field
          fieldData={{
            dataName: dataName,
            options: [],
            labelId: dataName,
            helpers: {
              help: true,
              error: true,
            },
            dataId: dataName,
            fieldType: FieldTypeValue.DateField,
          }}
          indx={1}
        />
      </TravellersFieldsSelectionProvider>
    </TravellersFieldsDescriptorProvider>
  </TestingProviders>
);
describe('DateOfBith Validator', () => {
  describe('Adults', () => {
    it(`Should be invalid if it is empty`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Adult} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      input.focus();
      expect(await screen.findByText(fieldTranslations.help)).toBeVisible();
      input.blur();
      expect(
        await screen.findByText(fieldTranslations.errorMandatory)
      ).toBeVisible();
    });
    it(`Should be invalid if it is not adult at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Adult} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2020-10-16');
      input.blur();
      expect(await screen.findByText(fieldTranslations.error)).toBeVisible();
    });
    it(`Should be valid if it is adult at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Adult} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2003-10-16');
      input.blur();
      expect(
        await screen.queryByText(fieldTranslations.error)
      ).not.toBeInTheDocument();
      expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    });
  });

  describe('Child', () => {
    it(`Should be invalid if it is empty`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Child} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      input.focus();
      expect(await screen.findByText(fieldTranslations.help)).toBeVisible();
      input.blur();
      expect(
        await screen.findByText(fieldTranslations.errorMandatory)
      ).toBeVisible();
    });
    it(`Should be invalid if it is not child at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Child} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2002-10-16');
      input.blur();
      expect(await screen.findByText(fieldTranslations.error)).toBeVisible();

      userEvent.type(input, '2025-10-16');
      input.blur();
      expect(await screen.findByText(fieldTranslations.error)).toBeVisible();
    });
    it(`Should be valid if it is child at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Child} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2020-10-16');
      await input.blur();
      expect(
        await screen.queryByText(fieldTranslations.error)
      ).not.toBeInTheDocument();
      expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    });
  });

  describe('Infant', () => {
    it(`Should be invalid if it is empty`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Infant} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      input.focus();
      expect(await screen.findByText(fieldTranslations.help)).toBeVisible();
      input.blur();
      expect(
        await screen.findByText(fieldTranslations.errorMandatory)
      ).toBeVisible();
    });
    it(`Should be invalid if it is not infant at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Infant} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2024-06-16');
      input.blur();
      expect(await screen.findByText(fieldTranslations.error)).toBeVisible();
    });
    it(`Should be valid if it is infant at the lastFlight date`, async () => {
      render(<RenderComponent travellerType={TravellerAgeType.Infant} />);
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();
      const input = await screen.getByLabelText(fieldTranslations.label);
      userEvent.type(input, '2024-07-16');
      input.blur();
      expect(
        await screen.queryByText(fieldTranslations.error)
      ).not.toBeInTheDocument();
      expect(await screen.getByLabelText('Valid Value')).toBeVisible();
    });
  });
});
