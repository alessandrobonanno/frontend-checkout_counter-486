import Field, { FieldComponentProps } from '../../Field';
import { render, screen } from '@testing-library/react';
import React from 'react';

import { TestingProviders } from '../../../../../../../testUtils';
import TravellersFieldsDescriptorProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldTypeValue,
  FieldValidatorType,
} from '../../../../../../../generated/graphql';
import TravellersFieldsSelectionProvider from '../../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { passengerWithGenericMandatoryFieldsMock } from '../../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { TranslationsObjType } from '../FieldTest.test';
import userEvent from '@testing-library/user-event';

const fieldData = (
  fieldName: string,
  fieldType: FieldTypeValue
): FieldComponentProps['fieldData'] => {
  return {
    dataName: fieldName,
    options: [],
    labelId: fieldName,
    helpers: {
      help: true,
      error: true,
    },
    dataId: fieldName,
    fieldType: fieldType,
  };
};

const generateTranslations = (fieldName: string): TranslationsObjType => {
  return {
    passengersManager: {
      form: {
        [fieldName]: {
          label: `${fieldName.toUpperCase()}Label`,
          help: `${fieldName.toUpperCase()}Help`,
          error: `${fieldName.toUpperCase()}ErrorValidation`,
          errorMandatory: `ErrorEmpty${fieldName.toUpperCase()}`,
        },
      },
    },
  };
};

const fieldsToTest = [
  {
    name: 'name',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.PaxNameRegex,
    wrongValue: '1111',
  },
  {
    name: 'email',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.EmailRegex,
    wrongValue: 'j.m@s.p',
  },
  {
    name: 'identification',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.NifRegex,
    wrongValue: '123456789',
  },
  {
    name: 'invoiceName',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.InvoiceNameRegex,
    wrongValue: 'test$`test',
  },
  {
    name: 'address',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.AddressRegex,
    wrongValue: 'Wrong$Address',
  },
  {
    name: 'phoneNumber',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.PhoneNumberRegex,
    wrongValue: '1234',
  },
  {
    name: 'zipCode',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.ZipCodeRegex,
    wrongValue: '12$23',
  },
  {
    name: 'city',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.CityRegex,
    wrongValue: '12$23',
  },
];

describe('TextInput Validators', () => {
  for (const field of fieldsToTest) {
    const { name, fieldType, validator, wrongValue } = field;
    const translations = generateTranslations(name);
    it(`Should show the appropriate texts text on focus, when it is empty and when it is wrong - ${name} FIELD`, async () => {
      render(
        <TestingProviders
          translations={translations}
          mocks={
            passengerWithGenericMandatoryFieldsMock([name], [name], [validator])
              .mocks
          }
        >
          <TravellersFieldsDescriptorProvider indx={0}>
            <TravellersFieldsSelectionProvider indx={0}>
              <Field fieldData={fieldData(name, fieldType)} indx={1} />
            </TravellersFieldsSelectionProvider>
          </TravellersFieldsDescriptorProvider>
        </TestingProviders>
      );
      expect(
        await screen.findByText(translations.passengersManager.form[name].label)
      ).toBeVisible();
      const input = await screen.getByRole('textbox');
      input.focus();
      expect(
        await screen.findByText(translations.passengersManager.form[name].help)
      ).toBeVisible();
      input.blur();
      expect(
        await screen.findByText(
          translations.passengersManager.form[name].errorMandatory
        )
      ).toBeVisible();
      userEvent.type(input, wrongValue);
      input.blur();
      expect(
        await screen.findByText(translations.passengersManager.form[name].error)
      ).toBeVisible();
    });
  }
});
