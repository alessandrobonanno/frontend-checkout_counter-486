import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TestingProviders } from '../../../../../../testUtils';
import Field from '../../Field';
import TravellersFieldsDescriptorProvider from '../../../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldTypeValue,
  FieldValidatorType,
} from '../../../../../../generated/graphql';
import TravellersFieldsSelectionProvider from '../../../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { passengerWithGenericMandatoryFieldsMock } from '../../../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import { FieldsProps } from '../Types';
import { FieldComponentProps } from '../Field';

const fieldData = (
  fieldName: string,
  fieldType: FieldTypeValue,
  options: FieldsProps['options'] = []
): FieldComponentProps['fieldData'] => {
  return {
    dataName: fieldName,
    options,
    labelId: fieldName,
    helpers: {
      help: true,
      error: true,
    },
    dataId: fieldName,
    fieldType: fieldType,
  };
};

export interface TranslationsObjType {
  passengersManager: {
    form: Record<string, Record<string, string>>;
  };
}

const generateTranslations = (fieldName: string): TranslationsObjType => {
  return {
    passengersManager: {
      form: {
        [fieldName]: {
          label: `${fieldName.toUpperCase()}Label`,
          help: `${fieldName.toUpperCase()}Help`,
          error: `${fieldName.toUpperCase()}ErrorValidation`,
          errorMandatory: `ErrorEmpty${fieldName.toUpperCase()}`,
        },
      },
    },
  };
};
interface FieldsToBeTestedAsText {
  name: string;
  fieldType: FieldTypeValue;
  validator: FieldValidatorType;
  wrongValue: string;
  validValue: string;
  options: FieldsProps['options'];
  pickValue: (_input: HTMLElement, _value: string) => void;
}

const fieldsToTestAsText: Array<FieldsToBeTestedAsText> = [
  {
    name: 'textField',
    fieldType: FieldTypeValue.TextField,
    validator: FieldValidatorType.PaxNameRegex,
    wrongValue: '12345',
    validValue: 'SomeName',
    options: [],
    pickValue: (input, value): void => userEvent.type(input, value),
  },
  {
    name: 'dateOfBirth',
    fieldType: FieldTypeValue.DateField,
    validator: FieldValidatorType.AdultDateValidator,
    wrongValue: '2025-05-02',
    validValue: '1987-05-02',
    options: [],
    pickValue: (input, value): void => userEvent.type(input, value),
  },
];

describe('Field Component', () => {
  for (const field of fieldsToTestAsText) {
    const {
      name,
      fieldType,
      validator,
      validValue,
      wrongValue,
      options,
      pickValue,
    } = field;
    const translations = generateTranslations(name);
    const fieldTranslations = translations.passengersManager.form[name];
    const expectedMissingTracking = {
      action: 'pax_details_error',
      label: `pax_${name.toLowerCase()}_inline_missing_error`,
    };
    it(`Should show label text, helpertext, valid and invalid icon and tracking for all text types - ${fieldType} Type`, async () => {
      const ga = jest.fn();
      render(
        <TestingProviders
          window={{ ga }}
          translations={translations}
          trackingCategory="flights_pax_page"
          trackingPage="flights/details-extras/"
          mocks={
            passengerWithGenericMandatoryFieldsMock([name], [name], [validator])
              .mocks
          }
        >
          <TravellersFieldsDescriptorProvider indx={0}>
            <TravellersFieldsSelectionProvider indx={0}>
              <Field fieldData={fieldData(name, fieldType, options)} indx={1} />
            </TravellersFieldsSelectionProvider>
          </TravellersFieldsDescriptorProvider>
        </TestingProviders>
      );
      expect(await screen.findByText(fieldTranslations.label)).toBeVisible();

      const input = await screen.findByLabelText(fieldTranslations.label);
      input.focus();
      expect(await screen.findByText(fieldTranslations.help)).toBeVisible();

      input.blur();
      expect(
        await screen.findByText(fieldTranslations.errorMandatory)
      ).toBeVisible();
      await pickValue(input, validValue);
      expect(await screen.getByLabelText('Reset Value')).toBeVisible();
      input.blur();

      expect(await screen.getByLabelText('Valid Value')).toBeVisible();
      expect(
        await screen.queryByText(fieldTranslations.error)
      ).not.toBeInTheDocument();
      userEvent.click(input);
      userEvent.click(await screen.getByLabelText('Reset Value'));

      await pickValue(input, wrongValue);
      input.blur();

      expect(
        await screen.findByText(fieldTranslations.error)
      ).toBeInTheDocument();
      expect(ga).toTrackPaxPageNthWith(2, {
        action: 'pax_details_error',
        label: `pax_${name.toLowerCase()}_inline_invalid_error`,
      });
      expect(ga).toTrackPaxPageNthWith(1, expectedMissingTracking);
      expect(ga).toTrackPaxPageNthWith(2, {
        action: 'pax_details_error',
        label: `pax_${name.toLowerCase()}_inline_invalid_error`,
      });
    });
  }

  it(`Should not show helpText and errorText when they are false`, async () => {
    const ga = jest.fn();
    const name = 'fieldName';
    const translations = generateTranslations(name);
    const fieldTranslations = translations.passengersManager.form[name];
    render(
      <TestingProviders
        window={{ ga }}
        translations={translations}
        mocks={
          passengerWithGenericMandatoryFieldsMock(
            [name],
            [name],
            [FieldValidatorType.NotEmpty]
          ).mocks
        }
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersFieldsSelectionProvider indx={0}>
            <Field
              fieldData={{
                dataName: name,
                options: [],
                labelId: name,
                helpers: {
                  help: false,
                  error: false,
                },
                dataId: name,
                fieldType: FieldTypeValue.TextField,
              }}
              indx={1}
            />
          </TravellersFieldsSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );
    expect(await screen.findByText(fieldTranslations.label)).toBeVisible();

    const input = await screen.findByLabelText(fieldTranslations.label);
    userEvent.type(input, 'SomeValue');
    expect(
      await screen.queryByText(fieldTranslations.help)
    ).not.toBeInTheDocument();
    input.blur();
    userEvent.type(input, '');
    input.blur();
    expect(
      await screen.queryByText(fieldTranslations.errorMandatory)
    ).not.toBeInTheDocument();
  });
});
