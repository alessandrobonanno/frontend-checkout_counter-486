import React, { FC } from 'react';
import { Flex } from 'prisma-design-system';
import Field from './Field';
import { TravellerField } from '../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import { Maybe } from '../../../../generated/graphql';

interface FieldGroupProps {
  fieldsData:
    | Maybe<Array<Maybe<Array<Omit<TravellerField, 'position'>>>>>
    | undefined;
  indx: number;
}

const FieldGroup: FC<FieldGroupProps> = ({ fieldsData = [], indx }) => {
  return (
    <>
      {fieldsData?.map((row, index) =>
        (row || []).map((field) => {
          return (
            <Flex
              id={`${field.dataId}_row${index}_pax${indx}`}
              key={`${field.dataId}_row${index}_pax${indx}`}
              width="100%"
            >
              <Field fieldData={field} indx={indx} />
            </Flex>
          );
        })
      )}
    </>
  );
};

export default FieldGroup;
