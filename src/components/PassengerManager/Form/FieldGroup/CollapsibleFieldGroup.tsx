import React, { FC, useCallback, useState } from 'react';
import { useTranslation, useTracking } from '@frontend-shell/react-hooks';
import { Flex, Checkbox, Collapsible } from 'prisma-design-system';

import FieldGroup from './FieldGroup';
import { Maybe } from '../../../../generated/graphql';
import { TravellerField } from '../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import {
  FieldGroups,
  useTravellersSelectionActions,
} from '../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';

interface CollapsibleFieldGroupProps {
  fieldsData: Array<Maybe<Array<Omit<TravellerField, 'position'>>>>;
  groupId: FieldGroups.OPTIONAL_REQUEST_FIELDS | FieldGroups.OPTIONAL_FIELDS;
  indx: number;
}

const CollapsibleFieldGroup: FC<CollapsibleFieldGroupProps> = ({
  fieldsData,
  groupId,
  indx,
}) => {
  const { t } = useTranslation();
  const { trackEvent } = useTracking();
  const { setGroupFieldsWithValueAsIncluded } = useTravellersSelectionActions();
  const [open, setOpen] = useState(false);
  const onChange = useCallback(
    ({ target: { checked } }): void => {
      setOpen(checked);
      if (checked && groupId === FieldGroups.OPTIONAL_REQUEST_FIELDS) {
        trackEvent({
          action: 'frequent_flyer_widget',
          label: 'frequent_flyer_widget_click_open',
        });
      }

      setGroupFieldsWithValueAsIncluded({ included: checked, group: groupId });
    },
    [setGroupFieldsWithValueAsIncluded, setOpen, groupId, trackEvent]
  );

  return (
    <>
      <Flex mb={4}>
        <Checkbox checked={open} onChange={onChange}>
          {t(
            groupId === FieldGroups.OPTIONAL_FIELDS
              ? 'passengersManager.form.apis.check'
              : 'passengersManager.form.optional.check'
          )}
        </Checkbox>
      </Flex>
      <Collapsible open={open} aria-label={groupId}>
        <FieldGroup fieldsData={fieldsData} indx={indx} />
      </Collapsible>
    </>
  );
};

export default CollapsibleFieldGroup;
