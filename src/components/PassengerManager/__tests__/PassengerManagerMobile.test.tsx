import React from 'react';
import {
  cleanup,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  fieldsMock,
  DATE_OF_BIRTH_TEXT,
  EMAIL_TEXT,
  FEMALE_TEXT,
  MALE_TEXT,
  NATIONAL_ID_CARD_TEXT,
  PASSENGER_CARD_TEXT,
  PASSPORT_TEXT,
  SECOND_EMAIL_TEXT,
  NAME_TEXT,
  SURNAME_TEXT,
  multiplePassengerFieldsWithPrimeLoggedUser,
  IDENTIFICATION_TYPE_TEXT,
  multiplePassengersFieldsMock,
  NAME_MANDATORY_TEXT,
  SURNAME_MANDATORY_TEXT,
} from '../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import TestingProviders from '../../../testUtils/providers/TestingProviders';
import PassengerManagerMobile from '../PassengerManagerMobile';
import PaxPageContinueButton from '../../PaxPageContinueButton';

const PRIME_TEXT = 'prime';
const PASSENGER_TEXT = 'Passenger';
const TRAVELLER_TYPE_TEXT = 'Adult';
const GDPR_INTRO_TEXT = 'gdpr';
const OPTIONAL_FIELDS_TEXT = 'Optional fields';
const OPTIONAL_REQUEST_FIELDS_TEXT = 'Optional request fields';
const DEFAULT_TEXT = 'new pax';
const STORED_TRAVELLER_TEXT = 'stored';
const CHANGE_TEXT = 'change';
const PRIME_OPTION_TEXT = 'prime member';

const translations = {
  passengersPage: {
    continueButton: 'ContinueButton text',
  },
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    addPassenger: PASSENGER_TEXT,
    storedTravellers: {
      default: DEFAULT_TEXT,
      label: STORED_TRAVELLER_TEXT,
    },
    travellerType: {
      ADULT: TRAVELLER_TYPE_TEXT,
    },
    gdpr: {
      introduction: GDPR_INTRO_TEXT,
    },
    prime: {
      primeOption: {
        label: PRIME_OPTION_TEXT,
      },
      primeOptionSelected: {
        label: PRIME_TEXT,
      },
      changePaxModal: {
        changeLabel: CHANGE_TEXT,
      },
    },
    form: {
      dateOfBirth: {
        ADULT: {
          label: DATE_OF_BIRTH_TEXT,
        },
      },
      email: {
        label: EMAIL_TEXT,
      },
      secondEmail: {
        label: SECOND_EMAIL_TEXT,
      },
      passengerCardNumber: {
        label: PASSENGER_CARD_TEXT,
      },
      name: {
        label: NAME_TEXT,
        errorMandatory: NAME_MANDATORY_TEXT,
      },
      firstLastName: {
        label: SURNAME_TEXT,
        errorMandatory: SURNAME_MANDATORY_TEXT,
      },
      identificationType: {
        label: IDENTIFICATION_TYPE_TEXT,
      },
      apis: {
        check: OPTIONAL_FIELDS_TEXT,
      },
      optional: {
        check: OPTIONAL_REQUEST_FIELDS_TEXT,
      },
    },
  },
};

describe('Passenger Manager', () => {
  beforeEach(() => {
    cleanup();
  });

  it('Does not show name and surname fields for logged prime user', async () => {
    render(
      <TestingProviders
        mocks={multiplePassengerFieldsWithPrimeLoggedUser().mocks}
        translations={translations}
      >
        <PassengerManagerMobile />
      </TestingProviders>
    );
    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    expect(screen.getByText(`${PASSENGER_TEXT} 2`)).toBeVisible();
    expect(await screen.getByText(PRIME_TEXT)).toBeVisible();
    expect(screen.queryAllByText(NAME_TEXT)).toHaveLength(1);
    expect(screen.queryAllByText(SURNAME_TEXT)).toHaveLength(1);
  });

  it('Displays elements on single passenger', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <PassengerManagerMobile />
      </TestingProviders>
    );
    await waitFor(() => {
      expect(screen.getByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    });

    expect(screen.getByText(TRAVELLER_TYPE_TEXT)).toBeVisible();

    const icon = screen.getByLabelText('GDPR');
    userEvent.click(icon);
    await waitFor(() => {
      expect(screen.getByText(GDPR_INTRO_TEXT)).toBeVisible();
    });

    userEvent.click(screen.getByRole('button', { name: 'close' }));
    await waitForElementToBeRemoved(() => screen.getByText(GDPR_INTRO_TEXT));
    expect(screen.getByText(TRAVELLER_TYPE_TEXT)).toBeVisible();
    expect(screen.getByText(MALE_TEXT)).toBeVisible();
    expect(screen.getByText(FEMALE_TEXT)).toBeVisible();
    expect(screen.getByText(DATE_OF_BIRTH_TEXT)).toBeVisible();
    expect(screen.getByText(EMAIL_TEXT)).toBeVisible();
    expect(screen.getByText(SECOND_EMAIL_TEXT)).toBeVisible();
    expect(screen.getByText(OPTIONAL_FIELDS_TEXT)).toBeVisible();
    expect(screen.getByText(OPTIONAL_REQUEST_FIELDS_TEXT)).toBeVisible();
  });

  it('Displays elements on multiple passengers', async () => {
    render(
      <TestingProviders
        mocks={multiplePassengersFieldsMock().mocks}
        translations={translations}
      >
        <PassengerManagerMobile />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    expect(screen.getByText(`${PASSENGER_TEXT} 2`)).toBeVisible();
    expect(screen.getAllByText(TRAVELLER_TYPE_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(NAME_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(SURNAME_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(DATE_OF_BIRTH_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(EMAIL_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(SECOND_EMAIL_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(OPTIONAL_FIELDS_TEXT)).toHaveLength(2);
    expect(screen.getAllByText(OPTIONAL_REQUEST_FIELDS_TEXT)).toHaveLength(2);
  });

  it('Shows name and surname fields after changing stored prime user selection', async () => {
    render(
      <TestingProviders
        mocks={multiplePassengerFieldsWithPrimeLoggedUser().mocks}
        translations={translations}
      >
        <PassengerManagerMobile />
      </TestingProviders>
    );

    const select = await screen.findByLabelText(PRIME_TEXT);

    userEvent.click(select);
    userEvent.selectOptions(select, screen.getAllByText(DEFAULT_TEXT)[0]);

    userEvent.click(await screen.getByText(CHANGE_TEXT));
    const name = (await screen.queryAllByText(
      NAME_TEXT
    )[0]) as HTMLInputElement;
    const surname = (await screen.queryAllByText(
      SURNAME_TEXT
    )[0]) as HTMLInputElement;

    await waitFor(() => {
      expect(screen.queryAllByText(NAME_TEXT)).toHaveLength(2);
      expect(name.value).toBe(undefined);
    });

    expect(screen.queryAllByText(SURNAME_TEXT)).toHaveLength(2);
    expect(surname.value).toBe(undefined);
  });

  it('Should call continue if form is valid without optionals', async () => {
    const continueWithPassengersPageValidations = jest.fn();
    render(
      <TestingProviders
        mocks={fieldsMock().mocks}
        continueWithPassengersPageValidations={
          continueWithPassengersPageValidations
        }
        translations={translations}
      >
        <PassengerManagerMobile />
        <PaxPageContinueButton />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    const continueButton = await screen.findByText(
      translations.passengersPage.continueButton
    );
    const dateOfBithInput = await screen.findByLabelText(DATE_OF_BIRTH_TEXT);
    const emailInput = await screen.findByLabelText(EMAIL_TEXT);
    const secondEmailInput = await screen.findByLabelText(SECOND_EMAIL_TEXT);

    expect(continueButton).toBeVisible();
    userEvent.type(dateOfBithInput, '1977-10-05');
    userEvent.type(emailInput, 'aaa@aa.es');
    userEvent.type(secondEmailInput, 'aaa@aa.es');

    userEvent.click(continueButton);
    await waitFor(() => {
      expect(continueWithPassengersPageValidations).toHaveBeenNthCalledWith(1, [
        {
          dateOfBirth: '1977-10-05',
          email: 'aaa@aa.es',
          secondEmail: 'aaa@aa.es',
          travellerGender: 'MALE',
          travellerType: 'ADULT',
        },
      ]);
    });
  });

  it('Should call continue if form is valid with optional fields', async () => {
    const continueWithPassengersPageValidations = jest.fn();
    render(
      <TestingProviders
        mocks={fieldsMock().mocks}
        continueWithPassengersPageValidations={
          continueWithPassengersPageValidations
        }
        translations={translations}
      >
        <PassengerManagerMobile />
        <PaxPageContinueButton />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    const continueButton = await screen.findByText(
      translations.passengersPage.continueButton
    );

    const optionalRequestFieldsCheckbox = await screen.findByLabelText(
      OPTIONAL_REQUEST_FIELDS_TEXT
    );
    const dateOfBithInput = await screen.findByLabelText(DATE_OF_BIRTH_TEXT);
    const emailInput = await screen.findByLabelText(EMAIL_TEXT);
    const secondEmailInput = await screen.findByLabelText(SECOND_EMAIL_TEXT);

    expect(continueButton).toBeVisible();
    userEvent.type(dateOfBithInput, '1977-10-05');
    userEvent.type(emailInput, 'aaa@aa.es');
    userEvent.type(secondEmailInput, 'aaa@aa.es');
    userEvent.click(optionalRequestFieldsCheckbox);
    const passengerCardNumberInput = await screen.findByLabelText(
      PASSENGER_CARD_TEXT
    );

    userEvent.type(passengerCardNumberInput, 'ASKT43');
    userEvent.click(continueButton);

    await waitFor(() => {
      expect(continueWithPassengersPageValidations).toHaveBeenNthCalledWith(1, [
        {
          dateOfBirth: '1977-10-05',
          email: 'aaa@aa.es',
          secondEmail: 'aaa@aa.es',
          travellerGender: 'MALE',
          passengerCardNumber: 'ASKT43',
          meal: 'STANDARD',
          travellerType: 'ADULT',
        },
      ]);
    });
  });

  it('Should not call continue if form is invalid', async () => {
    const continueWithPassengersPageValidations = jest.fn();
    render(
      <TestingProviders
        mocks={multiplePassengersFieldsMock().mocks}
        continueWithPassengersPageValidations={
          continueWithPassengersPageValidations
        }
        translations={translations}
      >
        <PassengerManagerMobile />
        <PaxPageContinueButton />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    const continueButton = await screen.findByText(
      translations.passengersPage.continueButton
    );
    expect(continueButton).toBeVisible();

    userEvent.click(continueButton);

    await waitFor(() => {
      expect(screen.getAllByText(NAME_MANDATORY_TEXT)).toHaveLength(2);
    });
    expect(screen.getAllByText(SURNAME_MANDATORY_TEXT)).toHaveLength(2);
    expect(continueWithPassengersPageValidations).not.toBeCalled();
  });
});
