import React, { FC } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from 'prisma-design-system';

import Header from '../Header';
import Form from '../Form';
import TravellerFieldsDescriptorProvider from '../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import TravellerFieldsSelectionProvider from '../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import ResidentMessage from '../Header/ResidentMessage';
import { TravellerAgeType } from '../../../generated/graphql';
import StoredTravellerSelector from '../StoredTravellerSelector/StoredTravellerSelector';

interface TravellerProps {
  indx: number;
  isResident: boolean;
  travellerType?: TravellerAgeType;
}

const Traveller: FC<TravellerProps> = ({ indx, isResident, travellerType }) => (
  <>
    <Box pb={3}>
      <Header indx={indx} travellerType={travellerType} />
    </Box>
    <Box>
      {isResident && (
        <Flex alignItems={'flex-start'}>
          <ResidentMessage />
        </Flex>
      )}
      <TravellerFieldsDescriptorProvider indx={indx}>
        <TravellerFieldsSelectionProvider indx={indx}>
          <StoredTravellerSelector indx={indx} />
          <Form indx={indx} />
        </TravellerFieldsSelectionProvider>
      </TravellerFieldsDescriptorProvider>
    </Box>
  </>
);

Traveller.propTypes = {
  indx: PropTypes.number.isRequired,
  travellerType: PropTypes.oneOf(Object.values(TravellerAgeType)),
};

export default Traveller;
