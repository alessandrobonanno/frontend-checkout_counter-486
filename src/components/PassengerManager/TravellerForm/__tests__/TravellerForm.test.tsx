import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import TravellerForm from '../TravellerForm';
import {
  FEMALE_TEXT,
  fieldsMock,
  MALE_TEXT,
  multiplePassengerFieldsWithPrimeLoggedUser,
  NATIONAL_ID_CARD_TEXT,
  PASSPORT_TEXT,
} from '../../../../clientState/providers/Travellers/__tests__/fieldsMocks';
import TravellersFieldsDescriptorProvider from '../../../../clientState/providers/Travellers/TravellerFieldsDescriptorProvider';
import TravellersSelectionProvider from '../../../../clientState/providers/Travellers/TravellerFieldsSelectionProvider';
import { TestingProviders } from '../../../../testUtils';
import { TravellerAgeType } from '../../../../generated/graphql';

const DATE_OF_BIRTH_LABEL = 'date of birth';
const OPTIONAL_FIELD_LABEL = 'optional fields';
const OPTIONAL_REQUEST_FIELD_LABEL = 'optional request fields';
const IDENTIFICATION_LABEL = 'identification';
const PAX_CARD_NUMBER_LABEL = 'card number';
const PASSENGER_TEXT = 'passenger';
const TRAVELLER_TYPE_TEXT = 'Adult';
const GDPR_INTRO_TEXT = 'gdpr';
const RESIDENT_TITLE = 'resident title';
const RESIDENT_BODY = 'resident body';
const PRIME_LABEL = 'prime';

const translations = {
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    addPassenger: PASSENGER_TEXT,
    travellerType: {
      ADULT: TRAVELLER_TYPE_TEXT,
    },
    resident: {
      message: {
        title: RESIDENT_TITLE,
        body: RESIDENT_BODY,
      },
    },
    gdpr: {
      introduction: GDPR_INTRO_TEXT,
    },
    prime: {
      primeOptionSelected: {
        label: PRIME_LABEL,
      },
    },
    form: {
      dateOfBirth: {
        ADULT: {
          label: DATE_OF_BIRTH_LABEL,
        },
      },
      optional: {
        check: OPTIONAL_REQUEST_FIELD_LABEL,
      },
      apis: {
        check: OPTIONAL_FIELD_LABEL,
      },
      identificationType: {
        label: IDENTIFICATION_LABEL,
      },
      passengerCardNumber: {
        label: PAX_CARD_NUMBER_LABEL,
      },
    },
  },
};

describe('TravellerForm', () => {
  it('Displays the header elements', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <TravellerForm
          isResident={false}
          travellerType={TravellerAgeType.Adult}
          indx={0}
        />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    expect(screen.getByText(TRAVELLER_TYPE_TEXT)).toBeVisible();
    expect(screen.getByLabelText('GDPR')).toBeVisible();
  });

  it('Displays the resident message when isResident', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <TravellerForm
          isResident={true}
          travellerType={TravellerAgeType.Adult}
          indx={0}
        />
      </TestingProviders>
    );

    expect(await screen.findByText(RESIDENT_TITLE)).toBeVisible();
    expect(screen.getByText(RESIDENT_BODY)).toBeVisible();
  });

  it('Displays the traveller selector', async () => {
    render(
      <TestingProviders
        mocks={multiplePassengerFieldsWithPrimeLoggedUser().mocks}
        translations={translations}
      >
        <TravellerForm
          isResident={false}
          travellerType={TravellerAgeType.Adult}
          indx={0}
        />
      </TestingProviders>
    );
    await waitFor(() => screen.getByLabelText(PRIME_LABEL));
  });

  it('Displays the form elements', async () => {
    render(
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersSelectionProvider indx={0}>
            <TravellerForm
              isResident={false}
              travellerType={TravellerAgeType.Adult}
              indx={0}
            />
          </TravellersSelectionProvider>
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );

    expect(await screen.findByLabelText(MALE_TEXT)).toBeInTheDocument();
    expect(screen.getByLabelText(FEMALE_TEXT)).toBeInTheDocument();
    expect(screen.getByLabelText(DATE_OF_BIRTH_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(OPTIONAL_FIELD_LABEL)).toBeInTheDocument();
    expect(
      screen.getByLabelText(OPTIONAL_REQUEST_FIELD_LABEL)
    ).toBeInTheDocument();
    expect(screen.getByLabelText(IDENTIFICATION_LABEL)).toBeInTheDocument();
    expect(screen.getByLabelText(PAX_CARD_NUMBER_LABEL)).toBeInTheDocument();
  });
});
