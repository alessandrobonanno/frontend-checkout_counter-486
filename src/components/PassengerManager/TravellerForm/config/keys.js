const translations = [
  {
    inputPath: ['booking', 'paxinfo.apis.check.text'],
    outputPath: ['passengersManager', 'form', 'apis', 'check'],
  },
  {
    inputPath: ['booking', 'paxinfo.optional.requests.title'],
    outputPath: ['passengersManager', 'form', 'optional', 'check'],
  },
  {
    inputPath: ['booking', 'paxinfo.frequentflyer.code'],
    outputPath: ['passengersManager', 'form', 'passengerCardNumber', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.frequentflyer.code.helpmessage'],
    outputPath: ['passengersManager', 'form', 'passengerCardNumber', 'help'],
  },
  {
    inputPath: ['booking', 'paxinfo.frequentFlyerCardCodes.error'],
    outputPath: [
      'passengersManager',
      'form',
      'passengerCardNumber',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.travellerGender'],
    outputPath: ['passengersManager', 'form', 'travellerGender', 'label'],
  },
  {
    inputPath: ['booking', 'shopping.cart.MALE'],
    outputPath: ['booking', 'shopping', 'cart', 'MALE'],
  },
  {
    inputPath: ['booking', 'shopping.cart.FEMALE'],
    outputPath: ['booking', 'shopping', 'cart', 'FEMALE'],
  },
  {
    inputPath: ['booking', 'shopping.cart.PASSPORT'],
    outputPath: ['booking', 'shopping', 'cart', 'PASSPORT'],
  },
  {
    inputPath: ['booking', 'shopping.cart.NATIONAL_ID_CARD'],
    outputPath: ['booking', 'shopping', 'cart', 'NATIONAL_ID_CARD'],
  },
  {
    inputPath: ['booking', 'shopping.cart.NIE'],
    outputPath: ['booking', 'shopping', 'cart', 'NIE'],
  },
  {
    inputPath: ['booking', 'shopping.cart.NIF'],
    outputPath: ['booking', 'shopping', 'cart', 'NIF'],
  },
  {
    inputPath: ['booking', 'paxinfo.name'],
    outputPath: ['passengersManager', 'form', 'name', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.name.placeholder'],
    outputPath: ['passengersManager', 'form', 'name', 'placeholder'],
  },
  {
    inputPath: ['booking', 'paxinfo.name.error'],
    outputPath: ['passengersManager', 'form', 'name', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.name.errorMandatory'],
    outputPath: ['passengersManager', 'form', 'name', 'errorMandatory'],
  },
  {
    inputPath: ['booking', 'paxinfo.name.helpmessage'],
    outputPath: ['passengersManager', 'form', 'name', 'help'],
  },
  {
    inputPath: ['booking', 'paxinfo.firstLastName'],
    outputPath: ['passengersManager', 'form', 'firstLastName', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.firstLastName.placeholder'],
    outputPath: ['passengersManager', 'form', 'firstLastName', 'placeholder'],
  },
  {
    inputPath: ['booking', 'paxinfo.firstLastName.error'],
    outputPath: ['passengersManager', 'form', 'firstLastName', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.firstLastName.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'firstLastName',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.firstLastName.helpmessage'],
    outputPath: ['passengersManager', 'form', 'firstLastName', 'help'],
  },
  {
    inputPath: ['booking', 'paxinfo.secondLastName'],
    outputPath: ['passengersManager', 'form', 'secondLastName', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.secondLastName.placeholder'],
    outputPath: ['passengersManager', 'form', 'secondLastName', 'placeholder'],
  },
  {
    inputPath: ['booking', 'paxinfo.secondLastName.error'],
    outputPath: ['passengersManager', 'form', 'secondLastName', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.secondLastName.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'secondLastName',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.secondLastName.helpmessage'],
    outputPath: ['passengersManager', 'form', 'secondLastName', 'help'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.ADULT'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'ADULT', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.CHILD'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'CHILD', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.INFANT'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'INFANT', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.placeholder'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'placeholder'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.error.ADULT'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'ADULT', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.error.CHILD'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'CHILD', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.error.INFANT'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'INFANT', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.dateOfBirth.errorMandatory'],
    outputPath: ['passengersManager', 'form', 'dateOfBirth', 'errorMandatory'],
  },
  {
    inputPath: ['booking', 'paxinfo.nationalityCountryCode'],
    outputPath: [
      'passengersManager',
      'form',
      'nationalityCountryCode',
      'label',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.nationalityCountryCode.placeholder'],
    outputPath: [
      'passengersManager',
      'form',
      'nationalityCountryCode',
      'placeholder',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.nationalityCountryCode.error'],
    outputPath: [
      'passengersManager',
      'form',
      'nationalityCountryCode',
      'error',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.nationalityCountryCode.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'nationalityCountryCode',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.countryCodeOfResidence'],
    outputPath: [
      'passengersManager',
      'form',
      'countryCodeOfResidence',
      'label',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.countryCodeOfResidence.placeholder'],
    outputPath: [
      'passengersManager',
      'form',
      'countryCodeOfResidence',
      'placeholder',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.countryCodeOfResidence.error'],
    outputPath: [
      'passengersManager',
      'form',
      'countryCodeOfResidence',
      'error',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.countryCodeOfResidence.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'countryCodeOfResidence',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationType'],
    outputPath: ['passengersManager', 'form', 'identificationType', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationType.placeholder'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationType',
      'placeholder',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationType.error'],
    outputPath: ['passengersManager', 'form', 'identificationType', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationType.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationType',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identification'],
    outputPath: ['passengersManager', 'form', 'identification', 'label'],
  },
  {
    inputPath: ['booking', 'paxinfo.identification.placeholder'],
    outputPath: ['passengersManager', 'form', 'identification', 'placeholder'],
  },
  {
    inputPath: ['booking', 'paxinfo.identification.error'],
    outputPath: ['passengersManager', 'form', 'identification', 'error'],
  },
  {
    inputPath: ['booking', 'paxinfo.identification.errorMandatory'],
    outputPath: [
      'passengersManager',
      'form',
      'identification',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identification.helpmessage'],
    outputPath: ['passengersManager', 'form', 'identification', 'help'],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationExpirationDate'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationExpirationDate',
      'label',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationExpirationDate.placeholder'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationExpirationDate',
      'placeholder',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationExpirationDate.error'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationExpirationDate',
      'error',
    ],
  },
  {
    inputPath: [
      'booking',
      'paxinfo.identificationExpirationDate.errorMandatory',
    ],
    outputPath: [
      'passengersManager',
      'form',
      'identificationExpirationDate',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.localityCodeOfResidence'],
    outputPath: [
      'passengersManager',
      'form',
      'localityCodeOfResidence',
      'label',
    ],
    scope: ['ED_es-ES', '?OP_es-ES'],
  },
  {
    inputPath: ['booking', 'paxinfo.localityCodeOfResidence.placeholder'],
    outputPath: [
      'passengersManager',
      'form',
      'localityCodeOfResidence',
      'default',
    ],
    scope: ['ED_es-ES', '?OP_es-ES'],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationIssueCountryCode'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationIssueCountryCode',
      'label',
    ],
  },
  {
    inputPath: [
      'booking',
      'paxinfo.identificationIssueCountryCode.placeholder',
    ],
    outputPath: [
      'passengersManager',
      'form',
      'identificationIssueCountryCode',
      'placeholder',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.identificationIssueCountryCode.error'],
    outputPath: [
      'passengersManager',
      'form',
      'identificationIssueCountryCode',
      'error',
    ],
  },
  {
    inputPath: [
      'booking',
      'paxinfo.identificationIssueCountryCode.errorMandatory',
    ],
    outputPath: [
      'passengersManager',
      'form',
      'identificationIssueCountryCode',
      'errorMandatory',
    ],
  },
  {
    inputPath: ['booking', 'paxinfo.meal.title'],
    outputPath: ['passengersManager', 'form', 'meal', 'label'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.STANDARD'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'STANDARD'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.LOW_CHOLESTEROL'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'LOW_CHOLESTEROL'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.DIABETIC'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'DIABETIC'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.VEGAN'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'VEGAN'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.HINDU'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'HINDU'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.CHILD'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'CHILD'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.BABY'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'BABY'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.HALAL'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'HALAL'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.LOW_SODIUM'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'LOW_SODIUM'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.KOSHER'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'KOSHER'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.STANDARD'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'STANDARD'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.GLUTEN_FREE'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'GLUTEN_FREE'],
  },
  {
    inputPath: ['booking', 'shopping.cart.meal.VEGETARIAN'],
    outputPath: ['booking', 'shopping', 'cart', 'meal', 'VEGETARIAN'],
  },
  {
    inputPath: ['paxinfo', 'resident.message.title'],
    outputPath: ['passengersManager', 'resident', 'message', 'title'],
    scope: ['ED_es-ES', '?OP_es-ES'],
  },
  {
    inputPath: ['paxinfo', 'resident.message'],
    outputPath: ['passengersManager', 'resident', 'message', 'body'],
    scope: ['ED_es-ES', '?OP_es-ES'],
  },
];

module.exports = translations;
