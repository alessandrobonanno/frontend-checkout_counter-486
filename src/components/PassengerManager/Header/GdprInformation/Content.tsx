import React, { FC } from 'react';
import { Box, Text, Link } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

import ContentRow from './ContentRow';

const keyIds = [
  'dataController',
  'processingPurpose',
  'legalGround',
  'recipients',
  'subjetRights',
];
interface ContentProps {
  lineHeight: number;
}

const Content: FC<ContentProps> = ({ lineHeight }) => {
  const { t } = useTranslation();

  return (
    <Box overflowWrap="break-word" lineHeight={lineHeight}>
      <Box mb={3}>
        <Text color="neutrals.2" fontSize="body.0" fontWeight="normal">
          {t('passengersManager.gdpr.introduction')}
        </Text>
      </Box>
      <Box>
        {keyIds.map((keyId) => (
          <ContentRow key={keyId} keyId={keyId} />
        ))}
        <Box>
          <Text color="neutrals.0" fontSize="body.0" fontWeight="normal">
            {t('passengersManager.gdpr.moreInfo.before')}{' '}
            <Link href={t('passengersManager.gdpr.moreInfo.link')}>
              {t('passengersManager.gdpr.moreInfo.bold')}
            </Link>{' '}
            {t('passengersManager.gdpr.moreInfo.after')}
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

export default Content;
