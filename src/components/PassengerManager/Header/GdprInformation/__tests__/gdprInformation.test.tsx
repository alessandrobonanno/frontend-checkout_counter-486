import React from 'react';
import userEvent from '@testing-library/user-event';
import {
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import TestingProviders from '../../../../../testUtils/providers/TestingProviders';
import GdprInformation from '../GdprInformation';

const GDPR_LABEL = 'GDPR';
const GDPR_INTRO_TEXT = 'gdpr';
const GDPR_DATACONTROLLER_NAME = 'datacontroller name';
const GDPR_DATACONTROLLER_VALUE = 'datacontroller value';
const GDPR_PROCESSING_PURPOSE_NAME = 'processing name';
const GDPR_PROCESSING_PURPOSE_VALUE = 'processing value';
const GDPR_LEGAL_GROUND_NAME = 'legal name';
const GDPR_LEGAL_GROUND_VALUE = 'legal value';
const GDPR_RECIPIENTS_NAME = 'recipients name';
const GDPR_RECIPIENTS_VALUE = 'recipients value';
const GDPR_RIGHTS_NAME = 'rights name';
const GDPR_RIGHTS_VALUE = 'right value';
const GDPR_MORE_INFO_BEFORE_TEXT = 'before';
const GDPR_MORE_INFO_BOLD_TEXT = 'bold';
const GDPR_MORE_INFO_AFTER_TEXT = 'after';

const translations = {
  passengersManager: {
    gdpr: {
      introduction: GDPR_INTRO_TEXT,
      dataController: {
        name: GDPR_DATACONTROLLER_NAME,
        value: GDPR_DATACONTROLLER_VALUE,
      },
      processingPurpose: {
        name: GDPR_PROCESSING_PURPOSE_NAME,
        value: GDPR_PROCESSING_PURPOSE_VALUE,
      },
      legalGround: {
        name: GDPR_LEGAL_GROUND_NAME,
        value: GDPR_LEGAL_GROUND_VALUE,
      },
      recipients: {
        name: GDPR_RECIPIENTS_NAME,
        value: GDPR_RECIPIENTS_VALUE,
      },
      subjetRights: {
        name: GDPR_RIGHTS_NAME,
        value: GDPR_RIGHTS_VALUE,
      },
      moreInfo: {
        before: GDPR_MORE_INFO_BEFORE_TEXT,
        bold: GDPR_MORE_INFO_BOLD_TEXT,
        after: GDPR_MORE_INFO_AFTER_TEXT,
      },
    },
  },
};

describe('GDPR Information', () => {
  it('Shows gdpr information', async () => {
    render(
      <TestingProviders mocks={{}} translations={translations}>
        <GdprInformation />
      </TestingProviders>
    );

    const icon = await screen.findByLabelText(GDPR_LABEL);
    userEvent.click(icon);

    await waitFor(() => {
      expect(screen.getByText(GDPR_INTRO_TEXT)).toBeVisible();
    });
    expect(screen.getByText(GDPR_DATACONTROLLER_NAME)).toBeVisible();
    expect(screen.getByText(GDPR_DATACONTROLLER_VALUE)).toBeVisible();
    expect(screen.getByText(GDPR_PROCESSING_PURPOSE_NAME)).toBeVisible();
    expect(screen.getByText(GDPR_PROCESSING_PURPOSE_VALUE)).toBeVisible();
    expect(screen.getByText(GDPR_RIGHTS_NAME)).toBeVisible();
    expect(screen.getByText(GDPR_RIGHTS_VALUE)).toBeVisible();
    expect(screen.getByText(GDPR_LEGAL_GROUND_NAME)).toBeVisible();
    expect(screen.getByText(GDPR_LEGAL_GROUND_VALUE)).toBeVisible();
    expect(screen.getByText(GDPR_RECIPIENTS_NAME)).toBeVisible();
    expect(screen.getByText(GDPR_RECIPIENTS_VALUE)).toBeVisible();
    expect(screen.getByText(GDPR_MORE_INFO_BOLD_TEXT)).toBeVisible();
    expect(
      screen.getByText(`${GDPR_MORE_INFO_BEFORE_TEXT}`, { exact: false })
    ).toBeVisible();
    expect(
      screen.getByText(`${GDPR_MORE_INFO_AFTER_TEXT}`, { exact: false })
    ).toBeVisible();
  });

  it('Closes gdpr information', async () => {
    render(
      <TestingProviders mocks={{}} translations={translations}>
        <GdprInformation />
      </TestingProviders>
    );

    const icon = await screen.findByLabelText(GDPR_LABEL);
    userEvent.click(icon);

    await waitFor(() => {
      expect(screen.getByText(GDPR_INTRO_TEXT)).toBeVisible();
    });

    userEvent.click(screen.getByRole('button', { name: 'close' }));
    await waitForElementToBeRemoved(() => screen.getByText(GDPR_INTRO_TEXT));

    expect(screen.queryByText(GDPR_INTRO_TEXT)).not.toBeInTheDocument();
  });
});
