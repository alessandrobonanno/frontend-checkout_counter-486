import React, { FC } from 'react';

import { Modal, ModalHeader, ModalContent } from 'prisma-design-system';
interface ModalLayoutProps {
  open: boolean;
  onClose: () => void;
}

const ModalLayout: FC<ModalLayoutProps> = ({ open, onClose, children }) => (
  <Modal size="full" p={3} open={open} onClose={onClose}>
    <ModalHeader withCloseIcon />
    <ModalContent px={4} pb={4}>
      {children}
    </ModalContent>
  </Modal>
);

export default ModalLayout;
