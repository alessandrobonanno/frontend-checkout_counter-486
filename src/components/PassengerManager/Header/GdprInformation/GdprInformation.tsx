import React, { FC, useState, useRef } from 'react';
import {
  Box,
  InformationIconNegativeIcon,
  useMediaQueries,
} from 'prisma-design-system';

import PopoverLayout from './PopoverLayout';
import ModalLayout from './ModalLayout';
import Content from './Content';

const GdprInformation: FC = () => {
  const { md } = useMediaQueries();
  const ref = useRef<HTMLDivElement | null>(null);
  const [open, setOpen] = useState(false);

  const Layout = md ? PopoverLayout : ModalLayout;
  const lineHeight = md ? 0.75 : 1;
  return (
    <Box ref={ref} display={'inline-block'} position={'relative'} mx={2}>
      <InformationIconNegativeIcon
        color="neutrals.3"
        size="small"
        viewBox="2 2 28 28"
        aria-label={'GDPR'}
        onClick={(): void => setOpen(true)}
      />
      <Layout parentRef={ref} open={open} onClose={(): void => setOpen(false)}>
        <Content lineHeight={lineHeight} />
      </Layout>
    </Box>
  );
};

export default GdprInformation;
