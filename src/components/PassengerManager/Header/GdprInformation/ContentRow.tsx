import React, { FC } from 'react';

import { Box, Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

interface ContentRowProps {
  keyId: string;
}

const ContentRow: FC<ContentRowProps> = ({ keyId }) => {
  const { t } = useTranslation();

  return (
    <Box mb={2}>
      <Text color="neutrals.0" fontSize="body.0" fontWeight="normal">
        {t(`passengersManager.gdpr.${keyId}.name`)}
      </Text>
      <br />
      <Text color="neutrals.0" fontSize="body.0" fontWeight="bold">
        {t(`passengersManager.gdpr.${keyId}.value`)}
      </Text>
    </Box>
  );
};

export default ContentRow;
