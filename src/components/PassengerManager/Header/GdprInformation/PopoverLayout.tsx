import React, { FC } from 'react';
import { Popover, useDirection, useEventListener } from 'prisma-design-system';

interface PopoverLayoutProps {
  parentRef?: React.RefObject<HTMLDivElement>;
  open: boolean;
  onClose: () => void;
}

const PopoverLayout: FC<PopoverLayoutProps> = ({
  parentRef,
  open,
  onClose,
  children,
}) => {
  const isRtl = useDirection();
  useEventListener('click', (e) => {
    if (!parentRef?.current?.contains?.(e.target)) {
      onClose();
    }
  });

  return (
    <Popover align="start" placement={isRtl ? 'left' : 'right'} open={open}>
      {children}
    </Popover>
  );
};

export default PopoverLayout;
