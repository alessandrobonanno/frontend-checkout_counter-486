import React, { FC } from 'react';
import { Flex, Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

import GdprInformation from './GdprInformation/GdprInformation';

interface TitleProps {
  indx: number;
}

const Title: FC<TitleProps> = ({ indx }) => {
  const { t } = useTranslation();

  return (
    <Flex alignItems="center">
      <Text fontSize="heading.0" color="neutrals.1" fontWeight="medium">
        {`${t('passengersManager.addPassenger')} ${indx}`}
        <GdprInformation />
      </Text>
    </Flex>
  );
};

export default Title;
