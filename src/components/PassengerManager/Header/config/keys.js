const translations = [
  {
    inputPath: ['booking', 'paxinfo.heading'],
    outputPath: ['passengersManager', 'header'],
  },
  {
    inputPath: ['booking', 'paxinfo.heading'],
    outputPath: ['passengersManager', 'header'],
  },
  {
    inputPath: ['passengersManager', 'addPassenger'],
    outputPath: ['passengersManager', 'addPassenger'],
  },
  {
    inputPath: ['passengersManager', 'ADULT'],
    outputPath: ['passengersManager', 'travellerType', 'ADULT'],
  },
  {
    inputPath: ['passengersManager', 'CHILD'],
    outputPath: ['passengersManager', 'travellerType', 'CHILD'],
  },
  {
    inputPath: ['passengersManager', 'INFANT'],
    outputPath: ['passengersManager', 'travellerType', 'INFANT'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.introduction'],
    outputPath: ['passengersManager', 'gdpr', 'introduction'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.data.controller.name'],
    outputPath: ['passengersManager', 'gdpr', 'dataController', 'name'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.data.controller.value'],
    outputPath: ['passengersManager', 'gdpr', 'dataController', 'value'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.processing.purpose.name'],
    outputPath: ['passengersManager', 'gdpr', 'processingPurpose', 'name'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.processing.purpose.value'],
    outputPath: ['passengersManager', 'gdpr', 'processingPurpose', 'value'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.legal.ground.name'],
    outputPath: ['passengersManager', 'gdpr', 'legalGround', 'name'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.legal.ground.value'],
    outputPath: ['passengersManager', 'gdpr', 'legalGround', 'value'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.recipients.name'],
    outputPath: ['passengersManager', 'gdpr', 'recipients', 'name'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.recipients.value'],
    outputPath: ['passengersManager', 'gdpr', 'recipients', 'value'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.data.subjet.rights.name'],
    outputPath: ['passengersManager', 'gdpr', 'subjetRights', 'name'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.data.subjet.rights.value'],
    outputPath: ['passengersManager', 'gdpr', 'subjetRights', 'value'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.more.info.text'],
    outputPath: ['passengersManager', 'gdpr', 'moreInfo', 'text'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.more.info.text.before'],
    outputPath: ['passengersManager', 'gdpr', 'moreInfo', 'before'],
  },
  {
    inputPath: ['booking', 'gdpr.more.info.text.after'],
    outputPath: ['passengersManager', 'gdpr', 'moreInfo', 'after'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.more.info.text.link'],
    outputPath: ['passengersManager', 'gdpr', 'moreInfo', 'link'],
  },
  {
    inputPath: ['booking', 'gdpr.pax.more.info.text.bold'],
    outputPath: ['passengersManager', 'gdpr', 'moreInfo', 'bold'],
  },
];

module.exports = translations;
