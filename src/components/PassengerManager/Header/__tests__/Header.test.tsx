import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen, waitFor } from '@testing-library/react';
import Header from '../Header';
import TestingProviders from '../../../../testUtils/providers/TestingProviders';
import { TravellerAgeType } from '../../../../generated/graphql';

const PASSENGER_TEXT = 'Passenger';
const TRAVELLER_TYPE_TEXT = 'Adult';
const GDPR_INTRO_TEXT = 'gdpr';

const translations = {
  passengersManager: {
    addPassenger: PASSENGER_TEXT,
    travellerType: {
      ADULT: TRAVELLER_TYPE_TEXT,
    },
    gdpr: {
      introduction: GDPR_INTRO_TEXT,
    },
  },
};

describe('Header', () => {
  it('Shows pax index and traveller age type', async () => {
    render(
      <TestingProviders translations={translations}>
        <Header indx={0} travellerType={TravellerAgeType.Adult} />
      </TestingProviders>
    );

    expect(await screen.findByText(`${PASSENGER_TEXT} 1`)).toBeVisible();
    expect(screen.getByText(TRAVELLER_TYPE_TEXT)).toBeVisible();
  });

  it('Shows gdpr information upon clicking information icon', async () => {
    render(
      <TestingProviders translations={translations}>
        <Header indx={0} travellerType={TravellerAgeType.Adult} />
      </TestingProviders>
    );

    const icon = await screen.findByLabelText('GDPR');
    userEvent.click(icon);

    await waitFor(() => {
      expect(screen.getByText(GDPR_INTRO_TEXT)).toBeVisible();
    });
  });
});
