import React, { FC } from 'react';
import { Flex, Text, WarnIcon } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';

const ResidentMessage: FC = () => {
  const { t } = useTranslation();

  return (
    <Flex alignItems="flex-start" mb={5}>
      <Flex flexShrink={'0'} mr={2}>
        <WarnIcon size={'large'} color={'neutrals.0'} />
      </Flex>
      <div>
        <Text fontSize="body.1" color="warning.default" fontWeight="bold">
          {t('passengersManager.resident.message.title')}
        </Text>{' '}
        <Text fontSize="body.1" color="neutrals.1" fontWeight="medium">
          {t('passengersManager.resident.message.body')}
        </Text>
      </div>
    </Flex>
  );
};

export default ResidentMessage;
