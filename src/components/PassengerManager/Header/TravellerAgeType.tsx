import React, { FC } from 'react';
import { Text } from 'prisma-design-system';
import { useTranslation } from '@frontend-shell/react-hooks';
import { TravellerAgeType as TravellerAgeTypeEnum } from '../../../generated/graphql';

interface TravellerAgeTypeProps {
  type?: TravellerAgeTypeEnum;
}

const TravellerAgeTypeComponent: FC<TravellerAgeTypeProps> = ({ type }) => {
  const { t } = useTranslation();

  return type ? (
    <Text fontSize="body.1" color="neutrals.3">
      {t(`passengersManager.travellerType.${type}`)}
    </Text>
  ) : null;
};

export default TravellerAgeTypeComponent;
