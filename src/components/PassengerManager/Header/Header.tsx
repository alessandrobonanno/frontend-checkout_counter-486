import React, { FC } from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'prisma-design-system';

import UserIcon from './UserIcon';
import Title from './Title';
import TravellerAgeType from './TravellerAgeType';
import { TravellerAgeType as TravellerAgeTypeEnum } from '../../../generated/graphql';

interface PassengersHeaderProps {
  indx: number;
  travellerType?: TravellerAgeTypeEnum;
}

const Header: FC<PassengersHeaderProps> = ({ indx, travellerType }) => (
  <Flex alignItems={'center'}>
    <UserIcon />
    <Flex flexDirection="column" pl={1}>
      <Title indx={indx + 1} />
      <TravellerAgeType type={travellerType} />
    </Flex>
  </Flex>
);

Header.propTypes = {
  indx: PropTypes.number.isRequired,
};

export default Header;
