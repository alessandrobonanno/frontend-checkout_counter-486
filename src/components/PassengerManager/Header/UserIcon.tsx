import React, { FC } from 'react';

import { Flex, UserAreaNegativeIcon } from 'prisma-design-system';

const UserIcon: FC = () => (
  <Flex flexShrink={0}>
    <UserAreaNegativeIcon size="48px" color="brandPrimary.1" />
  </Flex>
);

export default UserIcon;
