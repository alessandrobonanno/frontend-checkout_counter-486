import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { withDesign } from 'storybook-addon-designs';
import { api as cmsAddonApi } from 'frontend-commons-storybook-cms';
import configKeys from './config/keys';
import CampaignBanner from '.';
import { CampaignBannerProps } from './CampaignBanner';
import { CampaignBannerPages } from './constants';

interface ExtendedCampaignBannerProps extends CampaignBannerProps {
  size: 'big' | 'small';
}

const story = {
  title: 'Components/CampaignBanner',
  component: CampaignBanner,
  decorators: [withDesign],
  parameters: {
    design: {
      type: 'experimental-figspec',
      url: 'https://www.figma.com/file/vIfpwfq9KFC7bI1Y3sukTU/Campaigns-2021-Prime-Day-2?node-id=132%3A1116',
    },
    translationsEndPaths: ['/default', '/campaign'],
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default: Story<ExtendedCampaignBannerProps> = (args) => {
  const size = args.size === 'big' ? '1140px' : '940px';
  return (
    <div style={{ maxWidth: size }}>
      <CampaignBanner {...args} />
    </div>
  );
};

Default.args = {
  page: CampaignBannerPages.RESULTS,
};
Default.argTypes = {
  size: {
    control: {
      type: 'select',
      options: ['big', 'small'],
    },
    defaultValue: 'big',
  },
};

Default.parameters = {
  design: {
    type: 'experimental-figspec',
    url: 'https://www.figma.com/file/vIfpwfq9KFC7bI1Y3sukTU/Campaigns-2021-Prime-Day-2?node-id=132%3A1116',
  },
};

export default story as Meta;
