import React from 'react';
import { fireEvent, screen, render } from '@testing-library/react';
import {
  TestingProviders,
  setViewport,
  resetViewport,
} from '../../../testUtils';
import CampaignBanner from '..';
import { TRACKING_LABEL } from '../constants';

const RESULTS_CONTENT_S = 'Some results content s';
const RESULTS_CONTENT_L = 'Some results content l';
const PAX_CONTENT_S = 'Some pax content s';
const PAX_CONTENT_L = 'Some pax content l';
const PAYMENT_CONTENT_S = 'Some payment content s';
const PAYMENT_CONTENT_L = 'Some payment content l';

beforeEach(() => {
  resetViewport();
});

describe('Campaign Banner', () => {
  test('Shall show all content for page if it is defined for desktop', async () => {
    const translations = {
      campaign: {
        banner: {
          results: {
            l: RESULTS_CONTENT_L,
          },
          pax: {
            l: PAX_CONTENT_L,
          },
          payment: {
            l: PAYMENT_CONTENT_L,
          },
        },
      },
    };
    setViewport({ width: 940, height: 768 });

    render(
      <TestingProviders translations={translations} locale="it-IT">
        <CampaignBanner page="pax" />
      </TestingProviders>
    );

    expect(await screen.findByText(PAX_CONTENT_L)).toBeVisible();
    expect(await screen.queryByText(PAYMENT_CONTENT_L)).not.toBeInTheDocument();
    expect(await screen.queryByText(RESULTS_CONTENT_L)).not.toBeInTheDocument();
    expect(await screen.getByAltText('campaign-content-image')).toHaveAttribute(
      'src',
      '/images/onefront/branded/campaign_contentimage_pax_l_positive_it.png'
    );
  });
  test('Shall show all content for page if it is defined in mobile', async () => {
    const ga = jest.fn();
    const page = 'results';
    const translations = {
      campaign: {
        banner: {
          results: {
            s: RESULTS_CONTENT_S,
          },
          pax: {
            s: PAX_CONTENT_S,
          },
          payment: {
            s: PAYMENT_CONTENT_S,
          },
        },
      },
    };

    render(
      <TestingProviders
        translations={translations}
        brand="GO"
        locale="fr"
        window={{ ga }}
        trackingCategory="flights_results"
        trackingPage="results"
      >
        <CampaignBanner page={page} />
      </TestingProviders>
    );

    fireEvent.click(await screen.findByText(RESULTS_CONTENT_S));

    expect(ga).toTrackResultsOnceWith({
      page: `/BF/${page}`,
      action: 'prime_days',
      label: `prime-days-banner_click_pag:${TRACKING_LABEL[page]}`,
    });
    expect(await screen.findByText(RESULTS_CONTENT_S)).toBeVisible();
    expect(await screen.queryByText(PAX_CONTENT_S)).not.toBeInTheDocument();
    expect(await screen.queryByText(PAYMENT_CONTENT_S)).not.toBeInTheDocument();
    expect(await screen.getByAltText('campaign-content-image')).toHaveAttribute(
      'src',
      '/images/onefront/branded/campaign_contentimage_results_s_positive_fr.png'
    );
  });
});
