const translations = [
  {
    inputPath: ['campaign', 'banner.results.s'],
    outputPath: ['campaign', 'banner', 'results', 's'],
    chunks: ['campaign'],
  },
  {
    inputPath: ['campaign', 'banner.results.l'],
    outputPath: ['campaign', 'banner', 'results', 'l'],
    chunks: ['campaign'],
  },
  {
    inputPath: ['campaign', 'banner.pax.s'],
    outputPath: ['campaign', 'banner', 'pax', 's'],
  },
  {
    inputPath: ['campaign', 'banner.pax.l'],
    outputPath: ['campaign', 'banner', 'pax', 'l'],
  },
  {
    inputPath: ['campaign', 'banner.payment.s'],
    outputPath: ['campaign', 'banner', 'payment', 's'],
  },
  {
    inputPath: ['campaign', 'banner.payment.l'],
    outputPath: ['campaign', 'banner', 'payment', 'l'],
  },
];

module.exports = translations;
