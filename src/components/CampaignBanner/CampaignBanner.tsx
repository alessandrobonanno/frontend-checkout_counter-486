import React, { FC, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import {
  BackgroundImage,
  Box,
  Flex,
  Text,
  Picture,
  useMediaQueries,
} from 'prisma-design-system';
import {
  useBrand,
  useLocale,
  useTranslation,
  useTracking,
  Trans,
} from '@frontend-shell/react-hooks';
import {
  getContentImageUrl,
  SIZED_PROPS,
  BRANDS_CAMPAIGNS_THEMES,
  CampaignBannerPages,
  Sizes,
  TRACKING_LABEL,
} from './constants';

export interface CampaignBannerProps {
  page: CampaignBannerPages;
}

interface CampaignContentProps extends CampaignBannerProps {
  imageSize: Sizes;
  hasContent: boolean;
}

const CampaignContentImage: FC<CampaignContentProps> = ({
  page,
  imageSize,
  hasContent,
}) => {
  const [imageFound, setImageFound] = useState(true);
  const brand = useBrand();
  const locale = useLocale();
  const contentImageImageUrl = getContentImageUrl(
    page,
    imageSize,
    brand,
    locale.split('-')[0]
  );
  const failSourceImageHandler = useCallback(() => {
    setImageFound(false);
  }, [setImageFound]);
  return imageFound ? (
    <Box
      flexShrink={0}
      height="100%"
      mr={hasContent ? SIZED_PROPS.itemsSepation[imageSize] : 0}
    >
      <Picture
        src={contentImageImageUrl}
        alt="campaign-content-image"
        styles={{
          maxWidth: SIZED_PROPS.contentImageMaxWidth[imageSize],
          height: 'auto',
        }}
        onError={failSourceImageHandler}
      />
    </Box>
  ) : null;
};

const CampaignBanner: FC<CampaignBannerProps> = ({ page }) => {
  const { t } = useTranslation();
  const brand = useBrand();
  const { md } = useMediaQueries();
  const { trackEvent } = useTracking();
  const size = md ? Sizes.L : Sizes.S;
  const theme = BRANDS_CAMPAIGNS_THEMES[brand];
  const backgroudImageUrl = `/images/onefront/branded/campaign_${page}_${size}_${theme}.png`;
  const key = `campaign.banner.${page}.${size}`;
  const content = t(`campaign.banner.${page}.${size}`);
  const handleClick = (): void => {
    trackEvent({
      action: 'prime_days',
      label: `prime-days-banner_click_pag:${TRACKING_LABEL[page]}`,
    });
  };
  return (
    <BackgroundImage
      width="100%"
      height={md ? '90px' : '85px'}
      backgroundRepeat="no-repeat"
      backgroundPosition="center"
      backgroundSize="auto 100%"
      backgroundImage={`url(${backgroudImageUrl})`}
      onClick={handleClick}
    >
      <Flex
        justifyContent="center"
        alignItems="center"
        height="100%"
        py={2}
        pl={md ? '2' : '10%'}
        pr={md ? '2' : '0'}
      >
        <Flex
          px={md ? '0' : '4'}
          width={md ? '50%' : '90%'}
          alignItems={md ? 'center' : 'flex-start'}
          flexDirection={md ? 'row' : 'column'}
        >
          <CampaignContentImage
            page={page}
            imageSize={size}
            hasContent={content !== ''}
          />
          <Box pt={md ? '4' : '0'}>
            <Text
              fontSize={SIZED_PROPS.textType[size]}
              color={
                theme === 'positive' ? 'promotional.primary.dark' : 'white'
              }
            >
              <Trans i18nKey={key} components={{ b: <b /> }} />
            </Text>
          </Box>
        </Flex>
      </Flex>
    </BackgroundImage>
  );
};

CampaignBanner.propTypes = {
  page: PropTypes.oneOf(Object.values(CampaignBannerPages)).isRequired,
};

export default CampaignBanner;
