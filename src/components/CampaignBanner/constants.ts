export const BRANDS_CAMPAIGNS_THEMES = {
  ED: 'positive',
  OP: 'positive',
  GO: 'positive',
  TR: 'positive',
};

export const TRACKING_LABEL = {
  results: 'Flight results page',
  pax: 'Flight details page',
  payment: 'Flight payment page',
};

export const SIZED_PROPS = {
  contentImageMaxWidth: {
    l: '200px',
    s: '145px',
  },
  itemsSepation: {
    l: 5,
    s: 5,
  },
  textType: {
    l: 'body.2',
    s: 'body.1',
  },
};

export enum CampaignBannerPages {
  RESULTS = 'results',
  PAX = 'pax',
  PAYMENT = 'payment',
}

export enum Sizes {
  S = 's',
  L = 'l',
}

export const getContentImageUrl = (
  page: 'results' | 'pax' | 'payment',
  size: Sizes,
  brand: 'ED' | 'OP' | 'GO' | 'TR',
  locale: string
): string =>
  `/images/onefront/branded/campaign_contentimage_${page}_${size}_${BRANDS_CAMPAIGNS_THEMES[brand]}_${locale}.png`;
