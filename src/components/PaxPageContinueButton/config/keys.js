const translations = [
  {
    inputPath: ['home', 'continueButton'],
    outputPath: ['passengersPage', 'continueButton'],
  },
];

module.exports = translations;
