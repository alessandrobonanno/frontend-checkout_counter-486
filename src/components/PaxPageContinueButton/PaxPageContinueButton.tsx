import React, { FC } from 'react';
import { useTranslation, useTracking } from '@frontend-shell/react-hooks';
import { Button, Box } from 'prisma-design-system';
import { useAllTravellersSelectionState } from '../../clientState/providers/Travellers/AllTravellersFieldsSelectionProvider';
import { useAddPersonalInfoActions } from '../../clientState/providers/AddPersonalInfoActionsProvider';
import { useSetTryingToContinueWithInvalidForm } from '../../clientState/providers/Travellers/AllTravellersFieldsSelectionProvider/AllTravellersFieldSelectionProvider';

const PaxPageContinueButton: FC = () => {
  const { t } = useTranslation();
  const { trackEvent } = useTracking();
  const setTryingToContinue = useSetTryingToContinueWithInvalidForm();
  const travellersSelection = useAllTravellersSelectionState();

  const { continueWithPassengersPageValidations } = useAddPersonalInfoActions();

  const clickHandler = (): void => {
    const isValidForm = travellersSelection.valid;

    if (!isValidForm) {
      const invalidTraveller = travellersSelection.travellers.find(
        ({ validTraveller }) => !validTraveller
      );

      trackEvent({
        action: 'continue_next_step',
        label: 'continue_clicks_flight_FE_error',
      });

      if (invalidTraveller?.ref?.current?.offsetTop)
        window.scrollTo(0, invalidTraveller?.ref?.current?.offsetTop);
      setTryingToContinue(true);
    } else {
      const travellersData = travellersSelection.travellers.map(
        ({ includedFields, travellerType }) => {
          const includedFieldsObj = Object.keys(includedFields).reduce(
            (memo, fieldName) => {
              memo[fieldName] = includedFields[fieldName].value;
              return memo;
            },
            {}
          );
          return {
            ...includedFieldsObj,
            travellerType,
          };
        }
      );
      continueWithPassengersPageValidations(travellersData);
    }
  };

  // const blurHandler = (): void => {
  //   setTryingToContinue(false);
  // };

  return (
    <Box my={5}>
      <Button
        type="primary"
        fullWidth
        size="large"
        onClick={clickHandler}
        // onBlur={blurHandler}
      >
        {t('passengersPage.continueButton')}
      </Button>
    </Box>
  );
};

export default PaxPageContinueButton;
