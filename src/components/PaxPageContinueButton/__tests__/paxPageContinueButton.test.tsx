import React from 'react';
import { screen, render } from '@testing-library/react';
import { TestingProviders } from '../../../testUtils';
import PaxPageContinueButton from '..';
import userEvent from '@testing-library/user-event';
import { emptyUserMocks } from '../../../clientState/providers/__tests__/userMocks';

describe('Pax Page Continue Button', () => {
  test('Shall be visible and tracking info if it is invalid', async () => {
    const translations = {
      passengersPage: {
        continueButton: 'Continue',
      },
    };
    const continueWithPassengersPageValidations = jest.fn();
    const ga = jest.fn();

    render(
      <TestingProviders
        window={{ ga }}
        mocks={emptyUserMocks().mocks}
        trackingCategory="flights_pax_page"
        trackingPage="flights/details-extras/"
        translations={translations}
        continueWithPassengersPageValidations={
          continueWithPassengersPageValidations
        }
      >
        <PaxPageContinueButton />
      </TestingProviders>
    );
    const continueButton = await screen.findByText(
      translations.passengersPage.continueButton
    );
    expect(continueButton).toBeVisible();
    userEvent.click(continueButton);
    expect(ga).toTrackPaxPageNthWith(1, {
      action: 'continue_next_step',
      label: 'continue_clicks_flight_FE_error',
    });
    expect(continueWithPassengersPageValidations).not.toBeCalled();
  });
});
