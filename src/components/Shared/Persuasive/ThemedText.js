import React from 'react';
import { Text, useGetThemeVersion } from 'prisma-design-system';

const themes = {
  blueStone: {
    fontSize: 'body.1',
  },
  cobalt: {
    fontSize: 'body.2',
  },
};

const ThemedText = ({ children }) => {
  const version = useGetThemeVersion();
  const props = themes[version];

  return (
    <Text color="neutrals.1" lineHeight="body" {...props}>
      {children}
    </Text>
  );
};

export default ThemedText;
