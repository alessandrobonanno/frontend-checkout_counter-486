import React from 'react';
import { Box, useGetThemeVersion } from 'prisma-design-system';

const themes = {
  blueStone: {
    pt: 5,
    px: 5,
    pb: 4,
  },
  cobalt: {
    pt: 0,
    px: 0,
    pb: 0,
  },
};

const ThemedText = ({ children }) => {
  const version = useGetThemeVersion();
  const props = themes[version];

  return <Box {...props}>{children}</Box>;
};

export default ThemedText;
