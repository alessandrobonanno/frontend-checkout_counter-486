import React from 'react';
import { Text, useGetThemeVersion } from 'prisma-design-system';

const themes = {
  blueStone: {
    fontSize: 'body.1',
    fontWeight: 'bold',
  },
  cobalt: {
    fontSize: 'body.2',
    fontWeight: 'medium',
  },
};

const ThemedMessage = ({ children }) => {
  const version = useGetThemeVersion();
  const props = themes[version];

  return (
    <Text lineHeight="body" {...props}>
      {children}
    </Text>
  );
};

export default ThemedMessage;
