import {
  createQueryHook,
  usePersistedSessionState,
} from '@frontend-shell/react-hooks';
import {
  GetShoppingCartTravellersFieldsQuery,
  QueryGetShoppingCartArgs,
} from '../../generated/graphql';
import { GET_TRAVELLER_FIELDS } from '../providers/operations/shoppingCartTravellerFields';
import QueryType from './QueryInterface';
import useBookingIdFromUrl from './useBookingIdFromUrl';

type useTravellersFieldsQueryType =
  QueryType<GetShoppingCartTravellersFieldsQuery>;

interface UseTravellersFieldsArgsType {
  variables: QueryGetShoppingCartArgs;
}

interface useShoppingCartQueryType {
  (args: UseTravellersFieldsArgsType): useTravellersFieldsQueryType;
}

const useQuery: useShoppingCartQueryType = createQueryHook(
  GET_TRAVELLER_FIELDS,
  {
    isOf: true,
  }
);

const useTravellersFieldsQuery = (): useTravellersFieldsQueryType => {
  const bookingIdFromUrl = useBookingIdFromUrl();
  const [bookingIdFromSession] = usePersistedSessionState('bookingId');
  const bookingId = bookingIdFromUrl || (bookingIdFromSession as number) || 0;
  return useQuery({
    variables: {
      bookingId,
    },
  });
};

export default useTravellersFieldsQuery;
