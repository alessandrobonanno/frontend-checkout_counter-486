export default interface QueryType<R> {
  loading: boolean;
  error?: string;
  data?: R;
}
