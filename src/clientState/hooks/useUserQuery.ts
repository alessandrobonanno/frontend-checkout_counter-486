import { createQueryHook } from '@frontend-shell/react-hooks';
import { Query } from '../../generated/graphql';
import { GET_USER } from '../providers/operations/user';
import QueryType from './QueryInterface';

type useUserQueryType = QueryType<Query>;

interface useUserType {
  (args): useUserQueryType;
}

const useQuery: useUserType = createQueryHook(GET_USER);

const useUserQuery = (): useUserQueryType => {
  return useQuery({});
};

export default useUserQuery;
