import { useUrlParameterFromRegex } from '@frontend-shell/react-hooks';

const useBookingIdFromUrl = (): number => {
  const bookingIdRegex = new RegExp(/.*\/(\d{10,11})\/.*$/);
  const [, bookingId] = useUrlParameterFromRegex(bookingIdRegex);
  return parseInt(bookingId, 10) || 0;
};

export default useBookingIdFromUrl;
