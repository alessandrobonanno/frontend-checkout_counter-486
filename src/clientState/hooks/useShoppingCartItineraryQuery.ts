import {
  createQueryHook,
  usePersistedSessionState,
} from '@frontend-shell/react-hooks';
import {
  GetShoppingCartItineraryQuery,
  QueryGetShoppingCartArgs,
} from '../../generated/graphql';
import { GET_SHOPPING_CART_ITINERARY } from '../providers/operations/shoppingCartItinerary';
import QueryType from './QueryInterface';
import useBookingIdFromUrl from './useBookingIdFromUrl';

type useShoppingCartItineraryQueryType =
  QueryType<GetShoppingCartItineraryQuery>;

interface UseShoppingCartItineraryArgsType {
  variables: QueryGetShoppingCartArgs;
}

interface useShoppingCartQueryType {
  (args: UseShoppingCartItineraryArgsType): useShoppingCartItineraryQueryType;
}

const useQuery: useShoppingCartQueryType = createQueryHook(
  GET_SHOPPING_CART_ITINERARY,
  {
    isOf: true,
  }
);

const useShoppingCartItineraryQuery = (): useShoppingCartItineraryQueryType => {
  const bookingIdFromUrl = useBookingIdFromUrl();
  const [bookingIdFromSession] = usePersistedSessionState('bookingId');
  const bookingId = bookingIdFromUrl || (bookingIdFromSession as number) || 0;
  return useQuery({
    variables: {
      bookingId,
    },
  });
};

export default useShoppingCartItineraryQuery;
