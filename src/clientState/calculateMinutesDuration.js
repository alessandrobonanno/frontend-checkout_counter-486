import differenceInMinutes from 'date-fns/differenceInMinutes';
import parseISO from 'date-fns/parseISO';

const calculateMinutesDuration = (ending, starting) =>
  differenceInMinutes(parseISO(ending), parseISO(starting));

export default calculateMinutesDuration;
