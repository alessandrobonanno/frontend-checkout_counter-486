import React, { createContext, FC, useContext } from 'react';
import { TravellerAgeType } from '../../generated/graphql';

type ContinueWithPassengersPageType = Record<string, string> & {
  travellerType: TravellerAgeType;
};
export interface AddPersonalInfoActionsContextType {
  continueWithPassengersPageValidations: (
    travellers: Array<ContinueWithPassengersPageType>
  ) => void;
}

const AddPersonalInfoActionsContext =
  createContext<AddPersonalInfoActionsContextType>({
    continueWithPassengersPageValidations: (_travellers) => {
      /* */
    },
  });

interface ActionsProviderProps {
  continueWithPassengersPageValidations: (
    travellers: Array<ContinueWithPassengersPageType>
  ) => void;
}

const AddPersonalInfoActionsProvider: FC<ActionsProviderProps> = ({
  continueWithPassengersPageValidations,
  children,
}) => {
  return (
    <AddPersonalInfoActionsContext.Provider
      value={{
        continueWithPassengersPageValidations,
      }}
    >
      {children}
    </AddPersonalInfoActionsContext.Provider>
  );
};

export const useAddPersonalInfoActions =
  (): AddPersonalInfoActionsContextType =>
    useContext(AddPersonalInfoActionsContext);

export default AddPersonalInfoActionsProvider;
