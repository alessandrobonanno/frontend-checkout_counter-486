import { createTrackingPageProvider } from '@frontend-shell/react-hooks';

export const FLIGHT_SUMMARY_PAGE = '/BF/flights/details-extras/';

export const FLIGHT_SUMMARY_CATEGORY = 'flights_pax_page';

const pagesMap = {
  flight_summary: FLIGHT_SUMMARY_PAGE,
};

const pageMapper = (page) => pagesMap[page] || FLIGHT_SUMMARY_PAGE;

export const FlightSummaryPageTrackingProvider = createTrackingPageProvider({
  category: FLIGHT_SUMMARY_CATEGORY,
  page: FLIGHT_SUMMARY_PAGE,
  pageMapper,
});
