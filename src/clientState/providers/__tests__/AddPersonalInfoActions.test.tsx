import React, { FC } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import AddPersonalInfoActionsProvider, {
  useAddPersonalInfoActions,
} from '../AddPersonalInfoActionsProvider';
import { act } from 'react-dom/test-utils';

describe('AddPersonalInfoActions Provider', () => {
  it('Should call callback', async () => {
    const continueWithPassengers = jest.fn();
    const wrapper: FC = ({ children }) => (
      <AddPersonalInfoActionsProvider
        continueWithPassengersPageValidations={continueWithPassengers}
      >
        {children}
      </AddPersonalInfoActionsProvider>
    );
    const { result } = renderHook(() => useAddPersonalInfoActions(), {
      wrapper,
    });
    act(() => {
      result.current.continueWithPassengersPageValidations([]);
    });
    expect(continueWithPassengers).toHaveBeenCalledWith([]);
  });
});
