import {
  GetUserQuery,
  TravellerAgeType,
  TravellerGender,
  TravellerTitleType,
  UserTraveller,
} from '../../../generated/graphql';

export const USER_ID = 3012;
export const MEMBERSHIP_ID = 456;
export const USER_EMAIL = 'user@gmail.com';
export const PRIME_USER_EMAIL = 'primeuser@gmail.com';
export const PRIME_TRAVELLER = {
  id: 0,
  buyer: true,
  importedFromLocal: false,
  email: PRIME_USER_EMAIL,
  isDefault: true,
  middleName: 'Uuuu',
  personalInfo: {
    name: 'Martina',
    firstLastName: 'Gluu',
    secondLastName: 'Brave',
    ageType: TravellerAgeType.Adult,
    birthDate: '1985-05-02',
    title: TravellerTitleType.Mrs,
    travellerGender: TravellerGender.Female,
    nationalityCountryCode: 'es',
  },
  address: null,
  meal: null,
  prefixPhoneNumber: null,
  alternatePhoneNumber: null,
  phoneNumber: null,
  mobilePhoneNumber: null,
  prefixAlternatePhoneNumber: null,
  frequentFlyerNumbers: [],
  identificationList: [],
  cpf: null,
};
export const getGenericTraveller = (id = 123): UserTraveller => ({
  id,
  buyer: false,
  importedFromLocal: false,
  isDefault: false,
  email: 'bbb@bb.es',
  middleName: 'Uuuu',
  personalInfo: {
    name: 'Juanita',
    firstLastName: 'Blabla',
    secondLastName: 'Bleble',
    ageType: TravellerAgeType.Adult,
    birthDate: '1989-10-03',
    title: TravellerTitleType.Mrs,
    travellerGender: TravellerGender.Female,
    nationalityCountryCode: 'es',
  },
  address: null,
  meal: null,
  prefixPhoneNumber: null,
  alternatePhoneNumber: null,
  phoneNumber: null,
  mobilePhoneNumber: null,
  prefixAlternatePhoneNumber: null,
  frequentFlyerNumbers: [],
  identificationList: [],
  cpf: null,
});

export const emptyUserMocks = (): { mocks: Record<string, () => unknown> } => ({
  mocks: {
    Long: (): number => Math.random(),
    User: (): GetUserQuery['getUser'] => undefined,
  },
});

export const userMocks = (): { mocks: Record<string, () => unknown> } => ({
  mocks: {
    Long: (): number => Math.random(),
    User: (): GetUserQuery['getUser'] => ({
      id: USER_ID,
      email: USER_EMAIL,
      membership: null,
      travellers: [getGenericTraveller()],
    }),
  },
});

export const userWithMultipleStoredTravellersMocks = (): {
  mocks: Record<string, () => unknown>;
} => ({
  mocks: {
    Long: (): number => Math.random(),
    User: (): GetUserQuery['getUser'] => ({
      id: USER_ID,
      email: USER_EMAIL,
      membership: null,
      travellers: [getGenericTraveller(), getGenericTraveller(456)],
    }),
  },
});

export const primeUserMocks = (): { mocks: Record<string, () => unknown> } => ({
  mocks: {
    Long: (): number => Math.random(),
    User: (): GetUserQuery['getUser'] => ({
      id: USER_ID,
      email: PRIME_USER_EMAIL,
      membership: {
        id: MEMBERSHIP_ID,
      },
      travellers: [getGenericTraveller(), PRIME_TRAVELLER],
    }),
  },
});
