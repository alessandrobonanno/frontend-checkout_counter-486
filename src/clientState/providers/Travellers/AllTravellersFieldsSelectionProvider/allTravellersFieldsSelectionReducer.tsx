import { TravellerAgeType } from '../../../../generated/graphql';
import {
  AllTravellersFieldSelectionStateType,
  defaultContextValue,
} from './AllTravellersFieldSelectionProvider';
import reducerCases from './allTravellersFieldsSelectionReducerCases';

export enum ActionsTypes {
  UPDATE_TRAVELLER_INCLUDED_FIELDS = 'UPDATE_TRAVELLER_INCLUDED_FIELDS',
  SET_TRAVELLER_REF = 'SET_TRAVELLER_REF',
}

interface BasePayload {
  indx: number;
}

export interface UpdateTravellerIncludedFieldsPayload extends BasePayload {
  travellerType: TravellerAgeType;
  includedFields: Record<
    string,
    {
      value: string | undefined;
      valid: boolean;
    }
  >;
}

export interface SetTryingToContinuePayload {
  tryingToContinueWithInvalidForm: boolean;
}

export interface SetTravellerRefPayload {
  indx: number;
  ref: React.MutableRefObject<HTMLDivElement | undefined>;
}
export interface UpdateTravellerIncludedFieldsAction {
  type: ActionsTypes.UPDATE_TRAVELLER_INCLUDED_FIELDS;
  payload: UpdateTravellerIncludedFieldsPayload;
}
export interface SetTravellerRefAction {
  type: ActionsTypes.SET_TRAVELLER_REF;
  payload: SetTravellerRefPayload;
}

export type AllTravellersFieldsSelectionActions =
  | UpdateTravellerIncludedFieldsAction
  | SetTravellerRefAction;

const allTravellersFieldsSelectionReducer: React.Reducer<
  AllTravellersFieldSelectionStateType,
  AllTravellersFieldsSelectionActions
> = (state = defaultContextValue.state, action) => {
  switch (action.type) {
    case ActionsTypes.UPDATE_TRAVELLER_INCLUDED_FIELDS:
      return reducerCases[action.type](state, action);
    case ActionsTypes.SET_TRAVELLER_REF:
      return reducerCases[action.type](state, action);
    default:
      return state;
  }
};

export default allTravellersFieldsSelectionReducer;
