import { AllTravellersFieldSelectionStateType } from './AllTravellersFieldSelectionProvider';
import {
  UpdateTravellerIncludedFieldsAction,
  ActionsTypes,
  SetTravellerRefAction,
} from './allTravellersFieldsSelectionReducer';

const updateTravellerIncludedFields = (
  state: AllTravellersFieldSelectionStateType,
  action: UpdateTravellerIncludedFieldsAction
): AllTravellersFieldSelectionStateType => {
  const updatedTravellersState = [...state.travellers];
  const {
    payload: { indx, travellerType, includedFields },
  } = action;
  const traveller = {
    ...updatedTravellersState?.[indx],
    travellerType,
    includedFields,
  };
  const travellers = [
    ...updatedTravellersState.slice(0, indx),
    traveller,
    ...updatedTravellersState.slice(indx + 1),
  ];

  return {
    ...state,
    travellers,
  };
};

const setTravellerRef = (
  state: AllTravellersFieldSelectionStateType,
  action: SetTravellerRefAction
): AllTravellersFieldSelectionStateType => {
  const updatedTravellersState = [...state.travellers];
  const {
    payload: { indx, ref },
  } = action;
  const traveller = {
    ...updatedTravellersState?.[indx],
    ref,
  };
  const travellers = [
    ...updatedTravellersState.slice(0, indx),
    traveller,
    ...updatedTravellersState.slice(indx + 1),
  ];

  return {
    ...state,
    travellers,
  };
};

type ReducerActionsTypes<AObj> = (
  state: AllTravellersFieldSelectionStateType,
  action: AObj
) => AllTravellersFieldSelectionStateType;

type ReducerCasesType = {
  [k in ActionsTypes]: k extends ActionsTypes.UPDATE_TRAVELLER_INCLUDED_FIELDS
    ? ReducerActionsTypes<UpdateTravellerIncludedFieldsAction>
    : k extends ActionsTypes.SET_TRAVELLER_REF
    ? ReducerActionsTypes<SetTravellerRefAction>
    : never;
};

const reducerCases: ReducerCasesType = {
  UPDATE_TRAVELLER_INCLUDED_FIELDS: updateTravellerIncludedFields,
  SET_TRAVELLER_REF: setTravellerRef,
};

export default reducerCases;
