import React, {
  FC,
  createContext,
  useContext,
  useReducer,
  useCallback,
  useState,
} from 'react';
import { TravellerAgeType } from '../../../../generated/graphql';
import allTravellersFieldsSelectionReducer, {
  ActionsTypes,
  AllTravellersFieldsSelectionActions,
  SetTravellerRefPayload,
  UpdateTravellerIncludedFieldsPayload,
} from './allTravellersFieldsSelectionReducer';

type AllTravellersFieldsSelectionDispatcher =
  React.Dispatch<AllTravellersFieldsSelectionActions>;

export interface AllTravellerSelectionStateTraveller {
  travellerType: TravellerAgeType;
  ref: React.MutableRefObject<HTMLDivElement | undefined>;
  includedFields: Record<
    string,
    {
      value: string | undefined;
      valid: boolean;
    }
  >;
}
export interface AllTravellersFieldSelectionStateType {
  travellers: Array<AllTravellerSelectionStateTraveller>;
}

export type TryingToContinueWithInvalidFormCorntextType = [
  boolean,
  React.Dispatch<React.SetStateAction<boolean>>
];

export const defaultContextValue = {
  state: {
    valid: false,
    travellers: [],
  },
  dispatch: (): void => {
    /* noop */
  },
};

const DispatchAllTravellerSelectionContext =
  createContext<AllTravellersFieldsSelectionDispatcher>(
    defaultContextValue.dispatch
  );
const StateAllTravellerSelectionContext =
  createContext<AllTravellersFieldSelectionStateType>(
    defaultContextValue.state
  );
const StateTryingToContinueWithInvalidFormContext =
  createContext<boolean>(false);
const SetTryingToContinueWithInvalidFormContext = createContext<
  React.Dispatch<React.SetStateAction<boolean>>
>(() => {
  /* */
});

const AllTravellersFieldSelectionProvider: FC = ({ children }) => {
  const [state, dispatch] = useReducer(
    allTravellersFieldsSelectionReducer,
    defaultContextValue.state
  );
  const [tryingToContinueWithInvalidForm, setTryingToContinueWithInvalidForm] =
    useState(false);

  return (
    <DispatchAllTravellerSelectionContext.Provider value={dispatch}>
      <StateAllTravellerSelectionContext.Provider value={state}>
        <StateTryingToContinueWithInvalidFormContext.Provider
          value={tryingToContinueWithInvalidForm}
        >
          <SetTryingToContinueWithInvalidFormContext.Provider
            value={setTryingToContinueWithInvalidForm}
          >
            {children}
          </SetTryingToContinueWithInvalidFormContext.Provider>
        </StateTryingToContinueWithInvalidFormContext.Provider>
      </StateAllTravellerSelectionContext.Provider>
    </DispatchAllTravellerSelectionContext.Provider>
  );
};

export interface UseAllTravellersSelectionActionsType {
  updateTravellerFields: (
    payload: UpdateTravellerIncludedFieldsPayload
  ) => void;
  setTravellerRef: (payload: SetTravellerRefPayload) => void;
}

export type UpdateTravellerFieldsDispatcher = (
  payload: UpdateTravellerIncludedFieldsPayload
) => void;

export type SetTravellerRefDispatcher = (
  payload: SetTravellerRefPayload
) => void;

export const useAllTravellersSelectionActions =
  (): UseAllTravellersSelectionActionsType => {
    const dispatch = useContext(DispatchAllTravellerSelectionContext);
    const updateTravellerFields: UpdateTravellerFieldsDispatcher = useCallback(
      (payload) => {
        dispatch({
          type: ActionsTypes.UPDATE_TRAVELLER_INCLUDED_FIELDS,
          payload,
        });
      },
      [dispatch]
    );
    const setTravellerRef: SetTravellerRefDispatcher = useCallback(
      (payload) => {
        dispatch({
          type: ActionsTypes.SET_TRAVELLER_REF,
          payload,
        });
      },
      [dispatch]
    );
    return {
      updateTravellerFields,
      setTravellerRef,
    };
  };

export type FormState = Omit<
  AllTravellersFieldSelectionStateType,
  'tryingToContinueWithInvalidForm'
>;
export interface UseAllTravellerSelectionStateTraveller
  extends AllTravellerSelectionStateTraveller {
  validTraveller: boolean;
}
interface UseAllTravellersFieldSelectionStateType {
  valid: boolean;
  travellers: Array<UseAllTravellerSelectionStateTraveller>;
}

export const useAllTravellersSelectionState =
  (): UseAllTravellersFieldSelectionStateType => {
    const { travellers } = useContext(StateAllTravellerSelectionContext);
    const travellersWithValidation = travellers.map((traveller) => ({
      ...traveller,
      validTraveller:
        !!Object.entries(traveller.includedFields || {}).length &&
        Object.keys(traveller.includedFields).every(
          (fieldName) => traveller.includedFields[fieldName].valid
        ),
    }));
    const valid =
      !!travellersWithValidation.length &&
      travellersWithValidation.every(({ validTraveller }) => validTraveller);
    return {
      travellers: travellersWithValidation,
      valid,
    };
  };

export const useTryingToContinueWithInvalidForm = (): boolean => {
  return useContext(StateTryingToContinueWithInvalidFormContext);
};
export const useSetTryingToContinueWithInvalidForm = (): React.Dispatch<
  React.SetStateAction<boolean>
> => {
  return useContext(SetTryingToContinueWithInvalidFormContext);
};

export default AllTravellersFieldSelectionProvider;
