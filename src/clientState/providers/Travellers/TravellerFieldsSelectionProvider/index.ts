import TravellerFieldsSelectionProvider, {
  useTravellersSelectionActions,
  useFieldSelection,
  useTravellerSelectionTravellerData,
  FieldsSelectionStateType,
  FieldGroups,
} from './TravellerFieldsSelectionProvider';
import type {
  UpdateFieldDispatcher,
  FieldSelection,
  UseTravellersSelectionActionsType,
} from './TravellerFieldsSelectionProvider';
import type { UpdateFieldAction } from './travellerFieldsSelectionReducer';
import { TRAVELLERS_TYPES_AGE_LIMITS } from './FieldValidators';

export {
  TRAVELLERS_TYPES_AGE_LIMITS,
  useTravellersSelectionActions,
  useFieldSelection,
  useTravellerSelectionTravellerData,
  FieldGroups,
};
export type {
  UpdateFieldDispatcher,
  FieldSelection,
  UpdateFieldAction,
  UseTravellersSelectionActionsType,
  FieldsSelectionStateType,
};
export default TravellerFieldsSelectionProvider;
