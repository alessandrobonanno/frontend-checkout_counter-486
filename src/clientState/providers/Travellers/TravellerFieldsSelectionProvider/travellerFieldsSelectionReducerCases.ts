import { TravellerGender, UserTraveller } from '../../../../generated/graphql';
import {
  ID_VALIDATORS,
  ValidatorFunction,
  VALIDATORS,
} from './FieldValidators';
import {
  FieldGroups,
  FieldSelectionStateGroupType,
  FieldsSelectionStateType,
} from './TravellerFieldsSelectionProvider';
import {
  UpdateFieldAction,
  ActionsTypes,
  SetFieldGroupAction,
  SetGroupFieldsWithValueAsIncludedAction,
  SetTravellerFieldsAction,
} from './travellerFieldsSelectionReducer';

const STORED_TRAVELLER_TO_TRAVELLER_MAPPER: Record<
  string,
  (travellerData?: UserTraveller) => string
> = {
  name: (travellerData) => travellerData?.personalInfo.name || '',
  firstLastName: (travellerData) =>
    travellerData?.personalInfo.firstLastName || '',
  secondLastName: (travellerData) =>
    travellerData?.personalInfo.secondLastName || '',
  travellerGender: (travellerData) => {
    const title =
      travellerData?.personalInfo.title === 'MR'
        ? TravellerGender.Male
        : TravellerGender.Female;
    return (
      travellerData?.personalInfo.travellerGender ||
      (travellerData?.personalInfo.title ? title : '')
    );
  },
  dateOfBirth: (travellerData) => travellerData?.personalInfo?.birthDate || '',
  nationalityCountryCode: (travellerData) =>
    travellerData?.personalInfo.nationalityCountryCode || '',
  travellerTitle: (travellerData) => travellerData?.personalInfo.title || '',
};

const DEPENDANT_FIELD_PROPS: Record<string, Array<string>> = {
  dateOfBirth: ['lastFlightDate'],
  secondEmail: ['email'],
  secondEmailCaseInsensitive: ['email'],
};

const VALIDATORS_BY_OTHER_FIELDS_MAPPER: Record<
  string,
  (group: FieldSelectionStateGroupType) => ValidatorFunction
> = {
  identification: (fieldsGroup) => {
    return ID_VALIDATORS[
      fieldsGroup ? fieldsGroup.identificationType?.value : ''
    ];
  },
};

const getDependantPropsValue = (
  value: string,
  state: FieldsSelectionStateType
): string => {
  const pathToProp = {
    lastFlightDate: state?.lastFlightDate,
    email: state?.mandatoryFields?.email?.value,
  };
  return pathToProp[value];
};

const getDependantProps = (
  name: string,
  state: FieldsSelectionStateType
): Record<string, string> => {
  const dependencies: Array<string> = DEPENDANT_FIELD_PROPS[name] || [];
  return dependencies.reduce((prev, curr) => {
    const propsValue = getDependantPropsValue(curr, state);
    return Object.assign({}, prev, { [curr]: propsValue });
  }, {});
};

const isValidField = (
  state: FieldsSelectionStateType,
  group: FieldGroups,
  fieldName: string,
  value?: string
): boolean => {
  const validatorFunction = VALIDATORS_BY_OTHER_FIELDS_MAPPER[fieldName]
    ? VALIDATORS_BY_OTHER_FIELDS_MAPPER[fieldName](state[group] || {})
    : VALIDATORS[state[group]?.[fieldName]?.validator || ''];
  const dependantProps = getDependantProps(fieldName, state);
  return validatorFunction?.(
    value ? value : state?.[group]?.[fieldName]?.value,
    dependantProps
  );
};

const setTravellerFields = (
  _state: FieldsSelectionStateType,
  action: SetTravellerFieldsAction
): FieldsSelectionStateType => {
  return action.payload.travellerFields;
};

const updateField = (
  state: FieldsSelectionStateType,
  action: UpdateFieldAction
): FieldsSelectionStateType => {
  let updatedState = { ...state };
  const {
    payload: { name, fieldGroup = FieldGroups.MANDATORY_FIELDS, value },
  } = action;
  const group = updatedState[fieldGroup];
  if (group) {
    const newGroup = {
      ...group,
      [name]: {
        ...group[name],
        value,
        included:
          (fieldGroup !== FieldGroups.MANDATORY_FIELDS &&
            !!value &&
            value !== '') ||
          group[name]?.included,
        valid: isValidField(state, fieldGroup, name, value),
      },
    };
    updatedState = { ...updatedState, [fieldGroup]: newGroup };
  }
  return updatedState;
};

const setMandatoryGroupWithStoredTraveller = (
  state: FieldsSelectionStateType,
  action: SetFieldGroupAction
): FieldsSelectionStateType => {
  const updatedState = { ...state };
  const {
    payload: { travellerData },
  } = action;
  const mandatoryFields = Object.entries(
    updatedState.mandatoryFields || {}
  ).reduce((mandatoryFields, [fieldName, fieldSelectionValue]) => {
    const value =
      STORED_TRAVELLER_TO_TRAVELLER_MAPPER[fieldName]?.(travellerData);

    const validatorFunction = VALIDATORS_BY_OTHER_FIELDS_MAPPER[fieldName]
      ? VALIDATORS_BY_OTHER_FIELDS_MAPPER[fieldName](
          updatedState.mandatoryFields || {}
        )
      : VALIDATORS[fieldSelectionValue?.validator || ''];
    const dependantProps = getDependantProps(fieldName, state);
    mandatoryFields[fieldName] = {
      ...fieldSelectionValue,
      value,
      valid: validatorFunction?.(value, dependantProps),
    };
    return mandatoryFields;
  }, {});
  return {
    ...updatedState,
    mandatoryFields,
  };
};

const setGroupFieldsWithValueAsIncluded = (
  state: FieldsSelectionStateType,
  action: SetGroupFieldsWithValueAsIncludedAction
): FieldsSelectionStateType => {
  const updatedState = { ...state };
  const {
    payload: { included, group },
  } = action;

  const newGroup = Object.entries(updatedState[group] || {}).reduce(
    (newGroup, [fieldName, fieldSelectionValue]) => {
      newGroup[fieldName] = {
        ...fieldSelectionValue,
        included: included && !!state[group]?.[fieldName]?.value,
        valid: isValidField(state, group, fieldName),
      };
      return newGroup;
    },
    {}
  );
  return {
    ...updatedState,
    [group]: newGroup,
  };
};

type ReducerActionsTypes<AObj> = (
  state: FieldsSelectionStateType,
  action: AObj
) => FieldsSelectionStateType;

type ReducerCasesType = {
  [k in ActionsTypes]: k extends ActionsTypes.SET_TRAVELLER_FIELDS
    ? ReducerActionsTypes<SetTravellerFieldsAction>
    : k extends ActionsTypes.UPDATE_FIELD
    ? ReducerActionsTypes<UpdateFieldAction>
    : k extends ActionsTypes.SET_MANDATORY_GROUP
    ? ReducerActionsTypes<SetFieldGroupAction>
    : k extends ActionsTypes.SET_GROUP_AS_INCLUDED
    ? ReducerActionsTypes<SetGroupFieldsWithValueAsIncludedAction>
    : never;
};

const reducerCases: ReducerCasesType = {
  SET_TRAVELLER_FIELDS: setTravellerFields,
  UPDATE_FIELD: updateField,
  SET_MANDATORY_GROUP: setMandatoryGroupWithStoredTraveller,
  SET_GROUP_AS_INCLUDED: setGroupFieldsWithValueAsIncluded,
};

export default reducerCases;
