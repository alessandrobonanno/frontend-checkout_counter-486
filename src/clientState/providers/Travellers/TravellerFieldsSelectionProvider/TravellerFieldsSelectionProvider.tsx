import React, {
  FC,
  createContext,
  useContext,
  useReducer,
  useMemo,
  useEffect,
  useState,
} from 'react';
import {
  FieldValidatorType,
  TravellerAgeType,
} from '../../../../generated/graphql';
import useShoppingCartItineraryQuery from '../../../hooks/useShoppingCartItineraryQuery';
import { useAllTravellersSelectionActions } from '../AllTravellersFieldsSelectionProvider';
import { useStoredTravellers } from '../StoredTravellersProvider';
import { useTravellerDescriptor } from '../TravellerFieldsDescriptorProvider';
import { getInitialFieldsValues } from './InitialFieldsValues';
import {
  ActionsTypes,
  getIncludedFields,
  TravellerSelectorActions,
  SetFieldGroupAction,
  SetGroupFieldsWithValueAsIncludedAction,
  UpdateFieldAction,
  travellerFieldsSelectionReducer,
} from './travellerFieldsSelectionReducer';

type TravellerSelectorDispatcher = React.Dispatch<TravellerSelectorActions>;

export interface FieldSelection {
  value: string;
  valid: boolean;
  included: boolean;
  validator?: FieldValidatorType | null;
}

export enum FieldGroups {
  MANDATORY_FIELDS = 'mandatoryFields',
  OPTIONAL_FIELDS = 'optionalFields',
  OPTIONAL_REQUEST_FIELDS = 'optionalRequestFields',
}

export type FieldSelectionStateGroupType = Record<string, FieldSelection>;

export type FieldsSelectionStateType = {
  indx: number;
  travellerType: TravellerAgeType;
  lastFlightDate?: string;
  mandatoryFields?: FieldSelectionStateGroupType;
  optionalFields?: FieldSelectionStateGroupType;
  optionalRequestFields?: FieldSelectionStateGroupType;
};

export type TravellerSelectionContextType = {
  state: FieldsSelectionStateType;
  dispatch: TravellerSelectorDispatcher;
};

export type UpdateFieldDispatcher = (
  payload: UpdateFieldAction['payload']
) => void;

export type SetGroupFieldsWithValueAsIncludedDispatcher = (
  payload: SetGroupFieldsWithValueAsIncludedAction['payload']
) => void;

export type SetMandatoryFieldsValuesDispatcher = (
  payload: SetFieldGroupAction['payload']
) => void;

const DispatchTravellerSelectionContext =
  createContext<TravellerSelectorDispatcher>(() => {
    /* noop */
  });

const StateTravellerSelectionContext = createContext<FieldsSelectionStateType>({
  indx: 0,
  travellerType: TravellerAgeType.Adult,
  lastFlightDate: '',
  mandatoryFields: {},
});

const TravellerFieldsSelectionProvider: FC<{ indx: number }> = ({
  children,
  indx,
}) => {
  const travellerFieldsDescription = useTravellerDescriptor();
  const { data: shoppingCartItinerary } = useShoppingCartItineraryQuery();
  const { updateTravellerFields } = useAllTravellersSelectionActions();
  const lastFlightDate = useMemo((): string => {
    const legs = shoppingCartItinerary?.getShoppingCart?.itinerary?.legs;
    const lastLegSegments = legs?.[legs.length - 1]?.segments;
    const lastSegmentSections =
      lastLegSegments?.[lastLegSegments?.length - 1].sections;
    return (
      lastSegmentSections?.[lastSegmentSections.length - 1].departureDate ||
      `${new Date().getTime()}`
    );
  }, [shoppingCartItinerary]);
  const { storedTravellers } = useStoredTravellers();

  const [state, dispatch] = useReducer(travellerFieldsSelectionReducer, {
    indx: travellerFieldsDescription.indx,
    travellerType: TravellerAgeType.Adult,
  });
  const [hasLoadDefaultSelection, setHasLoadDefaultSelection] = useState(false);

  useEffect(() => {
    const initialValues = getInitialFieldsValues(
      travellerFieldsDescription,
      lastFlightDate
    );
    dispatch({
      type: ActionsTypes.SET_TRAVELLER_FIELDS,
      payload: {
        travellerFields: initialValues,
      },
    });
    if (Object.keys(initialValues.mandatoryFields || {}).length)
      setHasLoadDefaultSelection(true);
  }, [travellerFieldsDescription, lastFlightDate]);

  useEffect(() => {
    if (hasLoadDefaultSelection && storedTravellers.length) {
      const selectedTraveller = storedTravellers.find(
        ({ travellerIndexPick }) => travellerIndexPick === indx
      );
      dispatch({
        type: ActionsTypes.SET_MANDATORY_GROUP,
        payload: { travellerData: selectedTraveller },
      });
    }
  }, [storedTravellers, hasLoadDefaultSelection, indx]);

  useEffect(() => {
    updateTravellerFields(getIncludedFields(state));
  }, [updateTravellerFields, state]);

  return (
    <DispatchTravellerSelectionContext.Provider value={dispatch}>
      <StateTravellerSelectionContext.Provider value={state}>
        {children}
      </StateTravellerSelectionContext.Provider>
    </DispatchTravellerSelectionContext.Provider>
  );
};

export interface UseTravellersSelectionActionsType {
  updateTravellerField: (payload: UpdateFieldAction['payload']) => void;
  setMandatoryFieldsWithTravellerData: (
    payload: SetFieldGroupAction['payload']
  ) => void;
  setGroupFieldsWithValueAsIncluded: (
    payload: SetGroupFieldsWithValueAsIncludedAction['payload']
  ) => void;
}

export const useTravellersSelectionActions =
  (): UseTravellersSelectionActionsType => {
    const dispatch = useContext(DispatchTravellerSelectionContext);

    return useMemo(() => {
      const updateTravellerField: UpdateFieldDispatcher = (payload) => {
        dispatch({ type: ActionsTypes.UPDATE_FIELD, payload });
      };
      const setMandatoryFieldsWithTravellerData: SetMandatoryFieldsValuesDispatcher =
        (payload) => {
          dispatch({
            type: ActionsTypes.SET_MANDATORY_GROUP,
            payload,
          });
        };
      const setGroupFieldsWithValueAsIncluded: SetGroupFieldsWithValueAsIncludedDispatcher =
        (payload) => {
          dispatch({
            type: ActionsTypes.SET_GROUP_AS_INCLUDED,
            payload,
          });
        };

      return {
        updateTravellerField,
        setMandatoryFieldsWithTravellerData,
        setGroupFieldsWithValueAsIncluded,
      };
    }, [dispatch]);
  };

export const useTravellerSelectionTravellerData = (): {
  lastFlightDate: FieldsSelectionStateType['lastFlightDate'];
  travellerType: TravellerAgeType;
} => {
  const { lastFlightDate, travellerType } = useContext(
    StateTravellerSelectionContext
  );
  return {
    lastFlightDate,
    travellerType,
  };
};

export const useFieldSelection = (
  dataName: string,
  fieldGroup: FieldGroups
): FieldSelection | Partial<FieldSelection> => {
  const state = useContext(StateTravellerSelectionContext);
  return state[fieldGroup]?.[dataName] || {};
};

export default TravellerFieldsSelectionProvider;
