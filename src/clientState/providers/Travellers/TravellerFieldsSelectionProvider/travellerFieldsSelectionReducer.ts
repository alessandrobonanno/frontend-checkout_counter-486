import { TravellerAgeType, UserTraveller } from '../../../../generated/graphql';
import { UpdateTravellerIncludedFieldsPayload } from '../AllTravellersFieldsSelectionProvider';
import {
  FieldGroups,
  FieldSelectionStateGroupType,
  FieldsSelectionStateType,
  TravellerSelectionContextType,
} from './TravellerFieldsSelectionProvider';
import reducerCases from './travellerFieldsSelectionReducerCases';

export enum ActionsTypes {
  SET_TRAVELLER_FIELDS = 'SET_TRAVELLER_FIELDS',
  UPDATE_FIELD = 'UPDATE_FIELD',
  SET_MANDATORY_GROUP = 'SET_MANDATORY_GROUP',
  SET_GROUP_AS_INCLUDED = 'SET_GROUP_AS_INCLUDED',
}
interface BasePayload {
  name: string;
  fieldGroup: FieldGroups;
}

export interface SetTravellerFieldsAction {
  type: ActionsTypes.SET_TRAVELLER_FIELDS;
  payload: {
    travellerFields: FieldsSelectionStateType;
  };
}
export interface UpdateFieldAction {
  type: ActionsTypes.UPDATE_FIELD;
  payload: BasePayload & {
    value: string;
  };
}
export interface SetFieldGroupAction {
  type: ActionsTypes.SET_MANDATORY_GROUP;
  payload: {
    travellerData?: UserTraveller;
  };
}
export interface SetGroupFieldsWithValueAsIncludedAction {
  type: ActionsTypes.SET_GROUP_AS_INCLUDED;
  payload: {
    included: boolean;
    group: FieldGroups;
  };
}

export type TravellerSelectorActions =
  | SetTravellerFieldsAction
  | UpdateFieldAction
  | SetFieldGroupAction
  | SetGroupFieldsWithValueAsIncludedAction;

export const travellerFieldsSelectionReducer: React.Reducer<
  TravellerSelectionContextType['state'],
  TravellerSelectorActions
> = (
  state = { indx: 0, travellerType: TravellerAgeType.Adult },
  action
): FieldsSelectionStateType => {
  switch (action.type) {
    case ActionsTypes.SET_TRAVELLER_FIELDS:
      return reducerCases[action.type](state, action);
    case ActionsTypes.UPDATE_FIELD:
      return reducerCases[action.type](state, action);
    case ActionsTypes.SET_MANDATORY_GROUP:
      return reducerCases[action.type](state, action);
    case ActionsTypes.SET_GROUP_AS_INCLUDED:
      return reducerCases[action.type](state, action);
    default:
      return state;
  }
};

const includedFieldsReducer = (
  group: FieldSelectionStateGroupType | undefined,
  memo: UpdateTravellerIncludedFieldsPayload,
  shallForceIncluded: boolean
): UpdateTravellerIncludedFieldsPayload =>
  Object.keys(group || {}).reduce(
    (includedFields, groupFieldName): UpdateTravellerIncludedFieldsPayload => {
      const field = group?.[groupFieldName];
      if (shallForceIncluded || field?.included) {
        includedFields.includedFields[groupFieldName] = {
          value: field?.value ? `${field?.value}` : '',
          valid: !!field?.valid,
        };
      }
      return includedFields;
    },
    memo
  );

export const getIncludedFields = (
  state: FieldsSelectionStateType
): UpdateTravellerIncludedFieldsPayload => {
  let includedFields = {
    indx: state.indx,
    travellerType: state.travellerType,
    includedFields: {},
  };
  includedFields = includedFieldsReducer(
    state.mandatoryFields,
    includedFields,
    true
  );
  includedFields = includedFieldsReducer(
    state.optionalFields,
    includedFields,
    false
  );
  includedFields = includedFieldsReducer(
    state.optionalRequestFields,
    includedFields,
    false
  );
  return includedFields;
};
