import {
  Maybe,
  TravellerAgeType,
  TravellerGender,
  TravellerMealType,
} from '../../../../generated/graphql';
import {
  TravellerDescriptorContextType,
  TravellerField,
} from '../TravellerFieldsDescriptorProvider';
import {
  FieldSelectionStateGroupType,
  FieldsSelectionStateType,
} from './TravellerFieldsSelectionProvider';

const INITIAL_FIELDS_VALUES = {
  meal: TravellerMealType.Standard,
  travellerGender: TravellerGender.Male,
};
const FIRST_OPTION_AS_DEFAULT_FIELDS = [
  'identificationType',
  'nationalityCountryCode',
  'identificationIssueCountryCode',
  'countryCodeOfResidence',
];

const fieldsSelectionReducerWithIncluded =
  (included: boolean) =>
  (
    travellerFieldsSelectionRow: FieldSelectionStateGroupType = {},
    row: Maybe<Array<TravellerField>>
  ): FieldSelectionStateGroupType => {
    if (row?.length) {
      return row?.reduce(
        (travellerFieldsSelection, { dataName, options, attributes }) => {
          travellerFieldsSelection[dataName] = {
            value: FIRST_OPTION_AS_DEFAULT_FIELDS.includes(dataName)
              ? options?.[0].value
              : INITIAL_FIELDS_VALUES[dataName],
            valid: !!INITIAL_FIELDS_VALUES[dataName],
            included,
            validator: attributes?.validator?.type,
          };
          return {
            ...travellerFieldsSelectionRow,
            ...travellerFieldsSelection,
          };
        },
        travellerFieldsSelectionRow
      );
    } else {
      return travellerFieldsSelectionRow;
    }
  };

export const getInitialFieldsValues = (
  {
    indx,
    travellerType,
    mandatoryFields = [],
    optionalFields = [],
    optionalRequestFields = [],
  }: TravellerDescriptorContextType,
  lastFlightDate: string
): FieldsSelectionStateType => {
  const initialFieldsSelection: FieldsSelectionStateType = {
    indx,
    travellerType: travellerType || TravellerAgeType.Adult,
    lastFlightDate,
    mandatoryFields: {},
  };

  if (mandatoryFields?.length) {
    initialFieldsSelection.mandatoryFields = mandatoryFields.reduce(
      fieldsSelectionReducerWithIncluded(true),
      initialFieldsSelection.mandatoryFields
    );
  }
  if (optionalFields?.length) {
    const initialOptionalFields = optionalFields.reduce(
      fieldsSelectionReducerWithIncluded(false),
      {}
    );
    initialFieldsSelection.optionalFields = initialOptionalFields;
  }
  if (optionalRequestFields?.length) {
    const initialOptionalRequestFields = optionalRequestFields.reduce(
      fieldsSelectionReducerWithIncluded(false),
      initialFieldsSelection.optionalRequestFields
    );
    initialFieldsSelection.optionalRequestFields = initialOptionalRequestFields;
  }

  return initialFieldsSelection;
};
