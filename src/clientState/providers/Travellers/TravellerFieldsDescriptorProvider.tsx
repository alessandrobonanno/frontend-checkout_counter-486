import React, { FC, createContext, useContext, useMemo } from 'react';
import { useTranslation } from '@frontend-shell/react-hooks';
import {
  FieldFragment,
  FieldObject,
  Maybe,
  RequiredFieldType,
  TravellerAgeType,
} from '../../../generated/graphql';
import useTravellersFieldsQuery from '../../hooks/useTravellersFieldsQuery';

interface TravellerFieldsDescriptorProviderProps {
  indx: number;
}

export type FieldOptions = Array<{ label: string; value: string | undefined }>;
export interface TravellerField extends FieldFragment {
  options: FieldOptions;
  labelId: FieldObject['dataName'];
  helpers: {
    help: boolean;
    error: boolean;
  };
}

type FieldsMap = Maybe<Array<Maybe<Array<TravellerField>>>>;

export interface TravellerDescriptorContextType {
  indx: number;
  travellerType?: TravellerAgeType;
  mandatoryFields?: FieldsMap;
  optionalFields?: FieldsMap;
  optionalRequestFields?: FieldsMap;
}

type translateOptionsType = (
  field: FieldFragment,
  t: (key: string) => string
) => Array<{ label: string; value: string | undefined }>;

const HELPER_LABEL_FIELDS = [
  'name',
  'firstLastName',
  'secondLastName',
  'identification',
  'passengerCardNumber',
];

const ERROR_HELPER_LABEL_FIELDS = [...HELPER_LABEL_FIELDS, 'dateOfBirth'];

const TRANSLATED_FIELDS = [
  'nationalityCountryCode',
  'countryCodeOfResidence',
  'identificationIssueCountryCode',
  'localityCodeOfResidence',
  'passengerCardNumber',
];

const FIELDS_WITH_ADDED_DEFAULT_OPTION = {
  localityCodeOfResidence: (
    t
  ): { label: string; value: string | undefined } => ({
    label: t('passengersManager.form.localityCodeOfResidence.default'),
    value: '',
  }),
};

const translateOptions: translateOptionsType = ({ dataName, options }, t) => {
  if (options) {
    const translatedOptions = options.map(({ option, value }) => {
      const label =
        !TRANSLATED_FIELDS.includes(dataName) && option ? t(option) : option;
      return {
        label: label || '',
        value: value || undefined,
      };
    });
    if (FIELDS_WITH_ADDED_DEFAULT_OPTION[dataName]) {
      translatedOptions.unshift(FIELDS_WITH_ADDED_DEFAULT_OPTION[dataName](t));
    }
    return translatedOptions;
  } else {
    return [];
  }
};

const getExtraFieldsFiltered = (
  travellerFields: FieldsMap,
  requiredFieldType: RequiredFieldType
): FieldsMap | undefined =>
  travellerFields
    ?.map((row) =>
      (row || []).filter((field) => field.mandatory === requiredFieldType)
    )
    .filter((row) => (row || []).length);

const TravellerDescriptorContext =
  createContext<TravellerDescriptorContextType>({ indx: 0 });

const TravellerDescriptorProvider: FC<TravellerFieldsDescriptorProviderProps> =
  ({ indx, children }) => {
    const { data } = useTravellersFieldsQuery();
    const { t } = useTranslation();
    const travellerType =
      data?.getShoppingCart?.requiredTravellerInformation?.[indx]
        ?.travellerType;
    const passengersFields = useMemo(
      () =>
        data?.getShoppingCart?.fieldGroups?.travellerFields?.[indx]
          ?.passengersFields || [],
      [data, indx]
    );
    const optionalRequestFields = useMemo(
      () =>
        data?.getShoppingCart?.fieldGroups?.travellerFields?.[indx]
          ?.optionalRequestFields || [],
      [data, indx]
    );

    const travellerFields = useMemo(
      () =>
        passengersFields?.map((row) =>
          (row || []).map((field) => ({
            ...field,
            options: translateOptions(field, t),
            labelId:
              field.dataName === 'dateOfBirth' && travellerType
                ? `${field.dataName}.${travellerType}`
                : field.dataName,
            helpers: {
              help: HELPER_LABEL_FIELDS.includes(field.dataName),
              error: ERROR_HELPER_LABEL_FIELDS.includes(field.dataName),
            },
          }))
        ),
      [passengersFields, t, travellerType]
    );

    const value = useMemo(() => {
      const additionalRequestsFields = optionalRequestFields?.map((row) =>
        (row || []).map((field) => ({
          ...field,
          options: translateOptions(field, t),
          labelId: field.dataName,
          helpers: {
            help: HELPER_LABEL_FIELDS.includes(field.dataName),
            error: ERROR_HELPER_LABEL_FIELDS.includes(field.dataName),
          },
        }))
      );

      const mandatoryFields = getExtraFieldsFiltered(
        travellerFields,
        RequiredFieldType.Mandatory
      );

      const optionalFields = getExtraFieldsFiltered(
        travellerFields,
        RequiredFieldType.Optional
      );

      return {
        indx,
        travellerType,
        mandatoryFields,
        optionalFields,
        optionalRequestFields: additionalRequestsFields,
      };
    }, [travellerType, travellerFields, optionalRequestFields, t, indx]);

    return (
      <TravellerDescriptorContext.Provider value={value}>
        {children}
      </TravellerDescriptorContext.Provider>
    );
  };

export const useTravellerDescriptor = (): TravellerDescriptorContextType =>
  useContext(TravellerDescriptorContext);

export default TravellerDescriptorProvider;
