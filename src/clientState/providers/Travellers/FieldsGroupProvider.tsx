import React, { FC, createContext, useContext } from 'react';
import { FieldGroups } from './TravellerFieldsSelectionProvider';

interface FieldsGroupProviderProps {
  fieldsGroup: FieldGroups;
}
export type FieldsGroupContextType = FieldGroups;

const FieldsGroupContext = createContext<FieldsGroupContextType>(
  FieldGroups.MANDATORY_FIELDS
);

const FieldsGroupProvider: FC<FieldsGroupProviderProps> = ({
  fieldsGroup,
  children,
}) => (
  <FieldsGroupContext.Provider value={fieldsGroup}>
    {children}
  </FieldsGroupContext.Provider>
);

export const useFieldsGroup = (): FieldsGroupContextType =>
  useContext(FieldsGroupContext);

export default FieldsGroupProvider;
