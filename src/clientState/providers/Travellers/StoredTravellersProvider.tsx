import React, { FC, createContext, useContext } from 'react';
import { UserTraveller } from '../../../generated/graphql';
import useUserQuery from '../../hooks/useUserQuery';
import { useState } from 'react';
import { useCallback } from 'react';
import { useEffect } from 'react';

interface SavedTraveller extends UserTraveller {
  travellerIndexPick?: number;
}

export type StoredTravellersContextType = {
  storedTravellers: Array<SavedTraveller>;
  pickSavedTraveller: (
    travellerIndex: number,
    storedTravellerId: number
  ) => void;
  hasPickedAPrimeStoredTraveller: (indx: number) => boolean;
};

const getState = (
  storedTravellersState,
  isMembership
): StoredTravellersContextType['storedTravellers'] => {
  const storedTravellers = storedTravellersState
    ? [...storedTravellersState]
    : [];
  let newStoredTravellers: Array<SavedTraveller> = [];
  let defaultSelectedTravellerIndex = 0;
  if (isMembership) {
    defaultSelectedTravellerIndex = storedTravellers.findIndex(
      ({ id, buyer }) => id === 0 && buyer
    );
  }
  if (storedTravellers.length) {
    const defaultStoredTravellerSelected = {
      ...storedTravellers?.[defaultSelectedTravellerIndex],
      travellerIndexPick: 0,
    };
    newStoredTravellers = [
      ...storedTravellers.slice(0, defaultSelectedTravellerIndex),
      defaultStoredTravellerSelected,
      ...storedTravellers.slice(defaultSelectedTravellerIndex + 1),
    ];
  }
  return newStoredTravellers;
};
const StoredTravellersContext = createContext<StoredTravellersContextType>({
  storedTravellers: [],
  pickSavedTraveller: (_id: number, _lastSelectedId: number) => {
    /* noop */
  },
  hasPickedAPrimeStoredTraveller: (_indx: number) => false,
});

const StoredTravellersProvider: FC = ({ children }) => {
  const { data, loading } = useUserQuery();
  const [storedTravellers, setStoredTravellers] = useState(
    getState(data?.getUser?.travellers, data?.getUser?.membership)
  );

  useEffect(() => {
    if (data?.getUser && !loading) {
      setStoredTravellers(
        getState(data?.getUser?.travellers, data.getUser?.membership)
      );
    }
  }, [data, loading]);

  const pickSavedTraveller: (
    travellerIndex: number,
    storedTravellerId: number
  ) => void = useCallback(
    (travellerIndex, storedTravellerId) => {
      let newStoredTravellers = storedTravellers.slice(0);
      const indx =
        storedTravellerId !== -1
          ? storedTravellers.findIndex(({ id }) => id === storedTravellerId)
          : -1;
      const nowAvailableIndx = storedTravellers.findIndex(
        ({ travellerIndexPick }) => travellerIndexPick === travellerIndex
      );

      if (indx !== -1) {
        const newStoredTraveller = {
          ...storedTravellers[indx],
          travellerIndexPick: travellerIndex,
        };
        newStoredTravellers = [
          ...newStoredTravellers.slice(0, indx),
          newStoredTraveller,
          ...newStoredTravellers.slice(indx + 1),
        ];
      }
      if (nowAvailableIndx !== -1) {
        const newFreeStoredTraveller = {
          ...storedTravellers[nowAvailableIndx],
          travellerIndexPick: undefined,
        };
        newStoredTravellers = [
          ...newStoredTravellers.slice(0, nowAvailableIndx),
          newFreeStoredTraveller,
          ...newStoredTravellers.slice(nowAvailableIndx + 1),
        ];
      }
      setStoredTravellers(newStoredTravellers);
    },
    [storedTravellers, setStoredTravellers]
  );

  const hasPickedAPrimeStoredTraveller = (indx): boolean =>
    !!storedTravellers.find(
      ({ travellerIndexPick, buyer, id }) =>
        travellerIndexPick === indx && buyer && id === 0
    );

  return (
    <StoredTravellersContext.Provider
      value={{
        storedTravellers,
        pickSavedTraveller,
        hasPickedAPrimeStoredTraveller,
      }}
    >
      {children}
    </StoredTravellersContext.Provider>
  );
};

export const useStoredTravellers = (): StoredTravellersContextType =>
  useContext(StoredTravellersContext);

export default StoredTravellersProvider;
