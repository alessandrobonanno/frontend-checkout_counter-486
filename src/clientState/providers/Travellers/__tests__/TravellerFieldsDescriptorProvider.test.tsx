import React, { FC } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { TestingProviders } from '../../../../testUtils';
import TravellersFieldsDescriptorProvider, {
  useTravellerDescriptor,
} from '../TravellerFieldsDescriptorProvider';
import { TravellerAgeType } from '../../../../generated/graphql';
import {
  FEMALE_TEXT,
  fieldsMock,
  MALE_TEXT,
  mandatoryDayOfBirthFieldParsed,
  mandatoryEmailFieldParsed,
  mandatoryFieldParsed,
  mandatorySecondEmailFieldParsed,
  NATIONAL_ID_CARD_TEXT,
  optionalFieldParsed,
  optionalRequestField2Parsed,
  optionalRequestFieldParsed,
  PASSPORT_TEXT,
} from './fieldsMocks';

const translations = {
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
  passengersManager: {
    header: {
      title: 'Itinerary:',
      flight: 'Flight',
      train: 'Train',
    },
  },
};
describe('TravellerFieldsDescriptor Provider', () => {
  it('Should return travellers fields parsed', async () => {
    const wrapper: FC = ({ children }) => (
      <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
        <TravellersFieldsDescriptorProvider indx={0}>
          {children}
        </TravellersFieldsDescriptorProvider>
      </TestingProviders>
    );
    const { result, waitForNextUpdate } = renderHook(
      () => useTravellerDescriptor(),
      {
        wrapper,
      }
    );
    await waitForNextUpdate();
    expect(result.current).toEqual({
      indx: 0,
      travellerType: TravellerAgeType.Adult,
      mandatoryFields: [
        [mandatoryFieldParsed],
        [mandatoryDayOfBirthFieldParsed],
        [mandatoryEmailFieldParsed],
        [mandatorySecondEmailFieldParsed],
      ],
      optionalFields: [[optionalFieldParsed]],
      optionalRequestFields: [
        [optionalRequestFieldParsed, optionalRequestField2Parsed],
      ],
    });
  });
});
