import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import StoredTravellersProvider, {
  useStoredTravellers,
} from '../StoredTravellersProvider';
import TestingProviders from '../../../../testUtils/providers/TestingProviders';
import {
  PRIME_TRAVELLER,
  primeUserMocks,
  userMocks,
  userWithMultipleStoredTravellersMocks,
  getGenericTraveller,
} from '../../__tests__/userMocks';

describe('StoredTravellers Provider', () => {
  it('Should return stored travellers with first traveller selected by default, select function, and prime check function', async () => {
    const wrapper = ({ children }): JSX.Element => (
      <TestingProviders mocks={userMocks().mocks}>
        <StoredTravellersProvider>{children}</StoredTravellersProvider>
      </TestingProviders>
    );
    const { result, waitForNextUpdate } = renderHook(
      () => useStoredTravellers(),
      {
        wrapper,
      }
    );

    await waitForNextUpdate();

    const {
      storedTravellers,
      pickSavedTraveller,
      hasPickedAPrimeStoredTraveller,
    } = result.current;

    expect(storedTravellers).toEqual([
      { ...getGenericTraveller(), travellerIndexPick: 0 },
    ]);
    expect(pickSavedTraveller).toBeDefined();
    expect(hasPickedAPrimeStoredTraveller).toBeDefined();
  });

  it('Picks a stored traveller', async () => {
    const wrapper = ({ children }): JSX.Element => (
      <TestingProviders mocks={userWithMultipleStoredTravellersMocks().mocks}>
        <StoredTravellersProvider>{children}</StoredTravellersProvider>
      </TestingProviders>
    );
    const { result, waitForNextUpdate } = renderHook(
      () => useStoredTravellers(),
      {
        wrapper,
      }
    );
    await waitForNextUpdate();
    expect(result.current.storedTravellers).toEqual([
      {
        ...getGenericTraveller(),
        travellerIndexPick: 0,
      },
      getGenericTraveller(456),
    ]);

    act(() => result.current.pickSavedTraveller(0, 456));
    expect(result.current.storedTravellers).toEqual([
      getGenericTraveller(),
      {
        ...getGenericTraveller(456),
        travellerIndexPick: 0,
      },
    ]);
  });

  test('Prime stored traveller selection', async () => {
    const wrapper = ({ children }): JSX.Element => (
      <TestingProviders mocks={primeUserMocks().mocks}>
        <StoredTravellersProvider>{children}</StoredTravellersProvider>
      </TestingProviders>
    );
    const { result, waitForNextUpdate } = renderHook(
      () => useStoredTravellers(),
      {
        wrapper,
      }
    );
    await waitForNextUpdate();
    expect(result.current.storedTravellers).toEqual([
      getGenericTraveller(),
      {
        ...PRIME_TRAVELLER,
        travellerIndexPick: 0,
      },
    ]);
    expect(result.current.hasPickedAPrimeStoredTraveller(0)).toBeTruthy();
  });

  test('Non Prime stored traveller selection', async () => {
    const wrapper = ({ children }): JSX.Element => (
      <TestingProviders mocks={userMocks().mocks}>
        <StoredTravellersProvider>{children}</StoredTravellersProvider>
      </TestingProviders>
    );
    const { result, waitForNextUpdate } = renderHook(
      () => useStoredTravellers(),
      {
        wrapper,
      }
    );
    await waitForNextUpdate();
    act(() => result.current.pickSavedTraveller(0, 123));
    expect(result.current.hasPickedAPrimeStoredTraveller(123)).toBeFalsy();
  });
});
