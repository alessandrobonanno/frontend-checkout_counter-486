import React, { FC } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import AllTravellersFieldSelectionProvider, {
  useAllTravellersSelectionState,
} from '../AllTravellersFieldsSelectionProvider';

describe('AllTravellersFieldsSelection Provider', () => {
  it('Should be invalid by default', async () => {
    const wrapper: FC = ({ children }) => (
      <AllTravellersFieldSelectionProvider>
        {children}
      </AllTravellersFieldSelectionProvider>
    );
    const { result } = renderHook(() => useAllTravellersSelectionState(), {
      wrapper,
    });
    expect(result.current.travellers).toEqual([]);
    expect(result.current.valid).toEqual(false);
  });
});
