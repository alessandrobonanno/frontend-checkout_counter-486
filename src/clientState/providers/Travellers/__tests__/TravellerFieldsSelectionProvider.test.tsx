import React, { FC } from 'react';
import { render, screen } from '@testing-library/react';
import { cleanup, renderHook } from '@testing-library/react-hooks';
import { TestingProviders } from '../../../../testUtils';
import TravellersFieldsDescriptorProvider, {
  useTravellerDescriptor,
} from '../TravellerFieldsDescriptorProvider';
import TravellersFieldsSelectionProvider, {
  useFieldSelection,
} from '../TravellerFieldsSelectionProvider';
import {
  FieldTypeValue,
  FieldValidatorType,
  TravellerAgeType,
  TravellerGender,
} from '../../../../generated/graphql';
import {
  FEMALE_TEXT,
  fieldsMock,
  MALE_TEXT,
  NATIONAL_ID_CARD_TEXT,
  passengerWithGenericFieldsGroupsFieldsMock,
  PASSPORT_TEXT,
} from './fieldsMocks';
import { FieldGroups } from '../TravellerFieldsSelectionProvider';
import FieldsGroupProvider from '../FieldsGroupProvider';
import Field from '../../../../components/PassengerManager/Form/FieldGroup/Field';
import userEvent from '@testing-library/user-event';
import { useAllTravellersSelectionState } from '../AllTravellersFieldsSelectionProvider';

const translations = {
  booking: {
    shopping: {
      cart: {
        MALE: MALE_TEXT,
        FEMALE: FEMALE_TEXT,
        NATIONAL_ID_CARD: NATIONAL_ID_CARD_TEXT,
        PASSPORT: PASSPORT_TEXT,
      },
    },
  },
};

const Wrapper: FC = ({ children }) => {
  return (
    <TestingProviders mocks={fieldsMock().mocks} translations={translations}>
      <TravellersFieldsDescriptorProvider indx={0}>
        <TravellersFieldsSelectionProvider indx={0}>
          {children}
        </TravellersFieldsSelectionProvider>
      </TravellersFieldsDescriptorProvider>
    </TestingProviders>
  );
};

const TestComponent: FC = () => {
  const travellersSelection = useAllTravellersSelectionState();
  const { mandatoryFields } = useTravellerDescriptor();
  return (
    <>
      {mandatoryFields?.length ? <div>TestText</div> : null}
      <div data-testid="test">{JSON.stringify(travellersSelection)}</div>
    </>
  );
};

const expectedDefaultState = [
  {
    name: 'dateOfBirth',
    validator: FieldValidatorType.AdultDateValidator,
    value: undefined,
    valid: false,
    included: true,
  },
  {
    name: 'travellerGender',
    validator: FieldValidatorType.NotEmpty,
    value: TravellerGender.Male,
    valid: true,
    included: true,
  },
  {
    name: 'email',
    included: true,
    valid: false,
    validator: FieldValidatorType.EmailRegex,
    value: undefined,
  },
  {
    name: 'secondEmail',
    included: true,
    valid: false,
    validator: FieldValidatorType.SecondEmailValidator,
    value: undefined,
  },
];

describe('TravellerFieldsSelection Provider', () => {
  afterEach(() => cleanup());

  for (const expectedField of expectedDefaultState) {
    it(`Should return default values on init: ${expectedField.name}`, async () => {
      const { result, waitForNextUpdate } = renderHook(
        () =>
          useFieldSelection(expectedField.name, FieldGroups.MANDATORY_FIELDS),
        {
          wrapper: Wrapper,
        }
      );

      await waitForNextUpdate();
      expect(result.current.valid).toEqual(expectedField.valid);
      expect(result.current.value).toEqual(expectedField.value);
      expect(result.current.included).toEqual(expectedField.included);
    });
  }

  it('Should update AllTravellersSelectionState', async () => {
    const translations = {
      passengersManager: {
        form: {
          name: {
            label: `NAMELabel`,
            help: `NAMEHelp`,
            error: `NAMEErrorValidation`,
            errorMandatory: `ErrorEmptyNAME`,
          },
          passengerCardNumber: {
            label: `PAXLabel`,
            help: `PAXHelp`,
            error: `PAXErrorValidation`,
            errorMandatory: `ErrorEmptyPAX`,
          },
        },
      },
    };
    const fieldsTransllations = translations.passengersManager.form;
    render(
      <TestingProviders
        translations={translations}
        mocks={
          passengerWithGenericFieldsGroupsFieldsMock(
            {
              mandatoryFields: {
                dataNames: ['name', 'passengerCardNumber'],
                dataIds: ['name', 'passengerCardNumber'],
                validators: [
                  FieldValidatorType.PaxNameRegex,
                  FieldValidatorType.FfcCodeRegex,
                ],
              },
            },
            TravellerAgeType.Adult
          ).mocks
        }
      >
        <TravellersFieldsDescriptorProvider indx={0}>
          <TravellersFieldsSelectionProvider indx={0}>
            <FieldsGroupProvider fieldsGroup={FieldGroups.MANDATORY_FIELDS}>
              <Field
                fieldData={{
                  dataName: 'name',
                  options: [],
                  labelId: 'name',
                  helpers: {
                    help: true,
                    error: true,
                  },
                  dataId: 'name',
                  fieldType: FieldTypeValue.TextField,
                }}
                indx={1}
              />
              <Field
                fieldData={{
                  dataName: 'passengerCardNumber',
                  options: [
                    {
                      label: 'IB',
                      value: 'IB',
                    },
                  ],
                  labelId: 'passengerCardNumber',
                  helpers: {
                    help: true,
                    error: true,
                  },
                  dataId: 'passengerCardNumber',
                  fieldType: FieldTypeValue.TextField,
                }}
                indx={1}
              />
            </FieldsGroupProvider>
          </TravellersFieldsSelectionProvider>
        </TravellersFieldsDescriptorProvider>
        <TestComponent />
      </TestingProviders>
    );
    const testComponent = await screen.findByTestId('test');
    expect(JSON.parse(testComponent.innerHTML)).toStrictEqual({
      valid: false,
      travellers: [
        {
          includedFields: {
            name: { value: '', valid: false },
            passengerCardNumber: { value: '', valid: false },
          },
          travellerType: TravellerAgeType.Adult,
          validTraveller: false,
        },
      ],
    });
    const nameInput = await screen.findByLabelText(
      fieldsTransllations.name.label
    );
    const passengerCardNumberInput = await screen.findByLabelText(
      fieldsTransllations.passengerCardNumber.label
    );
    userEvent.type(nameInput, 'Ana');
    await userEvent.type(passengerCardNumberInput, 'AST3UYT');
    userEvent.click(nameInput);
    expect(await screen.getByLabelText('Reset Value')).toBeVisible();
    expect(JSON.parse(testComponent.innerHTML)).toStrictEqual({
      valid: true,
      travellers: [
        {
          includedFields: {
            name: { value: 'Ana', valid: true },
            passengerCardNumber: { value: 'AST3UYT', valid: true },
          },
          travellerType: TravellerAgeType.Adult,
          validTraveller: true,
        },
      ],
    });
  });
});
