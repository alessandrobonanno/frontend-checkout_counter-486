import React, { FC } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import FieldsGroupProvider, { useFieldsGroup } from '../FieldsGroupProvider';
import { FieldGroups } from '../TravellerFieldsSelectionProvider';
describe('FieldsGroupProvider', () => {
  it('Should return given fieldGroup', () => {
    const wrapper: FC = ({ children }) => (
      <FieldsGroupProvider fieldsGroup={FieldGroups.OPTIONAL_FIELDS}>
        {children}
      </FieldsGroupProvider>
    );
    const { result } = renderHook(() => useFieldsGroup(), { wrapper });

    expect(result.current).toBe(FieldGroups.OPTIONAL_FIELDS);
  });
  it('Should return mandatory fieldGroup by default', () => {
    const wrapper: FC = ({ children }) => <>{children}</>;
    const { result } = renderHook(() => useFieldsGroup(), { wrapper });

    expect(result.current).toBe(FieldGroups.MANDATORY_FIELDS);
  });
});
