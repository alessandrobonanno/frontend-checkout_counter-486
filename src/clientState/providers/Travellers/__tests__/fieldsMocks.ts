import {
  itineraryMock,
  ShoppingCartMockResponse,
  VALIDATORS_MAP,
} from '../../../../../.storybook/selectors/travellersFieldMockSelector';
import {
  FieldObject,
  FieldTypeValue,
  FieldValidatorType,
  GetUserQuery,
  RequiredFieldType,
  ResidentGroupContainer,
  TravellerAgeType,
  TravellerGender,
  TravellerTitleType,
} from '../../../../generated/graphql';
import {
  getGenericTraveller,
  MEMBERSHIP_ID,
  PRIME_TRAVELLER,
  PRIME_USER_EMAIL,
  USER_ID,
} from '../__tests__/../../__tests__/userMocks';

export const MALE_TEXT = 'Male';
export const FEMALE_TEXT = 'Female';
export const NAME_TEXT = 'Name';
export const NAME_MANDATORY_TEXT = 'Missing Name';
export const SURNAME_TEXT = 'Surname';
export const SURNAME_MANDATORY_TEXT = 'Missing Surname';
export const NATIONAL_ID_CARD_TEXT = 'National id Card';
export const PASSPORT_TEXT = 'Passport';
export const DATE_OF_BIRTH_TEXT = 'Date Of Birth';
export const EMAIL_TEXT = 'Email';
export const SECOND_EMAIL_TEXT = 'Second email';
export const PASSENGER_CARD_TEXT = 'Passenger Card';
export const IDENTIFICATION_TYPE_TEXT = 'Identification';

export const mandatoryField = (validator: FieldValidatorType): FieldObject => ({
  attributes: {
    validator: {
      type: validator,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'traveller-gender',
  dataName: 'travellerGender',
  fieldType: FieldTypeValue.RadioButton,
  mandatory: RequiredFieldType.Mandatory,
  options: [
    {
      option: 'booking.shopping.cart.MALE',
      value: 'MALE',
    },
    {
      option: 'booking.shopping.cart.FEMALE',
      value: 'FEMALE',
    },
  ],
  style: null,
  position: {
    row: 1,
    column: 1,
  },
});

const genericField = (
  dataName: string,
  dataId: string,
  validator: FieldValidatorType,
  type: RequiredFieldType
): FieldObject => ({
  attributes: {
    validator: {
      type: validator,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId,
  dataName,
  fieldType: FieldTypeValue.TextField,
  mandatory: type,
  options: [],
  style: null,
  position: {
    row: 1,
    column: 1,
  },
});

export const partialMandatoryField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'traveller-gender',
  dataName: 'travellerGender',
  fieldType: FieldTypeValue.RadioButton,
  mandatory: RequiredFieldType.Mandatory,
  options: [
    {
      option: 'booking.shopping.cart.MALE',
      value: 'MALE',
    },
    {
      option: 'booking.shopping.cart.FEMALE',
      value: 'FEMALE',
    },
  ],
  style: null,
};

export const genericMandatoryField = (
  dataName: string,
  dataId: string,
  validator: FieldValidatorType
): FieldObject =>
  genericField(dataName, dataId, validator, RequiredFieldType.Mandatory);

export const genericOptionalField = (
  dataName: string,
  dataId: string,
  validator: FieldValidatorType
): FieldObject =>
  genericField(dataName, dataId, validator, RequiredFieldType.Optional);

export const mandatoryFieldParsed = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  labelId: 'travellerGender',
  helpers: {
    error: false,
    help: false,
  },
  dataId: 'traveller-gender',
  dataName: 'travellerGender',
  fieldType: FieldTypeValue.RadioButton,
  mandatory: RequiredFieldType.Mandatory,
  options: [
    {
      label: MALE_TEXT,
      value: 'MALE',
    },
    {
      label: FEMALE_TEXT,
      value: 'FEMALE',
    },
  ],
  style: null,
};

const mandatoryDayOfBirthField = (travellerType): FieldObject => ({
  attributes: {
    validator: {
      type: VALIDATORS_MAP[travellerType],
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'date-of-birth-adult',
  dataName: 'dateOfBirth',
  fieldType: FieldTypeValue.DateField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
  position: {
    row: 4,
    column: 1,
  },
});

const mandatoryEmailField = (): FieldObject => ({
  attributes: {
    validator: {
      type: FieldValidatorType.EmailRegex,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'email',
  dataName: 'email',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
  position: {
    row: 4,
    column: 1,
  },
});

const mandatorySecondEmailField = (caseInsensitive?: boolean): FieldObject => ({
  attributes: {
    validator: {
      type: caseInsensitive
        ? FieldValidatorType.SecondEmailValidatorCaseInsensitive
        : FieldValidatorType.SecondEmailValidator,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'second-email',
  dataName: 'secondEmail',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
  position: {
    row: 4,
    column: 1,
  },
});

export const partialMandatoryDayOfBirthField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: VALIDATORS_MAP[TravellerAgeType.Adult],
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'date-of-birth-adult',
  dataName: 'dateOfBirth',
  fieldType: FieldTypeValue.DateField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
};

export const mandatoryDayOfBirthFieldParsed = {
  attributes: {
    validator: {
      type: FieldValidatorType.AdultDateValidator,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'date-of-birth-adult',
  labelId: 'dateOfBirth.ADULT',
  helpers: {
    error: true,
    help: false,
  },
  dataName: 'dateOfBirth',
  fieldType: FieldTypeValue.DateField,
  mandatory: RequiredFieldType.Mandatory,
  options: [],
  style: null,
};

export const partialMandatoryEmailField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.EmailRegex,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'email',
  dataName: 'email',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
};

export const mandatoryEmailFieldParsed = {
  attributes: {
    autoCapitalize: null,
    autoComplete: false,
    autoCorrect: null,
    validator: {
      type: FieldValidatorType.EmailRegex,
    },
  },
  dataId: 'email',
  dataName: 'email',
  fieldType: FieldTypeValue.TextField,
  helpers: {
    error: false,
    help: false,
  },
  labelId: 'email',
  mandatory: RequiredFieldType.Mandatory,
  options: [],
  style: null,
};

export const partialMandatorySecondEmailField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.SecondEmailValidator,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'second-email',
  dataName: 'secondEmail',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Mandatory,
  options: null,
  style: null,
};

export const mandatorySecondEmailFieldParsed = {
  attributes: {
    autoCapitalize: null,
    autoComplete: false,
    autoCorrect: null,
    validator: {
      type: FieldValidatorType.SecondEmailValidator,
    },
  },
  dataId: 'second-email',
  dataName: 'secondEmail',
  fieldType: FieldTypeValue.TextField,
  helpers: {
    error: false,
    help: false,
  },
  labelId: 'secondEmail',
  mandatory: RequiredFieldType.Mandatory,
  options: [],
  style: null,
};

export const partialOptionalField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: null,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'identification-type',
  dataName: 'identificationType',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'booking.shopping.cart.NATIONAL_ID_CARD',
      value: 'NATIONAL_ID_CARD',
    },
    {
      option: 'booking.shopping.cart.PASSPORT',
      value: 'PASSPORT',
    },
  ],
  style: null,
};

export const optionalField = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: null,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'identification-type',
  dataName: 'identificationType',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'booking.shopping.cart.NATIONAL_ID_CARD',
      value: 'NATIONAL_ID_CARD',
    },
    {
      option: 'booking.shopping.cart.PASSPORT',
      value: 'PASSPORT',
    },
  ],
  style: null,
  position: {
    row: 5,
    column: 1,
  },
};

export const optionalFieldParsed = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: null,
    autoCapitalize: null,
    autoCorrect: null,
  },
  dataId: 'identification-type',
  labelId: 'identificationType',
  helpers: {
    error: false,
    help: false,
  },
  dataName: 'identificationType',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      label: NATIONAL_ID_CARD_TEXT,
      value: 'NATIONAL_ID_CARD',
    },
    {
      label: PASSPORT_TEXT,
      value: 'PASSPORT',
    },
  ],
  style: null,
};

const passengerGenericMandatoryField: (
  dataName: string,
  dataId: string,
  validator: FieldValidatorType
) => Array<FieldObject> = (dataName, dataId, validator) => [
  genericMandatoryField(dataName, dataId, validator),
];

const passengerGenericOptionalField: (
  dataName: string,
  dataId: string,
  validator: FieldValidatorType
) => Array<FieldObject> = (dataName, dataId, validator) => [
  genericOptionalField(dataName, dataId, validator),
];

const passengerFields: (
  travellerType: TravellerAgeType,
  validator: FieldValidatorType,
  emailCaseInsensitive?: boolean
) => Array<Array<FieldObject>> = (
  travellerType,
  validator,
  emailCaseInsensitive
) => [
  [mandatoryField(validator)],
  [mandatoryDayOfBirthField(travellerType)],
  [mandatoryEmailField()],
  [mandatorySecondEmailField(emailCaseInsensitive)],
  [optionalField],
];

export const partialOptionalRequestField: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.FfcCodeRegex,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'passenger-card-number',
  dataName: 'passengerCardNumber',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'TK',
      value: 'Turkish Airlines',
    },
  ],
  style: null,
};

export const partialOptionalRequestField2: Partial<FieldObject> = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'meal',
  dataName: 'meal',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'booking.shopping.cart.meal.STANDARD',
      value: 'STANDARD',
    },
    {
      option: 'booking.shopping.cart.meal.LOW_SODIUM',
      value: 'LOW_SODIUM',
    },
    {
      option: 'booking.shopping.cart.meal.GLUTEN_FREE',
      value: 'GLUTEN_FREE',
    },
  ],
  style: null,
};

export const optionalRequestField = {
  attributes: {
    validator: {
      type: FieldValidatorType.FfcCodeRegex,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'passenger-card-number',
  dataName: 'passengerCardNumber',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'TK',
      value: 'Turkish Airlines',
    },
  ],
  style: null,
  position: {
    row: 11,
    column: 1,
  },
};

const optionalRequestField2 = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'meal',
  dataName: 'meal',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      option: 'booking.shopping.cart.meal.STANDARD',
      value: 'STANDARD',
    },
    {
      option: 'booking.shopping.cart.meal.LOW_SODIUM',
      value: 'LOW_SODIUM',
    },
    {
      option: 'booking.shopping.cart.meal.GLUTEN_FREE',
      value: 'GLUTEN_FREE',
    },
  ],
  style: null,
  position: {
    row: 11,
    column: 1,
  },
};

export const optionalRequestFieldParsed = {
  attributes: {
    validator: {
      type: FieldValidatorType.FfcCodeRegex,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'passenger-card-number',
  labelId: 'passengerCardNumber',
  helpers: {
    error: true,
    help: true,
  },
  dataName: 'passengerCardNumber',
  fieldType: FieldTypeValue.TextField,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      label: 'TK',
      value: 'Turkish Airlines',
    },
  ],
  style: null,
};
export const optionalRequestField2Parsed = {
  attributes: {
    validator: {
      type: FieldValidatorType.NotEmpty,
    },
    autoComplete: false,
    autoCapitalize: null,
    autoCorrect: false,
  },
  dataId: 'meal',
  labelId: 'meal',
  helpers: {
    error: false,
    help: false,
  },
  dataName: 'meal',
  fieldType: FieldTypeValue.ComboBox,
  mandatory: RequiredFieldType.Optional,
  options: [
    {
      label: 'booking.shopping.cart.meal.STANDARD',
      value: 'STANDARD',
    },
    {
      label: 'booking.shopping.cart.meal.LOW_SODIUM',
      value: 'LOW_SODIUM',
    },
    {
      label: 'booking.shopping.cart.meal.GLUTEN_FREE',
      value: 'GLUTEN_FREE',
    },
  ],
  style: null,
};

const optionalRequestFields: Array<Array<FieldObject>> = [
  [optionalRequestField, optionalRequestField2],
];

export const userTravellerMock = {
  id: 123,
  buyer: false,
  importedFromLocal: false,
  email: 'bbb@bb.es',
  middleName: 'Uuuu',
  personalInfo: {
    name: 'User',
    firstLastName: 'Traveller',
    secondLastName: 'Stored',
    ageType: TravellerAgeType.Adult,
    birthDate: '1970-01-03',
    title: TravellerTitleType.Mrs,
    travellerGender: TravellerGender.Female,
    nationalityCountryCode: 'es',
  },
};
interface GenerycGroupProps {
  dataNames: Array<string>;
  dataIds: Array<string>;
  validators: Array<FieldValidatorType>;
}
interface GenericFieldGroupProps {
  mandatoryFields: GenerycGroupProps;
  optionalPassengerFields?: GenerycGroupProps;
  optionalRequestField?: GenerycGroupProps;
}

export const passengerWithGenericFieldsGroupsFieldsMock = (
  {
    mandatoryFields,
    optionalPassengerFields,
    optionalRequestField,
  }: GenericFieldGroupProps,
  travellerType: TravellerAgeType
): { mocks: Record<string, () => unknown> } => {
  const passengersFields = mandatoryFields.dataNames.map((dataName, index) => {
    return passengerGenericMandatoryField(
      dataName,
      mandatoryFields.dataIds[index],
      mandatoryFields.validators[index]
    );
  });
  const optionalPassengersFields =
    optionalPassengerFields?.dataNames.map((dataName, index) => {
      return passengerGenericOptionalField(
        dataName,
        optionalPassengerFields?.dataIds[index],
        optionalPassengerFields?.validators[index]
      );
    }) || [];
  const optionalRequestPassengersFields =
    optionalRequestField?.dataNames.map((dataName, index) => {
      return passengerGenericOptionalField(
        dataName,
        optionalRequestField?.dataIds[index],
        optionalRequestField?.validators[index]
      );
    }) || [];
  return {
    mocks: {
      Long: (): number => Math.random(),
      ShoppingCart: (): ShoppingCartMockResponse => ({
        requiredTravellerInformation: [
          {
            travellerType,
          },
        ],
        fieldGroups: {
          travellerFields: [
            {
              passengersFields: [
                ...passengersFields,
                ...optionalPassengersFields,
              ],
              optionalRequestFields: optionalRequestPassengersFields,
            },
          ],
        },
        itinerary: itineraryMock,
      }),
      User: (): GetUserQuery['getUser'] => undefined,
    },
  };
};

const residentGroups: Array<ResidentGroupContainer> = [
  {
    residentLocalities: [
      {
        code: '001',
        name: 'Alaior',
      },
      {
        code: '002',
        name: 'Alaior',
      },
    ],
  },
];

export const passengerWithGenericMandatoryFieldsMock = (
  dataNames: Array<string>,
  dataIds: Array<string>,
  validators: Array<FieldValidatorType> = [FieldValidatorType.NotEmpty],
  travellerType: TravellerAgeType = TravellerAgeType.Adult
): { mocks: Record<string, () => unknown> } => {
  const passengersFields = dataNames.map((dataName, index) => {
    return passengerGenericMandatoryField(
      dataName,
      dataIds[index],
      validators[index]
    );
  });
  return {
    mocks: {
      Long: (): number => Math.random(),
      ShoppingCart: (): ShoppingCartMockResponse => ({
        requiredTravellerInformation: [
          {
            travellerType,
          },
        ],
        fieldGroups: {
          travellerFields: [
            {
              passengersFields,
              optionalRequestFields: optionalRequestFields,
            },
          ],
        },
        itinerary: itineraryMock,
      }),
      User: (): GetUserQuery['getUser'] => undefined,
    },
  };
};

export const fieldsMock = (
  travellerType: TravellerAgeType = TravellerAgeType.Adult,
  validator: FieldValidatorType = FieldValidatorType.NotEmpty,
  emailCaseInsensitive?: boolean
): { mocks: Record<string, () => unknown> } => ({
  mocks: {
    Long: (): number => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        {
          travellerType: travellerType,
          residentGroups: null,
        },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: passengerFields(
              travellerType,
              validator,
              emailCaseInsensitive
            ),
            optionalRequestFields: optionalRequestFields,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => undefined,
  },
});

export const multiplePassengersFieldsMock = (
  resident = false
): { mocks: Record<string, () => unknown> } => ({
  mocks: {
    Long: (): number => Math.random(),
    ShoppingCart: (): ShoppingCartMockResponse => ({
      requiredTravellerInformation: [
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: resident ? residentGroups : null,
        },
        {
          travellerType: TravellerAgeType.Adult,
          residentGroups: resident ? residentGroups : null,
        },
      ],
      fieldGroups: {
        travellerFields: [
          {
            passengersFields: [
              [
                genericMandatoryField(
                  'name',
                  'name',
                  FieldValidatorType.NotEmpty
                ),
              ],
              [
                genericMandatoryField(
                  'firstLastName',
                  'first-last-name',
                  FieldValidatorType.NotEmpty
                ),
              ],
              ...passengerFields(
                TravellerAgeType.Adult,
                FieldValidatorType.NotEmpty,
                false
              ),
            ],
            optionalRequestFields: optionalRequestFields,
          },
          {
            passengersFields: [
              [
                genericMandatoryField(
                  'name',
                  'name',
                  FieldValidatorType.NotEmpty
                ),
              ],
              [
                genericMandatoryField(
                  'firstLastName',
                  'first-last-name',
                  FieldValidatorType.NotEmpty
                ),
              ],
              ...passengerFields(
                TravellerAgeType.Adult,
                FieldValidatorType.NotEmpty,
                false
              ),
            ],
            optionalRequestFields: optionalRequestFields,
          },
        ],
      },
      itinerary: itineraryMock,
    }),
    User: (): GetUserQuery['getUser'] => undefined,
  },
});

export const multiplePassengerFieldsWithPrimeLoggedUser = (): {
  mocks: Record<string, () => unknown>;
} => ({
  mocks: {
    ...multiplePassengersFieldsMock().mocks,
    User: (): GetUserQuery['getUser'] => ({
      id: USER_ID,
      email: PRIME_USER_EMAIL,
      membership: {
        id: MEMBERSHIP_ID,
      },
      travellers: [getGenericTraveller(), PRIME_TRAVELLER],
    }),
  },
});

export const multiplePassengerFieldsWithLoggedUser = (): {
  mocks: Record<string, () => unknown>;
} => ({
  mocks: {
    ...multiplePassengersFieldsMock().mocks,
    User: (): GetUserQuery['getUser'] => ({
      id: USER_ID,
      email: PRIME_USER_EMAIL,
      membership: null,
      travellers: [getGenericTraveller(), getGenericTraveller(235)],
    }),
  },
});
