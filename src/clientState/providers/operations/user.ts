import gql from 'graphql-tag';

export const GET_USER = gql`
  query GetUser {
    getUser {
      id
      email
      membership {
        id
      }
      travellers {
        personalInfo {
          name
          firstLastName
          secondLastName
          ageType
          birthDate
          title
          travellerGender
          nationalityCountryCode
        }
        id
        buyer
        importedFromLocal
        email
        middleName
        prefixPhoneNumber
        phoneNumber
        prefixAlternatePhoneNumber
        mobilePhoneNumber
        alternatePhoneNumber
        isDefault
        cpf
        identificationList {
          identificationType
          identificationId
          identificationExpirationDate
          identificationCountryCode
        }
        address {
          address
          addressType
          city
          country
          postalCode
          isPrimary
          alias
          state
        }
        meal
        frequentFlyerNumbers {
          carrier
          number
        }
      }
    }
  }
`;
