import gql from 'graphql-tag';

export const GET_SHOPPING_CART_ITINERARY = gql`
  query GetShoppingCartItinerary($bookingId: Long!) {
    getShoppingCart(bookingId: $bookingId) {
      itinerary {
        freeCancellationLimit {
          hoursApart
        }
        ticketsLeft
        transportTypes
        hasFreeRebooking
        hasReliableCarriers
        hasRefundCarriers
        legs {
          segments {
            id
            duration
            transportTypes
            baggageCondition
            sections {
              isHub
              departureDate
              arrivalDate
              flightCode
              departureTerminal
              arrivalTerminal
              duration
              vehicleModel
              transportType
              cabinClass
              technicalStops {
                location {
                  id
                  iata
                  cityName
                  name
                  countryName
                }
                arrivalDate
                departureDate
              }
              carrier {
                id
                name
              }
              operatingCarrier {
                id
                name
              }
              departure {
                iata
                name
                cityName
                countryName
                locationType
              }
              destination {
                iata
                name
                cityName
                countryName
                locationType
              }
            }
          }
        }
      }
    }
  }
`;
