import gql from 'graphql-tag';

const TRAVELLER_FIELDS = gql`
  fragment Field on FieldObject {
    attributes {
      validator {
        type
      }
      autoComplete
      autoCapitalize
      autoCorrect
    }
    dataId
    dataName
    fieldType
    mandatory
    style {
      size
      space
    }
    options {
      option
      value
    }
  }
`;

export const GET_TRAVELLER_FIELDS = gql`
  ${TRAVELLER_FIELDS}
  query GetShoppingCartTravellersFields($bookingId: Long!) {
    getShoppingCart(bookingId: $bookingId) {
      requiredTravellerInformation {
        travellerType
        residentGroups {
          residentLocalities {
            code
            name
          }
          name
        }
      }
      fieldGroups {
        travellerFields {
          passengersFields {
            ...Field
          }
          optionalRequestFields {
            ...Field
          }
        }
      }
    }
  }
`;
