import React, { FC } from 'react';
import AddPersonalInfoActionsProvider, {
  AddPersonalInfoActionsContextType,
} from './AddPersonalInfoActionsProvider';
import AllTravellersFieldSelectionProvider from './Travellers/AllTravellersFieldsSelectionProvider';
import StoredTravellersProvider from './Travellers/StoredTravellersProvider';

interface PaxPageProvidersProps {
  continueWithPassengersPageValidations: AddPersonalInfoActionsContextType['continueWithPassengersPageValidations'];
}

const PaxPageProvider: FC<PaxPageProvidersProps> = ({
  continueWithPassengersPageValidations,
  children,
}) => {
  return (
    <AddPersonalInfoActionsProvider
      continueWithPassengersPageValidations={
        continueWithPassengersPageValidations
      }
    >
      <StoredTravellersProvider>
        <AllTravellersFieldSelectionProvider>
          {children}
        </AllTravellersFieldSelectionProvider>
      </StoredTravellersProvider>
    </AddPersonalInfoActionsProvider>
  );
};
export default PaxPageProvider;
