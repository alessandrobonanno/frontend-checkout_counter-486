//versioned components go here
import { versionedComponent } from '@frontend-shell/react-hooks';

export const cobaltDesktopFlightDetailsComponent =
  versionedComponent('FBO_BOOK3467');

export const aliases = ['FBO_BOOK3467'];
