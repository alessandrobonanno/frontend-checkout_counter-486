// Browser history not implement in jsdom:
// https://github.com/jsdom/jsdom/issues/1565

const dispatchPopState = (state) => {
  window.dispatchEvent(new PopStateEvent('popstate', { state }));
};

export default function createBrowserHistory() {
  let index = 0;
  const states = [
    {
      href: window.location.href,
    },
  ];
  const { history } = window;
  const originalReplaceState = history.replaceState;
  const originalPushState = history.pushState;
  const originalGo = history.go;
  const originalBack = history.back;
  const originalForward = history.forward;

  history.__proto__.replaceState = function (state, title, href) {
    originalReplaceState.call(this, state, title, href);
    states[index] = {
      href,
      state,
    };
  };

  history.__proto__.pushState = function (state, title, href) {
    originalReplaceState.call(this, state, title, href);
    ++index;
    states[index] = {
      href,
      state,
    };
  };

  history.__proto__.go = function patchedGo(increment) {
    const nextIndex = Math.min(Math.max(index + increment, 0), states.length);
    if (nextIndex === index) {
      return;
    }
    index = nextIndex;
    const { state, href } = states[index];
    originalReplaceState.call(this, state, undefined, href);
    dispatchPopState(state);
  };

  history.__proto__.back = function patchedBack() {
    history.go(-1);
  };

  history.__proto__.forward = function patchedForward() {
    history.go(1);
  };

  history.__restore__ = () => {
    history.__proto__.replaceState = originalReplaceState;
    history.__proto__.pushState = originalPushState;
    history.__proto__.go = originalGo;
    history.__proto__.back = originalBack;
    history.__proto__.forward = originalForward;
    delete history.__restore__;
  };

  return history;
}
