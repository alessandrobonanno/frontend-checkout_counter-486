const error = console.error;

console.error = (message, ...args) => {
  if (
    /Warning: An update to \S+ inside a test was not wrapped in act\(\.\.\.\)/gi.test(
      message
    )
  ) {
    throw new Error(message);
  }

  error.apply(console, [message, ...args]);
};
