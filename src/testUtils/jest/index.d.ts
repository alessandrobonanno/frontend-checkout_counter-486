export {};
declare global {
  namespace jest {
    interface Matchers<R> {
      toTrackPaxPageOnceWith: (eventObj: TrackEventObject) => void;
      toTrackPaxPageOnceWith: (eventObj: TrackEventObject) => void;
      toTrackPaxPageNthWith: (
        calls: number,
        eventObj: TrackEventObject
      ) => void;
      toTrackPaymentPageNthWith: (
        calls: number,
        eventObj: TrackEventObject
      ) => void;
      toTrackPaymentPageOnceWith: (eventObj: TrackEventObject) => void;
      toTrackNthWith: (calls: number, eventObj: TrackEventObject) => void;
      toTrackOnceWith: (eventObj: TrackEventObject) => void;
      toTrackFlightSummaryNthWith: (
        calls: number,
        eventObj: TrackEventObject
      ) => void;
      toTrackFlightSummaryOnceWith: (eventObj: TrackEventObject) => void;
      toTrackResultsNthWith: (
        calls: number,
        eventObj: TrackEventObject
      ) => void;
      toTrackResultsOnceWith: (eventObj: TrackEventObject) => void;
    }
  }
}
