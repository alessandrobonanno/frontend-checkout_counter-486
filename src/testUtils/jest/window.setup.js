import { enableFetchMocks } from 'jest-fetch-mock';
const noop = Function.prototype;
window.scrollTo = jest.fn();
window.scroll = jest.fn();
window.open = jest.fn();
window.innerWidth = 500;
window.IntersectionObserver = class IntersectionObserver {
  observe() {}
  disconnect() {}
};
enableFetchMocks();

/**
 *  Google Analytics
 */
window.ga = noop;
window.dataLayer = {
  push: noop,
};

/**
 * MatchMedia jsdom mock
 */
Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});
