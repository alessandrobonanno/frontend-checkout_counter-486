import { createTrackingTestExtensions } from '@frontend-shell/react-hooks';

expect.extend({
  ...createTrackingTestExtensions({
    category: 'flights_results',
    page: '/BF/results',
    aliases: {
      toTrackNthWith: 'toTrackResultsNthWith',
      toTrackOnceWith: 'toTrackResultsOnceWith',
    },
  }),
  ...createTrackingTestExtensions({
    category: 'flights_pax_page',
    page: '/BF/flights/details-extras/',
    aliases: {
      toTrackNthWith: 'toTrackPaxPageNthWith',
      toTrackOnceWith: 'toTrackPaxPageOnceWith',
    },
  }),
  ...createTrackingTestExtensions({
    category: 'flights_pay_page',
    aliases: {
      toTrackNthWith: 'toTrackPaymentPageNthWith',
      toTrackOnceWith: 'toTrackPaymentPageOnceWith',
    },
  }),
  ...createTrackingTestExtensions({
    category: 'category',
    aliases: {
      toTrackNthWith: 'toTrackNthWith',
      toTrackOnceWith: 'toTrackOnceWith',
    },
  }),
  ...createTrackingTestExtensions({
    category: 'flights_pax_page',
    aliases: {
      toTrackNthWith: 'toTrackFlightSummaryNthWith',
      toTrackOnceWith: 'toTrackFlightSummaryOnceWith',
    },
  }),
});
