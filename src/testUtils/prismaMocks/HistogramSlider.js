import React, { useCallback, useState } from 'react';
import { getByLabelText } from '@testing-library/react';

const MIN_HANDLE_LABEL = 'Min hour:';
const MAX_HANDLE_LABEL = 'Max hour:';

const HistogramSlider = ({
  min,
  max,
  onChange,
  defaultValues = [min, max],
  values = defaultValues,
}) => {
  const [minValue, setMin] = useState(defaultValues[0]);
  const [maxValue, setMax] = useState(defaultValues[1]);

  const handleChangeMin = useCallback(
    ({ target }) => {
      let { value } = target;
      setMin(value);
      onChange([Number(value), maxValue]);
    },
    [setMin, maxValue, onChange]
  );
  const handleChangeMax = useCallback(
    ({ target }) => {
      let { value } = target;
      setMax(value);
      onChange([minValue, Number(value)]);
    },
    [setMax, minValue, onChange]
  );

  return (
    <div>
      <label>
        {MIN_HANDLE_LABEL}
        <input
          type="number"
          role="slider"
          aria-valuenow={values[0]}
          value={values[0]}
          onChange={handleChangeMin}
        />
      </label>
      <label>
        {MAX_HANDLE_LABEL}
        <input
          type="number"
          role="slider"
          aria-valuenow={values[1]}
          value={values[1]}
          onChange={handleChangeMax}
        />
      </label>
    </div>
  );
};

export const getMinSliderHandle = (filter) =>
  getByLabelText(filter, MIN_HANDLE_LABEL);
export const getMaxSliderHandle = (filter) =>
  getByLabelText(filter, MAX_HANDLE_LABEL);

export default HistogramSlider;
