import React, { useCallback, useState } from 'react';
import {
  fireEvent,
  getByLabelText,
  findByLabelText,
} from '@testing-library/react';
import Slider from 'prisma-design-system/dist/components/Slider';

const HANDLE_LABEL = 'handle label';
const eventWithValue = (value) => ({
  target: {
    value,
  },
});

const SliderMock = ({
  min,
  onChange,
  defaultValue = min,
  value = defaultValue,
}) => {
  const [currentValue, setValue] = useState(value);

  const handleChange = useCallback(
    ({ target }) => {
      let { value } = target;
      setValue(value);
      onChange(parseInt(value));
    },
    [setValue, onChange]
  );

  return (
    <label>
      {HANDLE_LABEL}
      <input type="number" value={currentValue} onChange={handleChange} />
    </label>
  );
};

const getSliderHandle = (filter) => getByLabelText(filter, HANDLE_LABEL);
const findSliderHandle = (filter) => findByLabelText(filter, HANDLE_LABEL);

const changeSliderValue = (sliderComponent, value) =>
  fireEvent.change(sliderComponent, eventWithValue(value));

const mockSlider = () => {
  Slider.mockImplementation(({ ...props }) => <SliderMock {...props} />);
};

export default SliderMock;
export { changeSliderValue, getSliderHandle, mockSlider, findSliderHandle };
