import { AutoMockedClient } from '@frontend-shell/react-hooks';
import introspectionResult from '../../../graphql.schema.json';
const schema = introspectionResult.__schema;

export const MockedClient = ({ mocks, loading, variations }) => {
  return AutoMockedClient({ schema, mocks, loading, variations });
};
