import React, { useEffect } from 'react';
import {
  createTestingProvider,
  createTrackingPageProvider,
} from '@frontend-shell/react-hooks';

import { MockedClient } from './MockedClient';
import { PrismaProvider } from 'prisma-design-system';
import PaxPageProvider from '../../clientState/providers/PaxPageProvider';

const MODULE_NAMESPACE = 'checkout';
const defaultBookingId = '9999';

const ModuleProvider = ({
  trackingCategory = 'category',
  trackingPage = 'page',
  continueWithPassengersPageValidations = (_travellers) => {
    /* */
  },
  children,
}) => {
  useEffect(() => {
    sessionStorage.setItem('bookingId', defaultBookingId);
  });
  const PageTrackingProvider = createTrackingPageProvider({
    page: trackingPage,
    category: trackingCategory,
    pageMapper: () => `/BF/${trackingPage}`,
  });
  return (
    <PageTrackingProvider>
      <PaxPageProvider
        continueWithPassengersPageValidations={
          continueWithPassengersPageValidations
        }
      >
        {children}
      </PaxPageProvider>
    </PageTrackingProvider>
  );
};

const TestingProvider = createTestingProvider({
  PrismaProvider,
  ModuleProvider,
  namespace: MODULE_NAMESPACE,
  MockedClient,
  initialPage: 'test',
});

TestingProvider.defaultProps = {
  device: 'mobile',
  abs: [],
};

export default TestingProvider;
