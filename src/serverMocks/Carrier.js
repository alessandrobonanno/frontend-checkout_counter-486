export const Vueling = () => ({
  id: 'VY',
  name: 'Vueling',
});

export const Ryanair = () => ({
  id: 'FR',
  name: 'Ryanair',
});

export const AirEuropa = () => ({
  id: 'UX',
  name: 'Air Europa',
});

export const BritishAirways = () => ({
  id: 'BA',
  name: 'British Airways',
});

export const Easyjet = () => ({
  id: 'U2',
  name: 'Easyjet',
});

export const Iberia = () => ({
  id: 'IB',
  name: 'Iberia',
});

export const Renfe = () => ({
  id: '4R',
  name: 'Renfe',
});
