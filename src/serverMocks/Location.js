import { fromArray } from './entityGenerator';

export const Sants = () => ({
  id: 156,
  iata: 'BCN',
  cityIata: 'BCN',
  name: 'Estación de Sants',
  cityName: 'Barcelona',
  countryName: 'Spain',
  locationType: 'TRAIN_STATION',
});

export const ElPrat = () => ({
  id: 157,
  iata: 'BCN',
  cityIata: 'BCN',
  name: 'El Prat',
  cityName: 'Barcelona',
  countryName: 'Spain',
  locationType: 'AIRPORT',
});

export const Reus = () => ({
  id: 1581,
  iata: 'REU',
  cityIata: 'BCN',
  name: 'Reus',
  cityName: 'Barcelona',
  countryName: 'Spain',
  locationType: 'AIRPORT',
});

export const Gatwick = () => ({
  id: 1068,
  iata: 'LGW',
  cityIata: 'LON',
  name: 'Gatwick',
  cityName: 'London',
  countryName: 'United Kingdom',
  locationType: 'AIRPORT',
});

export const Heathrow = () => ({
  id: 1070,
  iata: 'LHR',
  cityIata: 'LON',
  name: 'Heathrow',
  cityName: 'London',
  countryName: 'United Kingdom',
  locationType: 'AIRPORT',
});

export const LutonInternational = () => ({
  id: 1121,
  iata: 'LTN',
  cityIata: 'LON',
  name: 'Luton International',
  cityName: 'London',
  countryName: 'United Kingdom',
  locationType: 'AIRPORT',
});

export const Southend = () => ({
  id: 1072447,
  iata: 'LCY',
  cityIata: 'LON',
  name: 'Southend',
  cityName: 'London',
  countryName: 'United Kingdom',
  locationType: 'AIRPORT',
});

export const Stansted = () => ({
  id: 1775,
  iata: 'STN',
  cityIata: 'LON',
  name: 'Stansted',
  cityName: 'London',
  locationType: 'AIRPORT',
  countryName: 'United Kingdom',
});

export const Barajas = () => ({
  id: 1147,
  iata: 'MAD',
  cityIata: 'MAD',
  name: 'Adolfo Suárez Madrid - Barajas',
  cityName: 'Madrid',
  countryName: 'Spain',
  locationType: 'AIRPORT',
});

export const CharlesDeGaulle = () => ({
  id: 330,
  iata: 'CDG',
  cityIata: 'PAR',
  name: 'Charles De Gaulle',
  cityName: 'Paris',
  countryName: 'France',
  locationType: 'AIRPORT',
});

export const Schoenefeld = () => ({
  id: 1804,
  iata: 'SXF',
  cityIata: 'BER',
  name: 'Schoenefeld',
  cityName: 'Berlin',
  countryName: 'Germany',
  locationType: 'AIRPORT',
});

export const Tegel = () => ({
  id: 1937,
  iata: 'TXL',
  cityIata: 'BER',
  name: 'Tegel',
  cityName: 'Berlin',
  countryName: 'Germany',
  locationType: 'AIRPORT',
});

export const PalmaDeMallorca = () => ({
  id: 1483,
  iata: 'PMI',
  cityIata: 'PMI',
  name: 'Palma de Mallorca',
  cityName: 'Palma de Mallorca',
  countryName: 'Spain',
  locationType: 'AIRPORT',
});

export const BaselEuroairport = () => ({
  id: 276,
  iata: 'BSL',
  cityIata: 'ZRH',
  name: 'Basel Euroairport',
  cityName: 'Zurich',
  countryName: 'Switzerland',
  locationType: 'AIRPORT',
});

export const Zurich = () => ({
  id: 2231,
  iata: 'ZRH',
  cityIata: 'ZRH',
  name: 'Zurich',
  cityName: 'Zurich',
  countryName: 'Switzerland',
  locationType: 'AIRPORT',
});

export const SingaporeChangiAirport = () => ({
  id: 9823,
  iata: 'SIN',
  cityIata: 'SIN',
  name: 'Singapore Changi Airport',
  cityName: 'Singapore',
  countryName: 'Singapore',
  locationType: 'AIRPORT',
});

export const SoekarnoHatta = () => ({
  id: 9723,
  iata: 'CGK',
  cityIata: 'JKT',
  name: 'Soekarno-Hatta International Airport',
  cityName: 'Jakarta',
  countryName: 'Indonesia',
  locationType: 'AIRPORT',
});

export const BarcelonaLocations = () => [Sants(), ElPrat(), Reus()];

export const MadridLocations = () => [Barajas()];

export const LondonLocations = () => [
  Gatwick(),
  Heathrow(),
  LutonInternational(),
  Southend(),
  Stansted(),
];

export const BerlinLocations = () => [Schoenefeld(), Tegel()];

export const ParisLocations = () => [CharlesDeGaulle()];

export const Locations = () => [
  Sants(),
  ElPrat(),
  Reus(),
  Gatwick(),
  Heathrow(),
  LutonInternational(),
  Southend(),
  Stansted(),
  Barajas(),
  CharlesDeGaulle(),
  Schoenefeld(),
  Tegel(),
  PalmaDeMallorca(),
  BaselEuroairport(),
  Zurich(),
  SingaporeChangiAirport(),
  SoekarnoHatta(),
];

export const locationGenerator = (locationOptions) => {
  return fromArray(locationOptions || Locations());
};
