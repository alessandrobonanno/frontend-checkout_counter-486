export const roundTrip = (isHub, isTrain) => ({
  mocks: {
    ShoppingCart: () => ({
      itinerary: {
        freeCancellationLimit: {
          limitTime: 1630105140000,
          hoursApart: 13,
        },
        ticketsLeft: 0,
        freeCancellation: '2021-08-27T23:59:00+01:00',
        transportTypes: isTrain ? ['TRAIN'] : ['PLANE'],
        legs: [
          {
            segments: [
              {
                id: '0',
                seats: null,
                duration: 1060,
                transportTypes: isTrain ? ['TRAIN'] : ['PLANE'],
                carrier: {
                  id: 'MS',
                  name: 'Egyptair',
                },
                sections: [
                  {
                    duration: 45,
                    vehicleModel: 'E90',
                    id: '0',
                    flightCode: 'TP1900',
                    departureDate: '2021-09-10T06:00:00+02:00',
                    departureTerminal: null,
                    departure: {
                      id: '602',
                      cityName: 'Faro',
                      name: 'Faro',
                      cityIata: 'FAO',
                      iata: 'FAO',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    isHub,
                    arrivalDate: '2021-09-10T06:45:00+02:00',
                    destination: {
                      id: '1077',
                      cityName: 'Lisboa',
                      name: 'Lisboa',
                      cityIata: 'LIS',
                      iata: 'LIS',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    arrivalTerminal: '1',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'TP',
                      name: 'TAP Portugal',
                    },
                    operatingCarrier: {
                      id: 'NI',
                      name: 'Portugalia',
                    },
                    cabinClass: 'TOURIST',
                    baggageAllowance: 0,
                    technicalStops: [],
                  },
                  {
                    duration: 215,
                    vehicleModel: '320',
                    id: '1',
                    flightCode: 'TP754',
                    departureDate: '2021-09-10T07:30:00+02:00',
                    departureTerminal: '1',
                    departure: {
                      id: '1077',
                      cityName: 'Lisboa',
                      name: 'Lisboa',
                      cityIata: 'LIS',
                      iata: 'LIS',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    arrivalDate: '2021-09-10T12:05:00+02:00',
                    destination: {
                      id: '414',
                      cityName: 'Copenhague',
                      name: 'Kastrup',
                      cityIata: 'CPH',
                      iata: 'CPH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Dinamarca',
                    },
                    isHub,
                    arrivalTerminal: '3',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'TP',
                      name: 'TAP Portugal',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 0,
                    technicalStops: [],
                  },
                  {
                    duration: 265,
                    vehicleModel: '32N',
                    id: '2',
                    flightCode: 'MS760',
                    departureDate: '2021-09-10T15:45:00+02:00',
                    departureTerminal: '3',
                    departure: {
                      id: '414',
                      cityName: 'Copenhague',
                      name: 'Kastrup',
                      cityIata: 'CPH',
                      iata: 'CPH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Dinamarca',
                    },
                    arrivalDate: '2021-09-10T20:10:00+02:00',
                    destination: {
                      id: '311',
                      cityName: 'El Cairo',
                      name: 'Cairo International Airport',
                      cityIata: 'CAI',
                      iata: 'CAI',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Egipto',
                    },
                    arrivalTerminal: '3',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'MS',
                      name: 'Egyptair',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 2,
                    technicalStops: [],
                  },
                  {
                    duration: 150,
                    vehicleModel: '32N',
                    id: '3',
                    flightCode: 'MS2649',
                    departureDate: '2021-09-10T22:10:00+02:00',
                    departureTerminal: '2',
                    departure: {
                      id: '311',
                      cityName: 'El Cairo',
                      name: 'Cairo International Airport',
                      cityIata: 'CAI',
                      iata: 'CAI',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Egipto',
                    },
                    arrivalDate: '2021-09-11T01:40:00+02:00',
                    destination: {
                      id: '1626',
                      cityName: 'Riad',
                      name: 'King Khaled Intl',
                      cityIata: 'RUH',
                      iata: 'RUH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Arabia Saudí',
                    },
                    arrivalTerminal: '1',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'MS',
                      name: 'Egyptair',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 2,
                    technicalStops: [],
                  },
                ],
              },
            ],
          },
          {
            segments: [
              {
                id: '1',
                seats: null,
                duration: 1390,
                transportTypes: isTrain ? ['TRAIN'] : ['PLANE'],
                carrier: {
                  id: 'MS',
                  name: 'Egyptair',
                },
                sections: [
                  {
                    duration: 160,
                    vehicleModel: '32N',
                    id: '4',
                    flightCode: 'MS2650',
                    departureDate: '2021-09-18T02:40:00+02:00',
                    departureTerminal: '1',
                    departure: {
                      id: '1626',
                      cityName: 'Riad',
                      name: 'King Khaled Intl',
                      cityIata: 'RUH',
                      iata: 'RUH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Arabia Saudí',
                    },
                    isHub,
                    arrivalDate: '2021-09-18T04:20:00+02:00',
                    destination: {
                      id: '311',
                      cityName: 'El Cairo',
                      name: 'Cairo International Airport',
                      cityIata: 'CAI',
                      iata: 'CAI',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Egipto',
                    },
                    arrivalTerminal: '3',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'MS',
                      name: 'Egyptair',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 2,
                    technicalStops: [],
                  },
                  {
                    duration: 275,
                    vehicleModel: '32N',
                    id: '5',
                    flightCode: 'MS759',
                    departureDate: '2021-09-18T10:10:00+02:00',
                    departureTerminal: '2',
                    departure: {
                      id: '311',
                      cityName: 'El Cairo',
                      name: 'Cairo International Airport',
                      cityIata: 'CAI',
                      iata: 'CAI',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Egipto',
                    },
                    isHub,
                    arrivalDate: '2021-09-18T14:45:00+02:00',
                    destination: {
                      id: '414',
                      cityName: 'Copenhague',
                      name: 'Kastrup',
                      cityIata: 'CPH',
                      iata: 'CPH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Dinamarca',
                    },
                    arrivalTerminal: '3',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'MS',
                      name: 'Egyptair',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 2,
                    technicalStops: [],
                  },
                  {
                    duration: 230,
                    vehicleModel: '320',
                    id: '6',
                    flightCode: 'TP759',
                    departureDate: '2021-09-18T17:35:00+02:00',
                    departureTerminal: '3',
                    departure: {
                      id: '414',
                      cityName: 'Copenhague',
                      name: 'Kastrup',
                      cityIata: 'CPH',
                      iata: 'CPH',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Dinamarca',
                    },
                    arrivalDate: '2021-09-18T20:25:00+02:00',
                    destination: {
                      id: '1077',
                      cityName: 'Lisboa',
                      name: 'Lisboa',
                      cityIata: 'LIS',
                      iata: 'LIS',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    arrivalTerminal: '1',
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'TP',
                      name: 'TAP Portugal',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 0,
                    technicalStops: [],
                  },
                  {
                    duration: 45,
                    vehicleModel: '319',
                    id: '7',
                    flightCode: 'TP1909',
                    departureDate: '2021-09-18T23:05:00+02:00',
                    departureTerminal: '1',
                    departure: {
                      id: '1077',
                      cityName: 'Lisboa',
                      name: 'Lisboa',
                      cityIata: 'LIS',
                      iata: 'LIS',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    arrivalDate: '2021-09-18T23:50:00+02:00',
                    destination: {
                      id: '602',
                      cityName: 'Faro',
                      name: 'Faro',
                      cityIata: 'FAO',
                      iata: 'FAO',
                      locationType: isTrain ? 'TRAIN_STATION' : 'AIRPORT',
                      countryName: 'Portugal',
                    },
                    arrivalTerminal: null,
                    transportType: isTrain ? 'TRAIN' : 'PLANE',
                    carrier: {
                      id: 'TP',
                      name: 'TAP Portugal',
                    },
                    operatingCarrier: null,
                    cabinClass: 'TOURIST',
                    baggageAllowance: 0,
                    technicalStops: [],
                  },
                ],
              },
            ],
          },
        ],
      },
    }),
  },
});
