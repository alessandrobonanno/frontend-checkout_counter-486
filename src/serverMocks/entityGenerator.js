export function* fromArray(entities) {
  let index = 0;
  while (true) {
    yield entities[index];
    index = (index + 1) % entities.length;
  }
}

export function* fromIterators(iterators) {
  while (true) {
    const entity = Object.entries(iterators).reduce(
      (entity, [key, iterator]) => {
        entity[key] = iterator.next().value;
        return entity;
      },
      {}
    );
    yield entity;
  }
}
