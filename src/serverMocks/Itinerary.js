import { Barajas, ElPrat, Gatwick, Zurich } from './Location';
import { Vueling, Ryanair } from './Carrier';

const location = (type) => (location) => ({
  ...location,
  type,
});

const connection = location('connection');
const absoluteLocation = location('absolute');

const trip = (withTechnicalStop, withStops, withOperatingCarrier) => {
  const trips = [];
  if (withTechnicalStop) {
    trips.push(
      {
        id: 0,
        carrier: Vueling(),
        operatingCarrier: withOperatingCarrier && Vueling(),
        departure: absoluteLocation(Barajas()),
        departureTerminal: '1',
        destination: connection(Zurich()),
        arrivalTerminal: '2',
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T06:30:00+01:00',
        arrivalDate: '2020-04-29T07:15:33+01:00',
        duration: 75,
        vehicleModel: '320',
        flightCode: 'BR2654',
        transportType: 'PLANE',
      },
      {
        id: 1,
        carrier: Vueling(),
        operatingCarrier: withOperatingCarrier && Vueling(),
        departure: connection(Zurich()),
        departureTerminal: '2',
        destination: location(withStops ? 'connection' : 'absolute')(ElPrat()),
        arrivalTerminal: '3',
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T07:40:00+01:00',
        arrivalDate: '2020-04-29T09:45:33+01:00',
        duration: 125,
        vehicleModel: '320',
        flightCode: 'BR2654',
        transportType: 'PLANE',
      }
    );
  } else {
    trips.push({
      id: 0,
      carrier: Vueling(),
      operatingCarrier: withOperatingCarrier && Vueling(),
      departure: connection(Barajas()),
      departureTerminal: '1',
      destination: location(withStops ? 'connection' : 'absolute')(ElPrat()),
      arrivalTerminal: '3',
      cabinClass: 'ECONOMIC_DISCOUNTED',
      departureDate: '2020-04-29T10:30:00+01:00',
      arrivalDate: '2020-04-29T11:45:33+01:00',
      duration: 200,
      vehicleModel: '320',
      flightCode: 'AD2654',
      transportType: 'PLANE',
    });
  }
  return trips;
};

const section1 = (technicalStops, withStopovers, withOperatingCarrier) => {
  return {
    id: 1,
    departureDate: '2020-04-29T06:30:00+01:00',
    arrivalDate: '2020-04-29T09:45:33+01:00',
    departure: Barajas(),
    departureTerminal: '1',
    destination: ElPrat(),
    arrivalTerminal: '3',
    carrier: Ryanair(),
    duration: 250,
    operatingCarrier: withOperatingCarrier && Ryanair(),
    trips: trip(technicalStops.length > 0, withStopovers, withOperatingCarrier),
    technicalStops,
    vehicleModel: '320',
    flightCode: 'AD2654',
    transportType: 'PLANE',
    cabinClass: 'ECONOMIC_DISCOUNTED',
  };
};

const section2 = (withOperatingCarrier) => {
  return {
    id: 2,
    departureDate: '2020-04-29T10:30:00+01:00',
    arrivalDate: '2020-04-29T11:45:33+01:00',
    departure: connection(ElPrat()),
    departureTerminal: '3',
    destination: absoluteLocation(Gatwick()),
    arrivalTerminal: '1',
    carrier: Vueling(),
    operatingCarrier: withOperatingCarrier && Vueling(),
    duration: 250,
    vehicleModel: '320',
    flightCode: 'AD2654',
    transportType: 'PLANE',
    cabinClass: 'ECONOMIC_DISCOUNTED',
    trips: [
      {
        id: 0,
        carrier: Vueling(),
        operatingCarrier: withOperatingCarrier && Vueling(),
        departure: connection(ElPrat()),
        departureTerminal: '3',
        destination: absoluteLocation(Gatwick()),
        arrivalTerminal: '4',
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T10:30:00+01:00',
        arrivalDate: '2020-04-29T11:45:33+01:00',
        duration: 999,
        vehicleModel: '320',
        flightCode: 'AD2654',
        transportType: 'PLANE',
      },
    ],
    technicalStops: [],
  };
};

const technicalStop = {
  location: Zurich(),
  arrivalDate: '2020-04-29T07:15:00+01:00',
  departureDate: '2020-04-29T07:40:00+01:00',
  duration: 25,
};

const sections = (
  withTechnicalStopovers,
  withStopovers,
  withOperatingCarrier
) => {
  const sections = [
    section1(
      withTechnicalStopovers ? [technicalStop] : [],
      withStopovers,
      withOperatingCarrier
    ),
  ];
  if (withStopovers) sections.push(section2(withOperatingCarrier));
  return sections;
};

const sections2 = (...args) => sections(...args).reverse();

const Segment = (
  sections,
  baggageCondition = 'CHECKIN_INCLUDED',
  withVinChange = false
) => {
  return {
    sections,
    stops:
      sections.length > 1
        ? [
            {
              duration: 75,
              locationChangeType: 'none',
              carrierChange: false,
              insuranceOffer: {
                policy: 'CARRIER_GUARANTEE',
                carrier: Vueling(),
              },
              vinRestriction: {
                enabled: withVinChange,
                country: 'Spain',
              },
            },
          ]
        : [],
    duration: 250,
    baggageCondition,
    transportTypes: ['TRAIN', 'PLANE'],
  };
};

const generateSegments = (
  length,
  withStopovers,
  withTechnicalStopovers,
  baggageCondition,
  withOperatingCarrier = false,
  withVinChange
) => {
  const array = Array.from({ length }, () =>
    Segment(
      sections(withTechnicalStopovers, withStopovers, withOperatingCarrier),
      baggageCondition,
      withVinChange
    )
  );

  array.push(
    Segment(
      sections2(withTechnicalStopovers, withStopovers, withOperatingCarrier),
      baggageCondition,
      withVinChange
    )
  );
  return array;
};

export default generateSegments;
