import { withModuleContext } from '@frontend-shell/react-hooks';
import { PrismaProvider } from 'prisma-design-system';
import { default as PersuasiveMessagesComponent } from './components/Persuasive/PersuasiveMessages';
import { default as FlightSummaryComponent } from './components/FlightSummary/FlightSummary';
import { default as FlightSummaryPaymentComponent } from './components/FlightSummary/FlightSummary/FlightSummaryPayment';
import { default as CampaignBannerComponent } from './components/CampaignBanner';
import { default as PassengerManagerMobileComponent } from './components/PassengerManager';
import { default as PaxPageContinueButtonComponent } from './components/PaxPageContinueButton';

const MODULE_NAMESPACE = 'checkout';

const withCheckoutContext = withModuleContext({
  namespace: MODULE_NAMESPACE,
  PrismaProvider,
});

export const PersuasiveMessages = withCheckoutContext(
  PersuasiveMessagesComponent
);
export const FlightSummary = withCheckoutContext(FlightSummaryComponent);
export const FlightSummaryPayment = withCheckoutContext(
  FlightSummaryPaymentComponent
);
export const CampaignBanner = withCheckoutContext(CampaignBannerComponent);
export const PassengerManagerMobile = withCheckoutContext(
  PassengerManagerMobileComponent
);
export const PaxPageContinueButton = withCheckoutContext(
  PaxPageContinueButtonComponent
);

export { default as PaxPageProvider } from './clientState/providers/PaxPageProvider';
export { aliases } from './versionedComponents';
