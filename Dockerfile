FROM nginx

COPY .out /var/www/html

RUN rm /etc/nginx/conf.d/default.conf
ADD config/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
