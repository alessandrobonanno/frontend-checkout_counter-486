# frontend-checkout

### How do I get set up?

To quickstart:

```bash
npm ci

npm start
```

### How to release a new version of the module?

Go to: http://bcn-jenkins-01.odigeo.org/jenkins/job/npm-release-pipeline/build
Project: `frontend-checkout`
Version: `PATCH` - If it does not contain any breaking changes
DEPLOY? `Check` - If you want to update the storybook

### Where is the storybook?

Here: http://lb.frontend-checkout.gke-apps.edo.sv/

- Development commands:

  - `npm run create-component MyNewComponent`:

    Create a new component scaffolding.

  - `npm run test`:

    Runs js test.

  - `npm run lint`:

    Check the code format.

  - `npm run pre-commit`:

    Formats your code following the code style guide and runs js test.

- Production commands:

  - `npm run prepublishOnly`:

    Build transpiled code and sourcemaps in dist/ folder, discarding tests.

- TODO - Migration
