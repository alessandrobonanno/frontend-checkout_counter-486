const argv = require('yargs').argv;
const filesManager = require('./utils/filesManager');
const { scopes } = require('./utils/constants');

const translationsDirPath = './translations';
const translationsResultFilePath = `${translationsDirPath}/.translationsrc.json`;
const keysJsonPathPattern = './src/components/**/config/keys.{js,ts}';

const flattenArray = (array) => [].concat(...array);

const readFileAndAppendToTranslationsFileJson = async (
  filepath = '',
  { transformations = [] }
) => {
  const translations = filesManager.readFile(filepath);
  if (translations) {
    const keys = flattenArray(translations);
    transformations.push(keys);
  }
};

const buildTranslationsJson = ({ transformations = [] }) => {
  return {
    dest: 'translations',
    scopes: scopes,
    transformations,
  };
};

const generateTranslationFile = async () => {
  filesManager.createDir(translationsDirPath);
  const transformations = [];
  const keysJsonFiles =
    (await filesManager.findFilesByPatternWithPromise(keysJsonPathPattern)) ||
    [];
  keysJsonFiles.forEach((filepath) =>
    readFileAndAppendToTranslationsFileJson(filepath, { transformations })
  );
  const translations = buildTranslationsJson({
    transformations: [].concat.apply([], transformations),
  });
  filesManager.writeJsonFile(translationsResultFilePath, translations);
};

const taskMap = {
  generateTranslationFile,
};

async function main() {
  taskMap[argv.task]();
}

main();
