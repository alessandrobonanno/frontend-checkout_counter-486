const opodo = [
  'OP_ar-AE',
  'OP_de-AT',
  'OP_de-DE',
  'OP_en-AE',
  'OP_en-GB',
  'OP_fi-FI',
  'OP_fr-FR',
  'OP_it-IT',
  'OP_no-NO',
  'OP_pt-PT',
  'OP_da-DK',
  'OP_de-CH',
  'OP_en',
  'OP_en-AU',
  'OP_es-ES',
  'OP_fr-CH',
  'OP_it-CH',
  'OP_nl-NL',
  'OP_pl-PL',
  'OP_sv-SE',
];

module.exports = {
  opodo,
};
