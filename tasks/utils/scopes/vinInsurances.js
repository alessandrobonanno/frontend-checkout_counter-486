const vinInsurances = [
  'BIG_SITES_VIN_INSURANCE',
  'MEDIUM_SITES_VIN_INSURANCE',
  'SMALL_SITES_VIN_INSURANCE',
  '?ARABIC_SITES',
];

module.exports = {
  vinInsurances,
};
