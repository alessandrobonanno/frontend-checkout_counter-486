const prime = [
  'ED_en',
  'ED_en-GB',
  'ED_en-US',
  'ED_es-US',
  'ED_es-ES',
  'ED_fr-FR',
  'ED_it-IT',
  'ED_pt-PT',
  'GO_fr-FR',
  'OP_en',
  'OP_de-DE',
  'OP_fr-FR',
  'OP_en-GB',
];

module.exports = {
  prime,
};
