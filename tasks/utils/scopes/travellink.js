const travellink = [
  'TR_da-DK',
  'TR_de-DE',
  'TR_en',
  'TR_fi-FI',
  'TR_is-IS',
  'TR_no-NO',
  'TR_sv-SE',
];

module.exports = {
  travellink,
};
