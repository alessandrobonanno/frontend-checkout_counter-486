const bigSites = [
  'ED_de-DE',
  'ED_es-ES',
  'ED_es-US',
  'ED_en',
  'ED_en-GB',
  'ED_en-US',
  'ED_fr-FR',
  'ED_it-IT',
  'GO_fr-FR',
  'OP_de-DE',
  'OP_en-GB',
  'OP_fr-FR',
];

module.exports = {
  bigSites,
};
