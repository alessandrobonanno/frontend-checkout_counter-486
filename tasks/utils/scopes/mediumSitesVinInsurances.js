const mediumSitesVinInsurances = [
  'MEDIUM_SITES',
  '?ED_fr-CA',
  '?ED_ro-RO',
  '?ED_ru-RU',
  '?ED_tr-TR',
  '?OP_es-ES',
];

module.exports = {
  mediumSitesVinInsurances,
};
