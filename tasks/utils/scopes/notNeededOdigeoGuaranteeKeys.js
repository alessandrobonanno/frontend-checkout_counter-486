const notNeededOdigeoGuaranteeKeys = [
  '?ED_en-AU',
  '?ED_en-CA',
  '?ED_es-CO',
  '?ED_en-CZ',
  '?ED_en-GB',
  '?ED_en-HK',
  '?ED_en-HU',
  '?ED_en-ID',
  '?ED_en-IN',
  '?ED_en-NZ',
  '?ED_en-PH',
  '?ED_en-QA',
  '?ED_en-SA',
  '?ED_en-SG',
  '?ED_en-US',
  '?ED_en-TH',
  '?ED_en-ZA',
  '?ED_es-AR',
  '?OP_de-AT',
  '?ED_es-CL',
  '?ED_es-MX',
  '?ED_es-PE',
  '?OP_en-AE',
  '?OP_en-AU',
  '?OP_en-GB',
  '?OP_nl-NL',
];

module.exports = {
  notNeededOdigeoGuaranteeKeys,
};
