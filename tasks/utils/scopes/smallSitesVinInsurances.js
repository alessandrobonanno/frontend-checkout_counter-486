const smallSitesVinInsurances = [
  'SMALL_SITES',
  '?ED_el-GR',
  '?ED_fr-MA',
  '?ED_ja-JP',
  '?ED_ko-KR',
  '?ED_zh-CN',
  '?ED_zh-TW',
  '?OP_da-DK',
  '?OP_pt-PT',
  '?TR_en',
  '?TR_is-IS',
];

module.exports = {
  smallSitesVinInsurances,
};
