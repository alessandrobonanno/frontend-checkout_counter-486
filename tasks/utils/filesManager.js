const glob = require('glob');
const fs = require('fs');
const path = require('path');

const findFilesByPatternWithPromise = filePattern => {
  return new Promise(resolve =>
    glob(filePattern, {}, (err, files) => {
      resolve(files);
    })
  );
};

const readFile = filePath => {
  return require(path.resolve(filePath));
};

const writeJsonFile = (outputPath, data) => {
  fs.writeFileSync(outputPath, JSON.stringify(data, null, 2));
};

const createDir = (dir = '') => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
};

module.exports = {
  findFilesByPatternWithPromise,
  readFile,
  writeJsonFile,
  createDir,
};
