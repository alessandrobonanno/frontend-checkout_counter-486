const { arabicSites } = require('./scopes/arabicSites');
const { travellink } = require('./scopes/travellink');
const { edreams } = require('./scopes/edreams');
const { opodo } = require('./scopes/opodo');
const { govoyage } = require('./scopes/govoyage');
const { prime } = require('./scopes/prime');
const { bigSites } = require('./scopes/bigSites');
const { mediumSites } = require('./scopes/mediumSites');
const { smallSites } = require('./scopes/smallSites');
const { bigSitesVinInsurances } = require('./scopes/bigSitesVinInsurances');
const {
  mediumSitesVinInsurances,
} = require('./scopes/mediumSitesVinInsurances');
const { smallSitesVinInsurances } = require('./scopes/smallSitesVinInsurances');
const { vinInsurances } = require('./scopes/vinInsurances');
const {
  notNeededOdigeoGuaranteeKeys,
} = require('./scopes/notNeededOdigeoGuaranteeKeys');
const { freeRebookingAvailable } = require('./scopes/freeRebookingAvailable');

const scopes = {
  ARABIC_SITES: arabicSites,
  TRAVELLINK: travellink,
  OPODO: opodo,
  EDREAMS: edreams,
  GOVOYAGE: govoyage,
  PRIME: prime,
  BIG_SITES: bigSites,
  MEDIUM_SITES: mediumSites,
  SMALL_SITES: smallSites,
  BIG_SITES_VIN_INSURANCE: bigSitesVinInsurances,
  MEDIUM_SITES_VIN_INSURANCE: mediumSitesVinInsurances,
  SMALL_SITES_VIN_INSURANCE: smallSitesVinInsurances,
  VIN_INSURANCE: vinInsurances,
  NOT_NEEDED_ODIGEO_GUARANTEE_KEYS: notNeededOdigeoGuaranteeKeys,
  FREE_REBOOKING_AVAILABLE: freeRebookingAvailable,
};

module.exports = {
  scopes,
};
