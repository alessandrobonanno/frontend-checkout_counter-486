// eslint-disable-next-line no-undef
System.config({
    "paths": {
        'decorators/*': './.storybook/decorators/*',
        'testUtils/*': './src/testUtils/*',
        "@core/*": "./src/core/*",
        "@ui/*": "./src/ui/*",
    },
});
