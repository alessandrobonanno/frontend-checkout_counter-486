const fs = require('fs');
const path = require('path');

const storyFileName = 'ComponentName.stories.js';
const indexFileName = 'index.js';
const keysFileName = 'keys.js';
const componentFileName = 'ComponentName.js';
const COMPONENTS_FOLDER = './src/components';
const TEMPLATES_FOLDER = './config/Template';
const configFolder = 'config';
const templateIndex = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  indexFileName
));
const templateComponent = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  componentFileName
));
const templateStory = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  storyFileName
));
const templateTest = require(path.join(
  process.cwd(),
  `${TEMPLATES_FOLDER}/__tests__/`,
  'index.test.js'
));
const templateKeys = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  keysFileName
));
const [, , NewFolderName] = process.argv;

/**
 * Create a new component from template
 * @returns
 */
function createComponent(NewFolderName) {
  if (!NewFolderName || NewFolderName === '') {
    console.log('-- Please specify a component/folder name');
    return false;
  }

  try {
    const newFolderPath = path.join(
      process.cwd(),
      `${COMPONENTS_FOLDER}/${NewFolderName}`
    );
    if (!fs.existsSync(newFolderPath)) {
      fs.mkdirSync(newFolderPath);
      fs.mkdirSync(path.join(newFolderPath, '__tests__'));
      fs.mkdirSync(path.join(newFolderPath, configFolder));

      fs.writeFileSync(
        `${newFolderPath}/${NewFolderName}.js`,
        templateComponent(NewFolderName)
      );
      fs.writeFileSync(
        `${newFolderPath}/${NewFolderName}.stories.js`,
        templateStory(NewFolderName)
      );
      fs.writeFileSync(
        `${newFolderPath}/${configFolder}/${keysFileName}`,
        templateKeys(NewFolderName)
      );
      fs.writeFileSync(
        `${newFolderPath}/index.js`,
        templateIndex(NewFolderName)
      );
      fs.writeFileSync(
        `${newFolderPath}/__tests__/index.test.js`,
        templateTest(NewFolderName)
      );
      console.info(`-- Component Created at ${newFolderPath}`);
      return true;
    } else {
      console.warn(
        '-- Component with this name ALREADY exists. Specify another name!'
      );
    }
  } catch (e) {
    console.error(e);
  }
}

createComponent(NewFolderName);
