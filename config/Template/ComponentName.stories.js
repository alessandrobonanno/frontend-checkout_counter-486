module.exports = (ComponentName) => `import React from "react";
import { api as cmsAddonApi } from "frontend-commons-storybook-cms";
import configKeys from './config/keys';
import ${ComponentName} from "./";

const story = {
  title: "Components/${ComponentName}",
  component: ${ComponentName},
  parameters: {
    ...cmsAddonApi.createPanelParameters(configKeys),
  },
};

export const Default = args => {
  return <${ComponentName} {...args} />;
};

Default.args = {
  myProp: "test"
};

export default story;
`;
