module.exports = ComponentName => `import React from "react";
import { render, cleanup } from '@testing-library/react';
import { axe } from "jest-axe";
import {
  TestingProviders,
  TranslationsTestHandler,
} from '@utils/test';
import ${ComponentName} from "../";

const TEST = 'test';

const TRANSLATION = {
  filters: TEST
};

const initI18n = () =>
  TranslationsTestHandler().init(TRANSLATION);

afterEach(cleanup);

initI18n();

describe("${ComponentName}", () => {
  test("It should render their children", async () => {
    const content = "text example";
    const { getByText } = await render(
      <TestingProviders>
        <${ComponentName} myProp={content} />
      </TestingProviders>
    );
    
    expect(getByText(content)).toBeVisible();
  });
  
  test('should not have basic accessibility issues', async () => {
    const content = "text example";
    const { container } = render(<TestingProviders>
        <${ComponentName} myProp={content} />
      </TestingProviders>);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});`;
